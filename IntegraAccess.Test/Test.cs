﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Controllers;
using NUnit.Framework;

namespace IntegraAccess.Test
{
    [TestFixture]
    public class Test
    {
        [Test]
        public void TestUnitFramework()
        {
            var data = "is test framework works?";
            Assert.AreEqual(data,data, "Test Framework works!");
        }
    }
}
