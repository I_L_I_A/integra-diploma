﻿using Autofac;

namespace IntegraAccess.Business
{
    public class InjectionConfigurations
    {
        private ContainerBuilder _containerBuilder;
        private IContainer _container;

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:System.Object" /> class.
        /// </summary>
        public InjectionConfigurations()
        {
            _containerBuilder = new ContainerBuilder();

            _containerBuilder.RegisterModule(new BusinessLayerRegistrationModule());
        }

        public IContainer Container
        {
            get
            {
                Initialize();
                return _container;
            }
        }

        protected ContainerBuilder ContainerBuilder
        {
            get { return _containerBuilder; }
        }

        protected virtual void RegisterTypes()
        {
        }

        protected virtual void Initialize()
        {
            RegisterTypes();
            _container = _containerBuilder.Build();
        }
    }
}