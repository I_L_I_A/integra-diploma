﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    public class OrderService : ServiceDbBase<Order>, IOrderService
    {
        protected IEmailService _emailService;

        public OrderService(IRepositoryBase repositoryBase, IEmailService emailService)
            : base(repositoryBase)
        {
            _emailService = emailService;
        }

        public void SendRequiredApprovalMessage(List<string> emails, Order order, string viewOrderUrl)
        {
            var emailSubject = string.Format("[DME Prime] Order {0} Requires Approval", order.Id);
            var emailBody = string.Format("A new order has been received and requires approval.<br/>" +
                                          "Account Group: {0} <br/>" +
                                          "Account: {1} <br/>" +
                                          "Patient Name: {2} {3} <br/>" +
                                          "Submitted On: {4} <br/>" +
                                          "<a href=\"{5}\">View Order</a> ",
                                          order.Account.AccountGroup.Name,
                                          order.Account.Name,
                                          order.PatientLastName, order.PatientFirstName,
                                          DateTime.Now.ToShortDateString(),
                                          viewOrderUrl);
            _emailService.SendEmail(emails, emailSubject, emailBody);
        }

        public Order GetById(int id)
        {
            return RepositoryBase.Find<Order>(x => x.Id == id).FirstOrDefault();
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }
    }
}