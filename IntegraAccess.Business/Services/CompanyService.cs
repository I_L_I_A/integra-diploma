﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    public class CompanyService : ServiceDbBase<Company>, ICompanyService
    {
        public CompanyService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public Company GetById(int id)
        {
            return RepositoryBase.Find<Company>(x => x.Id == id).FirstOrDefault();
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }
    }
}