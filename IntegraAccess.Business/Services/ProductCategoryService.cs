﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    class ProductCategoryService : ServiceDbBase<ProductCategory>, IProductCategoryService
    {
        public ProductCategoryService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public void Inactivate(List<int> productCategoryIds)
        {
            if (!productCategoryIds.Any()) return;

            var productCategories = GetAll(productCategoryIds);
            foreach (var patient in productCategories)
            {
                patient.IsActive = false;
                patient.EntityState = State.Modified;
                patient.UpdatedDateTime = DateTime.UtcNow;
            }
        }

        public ProductCategory GetById(int id)
        {
            return RepositoryBase.Find<ProductCategory>(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<ProductCategory> GetAll(List<int> ids)
        {
            return ids.Any() ? GetAll().Where(ac => ids.Contains(ac.Id)) : null;
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }
    }
}
