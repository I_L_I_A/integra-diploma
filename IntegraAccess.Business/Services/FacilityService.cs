﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    public class FacilityService : ServiceDbBase<Facility>, IFacilityService
    {
        public FacilityService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public Facility GetById(int id)
        {
            return RepositoryBase.Find<Facility>(x => x.Id == id).FirstOrDefault();
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }

        public IQueryable<Facility> GetAll(List<int> ids)
        {
            return ids.Any() ? GetAll().Where(ac => ids.Contains(ac.Id)) : null;
        }

        public void Inactivate(List<int> faclitiesIds)
        {
            if (!faclitiesIds.Any()) return;

            var facilities = GetAll(faclitiesIds);
            foreach (var facility in facilities)
            {
                facility.IsActive = false;
                facility.EntityState = State.Modified;
                facility.SetUpdated();
            }
        }
    }
}