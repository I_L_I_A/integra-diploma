﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    internal class ProductPriceSheetItemService : ServiceDbBase<ProductPriceSheetItem>, IProductPriceSheetItemService
    {
        public ProductPriceSheetItemService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public ProductPriceSheetItem GetById(int productId, int productPriceSheetId)
        {
            return
                RepositoryBase.Find<ProductPriceSheetItem>(
                    x => x.ProductId == productId && x.ProductPriceSheetId == productPriceSheetId).FirstOrDefault();
        }

        public IQueryable<ProductPriceSheetItem> GetAllAccountProductPriceSheetItems(int accountId)
        {
            var account = RepositoryBase.Find<Account>(x => x.Id == accountId).FirstOrDefault();
            if (account == null) return null;
            return account.PriceSheetId.HasValue
                ? GetAll().Where(x => x.ProductPriceSheetId == account.PriceSheetId.Value)
                : null;
        }

        public IQueryable<ProductPriceSheetItem> GetAllFromPriceSheet(List<int> productIds, int priceSheetId)
        {
            return GetAll().Where(x => x.ProductPriceSheet.Id == priceSheetId && productIds.Contains(x.ProductId));
        }

        public IQueryable<ProductPriceSheetItem> GetAll(int? productId = null, int? priceSheetId = null)
        {
            return base.GetAll().Where(x => (!productId.HasValue || x.ProductId == productId) &&
                (!priceSheetId.HasValue || x.ProductPriceSheetId == priceSheetId));
        }

        public bool Delete(int productId, int productPriceSheetId)
        {
            var entity = GetById(productId, productPriceSheetId);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }
    }
}
