﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    public abstract class ServiceDbBase<T> : IServiceDbBase<T> where T : class, IBaseEntity
    {
        private readonly List<IBaseEntity> _entities = new List<IBaseEntity>();
        private readonly List<object> _passedEntites = new List<object>();
        public ServiceDbBase(IRepositoryBase repositoryBase)
        {
            RepositoryBase = repositoryBase;
        }
        protected IRepositoryBase RepositoryBase { get; set; }

        public IQueryable<T> GetAll()
        {
            return RepositoryBase.GetAll<T>();
        }

        private bool ApplyChanges(T root)
        {
            return RepositoryBase.ApplyChanges(root);
        }

        public void Add(T entity)
        {
            Add(entity, new Expression<Func<T, object>>[] { });
        }

        protected virtual void Add(T entity, params Expression<Func<T, object>>[] include)
        {
            SetStateForEntities(entity, include, State.Added);
        }

        public void Update(T entity)
        {
            SetStateForEntities(entity, Enumerable.Empty<Expression<Func<T, object>>>(), State.Modified);
        }

        public bool Delete(T entity)
        {
            return Delete(entity, new Expression<Func<T, object>>[] { });
        }

        public void SaveChanges()
        {
            RepositoryBase.SaveChanges();
        }

        protected virtual bool Delete(T entity, params Expression<Func<T, object>>[] include)
        {
            return SetStateForEntities(entity, include, State.Deleted);
        }

        public void MarkAdded(IObjectWithState entity)
        {
            entity.EntityState = State.Added;
        }

        public void MarkDelete(IObjectWithState entity)
        {
            entity.EntityState = State.Deleted;
        }

        public void MarkUpdated(IObjectWithState entity)
        {
            entity.EntityState = State.Modified;
        }

        private bool SetStateForEntities(T obj, IEnumerable<Expression<Func<T, object>>> include, State state)
        {
            foreach (var property in include)
            {
                var value = property.Compile()(obj);
                var listType = GetEnumerableType(value.GetType());
                if (listType != null)
                {
                    var listProperty = value as IEnumerable;
                    if (listProperty != null)
                    {
                        foreach (var prop in listProperty)
                        {
                            ((IObjectWithState)prop).EntityState = state;
                        }
                    }
                }
                else
                {
                    ((IObjectWithState)value).EntityState = state;
                }
            }

            ((IObjectWithState)obj).EntityState = state;

            //ResolveEntityRef(obj);
            //CleanUpEntityResolver();
            return ApplyChanges(obj);
        }

        private void ResolveEntityRef(object root)
        {
            if (root != null && !_passedEntites.Contains(root))
            {
                _passedEntites.Add(root);
                var objectType = root.GetType();
                if (objectType.GetInterface("IEnumerable") != null)
                {
                    foreach (object item in (IEnumerable)root)
                    {
                        ResolveEntityRef(item);
                    }
                }
                else
                {
                    GetEntityRef(ref root);
                    RepositoryBase.DetachedExistingEntity(root);
                    foreach (PropertyInfo pi in objectType.GetProperties())
                    {
                        if (pi != null &&
                            !pi.PropertyType.IsValueType &&
                            !pi.PropertyType.IsPrimitive &&
                            pi.PropertyType != typeof(string))
                        {
                            var value = pi.GetValue(root);
                            ResolveEntityRef(value);
                        }
                    }
                }
            }
        }

        private void CleanUpEntityResolver()
        {
            _entities.Clear();
            _passedEntites.Clear();
        }

        private void GetEntityRef(ref object obj)
        {
            var baseEntity = obj as IBaseEntity;
            if (baseEntity != null)
            {
                if (_entities.Contains(baseEntity, new EntityEqulityComparer()))
                {
                    obj = _entities.Find(b => b.Equals(baseEntity) && b.GetType() == baseEntity.GetType());
                }
                else
                {
                    _entities.Add(baseEntity);
                }
            }
        }

        private Type GetEnumerableType(Type type)
        {
            foreach (Type intType in type.GetInterfaces())
            {
                if (intType.IsGenericType
                    && intType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                {
                    return intType.GetGenericArguments()[0];
                }
            }
            return null;
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> where)
        {
            return RepositoryBase.Find(where);
        }
    }
}