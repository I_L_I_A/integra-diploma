﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    public class PatientService : ServiceDbBase<Patient>, IPatientService
    {
        public PatientService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public IQueryable<Patient> GetAll(List<int> ids)
        {
            return ids.Any() ? GetAll().Where(ac => ids.Contains(ac.Id)) : null;
        }


        public void Inactivate(List<int> patientsIds)
        {
            if (!patientsIds.Any()) return;

            var patients = GetAll(patientsIds);
            foreach (var patient in patients)
            {
                patient.IsActive = false;
                patient.EntityState = State.Modified;
                patient.UpdatedDateTime = DateTime.UtcNow;
            }
        }

        public Patient GetById(int id)
        {
            return RepositoryBase.Find<Patient>(x => x.Id == id).FirstOrDefault();
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }

        public bool Exists(Patient patient)
        {
            var patientInDb = 
                !String.IsNullOrEmpty(patient.SSN) ? 
                    RepositoryBase.FirstOrDefault<Patient>(x =>
                        x.SSN.ToLower() == patient.SSN.ToLower()) :
                    RepositoryBase.FirstOrDefault<Patient>(x =>
                        x.FirstName.ToLower() == patient.FirstName.ToLower() &&
                        x.LastName.ToLower() == patient.LastName.ToLower() &&
                        x.BirthDate.Value.Day == patient.BirthDate.Value.Day &&
                        x.BirthDate.Value.Month == patient.BirthDate.Value.Month &&
                        x.BirthDate.Value.Year == patient.BirthDate.Value.Year);
            return patientInDb != null;
        }
    }

  
}