﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IUserService: IServiceDbBase<User>
    {
        User FindUserById(string userId);
        User FindUserByEmail(string email);
        User FindUserByUserName(string userName);
        IQueryable<User> GetAllActiveSuppliers();
        void SendResetPasswordMessageToUser(string url, User user);
        string CreateResetTokenForUser(User user);
        void ResetPassword(string userId, string resetToken, string newPassword);
        void ChangePassword(string userId, string currentPassword, string newPassword);
        void ForceChangePassword(string userId, string newPassword);
        Task<User> FindUser(string userName, string password);
    }
}
