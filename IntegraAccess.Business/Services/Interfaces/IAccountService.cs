﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IAccountService : IServiceDbBase<Account>
    {
        IQueryable<Account> GetAll(List<int> ids);
        Account GetById(int id);
        bool HasAccessToAccount(string userId, int accountId);
        bool Delete(int id);
        void Inactivate(List<int> accountIds);
        void GrantAccess(int accountId, User user);
        void GrantAccess(int accountId, List<User> users);
        void DenyAccess(int accountId, User user);
        void DenyAccess(int accountId, List<User> users);

        void ConnectFacility(int accountId, Facility facility);
        void ConnectFacilities(int accountId, List<Facility> facilities);
        void DisconnectFacility(int accountId, Facility facility);
        void DisconnectFacilities(int accountId, List<Facility> facilities);
        void AssignAccountsToPriceSheet(int priceSheetId, List<int> accountsIds, bool assign);

    }
}
