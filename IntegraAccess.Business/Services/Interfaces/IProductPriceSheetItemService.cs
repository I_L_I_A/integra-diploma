﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IProductPriceSheetItemService : IServiceDbBase<ProductPriceSheetItem>
    {
        ProductPriceSheetItem GetById(int productId, int productPriceSheetId);
        IQueryable<ProductPriceSheetItem> GetAllAccountProductPriceSheetItems(int accountId);
        bool Delete(int productId, int productPriceSheetId);
        IQueryable<ProductPriceSheetItem> GetAllFromPriceSheet(List<int> productIds, int priceSheetId);
        IQueryable<ProductPriceSheetItem> GetAll(int? productId = null, int? priceSheetId = null);
    }
}
