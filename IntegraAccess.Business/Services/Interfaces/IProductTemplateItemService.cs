﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IProductTemplateItemService : IServiceDbBase<ProductTemplateItem>
    {
        ProductTemplateItem GetById(int childProductId, int companyId, int templateId);
        bool Delete(int childProductId, int companyId, int templateId);
        IQueryable<ProductTemplateItem> GetTemplateNestedItems(int templateId);
        IQueryable<ProductTemplateItem> GetTemplateNestedItems(int templateId, int companyId);
        void Inactivate(int templateId, int childProductId, int companyId);
    }
}
