﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IProductCategoryService: IServiceDbBase<ProductCategory>
    {
        void Inactivate(List<int> productCategoryIds);
        ProductCategory GetById(int id);
        IQueryable<ProductCategory> GetAll(List<int> ids);
        bool Delete(int id);
    }
}
