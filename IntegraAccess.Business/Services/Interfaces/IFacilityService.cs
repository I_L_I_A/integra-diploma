﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IFacilityService:IServiceDbBase<Facility>
    {
        void Inactivate(List<int> faclitiesIds);
        Facility GetById(int id);
        IQueryable<Facility> GetAll(List<int> accountIds);
        bool Delete(int id);
    }
}
