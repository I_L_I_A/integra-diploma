﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface ICompanyService:IServiceDbBase<Company>
    {
        Company GetById(int id);
        bool Delete(int id);
    }
}
