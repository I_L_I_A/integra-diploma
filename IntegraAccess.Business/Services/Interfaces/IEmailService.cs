﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IEmailService
    {
        void SendEmail(string email, string subject, string body);
        void SendEmail(List<string> emails, string subject, string body);
    }
}
