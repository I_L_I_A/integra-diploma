﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IServiceDbBase<T> where T : IBaseEntity
    {
        IQueryable<T> GetAll();
        IQueryable<T> Find(Expression<Func<T, bool>> where);
        void Add(T entity);
        void Update(T entity);
        bool Delete(T entity);
        void SaveChanges();
    }
}
