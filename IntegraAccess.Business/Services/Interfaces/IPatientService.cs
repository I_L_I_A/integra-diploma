﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IPatientService : IServiceDbBase<Patient>
    {
        void Inactivate(List<int> patientsIds);
        IQueryable<Patient> GetAll(List<int> accountIds);
        Patient GetById(int id);
        bool Delete(int id);
        bool Exists(Patient patient);
    }
}
