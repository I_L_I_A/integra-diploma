﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IAccountGroupService : IServiceDbBase<AccountGroup>
    {
        void Inactivate(List<int> accountGroupsIds);
        AccountGroup GetById(int id);
        bool Delete(int id);
    }
}
