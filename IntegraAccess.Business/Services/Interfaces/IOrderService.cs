﻿using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IOrderService:IServiceDbBase<Order>
    {
        Order GetById(int id);
        void SendRequiredApprovalMessage(List<string> emails, Order order, string viewOrderUrl);
        bool Delete(int id);
    }
}
