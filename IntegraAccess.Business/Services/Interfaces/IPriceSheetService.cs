﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IPriceSheetService: IServiceDbBase<ProductPriceSheet>
    {
        ProductPriceSheet GetById(int id);
        bool Delete(int id);
        void Inactivate(List<int> ids);
        IQueryable<ProductPriceSheet> GetAll(List<int> ids);
    }
}
