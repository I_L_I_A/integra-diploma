﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Business.Services.Interfaces
{
    public interface IProductService : IServiceDbBase<Product>
    {
        Product GetById(int id);
        bool Delete(int id);
        void Inactivate(List<int> productIds);
        IQueryable<Product> GetAll(List<int> productIds);
        IQueryable<Product> GetAll(bool includeInactive = true, bool includeTemplates = true);
        IQueryable<Product> GetAll(int? priceSheetId, bool includeTemplates = true);
        IQueryable<Product> GetAccountProductAndTemplates(Account account, bool includeTempaltes = true);
        void AddToCategories(Product product, List<ProductCategory> categories, string updatedById);
        void AddToCategories(int productId, List<ProductCategory> categories, string updatedById);
    }
}
