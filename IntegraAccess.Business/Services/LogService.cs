﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.DataAccess.Repository.DataProtection;
using IntegraAccess.Entities.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;

namespace IntegraAccess.Business.Services
{
    public class LogService : ServiceDbBase<Log>, ILogService
    {
        public LogService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public void Log(Log log)
        {
            this.Add(log);
        }
    }
}
