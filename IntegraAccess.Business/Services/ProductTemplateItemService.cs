﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    class ProductTemplateItemService : ServiceDbBase<ProductTemplateItem>, IProductTemplateItemService
    {
        public ProductTemplateItemService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public ProductTemplateItem GetById(int childProductId, int companyId, int templateId)
        {
            return RepositoryBase.Find<ProductTemplateItem>(x => x.ProductId == templateId && x.CompanyId == companyId && x.ChildProductId == childProductId).FirstOrDefault();
        }

        public bool Delete(int childProductId, int companyId, int templateId)
        {
            var item = GetById(childProductId, companyId, templateId);
            return item != null && Delete(item);
        }

        public IQueryable<ProductTemplateItem> GetTemplateNestedItems(int templateId)
        {
            return RepositoryBase.GetAll<ProductTemplateItem>()
                .Where(x => x.IsActive && x.ChildProduct.IsActive && x.ProductId == templateId);
        }

        public IQueryable<ProductTemplateItem> GetTemplateNestedItems(int templateId, int companyId)
        {
            return GetTemplateNestedItems(templateId)
                .Where(x => x.IsActive && x.ChildProduct.IsActive && x.CompanyId == companyId);
        }

        public void Inactivate(int templateId, int childProductId, int companyId)
        {
            var templateItem = GetById(childProductId, companyId, templateId);
            if (templateItem == null) return;

            templateItem.IsActive = false;
            templateItem.SetUpdated();
            SaveChanges();
        }

        public new void Add(ProductTemplateItem item)
        {
            var templateItem = GetById(item.ChildProductId, item.CompanyId, item.ProductId);
            if (templateItem != null)
            {
                templateItem.IsActive = true;
                templateItem.SetUpdated();
                SaveChanges();
                return;
            }
            base.Add(item);
        }
    }
}
