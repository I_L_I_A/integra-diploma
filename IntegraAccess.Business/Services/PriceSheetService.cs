﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    class PriceSheetService : ServiceDbBase<ProductPriceSheet>, IPriceSheetService
    {
        public PriceSheetService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public ProductPriceSheet GetById(int id)
        {
            return RepositoryBase.Find<ProductPriceSheet>(x => x.Id == id).FirstOrDefault();
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }

        public void Inactivate(List<int> ids)
        {
            if (!ids.Any()) return;

            var priceSheets = GetAll(ids);
            foreach (var account in priceSheets)
            {
                account.IsActive = false;
                account.EntityState = State.Modified;
                account.SetUpdated();
            }
        }

        public IQueryable<ProductPriceSheet> GetAll(List<int> ids)
        {
            return ids.Any() ? GetAll().Where(ac => ids.Contains(ac.Id)) : null;
        }

    }
}
