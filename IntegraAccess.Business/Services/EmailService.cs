﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using IntegraAccess.Business.Services.Interfaces;

namespace IntegraAccess.Business.Services
{
    public class EmailService:  IEmailService
    {
        private SmtpClient emailClient = null;

        public EmailService()
        {
            InitializeEmailClient();
        }

        public void SendEmail(List<string> emails, string subject, string body)
        {
            MailMessage mailMessage = CreateMailMessage(emails, subject, body);
            try
            {
                emailClient.SendAsync(mailMessage, new object());
            }
            catch (SmtpFailedRecipientsException recipientsException)
            {
                String errorMessage = "Failed to send email to one or more recipients. Failed Recipients: " + recipientsException.FailedRecipient;
                throw new Exception(errorMessage, recipientsException);
            }
            catch (SmtpException exc)
            {
                throw new Exception("Failed to send email. See inner:", exc);
            }
        }

        public void SendEmail(string email, string subject, string body)
        {
            SendEmail(new List<string>() {email}, subject, body);
        }

        private MailMessage CreateMailMessage(List<string> emails, string subject, string body)
        {
            MailMessage resultMessage = new MailMessage();
            foreach (var email in emails)
            {
                resultMessage.To.Add(email);
            }
            resultMessage.Subject = subject;
            resultMessage.Body = body;
            resultMessage.IsBodyHtml = true;
            return resultMessage;
        }

        private void InitializeEmailClient()
        {
            emailClient = new SmtpClient();
        }
    }
}
