﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    public class AccountService : ServiceDbBase<Account>, IAccountService
    {
        public AccountService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public IQueryable<Account> GetAll(List<int> ids)
        {
            return ids.Any() ? GetAll().Where(ac => ids.Contains(ac.Id)) : null;
        }

        public Account GetById(int id)
        {
            return RepositoryBase.Find<Account>(x => x.Id == id).FirstOrDefault();
        }

        public bool HasAccessToAccount(string userId, int accountId)
        {
            return GetAll()
                .Any(x => x.IsActive 
                    && x.UserAccounts.Any(y => y.IsActive 
                        && y.AccountId == accountId 
                        && y.UserId == userId));
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }

        public void Inactivate(List<int> accountIds)
        {
            if (!accountIds.Any()) return;

            var accounts = GetAll(accountIds);
            foreach (var account in accounts)
            {
                account.IsActive = false;
                account.EntityState = State.Modified;
                account.SetUpdated();
            }
        }

        public void GrantAccess(int accountId, User user)
        {
            GrantAccess(accountId, new List<User>() { user });
        }

        public void GrantAccess(int accountId, List<User> users)
        {
            var account = GetById(accountId);
            if (account != null && account.UserAccounts != null && users != null && users.Any())
            {
                foreach (var user in users)
                {
                    var relationShip = account.UserAccounts.FirstOrDefault(x => x.UserId == user.Id);
                    if (relationShip != null)
                    {
                        relationShip.IsActive = true;
                    }
                    else
                    {
                        account.UserAccounts.Add(new UserAccounts()
                        {
                            Account = account,
                            User = user,
                            IsActive = true,
                        });
                    }
                }
            }
        }

        public void DenyAccess(int accountId, User user)
        {
            DenyAccess(accountId, new List<User>() { user });
        }

        public void DenyAccess(int accountId, List<User> users)
        {
            var account = GetById(accountId);
            if (account != null && account.UserAccounts != null && users != null && users.Any())
            {
                foreach (var user in users)
                {
                    var relationShip = account.UserAccounts.FirstOrDefault(x => x.UserId == user.Id);
                    if (relationShip != null)
                    {
                        relationShip.IsActive = false;
                    }
                }

            }
        }

        public void ConnectFacility(int accountId, Facility facility)
        {
            ConnectFacilities(accountId, new List<Facility>() { facility });
        }

        public void ConnectFacilities(int accountId, List<Facility> facilities)
        {
            var account = GetById(accountId);
            if (account != null && account.FacilityAccounts != null && facilities != null && facilities.Any())
            {
                foreach (var facility in facilities)
                {
                    var relationShip = account.FacilityAccounts.FirstOrDefault(x => x.FacilityId == facility.Id);
                    if (relationShip != null)
                    {
                        relationShip.IsActive = true;
                    }
                    else
                    {
                        account.FacilityAccounts.Add(new FacilityAccounts()
                        {
                            Account = account,
                            Facility = facility,
                            IsActive = true,
                        });
                    }
                }
            }
        }

        public void AssignAccountsToPriceSheet(int priceSheetId, List<int> accountsIds, bool assign)
        {
            var accounts = GetAll(accountsIds);
            if (accounts != null && accounts.Any())
            {
                foreach (var account in accounts)
                {
                    if (assign && account.PriceSheetId != priceSheetId)
                    {
                        account.PriceSheetId = priceSheetId;
                        account.SetUpdated();

                    }
                    if (!assign && account.PriceSheetId == priceSheetId)
                    {
                        account.PriceSheetId = null;
                        account.SetUpdated();
                    }

                }
            }
        }

        public void DisconnectFacility(int accountId, Facility facility)
        {
            DisconnectFacilities(accountId, new List<Facility>() { facility });
        }

        public void DisconnectFacilities(int accountId, List<Facility> facilities)
        {
            var account = GetById(accountId);
            if (account != null && account.FacilityAccounts != null && facilities != null && facilities.Any())
            {
                foreach (var facility in facilities)
                {
                    var relationShip = account.FacilityAccounts.FirstOrDefault(x => x.FacilityId == facility.Id);
                    if (relationShip != null)
                    {
                        relationShip.IsActive = false;
                    }
                }

            }
        }


    }
}