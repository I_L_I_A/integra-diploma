﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    class ProductService : ServiceDbBase<Product>, IProductService
    {
        public ProductService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }


        public Product GetById(int id)
        {
            return RepositoryBase.Find<Product>(x => x.Id == id).FirstOrDefault();
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }

        public void Inactivate(List<int> productsIds)
        {
            if (!productsIds.Any()) return;

            var patients = GetAll(productsIds);
            foreach (var patient in patients)
            {
                patient.IsActive = false;
                patient.EntityState = State.Modified;
                patient.UpdatedDateTime = DateTime.UtcNow;
            }
        }

        public IQueryable<Product> GetAll(List<int> ids)
        {
            return ids.Any() ? GetAll().Where(ac => ids.Contains(ac.Id)) : null;
        }

        public IQueryable<Product> GetAll(bool includeInactive = true, bool includeTemplates = true)
        {
            return base.GetAll()
                .Where(x => !x.IsTemplate || (includeTemplates && x.IsTemplate))
                .Where(x => x.IsActive || (includeInactive && !x.IsActive));
        }

        public IQueryable<Product> GetAll(int? priceSheetId, bool includeTemplates = true)
        {
            if (!priceSheetId.HasValue) return null;
            return GetAll().Where(x => (includeTemplates && x.IsTemplate) ||
                (!x.IsTemplate && x.ProductPriceSheetItems.Any(y => y.ProductPriceSheet.IsActive && y.ProductPriceSheetId == priceSheetId.Value)));
        }

        public IQueryable<Product> GetAccountProductAndTemplates(Account account, bool includeTemplates = true)
        {
            if (account == null) return null;


            return GetAll().Where(product =>
                //return templates with nested items from the same company as account
                (includeTemplates && product.IsTemplate && product.NestedTemplateItems.Any(z =>
                    z.IsActive && z.CompanyId == account.CompanyId &&
                    z.ChildProduct.IsActive &&
                    z.ChildProduct.ProductPriceSheetItems.Any(ppsItem =>
                        ppsItem.ProductPriceSheet.IsActive &&
                        ppsItem.ProductPriceSheetId == account.PriceSheetId &&
                        ppsItem.ProductPriceSheet.CompanyId == account.CompanyId))
                    )

                || //OR products with prices for account ( the same company and pricesheetId)
                (!product.IsTemplate && (product.AvailableOutsideTemplate || !product.ParentTemplateItems.Any()) &&
                 product.ProductPriceSheetItems.Any(y =>
                     y.ProductPriceSheet.IsActive &&
                     y.ProductPriceSheetId == account.PriceSheetId &&
                     y.ProductPriceSheet.CompanyId == account.CompanyId)));
        }

        public void AddToCategories(int productId, List<ProductCategory> categories, string updatedById)
        {
            AddToCategories(GetById(productId), categories, updatedById);
        }

        public void AddToCategories(Product product, List<ProductCategory> categories, string updatedById)
        {
            if (product != null)
            {
                product.ProductsToCategories = product.ProductsToCategories ?? new List<ProductsToCategories>();
                foreach (var productCategory in product.ProductsToCategories)
                {
                    productCategory.IsActive = false;
                    productCategory.SetUpdated(updatedById);
                }

                if (categories != null && categories.Any())
                {
                    foreach (var category in categories)
                    {
                        var relationShip = product.ProductsToCategories.FirstOrDefault(x => x.CategoryId == category.Id);
                        if (relationShip != null)
                        {
                            relationShip.IsActive = true;
                        }
                        else
                        {
                            product.ProductsToCategories.Add(new ProductsToCategories()
                            {
                                ProductId = product.Id,
                                CategoryId = category.Id,
                                IsActive = true,
                                UpdatedDateTime = DateTime.UtcNow,
                                UpdatedById = updatedById
                            });
                        }
                    }
                }
            }
        }
    }
}
