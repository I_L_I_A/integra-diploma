﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.DataAccess.Repository.DataProtection;
using IntegraAccess.Entities.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;

namespace IntegraAccess.Business.Services
{
    public class UserService : ServiceDbBase<User>, IUserService
    {
        private DbContext _ctx;
        private UserManager<User> _userManager;
        protected IAuthRepository _authRepository;
        protected IEmailService _emailService;
        public UserService(IRepositoryBase repositoryBase, IAuthRepository authRepository, IEmailService emailService)
            : base(repositoryBase)
        {
            _authRepository = authRepository;
            _ctx = repositoryBase.GetDbContext();
            _userManager = new UserManager<User>(new UserStore<User>(_ctx));
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<User>(new MachineKeyProtectionProvider().Create("PasswordReset"));
            _emailService = emailService;
        }

        public User FindUserById(string userId)
        {
            return _authRepository.FindUser(userId);
        }

        public User FindUserByEmail(string email)
        {
            var user = _authRepository.FindUserByEmail(email);
            if (user == null)
                throw new ApplicationException(string.Format("No user found with email '{0}'", email));

            return user;
        }

        public User FindUserByUserName(string userName)
        {
            var user = _authRepository.FindUserByUserName(userName);
            if (user == null)
                throw new Exception(string.Format("No user found with username '{0}", userName));
            if (user.Email.Equals(string.Empty))
                throw new Exception(string.Format("Email not specified for user with username '{0}'", userName));
            return user;
        }

        public IQueryable<User> GetAllActiveSuppliers()
        {
            return GetAll().Where(x => x.IsActive.HasValue && x.IsActive.Value && x.AccountType.Equals("Supplier"));
        }

        public void SendResetPasswordMessageToUser(string url, User user)
        {
            var emailSubject = "Integra Password Reset Request";
            var emailBody = string.Format("<p>Password reset process was initiated for user \"{0}\". To change password, plese follow the link: <a href=\"{1}\">Password Reset</a> </p>", user.UserName, url);
            _emailService.SendEmail(user.Email, emailSubject, emailBody);
        }

        public string CreateResetTokenForUser(User user)
        {
            var token = string.Empty;
            token = _userManager.GeneratePasswordResetToken(user.Id);
            return token;
        }

        public void ResetPassword(string userId, string resetToken, string newPassword)
        {
            var result = _userManager.ResetPassword(userId, resetToken, newPassword);
            if (result.Errors.Any())
            {
                var errorMessage = string.Empty;
                foreach (var error in result.Errors)
                {
                    errorMessage += error + "\n";
                }
                throw new Exception(errorMessage);
            }
        }

        public void ChangePassword(string userId, string currentPassword, string newPassword)
        {
            var result = _userManager.ChangePassword(userId, currentPassword, newPassword);
            if (result.Errors.Any())
            {
                var errorMessage = string.Empty;
                foreach (var error in result.Errors)
                {
                    errorMessage += error + "\n";
                }
                throw new Exception(errorMessage);
            }
        }

        public void ForceChangePassword(string userId, string newPassword)
        {
            var resetToken = _userManager.GeneratePasswordResetToken(userId);
            var result = _userManager.ResetPassword(userId, resetToken, newPassword);
            if (result.Errors.Any())
            {
                var errorMessage = string.Empty;
                foreach (var error in result.Errors)
                {
                    errorMessage += error + "\n";
                }
                throw new Exception(errorMessage);
            }
        }

        public Task<User> FindUser(string userName, string password)
        {
            return _authRepository.FindUser(userName, password);
        }
    }
}
