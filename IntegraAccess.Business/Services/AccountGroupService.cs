﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Services
{
    public class AccountGroupService : ServiceDbBase<AccountGroup>, IAccountGroupService
    {
        public AccountGroupService(IRepositoryBase repositoryBase)
            : base(repositoryBase)
        {
        }

        public IQueryable<AccountGroup> GetAll(List<int> ids)
        {
            return ids.Any() ? GetAll().Where(ac => ids.Contains(ac.Id)) : null;
        }

        public AccountGroup GetById(int id)
        {
            return RepositoryBase.Find<AccountGroup>(x => x.Id == id).FirstOrDefault();
        }

        public bool Delete(int id)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.EntityState = State.Deleted;
                return RepositoryBase.ApplyChanges(entity);
            }

            return false;
        }

        public void Inactivate(List<int> accountGroupsIds)
        {
            if (!accountGroupsIds.Any()) return;

            var accountGroups = GetAll(accountGroupsIds);
            foreach (var accountGroup in accountGroups)
            {
                accountGroup.IsActive = false;
                accountGroup.EntityState = State.Modified;
                accountGroup.SetUpdated();
            }
        }
    }
}