﻿using Autofac;
using IntegraAccess.Business.Services;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.DataAccess;
using IntegraAccess.DataAccess.Repository.Abstract;

namespace IntegraAccess.Business
{
    public class BusinessLayerRegistrationModule : DALRegistrationModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            
            builder.Register<IAccountGroupService>(x =>         new AccountGroupService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<IAccountService>(x =>              new AccountService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<IPatientService>(x =>              new PatientService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<IFacilityService>(x =>             new FacilityService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<ICompanyService>(x =>              new CompanyService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<IProductService>(x =>              new ProductService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<IOrderService>(x =>                new OrderService(x.Resolve<IRepositoryBase>(), x.Resolve<IEmailService>())).InstancePerRequest();
            builder.Register<IPriceSheetService>(x =>           new PriceSheetService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<ILogService>(x =>                  new LogService(x.Resolve<IRepositoryBase>())).InstancePerLifetimeScope();
            builder.Register<IEmailService>(x =>                new EmailService()).InstancePerRequest();
            builder.Register<IUserService>(x =>                 new UserService(x.Resolve<IRepositoryBase>(), 
                                                                                x.Resolve<IAuthRepository>(),
                                                                                x.Resolve<IEmailService>())).InstancePerRequest();
            builder.Register<IProductPriceSheetItemService>(x =>new ProductPriceSheetItemService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<IProductCategoryService>(x =>      new ProductCategoryService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
            builder.Register<IProductTemplateItemService>(x => new ProductTemplateItemService(x.Resolve<IRepositoryBase>())).InstancePerRequest();
        }
    }
}
