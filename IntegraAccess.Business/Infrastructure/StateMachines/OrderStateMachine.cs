﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Business.Infrastructure.StateMachines
{
    public class OrderStateMachine
    {
        private Order _order;
        private readonly User _user;

        public OrderStateMachine(Order order, User user)
        {
            _order = order;
            _user = user;
        }

        private OrderStates CurrentState
        {
            get { return _order.OrderStateEnumValue; }
        }

        public bool SetState(OrderStates state)
        {
            var possibleActions = GetPossibleOrderStateActions();
            if (possibleActions.Any(x => x.OrderState == state))
            {
                _order.OrderStateEnumValue = state;
                if (state == OrderStates.DMEApprovalRequired || (string.IsNullOrEmpty(_order.SubmittedById) && state == OrderStates.DMEAccepted))
                {
                    _order.SubmittedById = _user.Id;
                    _order.SubmittedAt = DateTime.UtcNow;
                }
                return true;
            }
            return false;
        }

        public List<OrderStateAction> GetPossibleOrderStateActions()
        {
            if (CurrentState == OrderStates.Saved)
            {
                return new List<OrderStateAction>()
                {
                    new OrderStateAction("Submit", GetInitStateForCreatedOrder(_user.IsSupplier, _user.IsOrderApprovalOverride)),
                    new OrderStateAction("Edit", OrderStates.Saved)
                };
            }

            if (_user.IsAccountUser)
            {
                if (CurrentState == OrderStates.ApprovalRequired && _user.IsAccountGroupAdmin || _user.IsOrderApprovalAdmin)
                {
                    return new List<OrderStateAction>()
                    {
                        new OrderStateAction("Approve", OrderStates.DMEApprovalRequired),
                        new OrderStateAction("Edit", OrderStates.ApprovalRequired),
                        new OrderStateAction("Reject", OrderStates.AccountRejected)
                    };
                }
                if (CurrentState == OrderStates.AccountRejected)
                {
                    return new List<OrderStateAction>()
                    {
                        new OrderStateAction("Edit", OrderStates.AccountRejected),
                        new OrderStateAction("Re-Submit", OrderStates.ApprovalRequired)
                    };
                }
            }

            if (_user.IsSupplier)
            {
                if (CurrentState == OrderStates.DMEApprovalRequired)
                {
                    return new List<OrderStateAction>()
                    {
                        new OrderStateAction("Accept", OrderStates.DMEAccepted),
                        new OrderStateAction("Hold", OrderStates.DMEHold),
                        new OrderStateAction("Cancel", OrderStates.DMECancelled)
                    };
                }
                if (CurrentState == OrderStates.DMEAccepted)
                {
                    return new List<OrderStateAction>()
                    {
                        new OrderStateAction("Complete", OrderStates.Completed),
                        new OrderStateAction("Hold", OrderStates.DMEHold),
                        new OrderStateAction("Cancel", OrderStates.DMECancelled)
                    };
                }
                if (CurrentState == OrderStates.DMEHold)
                {
                    return new List<OrderStateAction>()
                    {
                        new OrderStateAction("Accept", OrderStates.DMEAccepted),
                        new OrderStateAction("Cancel", OrderStates.DMECancelled)
                    };
                }
                if (CurrentState == OrderStates.Completed)
                {
                    return new List<OrderStateAction>()
                    {
                        new OrderStateAction("Cancel", OrderStates.DMECancelled)
                    };
                }
            }
            return new List<OrderStateAction>();
        }

        public bool HasUserAccessToOrder()
        {
            return HasUserAccessToOrder(_user, _order);
        }

        public static bool HasUserAccessToOrder(User user, Order order)
        {
            return FilterByStates(new List<Order>() { order }.AsQueryable(), user).Any();
        }

        public static IQueryable<Order> FilterByStates(IQueryable<Order> orders, User user)
        {
            return orders.Where(order => 
                //own orders
                (order.CreatedById == user.Id)
                //supplier's orders
                || (user.IsSupplier && order.OrderStateId >= (int)OrderStates.DMEApprovalRequired)
                //account's orders
                || ((user.IsAccountUser && (order.OrderStateId.HasValue && order.OrderStateId.Value != (int)OrderStates.Saved))
                    && ((user.IsAccountGroupAdmin && order.Account.AccountGroupId == user.AccountGroupId)
                        || (user.IsOrderApprovalAdmin && order.Account.UserAccounts.Any(x => x.IsActive && x.UserId == user.Id))))
                );
        }

        public static OrderStates GetInitStateForCreatedOrder(bool isSupplier, bool isOrderApprovalOverride)
        {
            if (isSupplier)
            {
                return OrderStates.DMEAccepted;
            }
            //if account user
            return isOrderApprovalOverride ? OrderStates.DMEApprovalRequired : OrderStates.ApprovalRequired;
        }

        public static OrderStates GetInitStateForCreatedOrder(User user)
        {
            return GetInitStateForCreatedOrder(user.IsSupplier, user.IsOrderApprovalOverride);
        }



        public class OrderStateAction
        {
            public string ActionName { get; set; }
            public OrderStates OrderState { get; set; }

            public OrderStateAction(string action, OrderStates state)
            {
                ActionName = action;
                OrderState = state;
            }
        }
    }
}