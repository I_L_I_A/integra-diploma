﻿using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities
{
    public interface IObjectWithState
    {
        State EntityState { get; set; }
    }
}