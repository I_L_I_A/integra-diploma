﻿using System.Collections.Generic;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Entities
{
    public class EntityEqulityComparer : IEqualityComparer<IBaseEntity>
    {
        public bool Equals(IBaseEntity x, IBaseEntity y)
        {
            if (x == null || y == null || x.GetType() != y.GetType() ||
                x.IsNew() || y.IsNew())
                return false;

            return x.Equals(y);
        }

        public int GetHashCode(IBaseEntity obj)
        {
            return obj.GetHashCode() ^ obj.GetType().GetHashCode();
        }
    }

  
}