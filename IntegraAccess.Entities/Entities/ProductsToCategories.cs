﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities
{
    public class ProductsToCategories : CustomKeyedBaseEntity
    {
        [Key, Column(Order = 0)]
        public int ProductId { get; set; }
        [Key, Column(Order = 1)]
        public int CategoryId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        [ForeignKey("CategoryId")]
        public virtual ProductCategory Category { get; set; }

        public bool IsActive { get; set; }

        public override bool IsNew()
        {
            return ProductId == 0 || CategoryId == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;
            if (obj is ProductsToCategories)
            {
                return GetHashCode() == (obj as ProductsToCategories).GetHashCode();
            }
            return false;
        }

        public override int GetHashCode()
        {
            return ProductId.GetHashCode() ^ CategoryId.GetHashCode() ^
                   string.Format("{0}{1}", ProductId, CategoryId).GetHashCode() ^ GetType().GetHashCode();
        }
    }
}
