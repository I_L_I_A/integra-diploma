﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities
{
    public class Patient : KeyedBaseEntity
    {
        // User details
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsDeceased { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DeceasedDate { get; set; }

        public int? LastAccountId { get; set; }
        public int? EHRId { get; set; }
        public int? AccountGroupId { get; set; }
        public int? AccountId { get; set; }
        public int? FacilityId { get; set; }
        //External
        public string ExternalCode { get; set; }
        public string ExternalEMRCode { get; set; }
        //Physician
        public string PhysicianFirstName { get; set; }
        public string PhysicianLastName { get; set; }
        public string PhysicianPhone { get; set; }
        public string PhysicianEmail { get; set; }
        //Contact
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }

        [ForeignKey("AccountGroupId")]
        public virtual AccountGroup AccountGroup { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        [ForeignKey("LastAccountId")]
        public virtual Account LastAccount { get; set; }

        [ForeignKey("FacilityId")]
        public virtual Facility Facility { get; set; }
    }
}


