﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;
using IntegraAccess.Entities.Enums;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IntegraAccess.Entities.Entities
{
    public class User : IdentityUser, IBaseEntity
    {
        // User details
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; } //Dr, RN, Mr, Ms
        public int? AccountGroupId { get; set; }
        public int? AccountId { get; set; }
        public string AccountType { get; set; }

        [ForeignKey("AccountGroupId")]
        public virtual AccountGroup AccountGroup { get; set; }

        public virtual ICollection<UserAccounts> UserAccounts { get; set; }

        //Permission
        public bool IsAccountGroupAdmin { get; set; }
        public bool IsOrderApprovalAdmin { get; set; }
        public bool IsOrderApprovalOverride { get; set; }
        [DefaultValue("false")]
        public bool ReceiveApprovalNeededEmail { get; set; }
        
        // Meta-data
        public DateTime UpdatedAt { get; set; }
        public string UpdatedById { get; set; }
        public bool ChangePasswordOnLogin { get; set; }

        [DefaultValue("true")]
        public bool? IsActive { get; set; }

        [NotMapped]
        public IList<string> UserRoles { get; set; }

        [NotMapped]
        public bool IsSupplier
        {
            get { return UserRoles.Contains("Supplier"); }
        }

        [NotMapped]
        public bool IsAccountUser
        {
            get { return UserRoles.Contains("Account"); }
        }

        [NotMapped]
        public State EntityState { get; set; }

        public bool IsNew()
        {
            return string.IsNullOrEmpty(Id);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;

            if (obj is User)
            {
                return GetHashCode() == (obj as User).GetHashCode();
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ GetType().GetHashCode();
        }

    }
}