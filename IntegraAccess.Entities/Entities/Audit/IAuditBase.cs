﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public interface IAuditBase
    {
        string AuditDataState { get; set; }

        string AuditDMLAction { get; set; }

        DateTime AuditDateTime { get; set; }

        int HistoryId { get; set; } 
    }
}