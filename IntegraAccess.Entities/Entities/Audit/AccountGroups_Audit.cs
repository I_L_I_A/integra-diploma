﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public partial class AccountGroups_Audit : IAuditBase
    {
        public int Id { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }
        public string ExternalCode { get; set; }
        public string Name { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public bool IsActive { get; set; }


        [StringLength(10)]
        public string AuditDataState { get; set; }
        [StringLength(10)]
        public string AuditDMLAction { get; set; }
        public DateTime AuditDateTime { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; }
    }
}
