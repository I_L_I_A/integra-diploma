﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public partial class ProductTemplateItems_Audit : IAuditBase
    {
        public int CompanyId { get; set; }
        public int ProductId { get; set; }
        public int ChildProductId { get; set; }
        public int Quantity { get; set; }
        public bool IsActive { get; set; }

        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }

        [StringLength(10)]
        public string AuditDataState { get; set; }
        [StringLength(10)]
        public string AuditDMLAction { get; set; }
        public DateTime AuditDateTime { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; }
    }
}
