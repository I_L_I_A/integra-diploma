﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public class ProductPriceSheets_Audit : IAuditBase
    {
        public int Id { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }

        public string Name { get; set; }
        public int? CompanyId { get; set; }
        public bool IsActive { get; set; }

        [StringLength(10)]
        public string AuditDataState { get; set; }
        [StringLength(10)]
        public string AuditDMLAction { get; set; }
        public DateTime AuditDateTime { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; }
    }
}
