﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public partial class Products_Audit : IAuditBase
    {
        public int Id { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }

        public string DisplayName { get; set; }
        public string ExternalCode { get; set; }
        public string ShortDescription { get; set; }
        public string ShortDescription2 { get; set; }
        public string PortalDescription { get; set; }
        public byte[] Image { get; set; }
        public string ImageName { get; set; }
        public string ManufacturerLink { get; set; }
        public decimal? PriceDefault { get; set; }
        public bool IsActive { get; set; }
        public bool IsTemplate { get; set; }
        public bool AvailableOutsideTemplate { get; set; }

        [StringLength(10)]
        public string AuditDataState { get; set; }
        [StringLength(10)]
        public string AuditDMLAction { get; set; }
        public DateTime AuditDateTime { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; }
    }
}
