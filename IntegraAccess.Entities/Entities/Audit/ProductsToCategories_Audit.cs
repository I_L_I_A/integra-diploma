﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public partial class ProductsToCategories_Audit : IAuditBase
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public bool IsActive { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }

        [StringLength(10)]
        public string AuditDataState { get; set; }
        [StringLength(10)]
        public string AuditDMLAction { get; set; }
        public DateTime AuditDateTime { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; }
    }
}
