﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public class ProductPriceSheetItems_Audit : IAuditBase
    {
        public int ProductPriceSheetId { get; set; }
        public int ProductId { get; set; }

        public decimal? PriceSelling { get; set; }
        public decimal? PriceRental { get; set; }
        public decimal? PriceDaily { get; set; }
        public bool IsSellingAllowed { get; set; }

        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }

        [StringLength(10)]
        public string AuditDataState { get; set; }
        [StringLength(10)]
        public string AuditDMLAction { get; set; }
        public DateTime AuditDateTime { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; }
    }
}
