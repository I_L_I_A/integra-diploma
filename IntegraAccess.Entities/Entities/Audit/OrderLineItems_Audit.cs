﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities.Audit
{
    public class OrderLineItems_Audit : IAuditBase
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int? PurchaseTypeId { get; set; }
        public int ItemNumber { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Comments { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }
        public int? AddedByProductTemplateId { get; set; }

        [StringLength(10)]
        public string AuditDataState { get; set; }
        [StringLength(10)]
        public string AuditDMLAction { get; set; }
        public DateTime AuditDateTime { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HistoryId { get; set; }
    }
}
