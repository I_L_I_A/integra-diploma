﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities
{
    public class Product : KeyedBaseEntity
    {
        public string DisplayName { get; set; }
        public string ExternalCode { get; set; }
        public string ShortDescription { get; set; }
        public string ShortDescription2 { get; set; }
        public string PortalDescription { get; set; }
        public byte[] Image { get; set; }
        public string ImageName { get; set; }
        public string ManufacturerLink { get; set; }
        public decimal? PriceDefault { get; set; }
        public bool IsActive { get; set; }
        public bool IsTemplate { get; set; }
        public bool AvailableOutsideTemplate { get; set; }

        public virtual ICollection<ProductsToCategories> ProductsToCategories { get; set; }
        public virtual ICollection<OrderLineItem> OrderLineItems { get; set; }
        public virtual ICollection<ProductPriceSheetItem> ProductPriceSheetItems { get; set; }
        public virtual ICollection<ProductTemplateItem> NestedTemplateItems { get; set; }
        public virtual ICollection<ProductTemplateItem> ParentTemplateItems { get; set; }
    }
}
