﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities
{
    public class OrderLineItem : KeyedBaseEntity
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        
        public int? AddedByProductTemplateId { get; set; }
        public int? PurchaseTypeId { get; set; }

        public int ItemNumber { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Comments { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        [ForeignKey("PurchaseTypeId")]
        public virtual PurchaseType PurchaseType { get; set; }
        [ForeignKey("AddedByProductTemplateId")]
        public virtual Product AddedByProductTemplate { get; set; }

    }
}
