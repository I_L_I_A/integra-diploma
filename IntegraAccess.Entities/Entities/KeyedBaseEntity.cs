﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    [Serializable()]
    public class KeyedBaseEntity : AuditingEntity, IBaseEntity
    {
        public KeyedBaseEntity()
        {
            EntityState = State.Unchanged;
            UpdatedDateTime = DateTime.UtcNow;
        }

        [Key]
		public int Id { get; set; }

        [NotMapped]
        public State EntityState { get; set; }

		public bool IsNew()
		{
			return Id == 0;
		}

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;

            if (Id == ((KeyedBaseEntity) obj).Id)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return Id ^ GetType().GetHashCode();
        }
    }
}