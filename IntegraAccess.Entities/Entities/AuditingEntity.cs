﻿using System;

namespace IntegraAccess.Entities.Entities
{
	[Serializable()]
	public class AuditingEntity
	{
	    public AuditingEntity()
	    {
            SetUpdated();
	    }

	    public DateTime UpdatedDateTime { get; set; }

        public string UpdatedById { get; set; }

		public void SetUpdated()
		{
            UpdatedDateTime = DateTime.UtcNow;
		}

        public void SetUpdated(string updatedById)
        {
            UpdatedById = updatedById;
            SetUpdated();
        }
	}
}