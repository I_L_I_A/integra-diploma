﻿
using System.Collections.Generic;

namespace IntegraAccess.Entities.Entities
{
    public class Company : KeyedBaseEntity
    {
        public string ExternalCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ProductPriceSheet> ProductPriceSheets { get; set; }
        public virtual ICollection<ProductTemplateItem> ProductTemplateItems { get; set; }
    }
}
