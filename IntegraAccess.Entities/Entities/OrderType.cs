﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities
{
    public class OrderType : KeyedBaseEntity
    {
        public string Name { get; set; }
    }
}
