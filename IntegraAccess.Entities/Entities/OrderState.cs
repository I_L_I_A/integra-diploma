﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities
{
    public class OrderState : KeyedBaseEntity
    {
        public string Name { get; set; }
    }
}
