﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    public class ProductPriceSheet : KeyedBaseEntity
    {
        public string Name { get; set; }
        public int? CompanyId { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        public virtual ICollection<ProductPriceSheetItem> ProductPriceSheetItems { get; set; }
    }
}
