﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    public class UserAccounts : CustomKeyedBaseEntity
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        [Key, Column(Order = 1)]
        public int AccountId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public bool IsActive { get; set; }

        public override bool IsNew()
        {
            return string.IsNullOrEmpty(UserId) || AccountId == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;
            if (obj is UserAccounts)
            {
                return GetHashCode() == (obj as UserAccounts).GetHashCode();
            }
            return false;
        }

        public override int GetHashCode()
        {
            return UserId.GetHashCode() ^ AccountId.GetHashCode() ^ GetType().GetHashCode();
        }
    }
}
