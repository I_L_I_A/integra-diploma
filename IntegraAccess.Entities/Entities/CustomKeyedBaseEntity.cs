﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    [Serializable()]
    public abstract class CustomKeyedBaseEntity : AuditingEntity, IBaseEntity
    {
        public CustomKeyedBaseEntity()
        {
            EntityState = State.Unchanged;
            UpdatedDateTime = DateTime.UtcNow;
        }

        [NotMapped]
        public State EntityState { get; set; }

        public abstract bool IsNew();

    }
}