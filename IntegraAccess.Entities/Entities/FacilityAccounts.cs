﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    public class FacilityAccounts : CustomKeyedBaseEntity
    {
        [Key, Column(Order = 0)]
        public int FacilityId { get; set; }
        [Key, Column(Order = 1)]
        public int AccountId { get; set; }
        [ForeignKey("FacilityId")]
        public virtual Facility Facility { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public bool IsActive { get; set; }

        public override bool IsNew()
        {
            return FacilityId == 0 || AccountId == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;
            if (obj is FacilityAccounts)
            {
                return GetHashCode() == (obj as FacilityAccounts).GetHashCode();
            }
            return false;
        }

        public override int GetHashCode()
        {
            return FacilityId.GetHashCode() ^ AccountId.GetHashCode() ^ GetType().GetHashCode();
        }
    }
}
