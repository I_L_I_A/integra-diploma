﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    public class ProductTemplateItem : CustomKeyedBaseEntity
    {
        [Key, Column(Order = 0)]
        public int CompanyId { get; set; }
        [Key, Column(Order = 1)]
        public int ProductId { get; set; }
        [Key, Column(Order = 2)]
        public int ChildProductId { get; set; }

        public int Quantity { get; set; }

        public bool IsActive { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [ForeignKey("ChildProductId")]
        public virtual Product ChildProduct { get; set; }

        public override bool IsNew()
        {
            return CompanyId == 0 || ProductId == 0 || ChildProductId == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;
            if (obj is ProductTemplateItem)
            {
                return GetHashCode() == (obj as ProductTemplateItem).GetHashCode();
            }
            return false;
        }

        public override int GetHashCode()
        {
            return CompanyId.GetHashCode() ^ ProductId.GetHashCode() ^ GetType().GetHashCode() ^ ChildProductId.GetHashCode();
        }
    }
}
