﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    public class Order : KeyedBaseEntity
    {
        public string ExternalOrderNumber { get; set; }
        public string PONumber { get; set; }
        public string OrderNotes { get; set; }
        public int? AccountId { get; set; }
        public int? CompanyId { get; set; }
        public int? OrderTypeId { get; set; }
        public int? OrderStateId { get; set; }
        public string CreatedById { get; set; }
        public string SubmittedById { get; set; }
        public DateTime? SubmittedAt { get; set; }
        public DateTime OrderDate { get; set; }
        public string ShippingStreet1 { get; set; }
        public string ShippingStreet2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingZip { get; set; }
        public string ReferralSource { get; set; }
        public DateTime? RequestedDeliveryDate { get; set; }
        public DateTime? RequestedDeliveryStartTime { get; set; }
        public DateTime? RequestedDeliveryEndTime { get; set; }
        public string DeliveryNotes { get; set; }

        //patient's Info
        public int? PatientId { get; set; }
        public int? PatientAccountId { get; set; }
        public int? PatientFacilityId { get; set; }
        public string PatientExternalCode { get; set; }
        public string PatientExternalEMRCode { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public DateTime? PatientBirthDate { get; set; }
        public string PatientGender { get; set; }
        public string PatientHeight { get; set; }
        public string PatientWeight { get; set; }
        public string PatientStreet1 { get; set; }
        public string PatientStreet2 { get; set; }
        public string PatientCity { get; set; }
        public string PatientState { get; set; }
        public string PatientZip { get; set; }
        public string PatientPhone { get; set; }
        public string PatientEmail { get; set; }
        public string PatientContactFirstName { get; set; }
        public string PatientContactLastName { get; set; }
        public string PatientContactPhone { get; set; }
        public string PatientPhysicianFirstName { get; set; }
        public string PatientPhysicianLastName { get; set; }
        public string PatientPhysicianNumber { get; set; }
        public bool PatientIsDeceased { get; set; }
        public DateTime? PatientDeceasedDate { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [ForeignKey("OrderTypeId")]
        public virtual OrderType OrderType { get; set; }

        [ForeignKey("OrderStateId")]
        public virtual OrderState OrderState { get; set; }

        [ForeignKey("CreatedById")]
        public virtual User CreatedBy { get; set; }

        [ForeignKey("SubmittedById")]
        public virtual User SubmittedBy { get; set; }

        [ForeignKey("PatientFacilityId")]
        public virtual Facility PatientFacility { get; set; }

        public virtual ICollection<OrderLineItem> OrderLineItems { get; set; }

        [NotMapped]
        public OrderStates OrderStateEnumValue
        {
            get
            {
                if (OrderStateId.HasValue)
                {
                    return (OrderStates)OrderStateId;
                }
                return OrderStates.Saved;
            }
            set { OrderStateId = (int)value; }
        }
    }
}
