﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    public class PurchaseType : KeyedBaseEntity
    {
        public string Name { get; set; }
    }
}
