﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IntegraAccess.Entities.Entities
{
    public class Facility : KeyedBaseEntity
    {
        public bool IsActive { get; set; }
        public int? AccountGroupId { get; set; }
        public int? CompanyId { get; set; }
        public string Name { get; set; }
        public string ExternalCode { get; set; }
      
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }

        public virtual ICollection<FacilityAccounts> FacilityAccounts { get; set; }

        [ForeignKey("AccountGroupId")]
        public virtual AccountGroup AccountGroup { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
    }
}
