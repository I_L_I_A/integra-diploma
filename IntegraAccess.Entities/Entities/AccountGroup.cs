﻿using System.Collections.Generic;

namespace IntegraAccess.Entities.Entities
{
    public class AccountGroup : KeyedBaseEntity
    {
        public string ExternalCode { get; set; }
        public string Name { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public bool IsActive { get; set; }
        public virtual List<Account> Accounts { get; set; }
        public virtual List<User> Users { get; set; }
        public virtual List<Facility> Facilities { get; set; }
    }
}
