﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities.Entities
{
    public class ProductPriceSheetItem : CustomKeyedBaseEntity
    {
        [Key, Column(Order = 0)]
        public int ProductPriceSheetId { get; set; }
        [Key, Column(Order = 1)]
        public int ProductId { get; set; }

        public decimal? PriceSelling { get; set; }
        public decimal? PriceRental { get; set; }
        public decimal? PriceDaily { get; set; }
        public bool IsSellingAllowed { get; set; }

        [ForeignKey("ProductPriceSheetId")]
        public virtual ProductPriceSheet ProductPriceSheet { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        public override bool IsNew()
        {
            return ProductPriceSheetId == 0 || ProductId == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;
            if (obj is ProductPriceSheetItem)
            {
                return GetHashCode() == (obj as ProductPriceSheetItem).GetHashCode();
            }
            return false;
        }

        public override int GetHashCode()
        {
            return ProductPriceSheetId.GetHashCode() ^ ProductId.GetHashCode() ^ GetType().GetHashCode();
        }

    }
}
