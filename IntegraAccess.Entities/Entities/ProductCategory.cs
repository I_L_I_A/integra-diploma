﻿
using System.Collections.Generic;

namespace IntegraAccess.Entities.Entities
{
    public class ProductCategory : KeyedBaseEntity
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<ProductsToCategories> ProductsToCategories { get; set; }
    }
}
