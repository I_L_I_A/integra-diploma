﻿namespace IntegraAccess.Entities.Enums
{
    public enum TransactionTypes
    {
        DbTransaction,
        TransactionScope
    }
}