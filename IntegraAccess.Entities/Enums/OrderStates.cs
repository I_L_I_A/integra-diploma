﻿namespace IntegraAccess.Entities.Enums
{
    public enum OrderStates
    {
        Saved = 1,
        ApprovalRequired = 2,
        AccountRejected = 3,
        Cancelled = 4,
        DMEApprovalRequired = 5,
        DMEAccepted = 6,
        DMEHold = 7,
        Completed = 8,
        DMECancelled = 9
    }
}