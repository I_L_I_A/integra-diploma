﻿namespace IntegraAccess.Entities.Enums
{
    public enum State
    {
        Added,
        Unchanged,
        Modified,
        Deleted
    }
}