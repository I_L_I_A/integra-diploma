﻿namespace IntegraAccess.Entities
{
    public interface IVersionable
    {
        int Version { get; set; }
    }
}
