﻿using System.ComponentModel.DataAnnotations.Schema;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Entities
{
    public interface IBaseEntity : IObjectWithState
    {
        bool IsNew();

        //override for * to * relationship entitits
        bool Equals(object obj);

        //override for * to * relationship entitits
        int GetHashCode();
    }
}