﻿using System;

namespace IntegraAccess.Common.Exceptions
{
    public class DALConcurrencyException : Exception
    {
        public DALConcurrencyException()
            : base("Optimistic concurrency violation occurs.")
        { }

        public DALConcurrencyException(Exception baseException)
            : base("Optimistic concurrency violation occurs.", baseException)
        { }
    }
}
