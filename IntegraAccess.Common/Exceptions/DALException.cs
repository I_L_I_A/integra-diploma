﻿using System;

namespace IntegraAccess.Common.Exceptions
{
    public class DALException : Exception
    {
        public DALException()
            : base("DAL Exception")
        { }

        public DALException(Exception baseException)
            : base("UnitOfWork failed to commit changes", baseException)
        { }
    }
}
