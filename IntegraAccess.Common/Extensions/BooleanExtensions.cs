﻿namespace IntegraAccess.Common.Extensions
{
    public static class BooleanExtensions
    {
        public static bool IsTrue(this bool? b)
        {
            return b.HasValue && b.Value;
        }

        public static bool IsFalse(this bool? b)
        {
            return !b.IsTrue();
        }
    }
}
