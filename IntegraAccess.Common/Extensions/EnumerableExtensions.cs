﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntegraAccess.Common.Extensions
{
    public class KeyEqualityComparer<T, TKey> : IEqualityComparer<T>
    {
        protected readonly Func<T, TKey> keyExtractor;

        public KeyEqualityComparer(Func<T, TKey> keyExtractor)
        {
            this.keyExtractor = keyExtractor;
        }

        public virtual bool Equals(T x, T y)
        {
            return this.keyExtractor(x).Equals(this.keyExtractor(y));
        }

        public int GetHashCode(T obj)
        {
            return this.keyExtractor(obj).GetHashCode();
        }
    }

    public static class EnumerableExtensions
    {
        public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> source, IEnumerable<TSource> second, Func<TSource, object> keyExtractor)
        {
            return source.Except(second, new KeyEqualityComparer<TSource, object>(keyExtractor));
        }

        public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> source, IEnumerable<TSource> second, Func<TSource, object> keyExtractor)
        {
            return source.Intersect(second, new KeyEqualityComparer<TSource, object>(keyExtractor));
        }

        public static ICollection<TSource> ToCollection<TSource>(this IEnumerable<TSource> source)
        {
            if (source is ICollection<TSource>)
            {
                return source as ICollection<TSource>;
            }
            return source.ToList<TSource>();
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
