﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntegraAccess.Common.Extensions
{
    public static class QuerableExtensions
    {
        public static IQueryable<TSource> GetPage<TSource>(this IQueryable<TSource> source, int page, int pageSize)
        {
            //paging
            if (page > 0 && pageSize > 0)
            {
                source = source.Skip((page - 1) * pageSize).Take(pageSize);
            }
            return source;
        }
    }
}
