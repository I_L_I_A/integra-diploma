﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace IntegraAccess.Common.Extensions
{
    public static class EnumExtensions
    {
        public static void Validate(this Enum e)
        {
            if (!Enum.IsDefined(e.GetType(), e))
                throw new InvalidOperationException("Enumeration value {0} is not defined for enum type {1}.".F(e, e.GetType()));
        }

        public static void ValidateParam(this Enum e, string paramName)
        {
            if (!Enum.IsDefined(e.GetType(), e))
                throw new ArgumentException("Enumeration value {0} is not defined for enum type {1}.".F(e, e.GetType()), paramName);
        }

        public static TEnum ParseEnum<TEnum>(this string enumValue) where TEnum : struct
        {
            StringExtensions.ValidateNullOrEmptyParam(enumValue, "enumValue");

            TEnum t;
            if (!Enum.TryParse(enumValue, out t))
                throw new InvalidOperationException("Cannot resolve {0} for enum type {1}".F(enumValue, typeof(TEnum)));

            return t;
        }

        public static bool In(this Enum value, params Enum[] list)
        {
            return list.Any(s => value.Equals(s));
        }

        public static IEnumerable<string> GetStringList<TEnum>(this IEnumerable<TEnum> e)
        {
            return e.Select(item => Enum.GetName(typeof(TEnum), item)).ToList();
        }

        public static string GetStringName<TEnum>(this TEnum e, bool withGap)
        {
            string result = Enum.GetName(typeof(TEnum), e);
            if (withGap)
            {
                result = result.AddSpacesToSentence();
            }
            return result;
        }

        public static string GetDescription(this Enum value)
        {
            value.ValidateParam("value");

            var field = value.GetType().GetField(value.ToString());
            var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
                        as DescriptionAttribute;

            return attribute == null ? "" : attribute.Description;
        }
    }
}
