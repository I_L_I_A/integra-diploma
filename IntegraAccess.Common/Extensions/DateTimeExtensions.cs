﻿using System;

namespace IntegraAccess.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime ConvertUtcByTimeZone(this DateTime date, string timezone)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(date, TimeZoneInfo.Utc.Id, timezone);
        }

        public static DateTime ConvertUtcByTimeZone(this DateTime date, TimeZoneInfo info)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(date, TimeZoneInfo.Utc.Id, info.Id);
        }

        public static DateTime ConvertToUtcFromTimeZone(this DateTime date, string timezone)
        {
            if (timezone.IsNullOrEmpty())
            {
                timezone = TimeZoneInfo.Utc.Id;
            }
            if (date.Kind != DateTimeKind.Unspecified)
            {
                date = DateTime.SpecifyKind(date, DateTimeKind.Unspecified);
            }
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(date, timezone, TimeZoneInfo.Utc.Id);
        }

        public static DateTime ConvertToUtcFromTimeZone(this DateTime date, TimeZoneInfo info)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(date, info.Id, TimeZoneInfo.Utc.Id);
        }
    }
}
