﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntegraAccess.Common.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        public static void ValidateNullOrEmpty(this string s)
        {
            if (s.IsNullOrEmpty())
                throw new InvalidOperationException();
        }

        public static void ValidateNullOrEmptyParam(this string s, string paramName)
        {
            if (s.IsNullOrEmpty())
                throw new ArgumentNullException(paramName);
        }

        public static string F(this string fmt, params object[] paramzz)
        {
            return string.Format(fmt, paramzz);
        }

        public static string Concat<T>(this IEnumerable<T> collection, Func<T, string> adapterT, string splitter = ", ")
        {
            collection.ValidateNullOrEmptyParam("collection");
            adapterT.ValidateParam("adapterT");

            var sb = new StringBuilder(adapterT(collection.First()));

            Action<string> builder;
            if (splitter != null)
            {
                builder = s => sb.Append(splitter).Append(s);
            }
            else
            {
                builder = s => sb.Append(s);
            }

            foreach (var t in collection.Skip(1))
                builder(adapterT(t));

            return sb.ToString();
        }

        public static string AddSpacesToSentence(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            var newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]) && text[i - 1] != ' ')
                    newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }
    }
}
