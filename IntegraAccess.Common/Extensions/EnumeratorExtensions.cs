﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntegraAccess.Common.Extensions
{
    public static class EnumeratorExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> e)
        {
            return e == null || !e.Any();
        }

        public static void ValidateNullOrEmpty<T>(this IEnumerable<T> e)
        {
            if (e.IsNullOrEmpty())
                throw new InvalidOperationException();
        }

        public static void ValidateNullOrEmptyParam<T>(this IEnumerable<T> e, string paramName)
        {
            if (e.IsNullOrEmpty())
                throw new ArgumentNullException(paramName);
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> e, Action<T> action)
        {
            e.ValidateParam("e");
            action.ValidateParam("action");

            foreach (T t in e)
            {
                action(t);
            }

            return e;
        }

        public static IEnumerable<TSource> Duplicates<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
        {
            var grouped = source.GroupBy(selector);
            var moreThen1 = grouped.Where(i => i.IsMultiple());
            return moreThen1.SelectMany(i => i);
        }

        public static IEnumerable<TSource> Duplicates<TSource, TKey>(this IEnumerable<TSource> source)
        {
            return source.Duplicates(i => i);
        }

        public static bool IsMultiple<T>(this IEnumerable<T> source)
        {
            var enumerator = source.GetEnumerator();
            return enumerator.MoveNext() && enumerator.MoveNext();
        }
    }
}
