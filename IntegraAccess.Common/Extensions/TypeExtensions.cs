﻿using System;

namespace IntegraAccess.Common.Extensions
{
    public static class TypeExtensions
    {
        public static string GetTypeLiteral(this Type type)
        {
            type.ValidateParam("type");

            return type.FullName.Replace('.', '_');
        }

        public static bool IsAny(this Type type, params Type[] types)
        {
            type.ValidateParam("type");

            var result = false;
            foreach (var t in types)
            {
                if (t == type)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }
    }
}
