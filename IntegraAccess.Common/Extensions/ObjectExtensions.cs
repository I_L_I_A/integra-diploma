﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IntegraAccess.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static void Validate(this object obj, string paramName = null)
        {
            if (obj == null)
            {
                throw paramName != null ?
                      new InvalidOperationException(paramName) :
                      new InvalidOperationException();
            }
        }

        public static T Validate<T>(this T obj, string paramName = null)
        {
            if (obj == null)
            {
                throw paramName != null ?
                      new InvalidOperationException(paramName) :
                      new InvalidOperationException();
            }
            return obj;
        }

        public static void ValidateParam(this object obj, string paramName)
        {
            if (obj == null)
                throw new ArgumentNullException(paramName);
        }

        public static IEnumerable<T> AsEnumerable<T>(this T obj)
        {
            yield return obj;
        }

        public static T1 GetIfNotNull<T, T1>(this T obj, Expression<Func<T, T1>> exp) where T : class
        {
            if (obj == null)
                return default(T1);

            return exp.Compile().Invoke(obj);
        }
    }
}
