﻿using System.Linq;
using System.Net.Http;
using System.Web.Http.Filters;
using IntegraAccess.Common.Helpers;

namespace IntegraAccess.Common.ActionFilters.GZip
{
    public class GZipCompressionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actContext)
        {
            if (actContext.Response == null)
            {
                base.OnActionExecuted(actContext);
                return;
            }
            if (actContext.ActionContext.ActionDescriptor.GetCustomAttributes<IgnoreGZipCompressionAttribute>().Any())
            {
                // The controller action is decorated with the [DontValidate]
                // custom attribute => don't do anything.
                return;
            }
            var content = actContext.Response.Content;
            var bytes = content == null ? null : content.ReadAsByteArrayAsync().Result;
            var zlibbedContent = bytes == null ? new byte[0] :
            CompressionHelper.GZipByte(bytes);
            actContext.Response.Content = new ByteArrayContent(zlibbedContent);
            actContext.Response.Content.Headers.Remove("Content-Type");
            actContext.Response.Content.Headers.Add("Content-encoding", "gzip");
            actContext.Response.Content.Headers.Add("Content-Type", "application/json");
            base.OnActionExecuted(actContext);
        }
    }
}