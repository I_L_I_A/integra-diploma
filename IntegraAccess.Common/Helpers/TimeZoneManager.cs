﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace IntegraAccess.Common.Helpers
{
    public static class TimeZoneManager
    {
        public static IDictionary<string, string> GetTimeZones(string culture = "en-US")
        {
            var zones = TimeZoneInfo.GetSystemTimeZones();
            var result = new Dictionary<string, string>();
            foreach (var z in zones)
            {
                result.Add(z.Id, z.DisplayName.ToString(CultureInfo.CreateSpecificCulture(culture)));
            }
            return result;
        }

        public static string GetIdByOffset(int hours)
        {
            var zones = TimeZoneInfo.GetSystemTimeZones();
            TimeSpan offsetTime = new TimeSpan(hours * (-1), 0, 0);
            foreach (var z in zones)
            {
                if (z.BaseUtcOffset == offsetTime)
                {
                    return z.Id;
                }
            }
            return null;
        }

        public static string GetTimeZoneById(string id, string culture)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(id).DisplayName.ToString(CultureInfo.CreateSpecificCulture(culture));
        }
    }
}
