﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegraAccess.DataAccess.Context;
using NUnit.Framework;

namespace IntegraAccess.DataAccess.Test
{
    [TestFixture]
    public class TestDataAccess
    {
        [Test]
        public void TestAuxDBContext()
        {
            var auxDBContext = new AuxDBContext();
            var results = auxDBContext.Database.SqlQuery<ReportActiveRentalItem>("exec usp_ReportActiveRentals 30, '30-SEAS', '30-1011', 'ALL'").ToList < ReportActiveRentalItem>();
        }

        public class ReportActiveRentalItem
        {
            public int? CompanyNumber { get; set; }
            public string Location { get; set; }
            public string PRODUCT_NUMBER { get; set; }
            public string PRODUCT_DESCRIPTION_1 { get; set; }
            public string Serial_Number { get; set; }
            public int? CARRIER_NUMBER { get; set; }
            public string CARRIER_NAME { get; set; }
            public int? PATIENT_NUMBER { get; set; }
            public string PATIENT_NAME { get; set; }
            public DateTime? FIRST_SERVICE_DATE { get; set; }
            public object Days_On_Rent { get; set; }
            public object QUANTITY_DELIVERED { get; set; }
            public Decimal? AVE_COST { get; set; }
            public Decimal? DailyPrice { get; set; }
            public Decimal? PerDiem_Quantity { get; set; }
            public Decimal? PerDiem { get; set; }
        }
    }
}
