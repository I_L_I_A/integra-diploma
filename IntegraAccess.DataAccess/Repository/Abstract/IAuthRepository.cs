﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntegraAccess.Entities.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IntegraAccess.DataAccess.Repository.Abstract
{
    public interface IAuthRepository : IDisposable
    {
        Task<IdentityResult> RegisterUser(User user, string password);
        Task<User> FindUser(string userName, string password);
        User FindUser(string id);
        User FindUserByEmail(string email);
        User FindUserByUserName(string userName);
        IdentityResult UpdateUser(User user);
        IdentityResult AddToRole(string userId, string role);
        IQueryable<User> GetUsers();
        IQueryable<User> GetUsers(List<string> userIds );
        void Inactivate(List<string> userIds);
        void SaveChanges();
        Task<User> FindAsync(UserLoginInfo loginInfo);
        Task<IdentityResult> CreateAsync(User user);
        Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login);
        void GrantAccess(string userId, Account account);
        void GrantAccess(string userId, List<Account> users);
        void DenyAccess(string userId, Account user);
        void DenyAccess(string userId, List<Account> users);
    }
}