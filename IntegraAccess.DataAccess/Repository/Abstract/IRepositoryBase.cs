﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using IntegraAccess.Entities;

namespace IntegraAccess.DataAccess.Repository.Abstract
{
    public interface IRepositoryBase : IDisposable
    {
        DbContext GetDbContext();
        /// <summary>
        /// Commit the transaction to the database.
        /// </summary>
        /// <param name="startNewTransaction"></param>
        void CommitTransaction(bool startNewTransaction = false);

        /// <summary>
        /// Count all entities of a specific type.
        /// </summary>
        /// <typeparam name="R"></typeparam>
        /// <returns></returns>
        Int32 Count<R>() where R : class;

        void Detach(object entity);
        void Detach(List<object> entities);

        IQueryable<R> Find<R>(Expression<Func<R, bool>> where) where R : class;
        IQueryable<R> Find<R>(Expression<Func<R, bool>> where, params Expression<Func<R, object>>[] includes) where R : class;

        R First<R>(Expression<Func<R, bool>> where) where R : class;
        R FirstOrDefault<R>(Expression<Func<R, bool>> where, params Expression<Func<R, object>>[] includes) where R : class;

        IQueryable<R> GetAll<R>() where R : class;
        IQueryable<R> GetAll<R>(params Expression<Func<R, object>>[] includes) where R : class;

        void SaveChanges();

        R Single<R>(Expression<Func<R, bool>> where) where R : class;
        R SingleOrDefault<R>(Expression<Func<R, bool>> where) where R : class;
        R SingleOrDefault<R>(Expression<Func<R, bool>> where, Expression<Func<R, object>> include) where R : class;

        bool ApplyChanges<R>(R root) where R : class, IObjectWithState;

        void DetachedExistingEntity(object obj);
    }
}