﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Transactions;
using IntegraAccess.DataAccess.Context;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities;
using IntegraAccess.Entities.Enums;
using IsolationLevel = System.Data.IsolationLevel;

namespace IntegraAccess.DataAccess.Repository
{
    using EntityEqulityComparer = IntegraAccess.Entities.EntityEqulityComparer;

    public class RepositoryBase<T> : IRepositoryBase
        where T : DbContext
    {
        public delegate void RepositoryBaseExceptionHandler(Exception exception);

        public delegate void RepositoryBaseRollBackOccuredHandler(MethodBase lastExecutedMethod);

        internal static bool _staticLazyConnectionOverrideUsed = false;
        internal static bool _lazyConnection = false;
        internal int CommandTimeout = 300;
        internal DbConnection Connection;
        internal string ConnectionString = string.Empty;

        internal MethodBase LastExecutedMethod = null;
        public DbContext Model;
        internal bool ProxyCreationEnabled = false;
        internal bool RethrowExceptions = true;
        internal bool SaveLastExcecutedMethodInfo = false;
        internal DbTransaction Transaction;
        internal TransactionScope TransactionScope;
        internal TransactionTypes TransactionType = TransactionTypes.DbTransaction;
        internal bool UseTransaction = true;
        private bool _disposing;
        internal IsolationLevel _isolationLevel = IsolationLevel.ReadUncommitted;

        public RepositoryBase()
        {
            InitializeRepository();
        }

        public RepositoryBase(bool throwExceptions, string connectionString = "", bool lazyConnection = false,
            TransactionTypes transactionType = TransactionTypes.DbTransaction,
            IsolationLevel isolationLevel = IsolationLevel.Snapshot,
            bool useTransactions = true, bool proxyCreationEnabled = false, int commandTimeout = 300,
            bool saveLastExecutedMethodInfo = false)
        {
            RethrowExceptions = throwExceptions;
            UseTransaction = useTransactions;
            ProxyCreationEnabled = proxyCreationEnabled;
            CommandTimeout = commandTimeout;
            _isolationLevel = IsolationLevel.Snapshot;
            ConnectionString = connectionString;
            SaveLastExcecutedMethodInfo = saveLastExecutedMethodInfo;

            if (_staticLazyConnectionOverrideUsed == false)
                _lazyConnection = lazyConnection;

            InitializeRepository();
        }

        public RepositoryBase(RepositoryBaseConfiguration configuration)
        {
            RethrowExceptions = configuration.RethrowExceptions;
            UseTransaction = configuration.UseTransaction;
            ProxyCreationEnabled = configuration.ProxyCreationEnabled;
            CommandTimeout = configuration.CommandTimeout;
            _isolationLevel = configuration.IsolationLevel;
            SaveLastExcecutedMethodInfo = configuration.SaveLastExecutedMethodInfo;

            if (_staticLazyConnectionOverrideUsed == false)
                _lazyConnection = configuration.LazyConnection;

            InitializeRepository();
        }

        public DbContext GetDbContext()
        {
            return Model;
        }

        public void CommitTransaction(bool startNewTransaction = false)
        {
            if (UseTransaction)
            {
                switch (TransactionType)
                {
                    case TransactionTypes.DbTransaction:
                        if (Transaction != null && Transaction.Connection != null)
                        {
                            SaveChanges();
                            Transaction.Commit();
                        }
                        break;

                    case TransactionTypes.TransactionScope:
                        try
                        {
                            if (TransactionScope != null)
                                TransactionScope.Complete();
                        }
                        catch (Exception error)
                        {
                            if (RethrowExceptions)
                            {
                                throw;
                            }
                            if (RepositoryBaseExceptionRaised != null)
                            {
                                RepositoryBaseExceptionRaised(error);
                            }
                        }

                        break;
                }

                if (startNewTransaction)
                    StartTransaction();
            }
            else
            {
                SaveChanges();
            }
        }

        public Int32 Count<R>() where R : class
        {
            return Model.Set<R>()
                .Count();
        }

        public void Detach(object entity)
        {
            if (entity != null)
            {
                ObjectContext objectContext = ((IObjectContextAdapter) Model).ObjectContext;
                DbEntityEntry entry = Model.Entry(entity);

                if (entry.State != EntityState.Detached)
                    objectContext.Detach(entity);
            }
        }

        public void Detach(List<object> entities)
        {
            entities.ForEach(Detach);
        }

        public IQueryable<R> Find<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = default(IQueryable<R>);

            ProcessTransactionableMethod(() => { entities = SetEntities<R>().Where(where); });

            return entities;
        }

        public IQueryable<R> Find<R>(Expression<Func<R, bool>> where, params Expression<Func<R, object>>[] includes)
            where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            ProcessTransactionableMethod(() =>
            {
                if (includes != null)
                {
                    entities = ApplyIncludesToQuery(entities, includes);
                }

                entities = entities.Where(where);
            });

            return entities;
        }

        public R First<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity = entities
                    .First(where);
            });

            return entity;
        }

        public R FirstOrDefault<R>(Expression<Func<R, bool>> where, params Expression<Func<R, object>>[] includes)
            where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                if (where != null)
                    entities = entities.Where(where);

                if (includes != null)
                {
                    entities = ApplyIncludesToQuery(entities, includes);
                }

                entity = entities.FirstOrDefault();
            });

            return entity;
        }

        public IQueryable<R> GetAll<R>() where R : class
        {
            IQueryable<R> entities = default(IQueryable<R>);

            ProcessTransactionableMethod(() => { entities = SetEntities<R>(); });

            return entities;
        }

        public IQueryable<R> GetAll<R>(params Expression<Func<R, object>>[] includes) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            if (includes != null)
            {
                entities = ApplyIncludesToQuery(entities, includes);
            }

            return entities;
        }

        public void SaveChanges()
        {
            if (Model != null)
            {
                Model.SaveChanges();
            }
        }

        public R Single<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity = entities
                    .Single(where);
            });

            return entity;
        }

        public R SingleOrDefault<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity = entities
                    .SingleOrDefault(where);
            });

            return entity;
        }

        public R SingleOrDefault<R>(Expression<Func<R, bool>> where, Expression<Func<R, object>> include)
            where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity = entities
                    .Include(include)
                    .SingleOrDefault(where);
            });

            return entity;
        }

        public void Dispose()
        {
            if (_disposing)
            {
                return;
            }

            _disposing = true;
            try
            {
                CommitTransaction();
            }
            catch (Exception error)
            {
                RollBack();
                if (RethrowExceptions)
                {
                    throw;
                }
                if (RepositoryBaseExceptionRaised != null)
                {
                    RepositoryBaseExceptionRaised(error);
                }
            }

            Transaction = null;
            TransactionScope = null;

            // Check if this can be reverted. There was a problem with closing connections with MS ServiceLocator.
            /*if (_connection != null && _connection.State != ConnectionState.Closed)
			{
				_connection.Close();
				_connection = null;
			}*/

            // Check if this can be deleted safely
            if (Model != null)
            {
                if (Model.Database.Connection != null && Model.Database.Connection.State != ConnectionState.Closed)
                {
                    Model.Database.Connection.Close();
                    Model.Database.Connection.Dispose();
                }

                if (!Equals(Model, null))
                {
                    Model.Dispose();
                    Model = null;
                }
            }
        }

        private EntityState ConvertState(State state)
        {
            switch (state)
            {
                case State.Added:
                    return EntityState.Added;
                case State.Modified:
                    return EntityState.Modified;
                case State.Deleted:
                    return EntityState.Deleted;
                default:
                    return EntityState.Unchanged;
            }
        }

        public void DetachedExistingEntity(object obj)
        {
            var entries = Model.ChangeTracker.Entries<IObjectWithState>().ToArray();
            for (int i = 0; i < entries.Count(); i++)
            {
                if (EntityEqulityComparer.Equals(entries[i], obj))
                {
                    entries[i].State = EntityState.Detached;
                }
            }
        }

        public bool ApplyChanges<R>(R root) where R : class, IObjectWithState
        {
            bool result = false;

            ProcessTransactionableMethod(() =>
            {
                try
                {
                    if (root.EntityState == State.Added)
                        Model.Set<R>().Add(root);
                    else
                        Model.Set<R>().Attach(root);

                    foreach (var entry in Model.ChangeTracker.Entries<IObjectWithState>())
                    {
                        IObjectWithState stateInfo = entry.Entity;
                        entry.State = ConvertState(stateInfo.EntityState);
                    }

                    SaveChanges();

                    result = true;
                }
                catch (OptimisticConcurrencyException)
                {
                    //To do
                }
                catch (Exception error)
                {
                    DbEntityEntry<R> entry = Model.Entry(root);
                    entry.State = EntityState.Unchanged;

                    RollBack();
                    Detach(root);

                    if (RethrowExceptions)
                    {
                        throw;
                    }
                    else
                    {
                        if (RepositoryBaseExceptionRaised != null)
                        {
                            RepositoryBaseExceptionRaised(error);
                        }
                    }
                }
                finally
                {
                }
            });

            return result;
        }

        public event RepositoryBaseExceptionHandler RepositoryBaseExceptionRaised;
        public event RepositoryBaseRollBackOccuredHandler RepositoryBaseRollBackRaised;

        internal void InitializeRepository()
        {
            if (Model == null)
            {
                var instance = (DbContext) Activator.CreateInstance(typeof (T));
                SetUnchangedEntity(instance);
                Model = instance;

                if (_lazyConnection == false)
                    InitializeConnection();

                Model.Configuration.ProxyCreationEnabled = ProxyCreationEnabled;
            }
            else
            {
                Model.Configuration.LazyLoadingEnabled = false;
            }
        }

        internal void InitializeConnection()
        {
            if (Model != null)
            {
                if (!string.IsNullOrEmpty(ConnectionString))
                {
                    Model.Database.Connection.ConnectionString = ConnectionString;
                }

                Connection = ((IObjectContextAdapter) Model).ObjectContext.Connection;
                Connection.Open();
            }
        }

        internal IQueryable<R> ApplyIncludesToQuery<R>(IQueryable<R> entities, Expression<Func<R, object>>[] includes)
            where R : class
        {
            if (includes != null)
                entities = includes.Aggregate(entities, (current, include) => current.Include(include));

            return entities;
        }

        internal void ProcessTransactionableMethod(Action action)
        {
            if (SaveLastExcecutedMethodInfo)
                LastExecutedMethod = MethodBase.GetCurrentMethod();

            StartTransaction();
            action();
        }

        internal IQueryable<R> SetEntities<R>() where R : class
        {
            DbSet<R> entities = Model.Set<R>();

            return entities;
        }

        internal DbSet<R> SetEntity<R>() where R : class
        {
            DbSet<R> entity = Model.Set<R>();

            return entity;
        }

        internal DbEntityEntry SetEntry<R>(R entity) where R : class
        {
            DbEntityEntry<R> entry = Model.Entry(entity);

            return entry;
        }

        internal IQueryable<T> GetQuery(Expression<Func<T, object>> include)
        {
            IQueryable<T> entities = SetEntities<T>()
                .Include(include);

            return entities;
        }

        internal void RollBack()
        {
            if (UseTransaction)
            {
                if (TransactionType == TransactionTypes.DbTransaction)
                {
                    if (Transaction != null && Transaction.Connection != null)
                    {
                        Transaction.Rollback();

                        if (RepositoryBaseRollBackRaised != null)
                        {
                            RepositoryBaseRollBackRaised(LastExecutedMethod);
                        }
                    }
                }
            }
        }

        internal void StartTransaction()
        {
            if (UseTransaction)
            {
                switch (TransactionType)
                {
                    case TransactionTypes.DbTransaction:
                        if (Transaction == null || Transaction.Connection == null)
                            Transaction = Connection.BeginTransaction(_isolationLevel);
                        break;

                    case TransactionTypes.TransactionScope:
                        TransactionScope = new TransactionScope();
                        break;
                }
            }
        }

        protected void OnRepositoryBaseExceptionRaised(Exception e)
        {
            RepositoryBaseExceptionHandler handler = RepositoryBaseExceptionRaised;
            if (handler != null)
            {
                handler(e);
            }
        }

        private void SetUnchangedEntity(DbContext instance)
        {
            ((IObjectContextAdapter) instance).ObjectContext
                .ObjectMaterialized += (sender, args) =>
                {
                    var entity = args.Entity as IObjectWithState;
                    if (entity != null)
                    {
                        entity.EntityState = State.Unchanged;
                    }
                };
        }
    }
}