﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using IntegraAccess.DataAccess.Context;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IntegraAccess.DataAccess.Repository
{
    public class AuthRepository : IAuthRepository
    {
        private DbContext _ctx;

        private UserManager<User> _userManager;

        public AuthRepository(IRepositoryBase repositoryBase)
        {
            _ctx = repositoryBase.GetDbContext();
            _userManager = new UserManager<User>(new UserStore<User>(_ctx));
        }


        public IQueryable<User> GetUsers()
        {
            return _userManager.Users;
        }

        public IQueryable<User> GetUsers(List<string> userIds)
        {

            return userIds.Any() ? GetUsers().Where(user => userIds.Contains(user.Id)) : null;
        }

        public void Inactivate(List<string> userIds)
        {
            if (!userIds.Any()) return;

            var users = GetUsers(userIds);
            foreach (var user in users)
            {
                user.IsActive = false;
                user.UpdatedAt = DateTime.UtcNow;
                //TODO: add updateBYID
            }
        }

        public void GrantAccess(string userId, Account account)
        {
            GrantAccess(userId, new List<Account>() { account });
        }

        public void GrantAccess(string userId, List<Account> accounts)
        {
            var user = FindUser(userId);
            if (user != null && user.UserAccounts != null && accounts != null && accounts.Any())
            {
                foreach (var account in accounts)
                {
                    var relationShip = user.UserAccounts.FirstOrDefault(x => x.AccountId == account.Id);
                    if (relationShip != null)
                    {
                        relationShip.IsActive = true;
                    }
                    else
                    {
                        user.UserAccounts.Add(new UserAccounts()
                        {
                            Account = account,
                            User = user,
                            IsActive = true,
                        });
                    }
                }
            }
        }

        public void DenyAccess(string userId, Account account)
        {
            DenyAccess(userId, new List<Account>() { account });
        }

        public void DenyAccess(string userId, List<Account> accounts)
        {
            var user = FindUser(userId);
            if (user != null && user.UserAccounts != null && accounts != null && accounts.Any())
            {
                foreach (var account in accounts)
                {
                    var relationShip = user.UserAccounts.FirstOrDefault(x => x.AccountId == account.Id);
                    if (relationShip != null)
                    {
                        relationShip.IsActive = false;
                    }
                }

            }
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }

        public async Task<IdentityResult> RegisterUser(User user, string password)
        {
            user.IsActive = true;
            user.UpdatedAt = DateTime.UtcNow;
            user.Id = Guid.NewGuid().ToString();
            var result = await _userManager.CreateAsync(user, password);
            await _userManager.AddToRoleAsync(user.Id, user.AccountType);

            return result;
        }

        public async Task<User> FindUser(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);

            if (user != null)
            {
                user.UserRoles = _userManager.GetRoles(user.Id);
            }

            return user;
        }

        public User FindUserByEmail(string email)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.Email == email);

            if (user != null)
            {
                user.UserRoles = _userManager.GetRoles(user.Id);
            }

            return user;
        }

        public User FindUserByUserName(string userName)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.UserName == userName);

            if (user != null)
            {
                user.UserRoles = _userManager.GetRoles(user.Id);
            }

            return user;
        }

        public User FindUser(string id)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.Id == id);

            if (user != null)
            {
                user.UserRoles = _userManager.GetRoles(user.Id);
            }

            return user;
        }


        public IdentityResult UpdateUser(User user)
        {
            user.UpdatedAt = DateTime.UtcNow;
            var result = _userManager.Update(user);
            if (!string.IsNullOrEmpty(user.AccountType) && _userManager.IsInRole(user.Id, user.AccountType))
            {
                _userManager.RemoveFromRoles(user.Id);
                _userManager.AddToRole(user.Id, user.AccountType);
            }
            return result;
        }

        public async Task<User> FindAsync(UserLoginInfo loginInfo)
        {
            var user = await _userManager.FindAsync(loginInfo);

            if (user != null)
            {
                user.UserRoles = _userManager.GetRoles(user.Id);
            }

            return user;
        }

        public IdentityResult AddToRole(string userId, string role)
        {
            return _userManager.AddToRole(userId, role);

        }

        public async Task<IdentityResult> CreateAsync(User user)
        {
            var result = await _userManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }

        public void Dispose()
        {
            _userManager.Dispose();
        }
    }
}