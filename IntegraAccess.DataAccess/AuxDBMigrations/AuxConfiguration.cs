using IntegraAccess.Entities.Entities;

namespace IntegraAccess.DataAccess.AuxDBMigrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class AuxConfiguration : DbMigrationsConfiguration<IntegraAccess.DataAccess.Context.AuxDBContext>
    {
        public AuxConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"AuxDBMigrations";
        }

        protected override void Seed(IntegraAccess.DataAccess.Context.AuxDBContext context)
        {
        }
    }
}
