namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveVersionColumnFromEntities : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.FacilityAccounts", "Version");
            DropColumn("dbo.ProductTemplates", "Version");
            DropColumn("dbo.UserAccounts", "Version");
            DropColumn("dbo.AccountGroups_Audit", "Version");
            DropColumn("dbo.Accounts_Audit", "Version");
            DropColumn("dbo.Companies_Audit", "Version");
            DropColumn("dbo.Facilities_Audit", "Version");
            DropColumn("dbo.FacilityAccounts_Audit", "Version");
            DropColumn("dbo.Orders_Audit", "Version");
            DropColumn("dbo.Patients_Audit", "Version");
            DropColumn("dbo.ProductCategories_Audit", "Version");
            DropColumn("dbo.ProductPriceSheetItems_Audit", "Version");
            DropColumn("dbo.ProductPriceSheets_Audit", "Version");
            DropColumn("dbo.Products_Audit", "Version");
            DropColumn("dbo.ProductTemplates_Audit", "Version");
            DropColumn("dbo.UserAccounts_Audit", "Version");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserAccounts_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTemplates_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Products_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheets_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheetItems_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductCategories_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Patients_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Orders_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.FacilityAccounts_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Facilities_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Companies_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Accounts_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.AccountGroups_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.UserAccounts", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTemplates", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.FacilityAccounts", "Version", c => c.Int(nullable: false));
        }
    }
}
