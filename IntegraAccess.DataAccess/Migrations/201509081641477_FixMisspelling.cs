namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixMisspelling : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "PatientZip", c => c.String());
            AddColumn("dbo.Orders", "PatientIsDeceased", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders_Audit", "PatientZip", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientIsDeceased", c => c.Boolean(nullable: false));
            DropColumn("dbo.Orders", "PatientCityZip");
            DropColumn("dbo.Orders", "PatientIsDeaceased");
            DropColumn("dbo.Orders_Audit", "PatientCityZip");
            DropColumn("dbo.Orders_Audit", "PatientIsDeaceased");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders_Audit", "PatientIsDeaceased", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders_Audit", "PatientCityZip", c => c.String());
            AddColumn("dbo.Orders", "PatientIsDeaceased", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "PatientCityZip", c => c.String());
            DropColumn("dbo.Orders_Audit", "PatientIsDeceased");
            DropColumn("dbo.Orders_Audit", "PatientZip");
            DropColumn("dbo.Orders", "PatientIsDeceased");
            DropColumn("dbo.Orders", "PatientZip");
        }
    }
}
