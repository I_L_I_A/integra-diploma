namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationToLogs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs", "Location", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs", "Location");
        }
    }
}
