namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductCategoriesLoad : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                     UPDATE ProductCategories SET IsActive = 0 WHERE Name='Oxygen';
                     UPDATE ProductCategories SET IsActive = 0 WHERE Name='Sleep Therapy';
                     UPDATE ProductCategories SET Name='Respiratory Servies' WHERE Name='RTS';
                     
                     IF NOT EXISTS (SELECT TOP 1 1 FROM ProductCategories WHERE Name = 'Respiratory Products')
                     BEGIN
                        INSERT INTO ProductCategories
                        (Name, IsActive, UpdatedDateTime, UpdatedById) VALUES
                        (N'Support Surfaces', '1', GETDATE(), 'DATALOAD'), 
                        (N'Ambulatory Aides', '1', GETDATE(), 'DATALOAD'),
                        (N'Pediatric', '1', GETDATE(), 'DATALOAD')
                     END
                " );
        }
        
        public override void Down()
        {
        }
    }
}
