namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreatedByIdToOrderTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CreatedById", c => c.String());
            AddColumn("dbo.Orders_Audit", "CreatedById", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders_Audit", "CreatedById");
            DropColumn("dbo.Orders", "CreatedById");
        }
    }
}
