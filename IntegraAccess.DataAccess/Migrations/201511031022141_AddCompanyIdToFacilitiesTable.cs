namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyIdToFacilitiesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Facilities", "CompanyId", c => c.Int());
            AddColumn("dbo.Facilities_Audit", "CompanyId", c => c.Int());
            CreateIndex("dbo.Facilities", "CompanyId");
            AddForeignKey("dbo.Facilities", "CompanyId", "dbo.Companies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Facilities", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Facilities", new[] { "CompanyId" });
            DropColumn("dbo.Facilities_Audit", "CompanyId");
            DropColumn("dbo.Facilities", "CompanyId");
        }
    }
}
