namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPatientEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        SSN = c.String(),
                        Gender = c.String(),
                        BirthDate = c.DateTime(),
                        Street1 = c.String(),
                        Street2 = c.String(),
                        City = c.String(),
                        CityState = c.String(),
                        Zip = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        IsDeceased = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        DeceasedDate = c.DateTime(),
                        LastAccountId = c.Int(),
                        EHRId = c.Int(),
                        AccountGroupId = c.Int(),
                        AccountId = c.Int(),
                        FacilityId = c.Int(),
                        ExternalBillingCode = c.String(),
                        ExternalEMRCode = c.String(),
                        PhysicianFirstName = c.String(),
                        PhysicianLastName = c.String(),
                        PhysicianPhone = c.String(),
                        PhysicianEmail = c.String(),
                        ContactName = c.String(),
                        ContactEmail = c.String(),
                        ContactPhone = c.String(),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .ForeignKey("dbo.AccountGroups", t => t.AccountGroupId)
                .ForeignKey("dbo.Accounts", t => t.LastAccountId)
                .Index(t => t.LastAccountId)
                .Index(t => t.AccountGroupId)
                .Index(t => t.AccountId);
            
            DropTable("dbo.TestEntities");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TestEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Age = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Patients", "LastAccountId", "dbo.Accounts");
            DropForeignKey("dbo.Patients", "AccountGroupId", "dbo.AccountGroups");
            DropForeignKey("dbo.Patients", "AccountId", "dbo.Accounts");
            DropIndex("dbo.Patients", new[] { "AccountId" });
            DropIndex("dbo.Patients", new[] { "AccountGroupId" });
            DropIndex("dbo.Patients", new[] { "LastAccountId" });
            DropTable("dbo.Patients");
        }
    }
}
