namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCreatedByAndSubmittedByToOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "SubmittedById", c => c.String(maxLength: 128));
            AddColumn("dbo.Orders", "SubmittedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orders_Audit", "SubmittedById", c => c.String());
            AddColumn("dbo.Orders_Audit", "SubmittedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Orders", "CreatedById", c => c.String(maxLength: 128));
            CreateIndex("dbo.Orders", "CreatedById");
            CreateIndex("dbo.Orders", "SubmittedById");
            AddForeignKey("dbo.Orders", "CreatedById", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Orders", "SubmittedById", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "SubmittedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Orders", "CreatedById", "dbo.AspNetUsers");
            DropIndex("dbo.Orders", new[] { "SubmittedById" });
            DropIndex("dbo.Orders", new[] { "CreatedById" });
            AlterColumn("dbo.Orders", "CreatedById", c => c.String());
            DropColumn("dbo.Orders_Audit", "SubmittedAt");
            DropColumn("dbo.Orders_Audit", "SubmittedById");
            DropColumn("dbo.Orders", "SubmittedAt");
            DropColumn("dbo.Orders", "SubmittedById");
        }
    }
}
