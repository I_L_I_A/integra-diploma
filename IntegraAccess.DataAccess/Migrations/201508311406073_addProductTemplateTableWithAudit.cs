namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addProductTemplateTableWithAudit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductTemplates",
                c => new
                    {
                        CompanyId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        TemplateName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.ProductId })
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.CompanyId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductTemplates_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        TemplateName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            AddColumn("dbo.FacilityAccounts", "ModifiedById", c => c.String());
            AddColumn("dbo.FacilityAccounts", "CreatedById", c => c.String());
            AddColumn("dbo.UserAccounts", "ModifiedById", c => c.String());
            AddColumn("dbo.UserAccounts", "CreatedById", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductTemplates", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductTemplates", "CompanyId", "dbo.Companies");
            DropIndex("dbo.ProductTemplates", new[] { "ProductId" });
            DropIndex("dbo.ProductTemplates", new[] { "CompanyId" });
            DropColumn("dbo.UserAccounts", "CreatedById");
            DropColumn("dbo.UserAccounts", "ModifiedById");
            DropColumn("dbo.FacilityAccounts", "CreatedById");
            DropColumn("dbo.FacilityAccounts", "ModifiedById");
            DropTable("dbo.ProductTemplates_Audit");
            DropTable("dbo.ProductTemplates");
        }
    }
}
