namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixProductTemplateKeys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductTemplateItems", "ChildProductId", "dbo.Products");
            DropForeignKey("dbo.ProductTemplateItems", "CompanyId", "dbo.Companies");
            AddForeignKey("dbo.ProductTemplateItems", "ChildProductId", "dbo.Products", "Id");
            AddForeignKey("dbo.ProductTemplateItems", "CompanyId", "dbo.Companies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductTemplateItems", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ProductTemplateItems", "ChildProductId", "dbo.Products");
            AddForeignKey("dbo.ProductTemplateItems", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProductTemplateItems", "ChildProductId", "dbo.Products", "Id", cascadeDelete: true);
        }
    }
}
