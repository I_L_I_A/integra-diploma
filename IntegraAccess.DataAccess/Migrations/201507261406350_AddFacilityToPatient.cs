namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFacilityToPatient : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Patients", "FacilityId");
            AddForeignKey("dbo.Patients", "FacilityId", "dbo.Facilities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Patients", "FacilityId", "dbo.Facilities");
            DropIndex("dbo.Patients", new[] { "FacilityId" });
        }
    }
}
