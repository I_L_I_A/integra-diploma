namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModifiedByAndCreatedByFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountGroups", "ModifiedById", c => c.String());
            AddColumn("dbo.AccountGroups", "CreatedById", c => c.String());
            AddColumn("dbo.Accounts", "ModifiedById", c => c.String());
            AddColumn("dbo.Accounts", "CreatedById", c => c.String());
            AddColumn("dbo.Companies", "ModifiedById", c => c.String());
            AddColumn("dbo.Companies", "CreatedById", c => c.String());
            AddColumn("dbo.Facilities", "ModifiedById", c => c.String());
            AddColumn("dbo.Facilities", "CreatedById", c => c.String());
            AddColumn("dbo.Patients", "ModifiedById", c => c.String());
            AddColumn("dbo.Patients", "CreatedById", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Patients", "CreatedById");
            DropColumn("dbo.Patients", "ModifiedById");
            DropColumn("dbo.Facilities", "CreatedById");
            DropColumn("dbo.Facilities", "ModifiedById");
            DropColumn("dbo.Companies", "CreatedById");
            DropColumn("dbo.Companies", "ModifiedById");
            DropColumn("dbo.Accounts", "CreatedById");
            DropColumn("dbo.Accounts", "ModifiedById");
            DropColumn("dbo.AccountGroups", "CreatedById");
            DropColumn("dbo.AccountGroups", "ModifiedById");
        }
    }
}
