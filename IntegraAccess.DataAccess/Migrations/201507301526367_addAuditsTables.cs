namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAuditsTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountGroups_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ExternalCode = c.String(),
                        Name = c.String(),
                        Street1 = c.String(),
                        Street2 = c.String(),
                        City = c.String(),
                        CityState = c.String(),
                        Zip = c.String(),
                        ContactName = c.String(),
                        ContactPhone = c.String(),
                        ContactEmail = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.Accounts_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ExternalId = c.String(),
                        AccountGroupId = c.Int(),
                        ExternalCompanyId = c.Int(),
                        Name = c.String(),
                        Street1 = c.String(),
                        Street2 = c.String(),
                        City = c.String(),
                        CityState = c.String(),
                        Zip = c.String(),
                        ContactName = c.String(),
                        ContactPhone = c.String(),
                        ContactEmail = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.Companies_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ExternalCode = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.Facilities_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        AccountGroupId = c.Int(),
                        Name = c.String(),
                        ExternalCode = c.String(),
                        Street1 = c.String(),
                        Street2 = c.String(),
                        City = c.String(),
                        CityState = c.String(),
                        Zip = c.String(),
                        ContactName = c.String(),
                        ContactPhone = c.String(),
                        ContactEmail = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.FacilityAccounts_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        FacilityId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.Patients_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        SSN = c.String(),
                        Gender = c.String(),
                        BirthDate = c.DateTime(),
                        Street1 = c.String(),
                        Street2 = c.String(),
                        City = c.String(),
                        CityState = c.String(),
                        Zip = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        IsDeceased = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        DeceasedDate = c.DateTime(),
                        LastAccountId = c.Int(),
                        EHRId = c.Int(),
                        AccountGroupId = c.Int(),
                        AccountId = c.Int(),
                        FacilityId = c.Int(),
                        ExternalBillingCode = c.String(),
                        ExternalEMRCode = c.String(),
                        PhysicianFirstName = c.String(),
                        PhysicianLastName = c.String(),
                        PhysicianPhone = c.String(),
                        PhysicianEmail = c.String(),
                        ContactName = c.String(),
                        ContactEmail = c.String(),
                        ContactPhone = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.UserAccounts_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        AccountId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserAccounts_Audit");
            DropTable("dbo.Patients_Audit");
            DropTable("dbo.FacilityAccounts_Audit");
            DropTable("dbo.Facilities_Audit");
            DropTable("dbo.Companies_Audit");
            DropTable("dbo.Accounts_Audit");
            DropTable("dbo.AccountGroups_Audit");
        }
    }
}
