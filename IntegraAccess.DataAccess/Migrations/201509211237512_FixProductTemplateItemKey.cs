namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixProductTemplateItemKey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ProductTemplateItems");
            AddPrimaryKey("dbo.ProductTemplateItems", new[] { "CompanyId", "ProductId", "ChildProductId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ProductTemplateItems");
            AddPrimaryKey("dbo.ProductTemplateItems", new[] { "CompanyId", "ProductId" });
        }
    }
}
