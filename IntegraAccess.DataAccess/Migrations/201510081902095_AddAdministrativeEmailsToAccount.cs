namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdministrativeEmailsToAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "AdministrativeEmails", c => c.String());
            AddColumn("dbo.Accounts_Audit", "AdministrativeEmails", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accounts", "AdministrativeEmails");
            DropColumn("dbo.Accounts_Audit", "AdministrativeEmails");
        }
    }
}
