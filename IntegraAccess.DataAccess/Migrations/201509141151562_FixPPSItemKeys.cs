namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixPPSItemKeys : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductPriceSheetItems_Audit", "ProductPriceSheetId", c => c.Int(nullable: false));
            AlterColumn("dbo.ProductPriceSheetItems_Audit", "ProductId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductPriceSheetItems_Audit", "ProductId", c => c.Int());
            AlterColumn("dbo.ProductPriceSheetItems_Audit", "ProductPriceSheetId", c => c.Int());
        }
    }
}
