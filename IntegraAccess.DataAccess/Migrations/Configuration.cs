using System.Collections.Generic;
using IntegraAccess.DataAccess.Context;
using IntegraAccess.Entities.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IntegraAccess.DataAccess.Context.DataBaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataBaseContext context)
        {
            CreateDefaultUsersAndRoles(context);

            CreateDictionaries(context);

            RebuildTriggers(context);
        }

        private void CreateDefaultUsersAndRoles(DataBaseContext context)
        {
            #region Add roles

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userRoles = new List<string> { "Supplier", "Account" };

            foreach (string role in userRoles)
            {
                if (!roleManager.RoleExists(role))
                {
                    IdentityResult result = roleManager.Create(new IdentityRole(role));
                    if (!result.Succeeded)
                    {
                        throw new ApplicationException("Failed to create default role.");
                    }
                }
            }

            #endregion

            #region Add users

            var userManager = new UserManager<User>(new UserStore<User>(context));
            string seedUserName = "integra";
            string seedPassword = "1234567";
            string seedUserRole = "Supplier";
            string seedUserAccountType = "Supplier";

            User user = userManager.FindByName(seedUserName);

            if (user == null)
            {
                user = new User
                {
                    UserName = seedUserName,
                    UpdatedAt = DateTime.Now,
                    AccountType = seedUserAccountType,
                    IsActive = true
                };
                IdentityResult result = userManager.Create(user, seedPassword);

                if (!result.Succeeded)
                {
                    throw new ApplicationException("Failed to create default user.");
                }
            }

            if (!userManager.IsInRole(user.Id, seedUserRole))
            {
                IdentityResult result = userManager.AddToRole(user.Id, seedUserRole);
            }

            #endregion
        }

        private void CreateDictionaries(DataBaseContext context)
        {
            context.OrderTypes.AddOrUpdate(new[]
            {
                new OrderType {Id = 1, Name = "Delivery"},
                new OrderType {Id = 2, Name = "Pickup"},
                new OrderType {Id = 3, Name = "Maintenance"}
            });

            context.OrderStates.AddOrUpdate(new[]
            {
                new OrderState {Id = 1, Name = "Saved"},
                new OrderState {Id = 2, Name = "Approval Required"},
                new OrderState {Id = 3, Name = "Account Rejected"},
                new OrderState {Id = 4, Name = "Cancelled"},
                new OrderState {Id = 5, Name = "DME Approval"},
                new OrderState {Id = 6, Name = "DME Accepted"},
                new OrderState {Id = 7, Name = "DME Hold"},
                new OrderState {Id = 8, Name = "Completed"},
                new OrderState {Id = 9, Name = "DME Cancelled"}
            });

            context.PurchaseTypes.AddOrUpdate(new[]
            {
                new PurchaseType {Id = 1, Name = "Daily"},
                new PurchaseType {Id = 2, Name = "Monthly"},
                new PurchaseType {Id = 3, Name = "Purchase"}
            });
        }

        private void RebuildTriggers(DataBaseContext context)
        {
            context.Database.ExecuteSqlCommand(@"IF OBJECT_ID('GenerateTriggers','P') IS NOT NULL DROP PROC GenerateTriggers;");
            context.Database.ExecuteSqlCommand(createSP);
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='UserAccounts', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='FacilityAccounts', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='AccountGroups', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='Accounts', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='Patients', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='Facilities', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='Companies', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='Products', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='ProductCategories', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='ProductPriceSheets', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='ProductPriceSheetItems', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='Orders', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='OrderLineItems', @GenerateScriptOnly=0");
            context.Database.ExecuteSqlCommand("exec GenerateTriggers @Tablename='ProductsToCategories', @GenerateScriptOnly=0");
        }

        private string createSP = @"
CREATE PROC GenerateTriggers  
 @Schemaname Sysname = 'dbo'  
,@Tablename  Sysname  
,@GenerateScriptOnly    bit = 1  
AS  
  
SET NOCOUNT ON  
  
/*  
Parameters  
@Schemaname            - SchemaName to which the table belongs to. Default value 'dbo'.  
@Tablename            - TableName for which the procs needs to be generated.  
@GenerateScriptOnly - When passed 1 , this will generate the scripts alone..  
                      When passed 0 , this will create the audit tables and triggers in the current database.  
                      Default value is 1  
*/  
  
DECLARE @SQL VARCHAR(MAX)  
DECLARE @SQLTrigger VARCHAR(MAX)  
DECLARE @AuditTableName SYSNAME  
  
SELECT @AuditTableName =  @Tablename + '_Audit'  

DECLARE @ColumnNames varchar(MAX)
SELECT 
    @ColumnNames = COALESCE(@ColumnNames + ', ', '') + c.name
FROM    
    sys.columns c
WHERE
    c.object_id = OBJECT_ID(@Tablename);
----------------------------------------------------------------------------------------------------------------------  
-- Create Insert Trigger  
----------------------------------------------------------------------------------------------------------------------  
  
  
SELECT @SQL = '  
IF EXISTS (SELECT 1   
             FROM sys.objects   
            WHERE Name=''' + @Tablename + '_Insert' + '''  
              AND Schema_id=Schema_id(''' + @Schemaname + ''')  
              AND Type = ''TR'')  
DROP TRIGGER ' + @Tablename + '_Insert  
'  
SELECT @SQLTrigger = '  
CREATE TRIGGER ' + @Tablename + '_Insert  
ON '+ @Schemaname + '.' + @Tablename + '  
FOR INSERT  
AS  
  
 INSERT INTO ' + @Schemaname + '.' + @AuditTableName + ' ('+@ColumnNames+ ',AuditDataState,AuditDMLAction,AuditDateTime)
 SELECT '+@ColumnNames+',''New'',''Insert'',GETDATE() FROM INSERTED   
  
'  
  
IF @GenerateScriptOnly = 1  
BEGIN  
    PRINT REPLICATE ('-',200)  
    PRINT '--Create Script Insert Trigger for ' + @Schemaname + '.' + @Tablename  
    PRINT REPLICATE ('-',200)  
    PRINT @SQL  
    PRINT 'GO'  
    PRINT @SQLTrigger  
    PRINT 'GO'  
END  
ELSE  
BEGIN  
    PRINT 'Creating Insert Trigger ' + @Tablename + '_Insert  for ' + @Schemaname + '.' + @Tablename  
    EXEC(@SQL)  
    EXEC(@SQLTrigger)  
    PRINT 'Trigger ' + @Schemaname + '.' + @Tablename + '_Insert  Created succefully'  
END  
  
  
----------------------------------------------------------------------------------------------------------------------  
-- Create Delete Trigger  
----------------------------------------------------------------------------------------------------------------------  
  
  
SELECT @SQL = '  
  
IF EXISTS (SELECT 1   
             FROM sys.objects   
            WHERE Name=''' + @Tablename + '_Delete' + '''  
              AND Schema_id=Schema_id(''' + @Schemaname + ''')  
              AND Type = ''TR'')  
DROP TRIGGER ' + @Tablename + '_Delete  
'  
  
SELECT @SQLTrigger =   
'  
CREATE TRIGGER ' + @Tablename + '_Delete  
ON '+ @Schemaname + '.' + @Tablename + '  
FOR DELETE  
AS  

 INSERT INTO ' + @Schemaname + '.' + @AuditTableName + ' ('+@ColumnNames+ ',AuditDataState,AuditDMLAction,AuditDateTime)
 SELECT '+@ColumnNames+',''Old'',''Delete'',GETDATE() FROM DELETED   
'  
  
IF @GenerateScriptOnly = 1  
BEGIN  
    PRINT REPLICATE ('-',200)  
    PRINT '--Create Script Delete Trigger for ' + @Schemaname + '.' + @Tablename  
    PRINT REPLICATE ('-',200)  
    PRINT @SQL  
    PRINT 'GO'  
    PRINT @SQLTrigger  
    PRINT 'GO'  
END  
ELSE  
BEGIN  
    PRINT 'Creating Delete Trigger ' + @Tablename + '_Delete  for ' + @Schemaname + '.' + @Tablename  
    EXEC(@SQL)  
    EXEC(@SQLTrigger)  
    PRINT 'Trigger ' + @Schemaname + '.' + @Tablename + '_Delete  Created succefully'  
END  
  
----------------------------------------------------------------------------------------------------------------------  
-- Create Update Trigger  
----------------------------------------------------------------------------------------------------------------------  
  
  
SELECT @SQL = '  
  
IF EXISTS (SELECT 1   
             FROM sys.objects   
            WHERE Name=''' + @Tablename + '_Update' + '''  
              AND Schema_id=Schema_id(''' + @Schemaname + ''')  
              AND Type = ''TR'')  
DROP TRIGGER ' + @Tablename + '_Update  
'  
  
SELECT @SQLTrigger =  
'  
CREATE TRIGGER ' + @Tablename + '_Update  
ON '+ @Schemaname + '.' + @Tablename + '  
FOR UPDATE  
AS  

 INSERT INTO ' + @Schemaname + '.' + @AuditTableName + ' ('+@ColumnNames+ ',AuditDataState,AuditDMLAction,AuditDateTime)
 SELECT '+@ColumnNames+',''New'',''Update'',GETDATE() FROM INSERTED   
'  
IF @GenerateScriptOnly = 1  
BEGIN  
    PRINT REPLICATE ('-',200)  
    PRINT '--Create Script Update Trigger for ' + @Schemaname + '.' + @Tablename  
    PRINT REPLICATE ('-',200)  
    PRINT @SQL  
    PRINT 'GO'  
    PRINT @SQLTrigger  
    PRINT 'GO'  
END  
ELSE  
BEGIN  
    PRINT 'Creating Delete Trigger ' + @Tablename + '_Update  for ' + @Schemaname + '.' + @Tablename  
    EXEC(@SQL)  
    EXEC(@SQLTrigger)  
    PRINT 'Trigger ' + @Schemaname + '.' + @Tablename + '_Update  Created succefully'  
END  

GRANT EXEC ON GenerateTriggers TO PUBLIC  
SET NOCOUNT OFF
";
    }
}
