namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixProductTemplateItem : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ProductTemplateItems");
            AddColumn("dbo.ProductTemplateItems", "CompanyId", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTemplateItems_Audit", "CompanyId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.ProductTemplateItems", new[] { "CompanyId", "ProductId" });
            CreateIndex("dbo.ProductTemplateItems", "CompanyId");
            AddForeignKey("dbo.ProductTemplateItems", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
            DropColumn("dbo.ProductTemplateItems", "Id");
            DropColumn("dbo.ProductTemplateItems_Audit", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductTemplateItems_Audit", "Id", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTemplateItems", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.ProductTemplateItems", "CompanyId", "dbo.Companies");
            DropIndex("dbo.ProductTemplateItems", new[] { "CompanyId" });
            DropPrimaryKey("dbo.ProductTemplateItems");
            DropColumn("dbo.ProductTemplateItems_Audit", "CompanyId");
            DropColumn("dbo.ProductTemplateItems", "CompanyId");
            AddPrimaryKey("dbo.ProductTemplateItems", "Id");
        }
    }
}
