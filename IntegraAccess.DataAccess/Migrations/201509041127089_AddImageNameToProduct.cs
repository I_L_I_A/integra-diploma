namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageNameToProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ImageName", c => c.String());
            AddColumn("dbo.Products_Audit", "ImageName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products_Audit", "ImageName");
            DropColumn("dbo.Products", "ImageName");
        }
    }
}
