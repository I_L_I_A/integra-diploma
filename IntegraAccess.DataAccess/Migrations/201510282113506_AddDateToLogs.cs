namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateToLogs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs", "Date");
        }
    }
}
