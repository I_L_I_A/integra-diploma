namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompaniesToAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "ExternalCompanyId", c => c.Int());
            CreateIndex("dbo.Accounts", "ExternalCompanyId");
            AddForeignKey("dbo.Accounts", "ExternalCompanyId", "dbo.Companies", "Id");
            DropColumn("dbo.Accounts", "ExternalCompanyCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accounts", "ExternalCompanyCode", c => c.String());
            DropForeignKey("dbo.Accounts", "ExternalCompanyId", "dbo.Companies");
            DropIndex("dbo.Accounts", new[] { "ExternalCompanyId" });
            DropColumn("dbo.Accounts", "ExternalCompanyId");
        }
    }
}
