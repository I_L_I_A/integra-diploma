namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPatientFacilityForeighKeyToOrder : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Orders", "PatientFacilityId");
            AddForeignKey("dbo.Orders", "PatientFacilityId", "dbo.Facilities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "PatientFacilityId", "dbo.Facilities");
            DropIndex("dbo.Orders", new[] { "PatientFacilityId" });
        }
    }
}
