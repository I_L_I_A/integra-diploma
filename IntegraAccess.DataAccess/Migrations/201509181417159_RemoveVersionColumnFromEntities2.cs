namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveVersionColumnFromEntities2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AccountGroups", "Version");
            DropColumn("dbo.Accounts", "Version");
            DropColumn("dbo.Companies", "Version");
            DropColumn("dbo.ProductPriceSheets", "Version");
            DropColumn("dbo.ProductPriceSheetItems", "Version");
            DropColumn("dbo.Products", "Version");
            DropColumn("dbo.Orders", "Version");
            DropColumn("dbo.OrderStates", "Version");
            DropColumn("dbo.OrderTypes", "Version");
            DropColumn("dbo.Facilities", "Version");
            DropColumn("dbo.PurchaseTypes", "Version");
            DropColumn("dbo.ProductCategories", "Version");
            DropColumn("dbo.Patients", "Version");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductCategories", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.PurchaseTypes", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Facilities", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.OrderTypes", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.OrderStates", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Orders", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Products", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheetItems", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheets", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Companies", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.Accounts", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.AccountGroups", "Version", c => c.Int(nullable: false));
        }
    }
}
