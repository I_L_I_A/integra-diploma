namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConnectFacilityAndUsersToAccounts : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FacilityAccount", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.FacilityAccount", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.UserAccount", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserAccount", "AccountId", "dbo.Accounts");
            DropIndex("dbo.FacilityAccount", new[] { "FacilityId" });
            DropIndex("dbo.FacilityAccount", new[] { "AccountId" });
            DropIndex("dbo.UserAccount", new[] { "UserId" });
            DropIndex("dbo.UserAccount", new[] { "AccountId" });
            CreateTable(
                "dbo.FacilityAccounts",
                c => new
                    {
                        FacilityId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.FacilityId, t.AccountId })
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.Facilities", t => t.FacilityId, cascadeDelete: true)
                .Index(t => t.FacilityId)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.UserAccounts",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        AccountId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.AccountId })
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.AccountId);
            
            DropTable("dbo.FacilityAccount");
            DropTable("dbo.UserAccount");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserAccount",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.AccountId });
            
            CreateTable(
                "dbo.FacilityAccount",
                c => new
                    {
                        FacilityId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FacilityId, t.AccountId });
            
            DropForeignKey("dbo.UserAccounts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.FacilityAccounts", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.FacilityAccounts", "AccountId", "dbo.Accounts");
            DropIndex("dbo.UserAccounts", new[] { "AccountId" });
            DropIndex("dbo.UserAccounts", new[] { "UserId" });
            DropIndex("dbo.FacilityAccounts", new[] { "AccountId" });
            DropIndex("dbo.FacilityAccounts", new[] { "FacilityId" });
            DropTable("dbo.UserAccounts");
            DropTable("dbo.FacilityAccounts");
            CreateIndex("dbo.UserAccount", "AccountId");
            CreateIndex("dbo.UserAccount", "UserId");
            CreateIndex("dbo.FacilityAccount", "AccountId");
            CreateIndex("dbo.FacilityAccount", "FacilityId");
            AddForeignKey("dbo.UserAccount", "AccountId", "dbo.Accounts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UserAccount", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FacilityAccount", "AccountId", "dbo.Accounts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.FacilityAccount", "FacilityId", "dbo.Facilities", "Id", cascadeDelete: true);
        }
    }
}
