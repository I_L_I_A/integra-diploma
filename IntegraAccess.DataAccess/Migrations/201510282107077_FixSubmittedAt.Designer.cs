// <auto-generated />
namespace IntegraAccess.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class FixSubmittedAt : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FixSubmittedAt));
        
        string IMigrationMetadata.Id
        {
            get { return "201510282107077_FixSubmittedAt"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
