namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameExternalCompanyIdForAccounts : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Accounts", name: "ExternalCompanyId", newName: "CompanyId");
            RenameIndex(table: "dbo.Accounts", name: "IX_ExternalCompanyId", newName: "IX_CompanyId");
            AddColumn("dbo.Accounts_Audit", "CompanyId", c => c.Int());
            DropColumn("dbo.Accounts_Audit", "ExternalCompanyId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accounts_Audit", "ExternalCompanyId", c => c.Int());
            DropColumn("dbo.Accounts_Audit", "CompanyId");
            RenameIndex(table: "dbo.Accounts", name: "IX_CompanyId", newName: "IX_ExternalCompanyId");
            RenameColumn(table: "dbo.Accounts", name: "CompanyId", newName: "ExternalCompanyId");
        }
    }
}
