namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixSubmittedAt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "SubmittedAt", c => c.DateTime());
            AlterColumn("dbo.Orders_Audit", "SubmittedAt", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders_Audit", "SubmittedAt", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Orders", "SubmittedAt", c => c.DateTime(nullable: false));
        }
    }
}
