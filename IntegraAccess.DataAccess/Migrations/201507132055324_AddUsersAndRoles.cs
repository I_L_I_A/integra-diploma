namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsersAndRoles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Login", c => c.String());
            AddColumn("dbo.AspNetUsers", "Title", c => c.String());
            AddColumn("dbo.AspNetUsers", "IsAccountGroupAdmin", c => c.Boolean());
            AddColumn("dbo.AspNetUsers", "IsOrderApprovalAdmin", c => c.Boolean());
            AddColumn("dbo.AspNetUsers", "IsOrderApprovalOverride", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsOrderApprovalOverride");
            DropColumn("dbo.AspNetUsers", "IsOrderApprovalAdmin");
            DropColumn("dbo.AspNetUsers", "IsAccountGroupAdmin");
            DropColumn("dbo.AspNetUsers", "Title");
            DropColumn("dbo.AspNetUsers", "Login");
        }
    }
}
