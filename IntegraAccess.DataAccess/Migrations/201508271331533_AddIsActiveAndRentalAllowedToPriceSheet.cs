namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsActiveAndRentalAllowedToPriceSheet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductPriceSheets", "IsRentalAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheets", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductPriceSheets", "IsActive");
            DropColumn("dbo.ProductPriceSheets", "IsRentalAllowed");
        }
    }
}
