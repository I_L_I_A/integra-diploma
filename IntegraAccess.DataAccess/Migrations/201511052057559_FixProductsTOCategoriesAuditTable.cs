namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixProductsTOCategoriesAuditTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductsToCategories_Audit", "CategoryId", c => c.Int(nullable: false));
            AlterColumn("dbo.ProductsToCategories_Audit", "ProductId", c => c.Int(nullable: false));
            DropColumn("dbo.ProductsToCategories_Audit", "CategorytId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductsToCategories_Audit", "CategorytId", c => c.Int(nullable: false));
            AlterColumn("dbo.ProductsToCategories_Audit", "ProductId", c => c.String());
            DropColumn("dbo.ProductsToCategories_Audit", "CategoryId");
        }
    }
}
