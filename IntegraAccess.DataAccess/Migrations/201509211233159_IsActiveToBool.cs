namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsActiveToBool : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductTemplateItems", "IsActive", c => c.Boolean(nullable: false));
            AlterColumn("dbo.ProductTemplateItems_Audit", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductTemplateItems_Audit", "IsActive", c => c.Int(nullable: false));
            AlterColumn("dbo.ProductTemplateItems", "IsActive", c => c.Int(nullable: false));
        }
    }
}
