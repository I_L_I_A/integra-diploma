namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRedundantFieldsFromPatientEntity : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Patients", "UpdatedAt");
            DropColumn("dbo.Patients", "UpdatedById");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients", "UpdatedById", c => c.String());
            AddColumn("dbo.Patients", "UpdatedAt", c => c.DateTime(nullable: false));
        }
    }
}
