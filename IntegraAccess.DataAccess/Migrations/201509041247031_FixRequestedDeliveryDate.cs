namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixRequestedDeliveryDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "RequestedDeliveryDate", c => c.DateTime());
            AddColumn("dbo.Orders_Audit", "RequestedDeliveryDate", c => c.DateTime());
            DropColumn("dbo.Orders", "RequestedDevliveryDate");
            DropColumn("dbo.Orders_Audit", "RequestedDevliveryDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders_Audit", "RequestedDevliveryDate", c => c.DateTime());
            AddColumn("dbo.Orders", "RequestedDevliveryDate", c => c.DateTime());
            DropColumn("dbo.Orders_Audit", "RequestedDeliveryDate");
            DropColumn("dbo.Orders", "RequestedDeliveryDate");
        }
    }
}
