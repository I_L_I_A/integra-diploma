namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsActiveToProductCategoriesRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductCategoryProducts", "ProductCategory_Id", "dbo.ProductCategories");
            DropForeignKey("dbo.ProductCategoryProducts", "Product_Id", "dbo.Products");
            DropIndex("dbo.ProductCategoryProducts", new[] { "ProductCategory_Id" });
            DropIndex("dbo.ProductCategoryProducts", new[] { "Product_Id" });
            CreateTable(
                "dbo.ProductsToCategories",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        UpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                    })
                .PrimaryKey(t => new { t.ProductId, t.CategoryId })
                .ForeignKey("dbo.ProductCategories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.ProductsToCategories_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        ProductId = c.String(),
                        CategorytId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        UpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            DropTable("dbo.ProductCategoryProducts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductCategoryProducts",
                c => new
                    {
                        ProductCategory_Id = c.Int(nullable: false),
                        Product_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductCategory_Id, t.Product_Id });
            
            DropForeignKey("dbo.ProductsToCategories", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductsToCategories", "CategoryId", "dbo.ProductCategories");
            DropIndex("dbo.ProductsToCategories", new[] { "CategoryId" });
            DropIndex("dbo.ProductsToCategories", new[] { "ProductId" });
            DropTable("dbo.ProductsToCategories_Audit");
            DropTable("dbo.ProductsToCategories");
            CreateIndex("dbo.ProductCategoryProducts", "Product_Id");
            CreateIndex("dbo.ProductCategoryProducts", "ProductCategory_Id");
            AddForeignKey("dbo.ProductCategoryProducts", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProductCategoryProducts", "ProductCategory_Id", "dbo.ProductCategories", "Id", cascadeDelete: true);
        }
    }
}
