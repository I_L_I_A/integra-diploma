namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddManyToManyUsersAccounts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserAccount",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        AccountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.AccountId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.AccountId);
            
            AddColumn("dbo.AspNetUsers", "AccountLId", c => c.Int());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserAccount", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.UserAccount", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserAccount", new[] { "AccountId" });
            DropIndex("dbo.UserAccount", new[] { "UserId" });
            DropColumn("dbo.AspNetUsers", "AccountLId");
            DropTable("dbo.UserAccount");
        }
    }
}
