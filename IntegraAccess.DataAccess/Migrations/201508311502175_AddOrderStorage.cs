namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderStorage : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ProductPriceSheets", "ProductId", "dbo.Products");
            DropIndex("dbo.ProductPriceSheets", new[] { "ProductId" });
            DropIndex("dbo.ProductPriceSheets", new[] { "CompanyId" });
            DropPrimaryKey("dbo.ProductPriceSheets");
            CreateTable(
                "dbo.OrderLineItems",
                c => new
                    {
                        OrderId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        PurchaseTypeId = c.Int(),
                        ItemNumber = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comments = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                    })
                .PrimaryKey(t => new { t.OrderId, t.ProductId })
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.PurchaseTypes", t => t.PurchaseTypeId)
                .Index(t => t.OrderId)
                .Index(t => t.ProductId)
                .Index(t => t.PurchaseTypeId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExternalOrderNumber = c.String(),
                        PatientNumber = c.String(),
                        CarrierNumber = c.String(),
                        CompanyNumber = c.String(),
                        PONumber = c.String(),
                        EnteredByPortalUser = c.String(),
                        OrderMessage = c.String(),
                        ShippingAddress1 = c.String(),
                        ShippingAddress2 = c.String(),
                        ShippingCity = c.String(),
                        ShippingCityState = c.String(),
                        ShippingZip = c.String(),
                        ReferralSource = c.String(),
                        DeliveryWindow = c.String(),
                        DeliveryDate = c.DateTime(),
                        OrderDate = c.DateTime(nullable: false),
                        OrderTypeId = c.Int(),
                        OrderStateId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderStates", t => t.OrderStateId)
                .ForeignKey("dbo.OrderTypes", t => t.OrderTypeId)
                .Index(t => t.OrderTypeId)
                .Index(t => t.OrderStateId);
            
            CreateTable(
                "dbo.OrderStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PurchaseTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ProductPriceSheet_Audit", "IsRentalAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheet_Audit", "IsActive", c => c.Boolean(nullable: false));
            AlterColumn("dbo.ProductPriceSheets", "ProductId", c => c.Int(nullable: false));
            AlterColumn("dbo.ProductPriceSheets", "CompanyId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.ProductPriceSheets", new[] { "ProductId", "CompanyId" });
            CreateIndex("dbo.ProductPriceSheets", "ProductId");
            CreateIndex("dbo.ProductPriceSheets", "CompanyId");
            AddForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProductPriceSheets", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
            DropColumn("dbo.ProductPriceSheets", "Id");
            DropColumn("dbo.ProductPriceSheets", "Version");
            DropColumn("dbo.ProductPriceSheet_Audit", "Id");
            DropColumn("dbo.ProductPriceSheet_Audit", "Version");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductPriceSheet_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheet_Audit", "Id", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheets", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheets", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.ProductPriceSheets", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.OrderLineItems", "PurchaseTypeId", "dbo.PurchaseTypes");
            DropForeignKey("dbo.OrderLineItems", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Orders", "OrderTypeId", "dbo.OrderTypes");
            DropForeignKey("dbo.Orders", "OrderStateId", "dbo.OrderStates");
            DropForeignKey("dbo.OrderLineItems", "OrderId", "dbo.Orders");
            DropIndex("dbo.Orders", new[] { "OrderStateId" });
            DropIndex("dbo.Orders", new[] { "OrderTypeId" });
            DropIndex("dbo.OrderLineItems", new[] { "PurchaseTypeId" });
            DropIndex("dbo.OrderLineItems", new[] { "ProductId" });
            DropIndex("dbo.OrderLineItems", new[] { "OrderId" });
            DropIndex("dbo.ProductPriceSheets", new[] { "CompanyId" });
            DropIndex("dbo.ProductPriceSheets", new[] { "ProductId" });
            DropPrimaryKey("dbo.ProductPriceSheets");
            AlterColumn("dbo.ProductPriceSheets", "CompanyId", c => c.Int());
            AlterColumn("dbo.ProductPriceSheets", "ProductId", c => c.Int());
            DropColumn("dbo.ProductPriceSheet_Audit", "IsActive");
            DropColumn("dbo.ProductPriceSheet_Audit", "IsRentalAllowed");
            DropTable("dbo.PurchaseTypes");
            DropTable("dbo.OrderTypes");
            DropTable("dbo.OrderStates");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderLineItems");
            AddPrimaryKey("dbo.ProductPriceSheets", "Id");
            CreateIndex("dbo.ProductPriceSheets", "CompanyId");
            CreateIndex("dbo.ProductPriceSheets", "ProductId");
            AddForeignKey("dbo.ProductPriceSheets", "ProductId", "dbo.Products", "Id");
            AddForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies", "Id");
        }
    }
}
