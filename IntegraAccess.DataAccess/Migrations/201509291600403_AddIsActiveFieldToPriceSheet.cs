namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddIsActiveFieldToPriceSheet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductPriceSheets", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.ProductPriceSheets_Audit", "IsActive", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.ProductPriceSheets_Audit", "IsActive");
            DropColumn("dbo.ProductPriceSheets", "IsActive");
        }
    }
}
