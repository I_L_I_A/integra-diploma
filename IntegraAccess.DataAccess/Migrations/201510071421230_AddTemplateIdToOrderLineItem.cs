namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTemplateIdToOrderLineItem : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderLineItems", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OrderLineItems", "OrderId", "dbo.Orders");
            DropPrimaryKey("dbo.OrderLineItems");
            AddColumn("dbo.OrderLineItems", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.OrderLineItems", "AddedByProductTemplateId", c => c.Int());
            AddColumn("dbo.OrderLineItems_Audit", "Id", c => c.Int(nullable: false));
            AddColumn("dbo.OrderLineItems_Audit", "AddedByProductTemplateId", c => c.Int());
            AddPrimaryKey("dbo.OrderLineItems", "Id");
            CreateIndex("dbo.OrderLineItems", "AddedByProductTemplateId");
            AddForeignKey("dbo.OrderLineItems", "AddedByProductTemplateId", "dbo.Products", "Id");
            AddForeignKey("dbo.OrderLineItems", "ProductId", "dbo.Products", "Id");
            AddForeignKey("dbo.OrderLineItems", "OrderId", "dbo.Orders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLineItems", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderLineItems", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OrderLineItems", "AddedByProductTemplateId", "dbo.Products");
            DropIndex("dbo.OrderLineItems", new[] { "AddedByProductTemplateId" });
            DropPrimaryKey("dbo.OrderLineItems");
            DropColumn("dbo.OrderLineItems_Audit", "AddedByProductTemplateId");
            DropColumn("dbo.OrderLineItems_Audit", "Id");
            DropColumn("dbo.OrderLineItems", "AddedByProductTemplateId");
            DropColumn("dbo.OrderLineItems", "Id");
            AddPrimaryKey("dbo.OrderLineItems", new[] { "OrderId", "ProductId" });
            AddForeignKey("dbo.OrderLineItems", "OrderId", "dbo.Orders", "Id", cascadeDelete: true);
            AddForeignKey("dbo.OrderLineItems", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
        }
    }
}
