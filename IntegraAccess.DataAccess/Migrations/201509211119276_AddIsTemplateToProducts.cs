namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsTemplateToProducts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "IsTemplate", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products_Audit", "IsTemplate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products_Audit", "IsTemplate");
            DropColumn("dbo.Products", "IsTemplate");
        }
    }
}
