namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductsAudit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCategories_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedById = c.String(),
                        ModifiedById = c.String(),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.ProductPriceSheet_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedById = c.String(),
                        ModifiedById = c.String(),
                        ProductId = c.Int(),
                        CompanyId = c.Int(),
                        PriceSelling = c.Decimal(precision: 18, scale: 2),
                        PriceRental = c.Decimal(precision: 18, scale: 2),
                        PriceDaily = c.Decimal(precision: 18, scale: 2),
                        IsSellingAllowed = c.Boolean(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.Products_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedById = c.String(),
                        ModifiedById = c.String(),
                        DisplayName = c.String(),
                        ExternalCode = c.String(),
                        ShortDescription = c.String(),
                        ShortDescription2 = c.String(),
                        PortalDescription = c.String(),
                        ProductCategoryId = c.Int(),
                        Image = c.Binary(),
                        ManufacturerLink = c.String(),
                        PriceDefault = c.Decimal(precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Products_Audit");
            DropTable("dbo.ProductPriceSheet_Audit");
            DropTable("dbo.ProductCategories_Audit");
        }
    }
}
