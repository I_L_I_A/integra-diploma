namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeProductTemplateToProductTemplateItem : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductTemplates", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ProductTemplates", "ProductId", "dbo.Products");
            DropIndex("dbo.ProductTemplates", new[] { "CompanyId" });
            DropIndex("dbo.ProductTemplates", new[] { "ProductId" });
            CreateTable(
                "dbo.ProductTemplateItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        ChildProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsActive = c.Int(nullable: false),
                        UpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ChildProductId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.ProductId)
                .Index(t => t.ChildProductId);
            
            CreateTable(
                "dbo.ProductTemplateItems_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        ChildProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        IsActive = c.Int(nullable: false),
                        UpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            DropTable("dbo.ProductTemplates");
            DropTable("dbo.ProductTemplates_Audit");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductTemplates_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        TemplateName = c.String(),
                        UpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.ProductTemplates",
                c => new
                    {
                        CompanyId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        TemplateName = c.String(),
                        UpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                    })
                .PrimaryKey(t => new { t.CompanyId, t.ProductId });
            
            DropForeignKey("dbo.ProductTemplateItems", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductTemplateItems", "ChildProductId", "dbo.Products");
            DropIndex("dbo.ProductTemplateItems", new[] { "ChildProductId" });
            DropIndex("dbo.ProductTemplateItems", new[] { "ProductId" });
            DropTable("dbo.ProductTemplateItems_Audit");
            DropTable("dbo.ProductTemplateItems");
            CreateIndex("dbo.ProductTemplates", "ProductId");
            CreateIndex("dbo.ProductTemplates", "CompanyId");
            AddForeignKey("dbo.ProductTemplates", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProductTemplates", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
        }
    }
}
