namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAccountEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExternalId = c.String(),
                        AccountGroupId = c.Int(),
                        Name = c.String(),
                        Street1 = c.String(),
                        Street2 = c.String(),
                        City = c.String(),
                        CityState = c.String(),
                        Zip = c.String(),
                        ContactName = c.String(),
                        ContactPhone = c.String(),
                        ContactEmail = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountGroups", t => t.AccountGroupId)
                .Index(t => t.AccountGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "AccountGroupId", "dbo.AccountGroups");
            DropIndex("dbo.Accounts", new[] { "AccountGroupId" });
            DropTable("dbo.Accounts");
        }
    }
}
