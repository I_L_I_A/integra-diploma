namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddVersionToManyToManyRelationships : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductTemplates", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheetItems", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.FacilityAccounts", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.UserAccounts", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.FacilityAccounts_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheetItems_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductTemplates_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.UserAccounts_Audit", "Version", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserAccounts_Audit", "Version");
            DropColumn("dbo.ProductTemplates_Audit", "Version");
            DropColumn("dbo.ProductPriceSheetItems_Audit", "Version");
            DropColumn("dbo.FacilityAccounts_Audit", "Version");
            DropColumn("dbo.UserAccounts", "Version");
            DropColumn("dbo.FacilityAccounts", "Version");
            DropColumn("dbo.ProductPriceSheetItems", "Version");
            DropColumn("dbo.ProductTemplates", "Version");
        }
    }
}
