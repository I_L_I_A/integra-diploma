namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        StackTrace = c.String(),
                        ErrorType = c.String(),
                        Username = c.String(),
                        StatusCode = c.String(),
                        HostName = c.String(),
                        LogLevel = c.String(),
                        AdditionalInfo = c.String(),
                        UpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedById = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Logs");
        }
    }
}
