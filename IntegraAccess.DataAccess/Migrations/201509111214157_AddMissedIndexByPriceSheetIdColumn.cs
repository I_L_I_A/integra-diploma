namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMissedIndexByPriceSheetIdColumn : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Accounts", "PriceSheetId");
            AddForeignKey("dbo.Accounts", "PriceSheetId", "dbo.ProductPriceSheets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "PriceSheetId", "dbo.ProductPriceSheets");
            DropIndex("dbo.Accounts", new[] { "PriceSheetId" });
        }
    }
}
