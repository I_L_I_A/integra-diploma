namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAccountGroup : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AccountGroups", "Address");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountGroups", "Address", c => c.String());
        }
    }
}
