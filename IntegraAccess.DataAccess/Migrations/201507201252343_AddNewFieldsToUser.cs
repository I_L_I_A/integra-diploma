namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewFieldsToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "AccountGroupId", c => c.Int());
            AddColumn("dbo.AspNetUsers", "IsActive", c => c.Boolean());
            CreateIndex("dbo.AspNetUsers", "AccountGroupId");
            AddForeignKey("dbo.AspNetUsers", "AccountGroupId", "dbo.AccountGroups", "Id");
            DropColumn("dbo.AspNetUsers", "IsInactive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "IsInactive", c => c.Boolean());
            DropForeignKey("dbo.AspNetUsers", "AccountGroupId", "dbo.AccountGroups");
            DropIndex("dbo.AspNetUsers", new[] { "AccountGroupId" });
            DropColumn("dbo.AspNetUsers", "IsActive");
            DropColumn("dbo.AspNetUsers", "AccountGroupId");
        }
    }
}
