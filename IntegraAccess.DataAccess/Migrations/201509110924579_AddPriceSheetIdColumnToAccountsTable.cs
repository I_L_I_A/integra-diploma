namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPriceSheetIdColumnToAccountsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "PriceSheetId", c => c.Int());
            AddColumn("dbo.Accounts_Audit", "PriceSheetId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accounts", "PriceSheetId");
            DropColumn("dbo.Accounts_Audit", "PriceSheetId");
        }
    }
}
