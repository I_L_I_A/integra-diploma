namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUsersToAcountGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "AccountId", c => c.Int());
            DropColumn("dbo.AspNetUsers", "AccountLId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "AccountLId", c => c.Int());
            DropColumn("dbo.AspNetUsers", "AccountId");
        }
    }
}
