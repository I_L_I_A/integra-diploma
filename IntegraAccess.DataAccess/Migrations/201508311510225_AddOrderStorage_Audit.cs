namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderStorage_Audit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderLineItems_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        PurchaseTypeId = c.Int(),
                        ItemNumber = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comments = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            CreateTable(
                "dbo.Orders_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        Version = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        CreatedById = c.String(),
                        ModifiedById = c.String(),
                        ExternalOrderNumber = c.String(),
                        PatientNumber = c.String(),
                        CarrierNumber = c.String(),
                        CompanyNumber = c.String(),
                        PONumber = c.String(),
                        EnteredByPortalUser = c.String(),
                        OrderMessage = c.String(),
                        ShippingAddress1 = c.String(),
                        ShippingAddress2 = c.String(),
                        ShippingCity = c.String(),
                        ShippingCityState = c.String(),
                        ShippingZip = c.String(),
                        ReferralSource = c.String(),
                        DeliveryWindow = c.String(),
                        DeliveryDate = c.DateTime(),
                        OrderDate = c.DateTime(nullable: false),
                        OrderTypeId = c.Int(),
                        OrderStateId = c.Int(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Orders_Audit");
            DropTable("dbo.OrderLineItems_Audit");
        }
    }
}
