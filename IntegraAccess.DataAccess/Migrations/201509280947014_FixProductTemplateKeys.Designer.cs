// <auto-generated />
namespace IntegraAccess.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class FixProductTemplateKeys : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FixProductTemplateKeys));
        
        string IMigrationMetadata.Id
        {
            get { return "201509280947014_FixProductTemplateKeys"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
