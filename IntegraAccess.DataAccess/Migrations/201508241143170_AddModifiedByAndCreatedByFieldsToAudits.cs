namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModifiedByAndCreatedByFieldsToAudits : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountGroups_Audit", "CreatedById", c => c.String());
            AddColumn("dbo.AccountGroups_Audit", "ModifiedById", c => c.String());
            AddColumn("dbo.Accounts_Audit", "CreatedById", c => c.String());
            AddColumn("dbo.Accounts_Audit", "ModifiedById", c => c.String());
            AddColumn("dbo.Companies_Audit", "CreatedById", c => c.String());
            AddColumn("dbo.Companies_Audit", "ModifiedById", c => c.String());
            AddColumn("dbo.Facilities_Audit", "CreatedById", c => c.String());
            AddColumn("dbo.Facilities_Audit", "ModifiedById", c => c.String());
            AddColumn("dbo.FacilityAccounts_Audit", "CreatedById", c => c.String());
            AddColumn("dbo.FacilityAccounts_Audit", "ModifiedById", c => c.String());
            AddColumn("dbo.Patients_Audit", "CreatedById", c => c.String());
            AddColumn("dbo.Patients_Audit", "ModifiedById", c => c.String());
            AddColumn("dbo.UserAccounts_Audit", "CreatedById", c => c.String());
            AddColumn("dbo.UserAccounts_Audit", "ModifiedById", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserAccounts_Audit", "ModifiedById");
            DropColumn("dbo.UserAccounts_Audit", "CreatedById");
            DropColumn("dbo.Patients_Audit", "ModifiedById");
            DropColumn("dbo.Patients_Audit", "CreatedById");
            DropColumn("dbo.FacilityAccounts_Audit", "ModifiedById");
            DropColumn("dbo.FacilityAccounts_Audit", "CreatedById");
            DropColumn("dbo.Facilities_Audit", "ModifiedById");
            DropColumn("dbo.Facilities_Audit", "CreatedById");
            DropColumn("dbo.Companies_Audit", "ModifiedById");
            DropColumn("dbo.Companies_Audit", "CreatedById");
            DropColumn("dbo.Accounts_Audit", "ModifiedById");
            DropColumn("dbo.Accounts_Audit", "CreatedById");
            DropColumn("dbo.AccountGroups_Audit", "ModifiedById");
            DropColumn("dbo.AccountGroups_Audit", "CreatedById");
        }
    }
}
