namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProducts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductPriceSheets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(),
                        CompanyId = c.Int(),
                        PriceSelling = c.Decimal(precision: 18, scale: 2),
                        PriceRental = c.Decimal(precision: 18, scale: 2),
                        PriceDaily = c.Decimal(precision: 18, scale: 2),
                        IsSellingAllowed = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.ProductId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(),
                        ExternalCode = c.String(),
                        ShortDescription = c.String(),
                        ShortDescription2 = c.String(),
                        PortalDescription = c.String(),
                        ProductCategoryId = c.Int(),
                        Image = c.Binary(),
                        ManufacturerLink = c.String(),
                        PriceDefault = c.Decimal(precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        CreatedById = c.String(),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductCategories", t => t.ProductCategoryId)
                .Index(t => t.ProductCategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductPriceSheets", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "ProductCategoryId", "dbo.ProductCategories");
            DropForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Products", new[] { "ProductCategoryId" });
            DropIndex("dbo.ProductPriceSheets", new[] { "CompanyId" });
            DropIndex("dbo.ProductPriceSheets", new[] { "ProductId" });
            DropTable("dbo.Products");
            DropTable("dbo.ProductPriceSheets");
            DropTable("dbo.ProductCategories");
        }
    }
}
