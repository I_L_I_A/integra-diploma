namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameExternalBillingCodeToExternalCodeForPatient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "PatientExternalCode", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientExternalCode", c => c.String());
            AddColumn("dbo.Patients", "ExternalCode", c => c.String());
            AddColumn("dbo.Patients_Audit", "ExternalCode", c => c.String());
            DropColumn("dbo.Orders", "PatientExternalBillingCode");
            DropColumn("dbo.Orders_Audit", "PatientExternalBillingCode");
            DropColumn("dbo.Patients", "ExternalBillingCode");
            DropColumn("dbo.Patients_Audit", "ExternalBillingCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients_Audit", "ExternalBillingCode", c => c.String());
            AddColumn("dbo.Patients", "ExternalBillingCode", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientExternalBillingCode", c => c.String());
            AddColumn("dbo.Orders", "PatientExternalBillingCode", c => c.String());
            DropColumn("dbo.Patients_Audit", "ExternalCode");
            DropColumn("dbo.Patients", "ExternalCode");
            DropColumn("dbo.Orders_Audit", "PatientExternalCode");
            DropColumn("dbo.Orders", "PatientExternalCode");
        }
    }
}
