namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCompaniesAndFacilities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Facilities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        AccountGroupId = c.Int(),
                        Name = c.String(),
                        ExternalCode = c.String(),
                        Street1 = c.String(),
                        Street2 = c.String(),
                        City = c.String(),
                        CityState = c.String(),
                        Zip = c.String(),
                        ContactName = c.String(),
                        ContactPhone = c.String(),
                        ContactEmail = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountGroups", t => t.AccountGroupId)
                .Index(t => t.AccountGroupId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExternalCode = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        Version = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FacilityAccount",
                c => new
                    {
                        FacilityId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false)
                    })
                .PrimaryKey(t => new { t.FacilityId, t.AccountId })
                .ForeignKey("dbo.Facilities", t => t.FacilityId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .Index(t => t.FacilityId)
                .Index(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FacilityAccount", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.FacilityAccount", "FacilityId", "dbo.Facilities");
            DropForeignKey("dbo.Facilities", "AccountGroupId", "dbo.AccountGroups");
            DropIndex("dbo.FacilityAccount", new[] { "AccountId" });
            DropIndex("dbo.FacilityAccount", new[] { "FacilityId" });
            DropIndex("dbo.Facilities", new[] { "AccountGroupId" });
            DropTable("dbo.FacilityAccount");
            DropTable("dbo.Companies");
            DropTable("dbo.Facilities");
        }
    }
}
