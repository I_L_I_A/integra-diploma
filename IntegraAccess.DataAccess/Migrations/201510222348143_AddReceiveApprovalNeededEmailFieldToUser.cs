namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReceiveApprovalNeededEmailFieldToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ReceiveApprovalNeededEmail", c => c.Boolean(nullable:false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ReceiveApprovalNeededEmail");
        }
    }
}
