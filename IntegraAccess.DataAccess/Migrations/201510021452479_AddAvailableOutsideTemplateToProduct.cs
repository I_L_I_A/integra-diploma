namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAvailableOutsideTemplateToProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "AvailableOutsideTemplate", c => c.Boolean(nullable: false));
            AddColumn("dbo.Products_Audit", "AvailableOutsideTemplate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products_Audit", "AvailableOutsideTemplate");
            DropColumn("dbo.Products", "AvailableOutsideTemplate");
        }
    }
}
