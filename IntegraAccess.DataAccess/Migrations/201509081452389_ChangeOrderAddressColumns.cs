namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeOrderAddressColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ShippingStreet1", c => c.String());
            AddColumn("dbo.Orders", "ShippingStreet2", c => c.String());
            AddColumn("dbo.Orders", "PatientStreet1", c => c.String());
            AddColumn("dbo.Orders", "PatientStreet2", c => c.String());
            AddColumn("dbo.Orders_Audit", "ShippingStreet1", c => c.String());
            AddColumn("dbo.Orders_Audit", "ShippingStreet2", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientStreet1", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientStreet2", c => c.String());
            DropColumn("dbo.Orders", "ShippingAddress1");
            DropColumn("dbo.Orders", "ShippingAddress2");
            DropColumn("dbo.Orders", "PatientAddress1");
            DropColumn("dbo.Orders", "PatientAddress2");
            DropColumn("dbo.Orders_Audit", "ShippingAddress1");
            DropColumn("dbo.Orders_Audit", "ShippingAddress2");
            DropColumn("dbo.Orders_Audit", "PatientAddress1");
            DropColumn("dbo.Orders_Audit", "PatientAddress2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders_Audit", "PatientAddress2", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientAddress1", c => c.String());
            AddColumn("dbo.Orders_Audit", "ShippingAddress2", c => c.String());
            AddColumn("dbo.Orders_Audit", "ShippingAddress1", c => c.String());
            AddColumn("dbo.Orders", "PatientAddress2", c => c.String());
            AddColumn("dbo.Orders", "PatientAddress1", c => c.String());
            AddColumn("dbo.Orders", "ShippingAddress2", c => c.String());
            AddColumn("dbo.Orders", "ShippingAddress1", c => c.String());
            DropColumn("dbo.Orders_Audit", "PatientStreet2");
            DropColumn("dbo.Orders_Audit", "PatientStreet1");
            DropColumn("dbo.Orders_Audit", "ShippingStreet2");
            DropColumn("dbo.Orders_Audit", "ShippingStreet1");
            DropColumn("dbo.Orders", "PatientStreet2");
            DropColumn("dbo.Orders", "PatientStreet1");
            DropColumn("dbo.Orders", "ShippingStreet2");
            DropColumn("dbo.Orders", "ShippingStreet1");
        }
    }
}
