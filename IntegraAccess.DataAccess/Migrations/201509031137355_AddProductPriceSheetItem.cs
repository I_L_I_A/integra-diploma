namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductPriceSheetItem : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductPriceSheets", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies");
            DropIndex("dbo.ProductPriceSheets", new[] { "ProductId" });
            DropIndex("dbo.ProductPriceSheets", new[] { "CompanyId" });
            DropPrimaryKey("dbo.ProductPriceSheets");
            CreateTable(
                "dbo.ProductPriceSheetItems",
                c => new
                    {
                        ProductPriceSheetId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        PriceSelling = c.Decimal(precision: 18, scale: 2),
                        PriceRental = c.Decimal(precision: 18, scale: 2),
                        PriceDaily = c.Decimal(precision: 18, scale: 2),
                        IsSellingAllowed = c.Boolean(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                    })
                .PrimaryKey(t => new { t.ProductPriceSheetId, t.ProductId })
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.ProductPriceSheets", t => t.ProductPriceSheetId, cascadeDelete: true)
                .Index(t => t.ProductPriceSheetId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductPriceSheetItems_Audit",
                c => new
                    {
                        HistoryId = c.Int(nullable: false, identity: true),
                        ProductPriceSheetId = c.Int(),
                        ProductId = c.Int(),
                        PriceSelling = c.Decimal(precision: 18, scale: 2),
                        PriceRental = c.Decimal(precision: 18, scale: 2),
                        PriceDaily = c.Decimal(precision: 18, scale: 2),
                        IsSellingAllowed = c.Boolean(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                        ModifiedById = c.String(),
                        AuditDataState = c.String(maxLength: 10),
                        AuditDMLAction = c.String(maxLength: 10),
                        AuditDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.HistoryId);
            
            AddColumn("dbo.ProductPriceSheets", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.ProductPriceSheets", "Name", c => c.String());
            AddColumn("dbo.ProductPriceSheets", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheets_Audit", "Id", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheets_Audit", "Version", c => c.Int(nullable: false));
            AddColumn("dbo.ProductPriceSheets_Audit", "Name", c => c.String());
            AlterColumn("dbo.ProductPriceSheets", "CompanyId", c => c.Int());
            AddPrimaryKey("dbo.ProductPriceSheets", "Id");
            CreateIndex("dbo.ProductPriceSheets", "CompanyId");
            AddForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies", "Id");
            DropColumn("dbo.ProductPriceSheets", "ProductId");
            DropColumn("dbo.ProductPriceSheets", "PriceSelling");
            DropColumn("dbo.ProductPriceSheets", "PriceRental");
            DropColumn("dbo.ProductPriceSheets", "PriceDaily");
            DropColumn("dbo.ProductPriceSheets", "IsSellingAllowed");
            DropColumn("dbo.ProductPriceSheets", "IsRentalAllowed");
            DropColumn("dbo.ProductPriceSheets", "IsActive");
            DropColumn("dbo.ProductPriceSheets_Audit", "ProductId");
            DropColumn("dbo.ProductPriceSheets_Audit", "PriceSelling");
            DropColumn("dbo.ProductPriceSheets_Audit", "PriceRental");
            DropColumn("dbo.ProductPriceSheets_Audit", "PriceDaily");
            DropColumn("dbo.ProductPriceSheets_Audit", "IsSellingAllowed");
            DropColumn("dbo.ProductPriceSheets_Audit", "IsRentalAllowed");
            DropColumn("dbo.ProductPriceSheets_Audit", "IsActive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductPriceSheets_Audit", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheets_Audit", "IsRentalAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheets_Audit", "IsSellingAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheets_Audit", "PriceDaily", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.ProductPriceSheets_Audit", "PriceRental", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.ProductPriceSheets_Audit", "PriceSelling", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.ProductPriceSheets_Audit", "ProductId", c => c.Int());
            AddColumn("dbo.ProductPriceSheets", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheets", "IsRentalAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheets", "IsSellingAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductPriceSheets", "PriceDaily", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.ProductPriceSheets", "PriceRental", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.ProductPriceSheets", "PriceSelling", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.ProductPriceSheets", "ProductId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ProductPriceSheetItems", "ProductPriceSheetId", "dbo.ProductPriceSheets");
            DropForeignKey("dbo.ProductPriceSheetItems", "ProductId", "dbo.Products");
            DropIndex("dbo.ProductPriceSheetItems", new[] { "ProductId" });
            DropIndex("dbo.ProductPriceSheetItems", new[] { "ProductPriceSheetId" });
            DropIndex("dbo.ProductPriceSheets", new[] { "CompanyId" });
            DropPrimaryKey("dbo.ProductPriceSheets");
            AlterColumn("dbo.ProductPriceSheets", "CompanyId", c => c.Int(nullable: false));
            DropColumn("dbo.ProductPriceSheets_Audit", "Name");
            DropColumn("dbo.ProductPriceSheets_Audit", "Version");
            DropColumn("dbo.ProductPriceSheets_Audit", "Id");
            DropColumn("dbo.ProductPriceSheets", "Version");
            DropColumn("dbo.ProductPriceSheets", "Name");
            DropColumn("dbo.ProductPriceSheets", "Id");
            DropTable("dbo.ProductPriceSheetItems_Audit");
            DropTable("dbo.ProductPriceSheetItems");
            AddPrimaryKey("dbo.ProductPriceSheets", new[] { "ProductId", "CompanyId" });
            CreateIndex("dbo.ProductPriceSheets", "CompanyId");
            CreateIndex("dbo.ProductPriceSheets", "ProductId");
            AddForeignKey("dbo.ProductPriceSheets", "CompanyId", "dbo.Companies", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProductPriceSheets", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
        }
    }
}
