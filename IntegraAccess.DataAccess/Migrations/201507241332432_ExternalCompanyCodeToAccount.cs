namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExternalCompanyCodeToAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "ExternalCompanyCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accounts", "ExternalCompanyCode");
        }
    }
}
