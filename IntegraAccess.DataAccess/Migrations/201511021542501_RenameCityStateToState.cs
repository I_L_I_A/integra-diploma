namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameCityStateToState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountGroups", "State", c => c.String());
            AddColumn("dbo.Accounts", "State", c => c.String());
            AddColumn("dbo.Orders", "ShippingState", c => c.String());
            AddColumn("dbo.Orders", "PatientState", c => c.String());
            AddColumn("dbo.Facilities", "State", c => c.String());
            AddColumn("dbo.AccountGroups_Audit", "State", c => c.String());
            AddColumn("dbo.Accounts_Audit", "State", c => c.String());
            AddColumn("dbo.Facilities_Audit", "State", c => c.String());
            AddColumn("dbo.Orders_Audit", "ShippingState", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientState", c => c.String());
            AddColumn("dbo.Patients", "State", c => c.String());
            AddColumn("dbo.Patients_Audit", "State", c => c.String());
            DropColumn("dbo.AccountGroups", "CityState");
            DropColumn("dbo.Accounts", "CityState");
            DropColumn("dbo.Orders", "ShippingCityState");
            DropColumn("dbo.Orders", "PatientCityState");
            DropColumn("dbo.Facilities", "CityState");
            DropColumn("dbo.AccountGroups_Audit", "CityState");
            DropColumn("dbo.Accounts_Audit", "CityState");
            DropColumn("dbo.Facilities_Audit", "CityState");
            DropColumn("dbo.Orders_Audit", "ShippingCityState");
            DropColumn("dbo.Orders_Audit", "PatientCityState");
            DropColumn("dbo.Patients", "CityState");
            DropColumn("dbo.Patients_Audit", "CityState");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Patients_Audit", "CityState", c => c.String());
            AddColumn("dbo.Patients", "CityState", c => c.String());
            AddColumn("dbo.Orders_Audit", "PatientCityState", c => c.String());
            AddColumn("dbo.Orders_Audit", "ShippingCityState", c => c.String());
            AddColumn("dbo.Facilities_Audit", "CityState", c => c.String());
            AddColumn("dbo.Accounts_Audit", "CityState", c => c.String());
            AddColumn("dbo.AccountGroups_Audit", "CityState", c => c.String());
            AddColumn("dbo.Facilities", "CityState", c => c.String());
            AddColumn("dbo.Orders", "PatientCityState", c => c.String());
            AddColumn("dbo.Orders", "ShippingCityState", c => c.String());
            AddColumn("dbo.Accounts", "CityState", c => c.String());
            AddColumn("dbo.AccountGroups", "CityState", c => c.String());
            DropColumn("dbo.Patients_Audit", "State");
            DropColumn("dbo.Patients", "State");
            DropColumn("dbo.Orders_Audit", "PatientState");
            DropColumn("dbo.Orders_Audit", "ShippingState");
            DropColumn("dbo.Facilities_Audit", "State");
            DropColumn("dbo.Accounts_Audit", "State");
            DropColumn("dbo.AccountGroups_Audit", "State");
            DropColumn("dbo.Facilities", "State");
            DropColumn("dbo.Orders", "PatientState");
            DropColumn("dbo.Orders", "ShippingState");
            DropColumn("dbo.Accounts", "State");
            DropColumn("dbo.AccountGroups", "State");
        }
    }
}
