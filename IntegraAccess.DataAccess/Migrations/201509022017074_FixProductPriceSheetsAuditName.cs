namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixProductPriceSheetsAuditName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ProductPriceSheet_Audit", newName: "ProductPriceSheets_Audit");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ProductPriceSheets_Audit", newName: "ProductPriceSheet_Audit");
        }
    }
}
