namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameCityZipToZip : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ShippingZip", c => c.String());
            AddColumn("dbo.Orders_Audit", "ShippingZip", c => c.String());
            DropColumn("dbo.Orders", "ShippingCityZip");
            DropColumn("dbo.Orders_Audit", "ShippingCityZip");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders_Audit", "ShippingCityZip", c => c.String());
            AddColumn("dbo.Orders", "ShippingCityZip", c => c.String());
            DropColumn("dbo.Orders_Audit", "ShippingZip");
            DropColumn("dbo.Orders", "ShippingZip");
        }
    }
}
