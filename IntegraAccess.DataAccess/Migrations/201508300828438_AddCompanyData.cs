namespace IntegraAccess.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyData : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO Companies
(ExternalCode, Name, Description, CreatedDate, ModifiedDate, Version, ModifiedById, CreatedById) VALUES
(N'1', N'INTEGRA HEALTHCARE EQUIPMENT LLC', N'747 N Church Road, Suite G7', '8/14/2015 4:39:53 PM', '8/14/2015 4:39:53 PM', 1, NULL, NULL), 
(N'2', N'Integra Respiratory Therapy Service', N'747 N. Church Rd.', '8/14/2015 4:39:53 PM', '8/14/2015 4:39:53 PM', 1, NULL, NULL), 
(N'4', N'BLOOMINGTON-INTEGRA EQUIPMENT', N'608 West Olive ST', '8/14/2015 4:39:53 PM', '8/14/2015 4:39:53 PM', 1, NULL, NULL), 
(N'5', N'ST LOUIS - INTEGRA EQUIPMENT', N'6121 Baumgartner Crossing', '8/14/2015 4:39:53 PM', '8/14/2015 4:39:53 PM', 1, NULL, NULL), 
(N'20', N'Integra Healthcare of Michigan, LLC', N'28339 Beck Road Suite F5', '8/14/2015 4:39:53 PM', '8/14/2015 4:39:53 PM', 1, NULL, NULL), 
(N'30', N'Integra Healthcare of Wisconsin LLC', N'2855 South 160th Street', '8/14/2015 4:39:53 PM', '8/14/2015 4:39:53 PM', 1, NULL, NULL)
                ");
        }
        
        public override void Down()
        {
        }
    }
}
