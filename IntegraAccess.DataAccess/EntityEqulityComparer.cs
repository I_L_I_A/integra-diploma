﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using IntegraAccess.Entities;

namespace IntegraAccess.DataAccess
{
    public class EntityEqulityComparer : IEqualityComparer<DbEntityEntry<IObjectWithState>>
    {
        public bool Equals(DbEntityEntry<IObjectWithState> x, DbEntityEntry<IObjectWithState> y)
        {
            if (x == null || y == null || x.Entity.GetType() != y.Entity.GetType() ||
                (int) x.CurrentValues["Id"] == 0 || (int) y.CurrentValues["Id"] == 0)
                return false;

            return (int) x.CurrentValues["Id"] == (int) y.CurrentValues["Id"];
        }

        public int GetHashCode(DbEntityEntry<IObjectWithState> obj)
        {
            return obj.CurrentValues["Id"].GetHashCode();
        }
    }
}