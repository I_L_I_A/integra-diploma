﻿using System.Data.Entity;
using Autofac;
using IntegraAccess.DataAccess.Context;
using IntegraAccess.DataAccess.Repository;
using IntegraAccess.DataAccess.Repository.Abstract;

namespace IntegraAccess.DataAccess
{
    public class DALRegistrationModule : Module
    {

        private class Starter : IStartable
        {
            public void Start()
            {
                Database.SetInitializer(new CreateDatabaseIfNotExists<DataBaseContext>());
                Database.SetInitializer<AuxDBContext>(null);
            }
        }

        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        /// <param name="builder">The builder through which components can be
        ///             registered.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new Starter()).As<IStartable>().SingleInstance();

            builder.Register<IRepositoryBase>(x => new RepositoryBase<DataBaseContext>(new RepositoryBaseConfiguration())).InstancePerLifetimeScope();
            builder.Register(x => new AuxDBContext()).InstancePerRequest();
            builder.Register<IAuthRepository>(x => new AuthRepository(x.Resolve<IRepositoryBase>())).InstancePerRequest();

        }

    }
}
