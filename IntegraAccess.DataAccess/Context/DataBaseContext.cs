﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Entities.Audit;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IntegraAccess.DataAccess.Context
{
    public class DataBaseContext : IdentityDbContext<IdentityUser>
    {
        public DataBaseContext()
            : base(ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataBaseContext, Migrations.Configuration>());

            modelBuilder.Entity<ProductTemplateItem>()
                .HasRequired(f => f.ChildProduct)
                .WithMany(x => x.ParentTemplateItems)
                .HasForeignKey(f => f.ChildProductId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductTemplateItem>()
               .HasRequired(f => f.Product)
               .WithMany(x => x.NestedTemplateItems)
               .HasForeignKey(f => f.ProductId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductTemplateItem>()
               .HasRequired(f => f.Company)
               .WithMany(x => x.ProductTemplateItems)
               .HasForeignKey(f => f.CompanyId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderLineItem>()
              .HasRequired(f => f.Product)
              .WithMany(x => x.OrderLineItems)
              .HasForeignKey(f => f.ProductId)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderLineItem>()
               .HasRequired(f => f.Order)
               .WithMany(x => x.OrderLineItems)
               .HasForeignKey(f => f.OrderId)
               .WillCascadeOnDelete(false);
        }

        public DbSet<UserAccounts> UserAccounts { get; set; }
        public DbSet<FacilityAccounts> FacilityAccounts { get; set; }
        public DbSet<AccountGroup> AccountGroups { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductsToCategories> ProductToCategories { get; set; }
        public DbSet<ProductPriceSheet> ProductPriceSheets { get; set; }
        public DbSet<ProductPriceSheetItem> ProductPriceSheetItems { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductTemplateItem> ProductTemplateItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLineItem> OrderLineItems { get; set; }
        public DbSet<OrderState> OrderStates { get; set; }
        public DbSet<OrderType> OrderTypes { get; set; }
        public DbSet<PurchaseType> PurchaseTypes { get; set; }
        public DbSet<Log> Logs { get; set; }


        //Audit Tables
        public DbSet<UserAccounts_Audit> UserAccounts_Audit { get; set; }
        public DbSet<FacilityAccounts_Audit> FacilityAccounts_Audit { get; set; }
        public DbSet<ProductsToCategories_Audit> ProductsToCategories_Audit { get; set; }
        public DbSet<AccountGroups_Audit> AccountGroups_Audit { get; set; }
        public DbSet<Accounts_Audit> Accounts_Audit { get; set; }
        public DbSet<Patients_Audit> Patients_Audit { get; set; }
        public DbSet<Facilities_Audit> Facilities_Audit { get; set; }
        public DbSet<Companies_Audit> Companies_Audit { get; set; }
        public DbSet<ProductCategories_Audit> ProductCategories_Audit { get; set; }
        public DbSet<ProductPriceSheets_Audit> ProductPriceSheets_Audit { get; set; }
        public DbSet<ProductPriceSheetItems_Audit> ProductPriceSheetItems_Audit { get; set; }
        public DbSet<Products_Audit> Products_Audit { get; set; }
        public DbSet<ProductTemplateItems_Audit> ProductTemplateItems_Audit { get; set; }
        public DbSet<Orders_Audit> Orders_Audit { get; set; }
        public DbSet<OrderLineItems_Audit> OrderLineItems_Audit { get; set; }

    }
}
