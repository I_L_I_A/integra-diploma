﻿using System.Configuration;
using System.Data;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.DataAccess.Context
{
    public class RepositoryBaseConfiguration
    {
        public TransactionTypes TransactionType = TransactionTypes.DbTransaction;
        public IsolationLevel IsolationLevel = IsolationLevel.Unspecified;

        public bool ProxyCreationEnabled = true;
        public bool RethrowExceptions = false;
        public bool UseTransaction = false;
        public bool LazyConnection = true;
        public bool SaveLastExecutedMethodInfo = false;

        public int CommandTimeout = 300;
    }
}