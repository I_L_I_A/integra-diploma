﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Entities.Audit;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IntegraAccess.DataAccess.Context
{
    public class AuxDBContext : DbContext
    {
        public AuxDBContext()
            : base(ConfigurationManager.ConnectionStrings["EFAuxDbContext"].ConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AuxDBContext, AuxDBMigrations.AuxConfiguration>());
        }
    }
}
