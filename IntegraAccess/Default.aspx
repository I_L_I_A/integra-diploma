﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IntegraAccess.Default" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="integraApp" runat="server">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <title>DME Prime</title>
    <%--<title data-ng-bind="pageTitle()"></title>--%>
    <!-- Loading core css-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300" />
    <link rel="stylesheet" href="Public/components/font-awesome/css/font-awesome.min.css?v=<%= CurrentVersion %>">
    <link rel="stylesheet" href="Public/components/bootstrap/css/bootstrap.min.css?v=<%= CurrentVersion %>">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="Public/components/themify-icons/themify-icons.css?v=<%= CurrentVersion %>">
    <%--    <!-- Table sorter -->--%>
    <link rel="stylesheet" href="Public/components/angular-tablesort/tablesort.css?v=<%= CurrentVersion %>">
    <%--    <!-- Loading Bar -->--%>
    <link rel="stylesheet" href="Public/components/angular-loading-bar/build/loading-bar.min.css?v=<%= CurrentVersion %>">
    <%--    <!-- Select 2 -->--%>
    <link rel="stylesheet" href="Public/components/angular-ui-select/dist/select.css?v=<%= CurrentVersion %>">
    <%--    <!-- Animate Css -->--%>
    <link rel="stylesheet" href="Public/components/animate.css/animate.min.css?v=<%= CurrentVersion %>">
    <%--    <!-- Sweet Alert CSS -->--%>
    <link rel="stylesheet" href="Public/components/sweetalert/lib/sweet-alert.css?v=<%= CurrentVersion %>">
    <link rel="stylesheet" href="Public/components/angular-chart/angular-chart.css">
    <!-- Clip-Two CSS -->
    <link rel="stylesheet" href="Public/vendors/clip-two/css/styles.css?v=<%= CurrentVersion %>">
    <link rel="stylesheet" href="Public/vendors/clip-two/css/plugins.css?v=<%= CurrentVersion %>">
    <!-- MAdmin Theme -->
    <link rel="stylesheet" href="Public/vendors/mAdmin/css/themes/style1/pink-blue.css?v=<%= CurrentVersion %>" />
    <link rel="stylesheet" href="Public/vendors/mAdmin/css/themes/style2/pink-blue.css?v=<%= CurrentVersion %>" />
    <link rel="stylesheet" href="Public/vendors/mAdmin/css/style-responsive.css?v=<%= CurrentVersion %>" />
    <!-- Own CSS -->
    <link rel="stylesheet" href="Public/content/css/app.css?v=<%= CurrentVersion %>">
    <link rel="stylesheet" href="Public/content/css/styles.css?v=<%= CurrentVersion %>">
</head>
<body ng-controller="AppCtrl" ng-class="[header.menu_style, header.menu_collapse, header.header_topbar, header.effect]" class="animated main-container glossed">
    <div ui-view id="app"></div>
    <!-- jQuery -->
    <script src="Public/components/jquery/dist/jquery.min.js?v=<%= CurrentVersion %>"></script>
    <!-- JSNLOG -->
    <script src="Public/components/jsnlog/jsnlog.min.js?v=<%= CurrentVersion %>"></script>
    <!-- Fastclick -->
    <script src="Public/components/fastclick/lib/fastclick.js?v=<%= CurrentVersion %>"></script>
    <!-- Angular -->
    <script src="Public/components/angular/angular.min.js"></script>
    <script src="Public/components/angular-resource/angular-resource.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-cookies/angular-cookies.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-animate/angular-animate.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-touch/angular-touch.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-moment/angular-moment.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-ui-utils/mask.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-sanitize/angular-sanitize.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-ui-router/release/angular-ui-router.min.js?v=<%= CurrentVersion %>"></script>
<%--    <script src="Public/components/angular-ui-router-extras/dist/ct-ui-router-extras.js?v=<%= CurrentVersion %>"></script>--%>
<%--    <script src="Public/components/angular-permission/dist/angular-permission.js?v=<%= CurrentVersion %>"></script>--%>
    <!-- Angular storage -->
    <script src="Public/components/ngstorage/ngStorage.min.js?v=<%= CurrentVersion %>"></script>

    <!-- oclazyload -->
    <script src="Public/components/oclazyload/dist/ocLazyLoad.min.js?v=<%= CurrentVersion %>"></script>
    <!-- breadcrumb -->
    <script src="Public/components/angular-breadcrumb/dist/angular-breadcrumb.min.js?v=<%= CurrentVersion %>"></script>
    <!-- Loading Bar -->
    <script src="Public/components/angular-loading-bar/build/loading-bar.min.js?v=<%= CurrentVersion %>"></script>
    <!-- Table sorter -->
    <script src="Public/components/angular-tablesort/js/angular-tablesort.js?v=<%= CurrentVersion %>"></script>
    <!-- Select 2 -->
    <script src="Public/components/angular-ui-select/dist/select.js?v=<%= CurrentVersion %>"></script>
    <!-- Angular Scroll -->
     <script src="Public/components/angular-scroll/angular-scroll.min.js?v=<%= CurrentVersion %>"></script>
    <!-- Loading global plugin-->
    <script src="Public/components/bootstrap/js/bootstrap.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-bootstrap/ui-bootstrap.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-bootstrap/ui-bootstrap-tpls.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/slimScroll/jquery.slimscroll.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/sweetalert/lib/sweet-alert.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-sweetalert-promised/SweetAlert.min.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/chartjs/Chart.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/components/angular-chart/angular-chart.js?v=<%= CurrentVersion %>"></script>
    <!-- Own Configuration -->
    <script src="Public/app.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/main.js?v=<%= CurrentVersion %>"></script>
    <script>
        angular.module('AppConfig', []).constant('AppSettings', {
        version: '<%= CurrentVersion %>',
        lastModifiedDate: '<%= LastModifiedDate %>',
        logClientSideErrors: '<%= ConfigurationManager.AppSettings["logClientSideErrors"]%>',
        });
    </script>
    <script src="Public/config.constant.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/config.interceptors.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/config.router.js"></script>
    <script src="Public/app/common/directives.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/app/common/filters.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/app/common/services/currentUserService.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/app/auth/authInterceptorService.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/app/auth/authService.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/app/common/services/usersService.js?v=<%= CurrentVersion %>"></script>
    <script src="Public/app/common/logging/logToServer.js?v=<%= CurrentVersion %>"></script>
    <!-- Clip-Two Controllers -->
    <script src="Public/app/layout/mainCtrl.js?v=<%= CurrentVersion %>"></script>
    <!-- Google Apis -->
    <script src="//maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather,visualization"></script>
    <!-- Application settings served with each page refresh -->

</body>
</html>
