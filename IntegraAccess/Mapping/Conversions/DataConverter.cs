﻿using System.Collections.Generic;
using AutoMapper;
using AutoMapper.Mappers;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Conversions
{
    public abstract class DataConverter : IDataConverter
    {
        #region Fields

        private ConfigurationStore _configuration;

        private MappingEngine _engine;

        #endregion

        #region Properties

        protected ConfigurationStore Configuration
        {
            get { return _configuration ?? (_configuration = GetConfiguration()); }
        }

        protected MappingEngine Engine
        {
            get { return _engine ?? (_engine = GetEngine()); }
        }

        #endregion

        #region Public Methods and Operators

        public TTo Map<TFrom, TTo>(TFrom source)
        {
            return Engine.Map<TFrom, TTo>(source);
        }

        public TTo Map<TFrom, TTo>(TFrom source, TTo destination)
        {
            Engine.DynamicMap(source, destination);
            return destination;
        }

        public TTo MapList<TFrom, TTo>(TFrom source)
        {
            return Engine.Map<TFrom, TTo>(source);
        }


        //Account Group
        public IEnumerable<AccountGroupModel> MapAccountGroups(IEnumerable<AccountGroup> group)
        {
            return MapList<IEnumerable<AccountGroup>, List<AccountGroupModel>>(group);
        }
        public AccountGroup MapAccountGroup(AccountGroupModel groupModel)
        {
            return Map<AccountGroupModel, AccountGroup>(groupModel);
        }
        public AccountGroupModel MapAccountGroupModel(AccountGroup group)
        {
            return Map<AccountGroup, AccountGroupModel>(group);
        }


        //Accounts
        public IEnumerable<AccountModel> MapAccounts(IEnumerable<Account> accounts)
        {
            return MapList<IEnumerable<Account>, List<AccountModel>>(accounts);
        }

        public AccountModel MapAccountModel(Account account)
        {
            return Map<Account, AccountModel>(account);
        }

        public Account MapAccount(AccountModel accountModel)
        {
            return Map<AccountModel, Account>(accountModel);
        }


        //Users
        public IEnumerable<UserModel> MapUsers(IEnumerable<User> users)
        {
            return MapList<IEnumerable<User>, List<UserModel>>(users);
        }

        public UserModel MapUserModel(User user)
        {
            return Map<User, UserModel>(user);
        }

        public User MapUser(UserModel userModel)
        {
            return Map<UserModel, User>(userModel);
        }


        //Patients
        public IEnumerable<PatientModel> MapPatients(IEnumerable<Patient> patients)
        {
            return MapList<IEnumerable<Patient>, List<PatientModel>>(patients);
        }

        public PatientModel MapPatientModel(Patient patient)
        {
            return Map<Patient, PatientModel>(patient);
        }

        public Patient MapPatient(PatientModel patientModel)
        {
            return Map<PatientModel, Patient>(patientModel);
        }

        public Patient MapCheckDuplicatePatientModel(CheckDuplicatePatientModel model)
        {
            return Map<CheckDuplicatePatientModel, Patient>(model);
        }


        //Facilities
        public IEnumerable<FacilityModel> MapFacilities(IEnumerable<Facility> facilities)
        {
            return MapList<IEnumerable<Facility>, List<FacilityModel>>(facilities);
        }

        public FacilityModel MapFacilityModel(Facility facility)
        {
            return Map<Facility, FacilityModel>(facility);
        }

        public Facility MapFacility(FacilityModel facilityModel)
        {
            return Map<FacilityModel, Facility>(facilityModel);
        }


        //Companies
        public IEnumerable<CompanyModel> MapCompanies(IEnumerable<Company> companies)
        {
            return MapList<IEnumerable<Company>, List<CompanyModel>>(companies);
        }

        public CompanyModel MapCompanyModel(Company company)
        {
            return Map<Company, CompanyModel>(company);
        }

        public Company MapCompany(CompanyModel companyModel)
        {
            return Map<CompanyModel, Company>(companyModel);
        }


        //Products
        public IEnumerable<ProductModel> MapProducts(IEnumerable<Product> products)
        {
            return MapList<IEnumerable<Product>, List<ProductModel>>(products);
        }

        public ProductModel MapProductModel(Product product)
        {
            return Map<Product, ProductModel>(product);
        }

        public Product MapProduct(ProductModel productModel)
        {
            return Map<ProductModel, Product>(productModel);
        }


        //Product Price Sheets
        public IEnumerable<ProductPriceSheetModel> MapToProductPriceSheetModels(IEnumerable<ProductPriceSheet> productPriceSheets)
        {
            return MapList<IEnumerable<ProductPriceSheet>, List<ProductPriceSheetModel>>(productPriceSheets);
        }

        public ProductPriceSheetModel MapToProductPriceSheetModel(ProductPriceSheet productPriceSheet)
        {
            return Map<ProductPriceSheet, ProductPriceSheetModel>(productPriceSheet);
        }

        public ProductPriceSheet MapToProductPriceSheet(ProductPriceSheetModel productPriceSheetModel)
        {
            return Map<ProductPriceSheetModel, ProductPriceSheet>(productPriceSheetModel);
        }


        //Product Price Sheet Items
        public IEnumerable<ProductPriceSheetItemModel> MapToPriceSheetItemModels(IEnumerable<ProductPriceSheetItem> priceSheetItems)
        {
            return MapList<IEnumerable<ProductPriceSheetItem>, List<ProductPriceSheetItemModel>>(priceSheetItems);
        }

        public ProductPriceSheetItemModel MapToPriceSheetItemModel(ProductPriceSheetItem priceSheetItem)
        {
            return Map<ProductPriceSheetItem, ProductPriceSheetItemModel>(priceSheetItem);
        }

        public ProductPriceSheetItem MapToPriceSheetItem(ProductPriceSheetItemModel priceSheetItemModel)
        {
            return Map<ProductPriceSheetItemModel, ProductPriceSheetItem>(priceSheetItemModel);
        }


        //Product Categories
        public IEnumerable<ProductCategoryModel> MapProductCategories(IEnumerable<ProductCategory> productCategories)
        {
            return MapList<IEnumerable<ProductCategory>, List<ProductCategoryModel>>(productCategories);
        }

        public ProductCategoryModel MapProductCategoryModel(ProductCategory productCategory)
        {
            return Map<ProductCategory, ProductCategoryModel>(productCategory);
        }

        public ProductCategory MapProductCategory(ProductCategoryModel productCategoryModel)
        {
            return Map<ProductCategoryModel, ProductCategory>(productCategoryModel);
        }

        //Logs
        public IEnumerable<LogModel> MapLogs(IEnumerable<Log> logs)
        {
            return MapList<IEnumerable<Log>, List<LogModel>>(logs);
        }

        public LogModel MapLog(Log log)
        {
            return Map<Log, LogModel>(log);
        }

        public Log MapLogModel(LogModel logModel)
        {
            return Map<LogModel, Log>(logModel);
        }

        public IEnumerable<OrderModel> MapOrders(IEnumerable<Order> orders)
        {
            return MapList<IEnumerable<Order>, List<OrderModel>>(orders);
        }

        public OrderModel MapOrderModel(Order order)
        {
            return Map<Order, OrderModel>(order);
        }

        public Order MapOrder(OrderModel orderModel)
        {
            return Map<OrderModel, Order>(orderModel);
        }

        public IEnumerable<OrderLineItemModel> MapOrderLineItems(IEnumerable<OrderLineItem> orderLineItems)
        {
            return MapList<IEnumerable<OrderLineItem>, List<OrderLineItemModel>>(orderLineItems);
        }

        public OrderLineItemModel MapOrderLineItemModel(OrderLineItem orderLineItem)
        {
            return Map<OrderLineItem, OrderLineItemModel>(orderLineItem);
        }

        public OrderLineItem MapOrderLineItem(OrderLineItemModel orderLineItemModel)
        {
            return Map<OrderLineItemModel, OrderLineItem>(orderLineItemModel);
        }

        #endregion

        #region Methods

        protected virtual ConfigurationStore GetConfiguration()
        {
            return new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);
        }

        protected virtual MappingEngine GetEngine()
        {
            RegisterMappings();
            return new MappingEngine(Configuration);
        }

        protected abstract void RegisterMappings();

        #endregion












    }
}