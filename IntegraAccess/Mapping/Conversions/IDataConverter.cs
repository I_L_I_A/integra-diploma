﻿using System.Collections.Generic;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Conversions
{
    public interface IDataConverter
    {
        #region Public Methods and Operators

        TTo Map<TFrom, TTo>(TFrom source);

        TTo MapList<TFrom, TTo>(TFrom source);

        TTo Map<TFrom, TTo>(TFrom source, TTo destination);

        //Account Group
        IEnumerable<AccountGroupModel> MapAccountGroups(IEnumerable<AccountGroup> group);
        AccountGroup MapAccountGroup(AccountGroupModel groupModel);
        AccountGroupModel MapAccountGroupModel(AccountGroup group);

        #endregion
        //accounts
        IEnumerable<AccountModel> MapAccounts(IEnumerable<Account> accounts);
        AccountModel MapAccountModel(Account account);
        Account MapAccount(AccountModel accountModel);

        //users
        IEnumerable<UserModel> MapUsers(IEnumerable<User> accounts);
        UserModel MapUserModel(User account);
        User MapUser(UserModel accountModel);

        //patients
        IEnumerable<PatientModel> MapPatients(IEnumerable<Patient> patients);
        PatientModel MapPatientModel(Patient patient);
        Patient MapPatient(PatientModel patientModel);
        Patient MapCheckDuplicatePatientModel(CheckDuplicatePatientModel patientModel);

        //facilities
        IEnumerable<FacilityModel> MapFacilities(IEnumerable<Facility> facilities);
        FacilityModel MapFacilityModel(Facility facility);
        Facility MapFacility(FacilityModel facilityModel);

        //companies
        IEnumerable<CompanyModel> MapCompanies(IEnumerable<Company> companies);
        CompanyModel MapCompanyModel(Company company);
        Company MapCompany(CompanyModel companyModel);

        //products
        IEnumerable<ProductModel> MapProducts(IEnumerable<Product> products);
        ProductModel MapProductModel(Product product);
        Product MapProduct(ProductModel productModel);

        //product price sheets
        IEnumerable<ProductPriceSheetModel> MapToProductPriceSheetModels(IEnumerable<ProductPriceSheet> productPriceSheets);
        ProductPriceSheetModel MapToProductPriceSheetModel(ProductPriceSheet productPriceSheet);
        ProductPriceSheet MapToProductPriceSheet(ProductPriceSheetModel productPriceSheetModel);

        //product price sheet items
        IEnumerable<ProductPriceSheetItemModel> MapToPriceSheetItemModels(IEnumerable<ProductPriceSheetItem> priceSheetItems);
        ProductPriceSheetItemModel MapToPriceSheetItemModel(ProductPriceSheetItem priceSheetItem);
        ProductPriceSheetItem MapToPriceSheetItem(ProductPriceSheetItemModel priceSheetItemModel);

        //product categories
        IEnumerable<ProductCategoryModel> MapProductCategories(IEnumerable<ProductCategory> productCategories);
        ProductCategoryModel MapProductCategoryModel(ProductCategory productCategory);
        ProductCategory MapProductCategory(ProductCategoryModel productCategoryModel);

        //product categories
        IEnumerable<LogModel> MapLogs(IEnumerable<Log> logs);
        LogModel MapLog(Log log);
        Log MapLogModel(LogModel logModel);

        //orders
        IEnumerable<OrderModel> MapOrders(IEnumerable<Order> orders);
        OrderModel MapOrderModel(Order order);
        Order MapOrder(OrderModel orderModel);

        //order line items
        IEnumerable<OrderLineItemModel> MapOrderLineItems(IEnumerable<OrderLineItem> orderLineItems);
        OrderLineItemModel MapOrderLineItemModel(OrderLineItem orderLineItem);
        OrderLineItem MapOrderLineItem(OrderLineItemModel orderLineItemModel);
    }
}