﻿using IntegraAccess.Mapping.Profiles;

namespace IntegraAccess.Mapping.Conversions
{
    public class ViewModelConverter : DataConverter
    {
        protected override void RegisterMappings()
        {
            Configuration.AddProfile(new AccountGroupProfile(this));
            Configuration.AddProfile(new AccountProfile(this));
            Configuration.AddProfile(new UserProfile(this));
            Configuration.AddProfile(new PatientProfile(this));
            Configuration.AddProfile(new CompanyProfile(this));
            Configuration.AddProfile(new FacilityProfile(this));
            Configuration.AddProfile(new LogProfile(this));
            Configuration.AddProfile(new FacilityAccountsProfile(this));
            Configuration.AddProfile(new UserAccountsProfile(this));
            Configuration.AddProfile(new ProductProfile(this));
            Configuration.AddProfile(new ProductsToCategoriesProfile(this));
            Configuration.AddProfile(new ProductPriceSheetProfile(this));
            Configuration.AddProfile(new ProductPriceSheetItemProfile(this));
            Configuration.AddProfile(new ProductCategoryProfile(this));
            Configuration.AddProfile(new OrderLineItemProfile(this));
            Configuration.AddProfile(new OrderProfile(this));
            Configuration.AddProfile(new ProductTemplateItemProfile(this));
        }
    }
}