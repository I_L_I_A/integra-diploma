﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class UserAccountsProfile : CommonDataProfile
    {
        public UserAccountsProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<UserAccounts, UserAccountsModel>()
                .ForMember(dest => dest.AccountName, opt => opt.MapFrom(src => src.Account.Name))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.Login));

            CreateMap<UserAccountsModel, UserAccounts>();
        }
    }
}