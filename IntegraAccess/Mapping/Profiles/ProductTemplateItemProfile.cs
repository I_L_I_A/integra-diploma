﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class ProductTemplateItemProfile : CommonDataProfile
    {
        public ProductTemplateItemProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<ProductTemplateItem, ProductTemplateItemModel>()
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.TemplateId, opt => opt.MapFrom(src => src.ProductId))
                .ForMember(dest => dest.TemplateName, opt => opt.MapFrom(src => src.Product.DisplayName))
                .ForMember(dest => dest.IsTemplateActive, opt => opt.MapFrom(src => src.Product.IsActive));

            CreateMap<ProductTemplateItemModel, ProductTemplateItem>()
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.TemplateId));
        }
    }
}