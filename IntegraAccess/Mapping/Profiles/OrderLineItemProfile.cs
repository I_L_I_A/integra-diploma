﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class OrderLineItemProfile : CommonDataProfile
    {
        public OrderLineItemProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<OrderLineItem, OrderLineItemModel>()
                .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Product))
                .ForMember(dest => dest.PurchaseTypeName, opt => opt.MapFrom(src => src.PurchaseType.Name));


            CreateMap<OrderLineItemModel, OrderLineItem>()
                .ForMember(dest => dest.Product, opt => opt.Ignore());
        }
    }
}