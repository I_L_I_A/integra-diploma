﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class AccountProfile : CommonDataProfile
    {
        public AccountProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<Account, AccountModel>()
                .ForMember(dest => dest.AccountGroupName, opt => opt.MapFrom(src => src.AccountGroup.Name))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Company.ExternalCode))
                .ForMember(dest => dest.UserAccounts, opt => opt.MapFrom(src => src.UserAccounts))
                .ForMember(dest => dest.FacilityAccounts, opt => opt.MapFrom(src => src.FacilityAccounts));

            CreateMap<AccountModel, Account>()
            .ForMember(dest => dest.UserAccounts, opt => opt.Ignore())
            .ForMember(dest => dest.FacilityAccounts, opt => opt.Ignore());
        }
    }
}