﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class AccountGroupProfile : CommonDataProfile
    {
        public AccountGroupProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<AccountGroup, AccountGroupModel>();

            CreateMap<AccountGroupModel, AccountGroup>();
        }
    }
}