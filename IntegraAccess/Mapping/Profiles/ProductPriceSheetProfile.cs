﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class ProductPriceSheetProfile : CommonDataProfile
    {
        public ProductPriceSheetProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<ProductPriceSheet, ProductPriceSheetModel>()
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name));

            CreateMap<ProductPriceSheetModel, ProductPriceSheet>();
        }
    }
}