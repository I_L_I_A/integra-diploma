﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class ProductCategoryProfile : CommonDataProfile
    {
        public ProductCategoryProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<ProductCategory, ProductCategoryModel>();
            CreateMap<ProductCategoryModel, ProductCategory>();
        }
    }
}