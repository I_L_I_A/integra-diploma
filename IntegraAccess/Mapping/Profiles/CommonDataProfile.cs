﻿using AutoMapper;
using IntegraAccess.Mapping.Conversions;

namespace IntegraAccess.Mapping.Profiles
{
    public abstract class CommonDataProfile : Profile
    {
        #region Constructors and Destructors

        protected CommonDataProfile(IDataConverter converter)
        {
            Converter = converter;
        }

        #endregion

        #region Properties

        protected IDataConverter Converter { get; private set; }

        public override string ProfileName
        {
            get
            {
                return GetType().FullName;
            }
        }

        #endregion
    }
}