﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class ProductProfile : CommonDataProfile
    {
        public ProductProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<Product, ProductModel>()
                .ForMember(dest => dest.HasImage, opt => opt.MapFrom(src => src.ImageName != null))
                .ForMember(dest => dest.ProductPriceSheetItems, opt => opt.MapFrom(src => src.ProductPriceSheetItems))
                .ForMember(dest => dest.ProductsToCategories, opt => opt.MapFrom(src => src.ProductsToCategories));

            CreateMap<ProductModel, Product>()
                .ForMember(dest => dest.ProductPriceSheetItems, opt => opt.Ignore())
                .ForMember(dest => dest.ProductsToCategories, opt => opt.Ignore());
        }
    }
}