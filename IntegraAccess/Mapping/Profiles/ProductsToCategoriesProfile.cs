﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class ProductsToCategoriesProfile : CommonDataProfile
    {
        public ProductsToCategoriesProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<ProductsToCategories, ProductsToCategoriesModel>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
                .ForMember(dest => dest.IsCategoryActive, opt => opt.MapFrom(src => src.Category.IsActive))
                .ForMember(dest => dest.IsProductActive, opt => opt.MapFrom(src => src.Product.IsActive))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.DisplayName));

            CreateMap<ProductsToCategoriesModel, ProductsToCategories>();
        }
    }
}