﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class FacilityAccountsProfile : CommonDataProfile
    {
        public FacilityAccountsProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<FacilityAccounts, FacilityAccountsModel>()
                .ForMember(dest => dest.AccountName, opt => opt.MapFrom(src => src.Account.Name))
                .ForMember(dest => dest.FacilityName, opt => opt.MapFrom(src => src.Facility.Name))
                .ForMember(dest => dest.IsFacilityActive, opt => opt.MapFrom(src => src.Facility.IsActive))
                .ForMember(dest => dest.IsAccountActive, opt => opt.MapFrom(src => src.Account.IsActive));

            CreateMap<FacilityAccountsModel, FacilityAccounts>();
        }
    }
}