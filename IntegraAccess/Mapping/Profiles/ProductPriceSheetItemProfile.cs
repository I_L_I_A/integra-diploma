﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class ProductPriceSheetItemProfile : CommonDataProfile
    {
        public ProductPriceSheetItemProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<ProductPriceSheetItem, ProductPriceSheetItemModel>()
                .ForMember(dest => dest.PriceSheetId, opt => opt.MapFrom(src => src.ProductPriceSheetId))
                .ForMember(dest => dest.PriceSheetName, opt => opt.MapFrom(src => src.ProductPriceSheet.Name))
                .ForMember(dest => dest.CompanyId, opt => opt.MapFrom(src => src.ProductPriceSheet.Company.Id))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.ProductPriceSheet.Company.Name))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.DisplayName))
                .ForMember(dest => dest.IsTemplate, opt => opt.MapFrom(src => src.Product.IsTemplate))
                .ForMember(dest => dest.ProductShortDescription, opt => opt.MapFrom(src => src.Product.ShortDescription))
                .ForMember(dest => dest.ProductShortDescription2, opt => opt.MapFrom(src => src.Product.ShortDescription2));

            CreateMap<ProductPriceSheetModel, ProductPriceSheet>();
        }
    }
}