﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class LogProfile : CommonDataProfile
    {
        public LogProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<Log, LogModel>();
            CreateMap<LogModel, Log>();
        }
    }
}