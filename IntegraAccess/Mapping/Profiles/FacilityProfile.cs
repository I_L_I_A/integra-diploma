﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class FacilityProfile : CommonDataProfile
    {
        public FacilityProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<Facility, FacilityModel>()
                .ForMember(dest => dest.AccountGroupName, opt => opt.MapFrom(src => src.AccountGroup.Name))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.FacilityAccounts, opt => opt.MapFrom(src => src.FacilityAccounts));

            CreateMap<FacilityModel, Facility>()
            .ForMember(dest => dest.FacilityAccounts, opt => opt.Ignore());
        }
    }
}