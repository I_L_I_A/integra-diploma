﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class OrderProfile : CommonDataProfile
    {
        public OrderProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<Order, OrderModel>()
                .ForMember(dest => dest.AccountName, opt => opt.MapFrom(src => src.Account.Name))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.AccountGroupId, opt => opt.MapFrom(src => src.Account.AccountGroupId))
                .ForMember(dest => dest.AccountGroupName, opt => opt.MapFrom(src => src.Account.AccountGroup.Name))
                .ForMember(dest => dest.CreatedByName, opt => opt.MapFrom(src => string.Format("{0}, {1}", src.CreatedBy.LastName, src.CreatedBy.FirstName)))
                .ForMember(dest => dest.SubmittedByName, opt => opt.MapFrom(src => string.Format("{0}, {1}", src.SubmittedBy.LastName, src.SubmittedBy.FirstName)))
                .ForMember(dest => dest.OrderTypeName, opt => opt.MapFrom(src => src.OrderType.Name))
                .ForMember(dest => dest.OrderStateName, opt => opt.MapFrom(src => src.OrderState.Name))
                .ForMember(dest => dest.OrderLineItems, opt => opt.MapFrom(src => src.OrderLineItems))
                .ForMember(dest => dest.Patient, opt => opt.MapFrom(src => new PatientModel()
                {
                    Id = src.PatientId.HasValue ? src.PatientId.Value : 0,
                    AccountId = src.PatientAccountId,
                    FacilityId = src.PatientFacilityId,
                    ExternalCode = src.PatientExternalCode,
                    ExternalEMRCode = src.PatientExternalEMRCode,
                    FirstName = src.PatientFirstName,
                    LastName = src.PatientLastName,
                    BirthDate = src.PatientBirthDate,
                    Gender = src.PatientGender,
                    Street1 = src.PatientStreet1,
                    Street2 = src.PatientStreet2,
                    City = src.PatientCity,
                    State = src.PatientState,
                    Zip = src.PatientZip,
                    Phone = src.PatientPhone,
                    Email = src.PatientEmail,
                    ContactName = src.PatientContactFirstName,
                    ContactPhone = src.PatientContactPhone,
                    PhysicianFirstName = src.PatientPhysicianFirstName,
                    PhysicianLastName = src.PatientPhysicianLastName,
                    IsDeceased = src.PatientIsDeceased,
                    DeceasedDate = src.PatientDeceasedDate,
                    FacilityName = src.PatientFacility.Name
                }));


            CreateMap<OrderModel, Order>()
                 .ForMember(dest => dest.OrderLineItems, opt => opt.Ignore());
        }
    }
}