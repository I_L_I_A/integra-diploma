﻿using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Mapping.Profiles
{
    public class PatientProfile : CommonDataProfile
    {
        public PatientProfile(IDataConverter converter)
            : base(converter)
        {
        }

        protected override void Configure()
        {
            base.Configure();

            //From Patient to PatientModel
            CreateMap<Patient, PatientModel>()
                .ForMember(dest => dest.AccountGroupName, opt => opt.MapFrom(src => src.AccountGroup.Name))
                .ForMember(dest => dest.AccountName, opt => opt.MapFrom(src => src.Account.Name))
                .ForMember(dest => dest.FacilityName, opt => opt.MapFrom(src => src.Facility.Name))
                .ForMember(dest => dest.LastAccountName, opt => opt.MapFrom(src => src.LastAccount.Name));

            //From PatientModel to Patient
            CreateMap<PatientModel, Patient>();
            //From CheckDuplicatePatientModel to Patient
            CreateMap<CheckDuplicatePatientModel, Patient>();
        }
    }
}