﻿using System.Web.Http;
using IntegraAccess.Common.ActionFilters.GZip;
using IntegraAccess.Mapping.Conversions;

namespace IntegraAccess.Controllers
{
    [GZipCompression]
    public class BaseApiController : ApiController
    {
        protected readonly IDataConverter _mapper;

        public BaseApiController(IDataConverter mapper)
        {
            _mapper = mapper;
        }
    }
}
