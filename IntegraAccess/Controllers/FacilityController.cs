﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Infrastructure;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class FacilityController : BaseApiController
    {
        protected readonly IFacilityService _facilityService;
        protected readonly IAccountService _accountService;
        protected readonly ILogService _logService;
        private const string DefaultSortColumn = "Name";

        public FacilityController(IFacilityService facilityService,IAccountService accountService, IDataConverter mapper, ILogService logService)
            : base(mapper)
        {
            _facilityService = facilityService;
            _accountService = accountService;
            _logService = logService;
        }

        [HttpGet]
        public IEnumerable<FacilityModel> Get()
        {
            var facilities = _facilityService.GetAll();
            return _mapper.MapFacilities(facilities);
        }

        [HttpGet]
        [Route("api/facilities/getPagedFacilities")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public IHttpActionResult GetPagedFacilities([FromUri] TableParamsModel tableParams, [FromUri] int? accountGroupId = null, [FromUri] int? accountId = null, [FromUri] bool onlyConnected = true)
        {
            var facilities = _facilityService.GetAll();

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                facilities = facilities.Where(x =>
                    x.Name.ToLower().Contains(tableParams.Search));
            }
            //restricted by Account Group
            if (accountGroupId.HasValue)
            {
                facilities = facilities.Where(x => x.AccountGroupId == accountGroupId.Value);
            }
            //restricted by Account and Company
            if (accountId.HasValue)
            {
                var account = _accountService.GetById(accountId.Value);
                facilities = facilities
                    .Where(x=>x.CompanyId.HasValue && account.CompanyId.HasValue && x.CompanyId.Value == account.CompanyId.Value)
                    .Where(x => x.AccountGroup.Accounts.Any(a => a.Id == accountId.Value));
                if (onlyConnected)
                {
                    facilities = facilities.Where(x => x.FacilityAccounts.Any(fa => fa.AccountId == accountId.Value && fa.IsActive));
                }
            }

            //include inactive
            if (!tableParams.IncludeInactive)
            {
                facilities = facilities.Where(ac => ac.IsActive == true);
            }
            var facilitiesPaged = facilities.OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                     ? DefaultSortColumn
                     : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                 .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = facilities.Count(),
                items = _mapper.MapFacilities(facilitiesPaged)
            });
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public FacilityModel Get(int id, [FromUri] int? accountGroupId = null)
        {
            var facility = _facilityService.GetById(id);
            if (facility != null && accountGroupId.HasValue && facility.AccountGroupId != accountGroupId.Value)
            {
                return null;
            }
            return facility != null ? _mapper.MapFacilityModel(facility) : null;
        }

        [HttpPost]
        [Authorize(Roles = "Supplier")]
        public IHttpActionResult Post(FacilityModel facilityModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var facility = _mapper.MapFacility(facilityModel);
                facility.SetUpdated();
                facility.IsActive = true;
                _facilityService.Add(facility);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        public IHttpActionResult Put(int id, [FromBody]FacilityModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var facility = _facilityService.GetById(model.Id);
            if (facility != null)
            {
                try
                {
                    facility = _mapper.Map(model, facility);
                    facility.SetUpdated();
                    _facilityService.Update(facility);
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        [Route("api/facilities/inactivate")]
        public IHttpActionResult InactivateFacilities([FromBody]List<int> facilitiesIds)
        {
            if (facilitiesIds.Any())
            {
                try
                {
                    _facilityService.Inactivate(facilitiesIds);
                    _facilityService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }


        // DELETE: api/AccoutGroup/5
        public void Delete(int id)
        {
        }
    }
}
