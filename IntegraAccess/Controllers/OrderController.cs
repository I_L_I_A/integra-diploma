﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Security;
using IntegraAccess.Business.Infrastructure.StateMachines;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;
using IntegraAccess.Infrastructure;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;
using Microsoft.AspNet.Identity;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class OrderController : BaseApiController
    {
        protected readonly IOrderService _orderService;
        protected readonly IUserService _userService;
        protected readonly IPatientService _patientService;
        protected readonly IAccountService _accountService;
        protected readonly ILogService _logService;
        private const string DefaultSortColumn = "Id";

        public OrderController(IOrderService orderService, IUserService userService, IAccountService accountService, IPatientService patientService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _patientService = patientService;
            _accountService = accountService;
            _userService = userService;
            _orderService = orderService;
            _logService = logService;
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IEnumerable<OrderModel> Get(int? accountGroupId = null, [FromUri] string userId = null)
        {
            var orders = _orderService.GetAll();
            //restricted by account group
            if (accountGroupId.HasValue)
            {
                orders = orders.Where(x => x.Account.AccountGroupId == accountGroupId.Value);
            }
            //restricted by user
            if (!string.IsNullOrEmpty(userId))
            {
                orders = orders.Where(x => x.Account.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive));
            }
            return _mapper.MapOrders(orders);
        }

        [HttpGet]
        [Route("api/orders/getPaged")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IHttpActionResult GetPagedOrders([FromUri] TableParamsModel tableParams, int? accountGroupId = null, [FromUri] string userId = null)
        {
            var orders = _orderService.GetAll();
            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                orders = orders.Where(x =>
                    x.Id.ToString().Contains(tableParams.Search) ||
                    x.ExternalOrderNumber.ToLower().Contains(tableParams.Search) ||
                    x.PONumber.ToLower().Contains(tableParams.Search));
            }
            //restricted by account group
            if (accountGroupId.HasValue)
            {
                orders = orders.Where(x => x.Account.AccountGroupId == accountGroupId.Value);
            }
            //restricted by user
            if (!string.IsNullOrEmpty(userId))
            {
                orders = orders.Where(x => x.Account.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive));
            }
            orders = OrderStateMachine.FilterByStates(orders, _userService.FindUserById(User.Identity.GetUserId()));

            var ordersPaged = orders.OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                   ? DefaultSortColumn
                   : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
               .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = orders.Count(),
                items = _mapper.MapOrders(ordersPaged)
            });
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public OrderModel Get(int id, int? accountGroupId = null, [FromUri] string userId = null)
        {
            var order = _orderService.GetById(id);
            //restricted by account group
            if (order == null || accountGroupId.HasValue && order.Account.AccountGroupId != accountGroupId.Value)
            {
                return null;
            }
            //restricted by user
            if (!string.IsNullOrEmpty(userId) && order.Account != null && !order.Account.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive))
            {
                return null;
            }
            var orderModel = _mapper.MapOrderModel(order);
            foreach (var orderLineItem in orderModel.OrderLineItems)
            {
                orderLineItem.Product.ImageUrl = Url.Route("ExtendedApi", new { controller = "product", action = "getImage", id = orderLineItem.ProductId });
            }
            return orderModel;
        }

        [HttpPost]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "userAccountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IHttpActionResult Post([FromBody]OrderModel model, bool isSubmitted = false, int? userAccountGroupId = null, string userId = null)
        {
            if (userAccountGroupId.HasValue && userAccountGroupId.Value != model.AccountGroupId)
            {
                ModelState.AddModelError(string.Empty, "No permissions to create orders for selected Account Group");
            }
            if (!model.AccountId.HasValue)
            {
                ModelState.AddModelError(string.Empty, "Account is required for the order");
            }
            if (!string.IsNullOrEmpty(userId) && model.AccountId.HasValue && !_accountService.HasAccessToAccount(userId, model.AccountId.Value))
            {
                ModelState.AddModelError(string.Empty, "No permissions to create orders for selected Account");
            }
            if (model.Patient == null)
            {
                ModelState.AddModelError(string.Empty, "Patient is required for the order");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var order = CreateOrder(model, isSubmitted);
                order.EntityState = State.Unchanged;

                //connect patient to Order
                if (!order.PatientId.HasValue)
                {
                    var patient = _mapper.MapPatient(model.Patient);
                    patient.IsActive = true;
                    _patientService.Add(patient);
                    order.PatientId = patient.Id;
                }
                //connect order line items
                MapOrderLineItems(model, order);
                order.EntityState = State.Modified;
                _orderService.SaveChanges();

                return Ok(new { id = order.Id });
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "userAccountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IHttpActionResult Put([FromBody] OrderModel model, bool isSubmitted = false,
            int? userAccountGroupId = null, string userId = null)
        {
            if (userAccountGroupId.HasValue && userAccountGroupId.Value != model.AccountGroupId)
            {
                ModelState.AddModelError(string.Empty, "No permissions to edit orders for selected Account Group");
            }
            if (!model.AccountId.HasValue)
            {
                ModelState.AddModelError(string.Empty, "Account is required for the order");
            }
            if (!string.IsNullOrEmpty(userId) && model.AccountId.HasValue &&
                !_accountService.HasAccessToAccount(userId, model.AccountId.Value))
            {
                ModelState.AddModelError(string.Empty, "No permissions to edit orders for selected Account");
            }
            if (model.Patient == null)
            {
                ModelState.AddModelError(string.Empty, "Patient is required for the order");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var order = _orderService.GetById(model.Id);
            try
            {
                //update order
                order = _mapper.Map(model, order);
                order.SetUpdated();
                order.OrderLineItems.ToList().ForEach(s => s.EntityState = State.Deleted);
                _orderService.Update(order);

                //connect order line items
                order.EntityState = State.Unchanged;
                MapOrderLineItems(model, order);
                order.EntityState = State.Modified;
                _orderService.SaveChanges();

                return Ok(new { id = order.Id });
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        private void MapOrderLineItems(OrderModel model, Order order)
        {
            foreach (var lineItemModel in model.OrderLineItems)
            {
                var lineItem = _mapper.MapOrderLineItem(lineItemModel);
                lineItem.OrderId = order.Id;
                lineItem.SetUpdated();
                order.OrderLineItems.Add(lineItem);
            }
        }

        private Order CreateOrder(OrderModel model, bool isSubmitted)
        {
            var order = _mapper.MapOrder(model);
            order.OrderDate = DateTime.UtcNow;
            order.CreatedById = User.Identity.GetUserId();
            order.UpdatedDateTime = DateTime.UtcNow;
            order.OrderStateEnumValue = isSubmitted ? GetOrderInitStateBasedOnUserPermissions() : OrderStates.Saved;
            if (isSubmitted && (order.OrderStateEnumValue == OrderStates.DMEAccepted || order.OrderStateEnumValue == OrderStates.DMEApprovalRequired))
            {
                order.SubmittedById = order.CreatedById;
                order.SubmittedAt = DateTime.UtcNow;
            }
            order.OrderLineItems = new List<OrderLineItem>();
            _orderService.Add(order);
            return order;
        }

        private OrderStates GetOrderInitStateBasedOnUserPermissions()
        {
            var principal = RequestContext.Principal as ClaimsPrincipal;
            if (principal != null)
            {
                var orderApprovalClaim = principal.Claims.FirstOrDefault(x => x.Type == "OrderApprovalOverride");
                var isOrderApprovalOverride = orderApprovalClaim != null && orderApprovalClaim.Value == "1";
                return OrderStateMachine.GetInitStateForCreatedOrder(principal.IsInRole("Supplier"), isOrderApprovalOverride);
            }
            return OrderStates.Saved;
        }

        [HttpGet]
        [Route("api/orders/getPossibleOrderStateActions")]
        public IHttpActionResult GetPossibleOrderStateActions(int id)
        {
            var order = _orderService.GetById(id);
            if (order != null)
            {
                var user = _userService.FindUserById(User.Identity.GetUserId());
                var actions = new OrderStateMachine(order, user).GetPossibleOrderStateActions();
                return Ok(new
                {
                    actions = actions.Where(x => x.OrderState != order.OrderStateEnumValue),
                    hasUserAccessToOrder = OrderStateMachine.HasUserAccessToOrder(user, order),
                    isOrderEditable = actions.Any(x => order.OrderStateEnumValue == x.OrderState),
                    order = _mapper.MapOrderModel(order)
                });
            }
            ModelState.AddModelError(null, "Order do not exist");
            return BadRequest(ModelState);
        }

        [HttpPut]
        [Route("api/orders/setState")]
        public async Task<IHttpActionResult> SetState([FromBody]OrderModel model, OrderStates state)
        {
            var order = _orderService.GetById(model.Id);
            var user = _userService.FindUserById(User.Identity.GetUserId());
            if (order != null)
            {
                try
                {
                    var orderStateMachine = new OrderStateMachine(order, user);
                    if (orderStateMachine.HasUserAccessToOrder() && orderStateMachine.SetState(state))
                    {
                        order.SetUpdated();
                        order.UpdatedById = User.Identity.GetUserId();
                        _orderService.Update(order);
                        await Task.Run(() => SendNotificationAsync(state, order));
                    }
                    else
                    {
                        ModelState.AddModelError(null, string.Format("User {0} hasn't permissions to update state for this order", user.Login));
                    }
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                ModelState.AddModelError(null, string.Format("Order do not exist", user.Login));
            }
            if (ModelState.IsValid && order != null && order.OrderStateId.HasValue)
            {
                return Ok(new { state = order.OrderStateId, name = order.OrderState.Name });
            }
            return BadRequest(ModelState);
        }

        private async Task SendNotificationAsync(OrderStates state, Order order)
        {
            try
            {
                if (state == OrderStates.DMEApprovalRequired)
                {
                    var emails = _userService.GetAllActiveSuppliers()
                        .Where(x => x.ReceiveApprovalNeededEmail).Select(x => x.Email).ToList();
                    _orderService.SendRequiredApprovalMessage(emails, order, GetViewOrderUrl(order.Id));
                }
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
            }
        }

        private string GetViewOrderUrl(int orderId)
        {
            var uri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, String.Empty));
            return string.Format("{0}#/app/orders/view/{1}", uri, orderId);
        }

        public void Delete(int id)
        {
        }
    }
}
