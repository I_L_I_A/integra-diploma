﻿using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using IntegraAccess.DataAccess.Context;
using IntegraAccess.Entities.Entities;

namespace IntegraAccess.Controllers
{
    public class AuxDBController : ApiController
    {
        private AuxDBContext _dbContext;

        public AuxDBController(AuxDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("api/auxDB/runProcedure")]
        public IHttpActionResult RunProcedure([FromUri] string procedureName)
        {

            using (_dbContext)
            {
                //the model must have the same columns as Procedure result
                var result1 = _dbContext.Database.SqlQuery<PurchaseType>("exec sp_get_all_purchase_types").ToList<PurchaseType>();

                ////with params to type
                var searhByName = new SqlParameter
                {
                    ParameterName = "Name",
                    Value = "Rental"
                };
                var result2 = _dbContext.Database.SqlQuery<PurchaseType>("exec sp_search_purchase_types @Name", searhByName).ToList<PurchaseType>();
            }
            return Ok();
        }
    }
}

