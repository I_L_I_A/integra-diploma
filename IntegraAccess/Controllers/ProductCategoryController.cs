﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class ProductCategoryController : BaseApiController
    {
        protected readonly IProductCategoryService _productCategoryService;
        protected readonly ILogService _logService;
        private const string DefaultSortColumn = "Name";

        public ProductCategoryController(IProductCategoryService productCategoryService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _productCategoryService = productCategoryService;
            _logService = logService;
        }

        [HttpGet]
        public IEnumerable<ProductCategoryModel> Get()
        {
            var productCategories = _productCategoryService.GetAll();
            return _mapper.MapProductCategories(productCategories);
        }

        [HttpGet]
        [Route("api/productCategories/getPaged")]
        public IHttpActionResult GetPagedProducts([FromUri] TableParamsModel tableParams)
        {
            var productCategories = _productCategoryService.GetAll();

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                productCategories = productCategories.Where(x =>
                    x.Name.ToLower().Contains(tableParams.Search));
            }
            //include inactive
            if (!tableParams.IncludeInactive)
            {
                productCategories = productCategories.Where(ac => ac.IsActive);
            }

            var productCategoriesPaged = productCategories
                .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                    ? DefaultSortColumn
                    : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = productCategories.Count(),
                items = _mapper.MapProductCategories(productCategoriesPaged)
            });
        }

        [HttpGet]
        public ProductCategoryModel Get(int id)
        {
            var productCategory = _productCategoryService.GetById(id);
            return productCategory != null ? _mapper.MapProductCategoryModel(productCategory) : null;
        }

        [HttpPost]
        public IHttpActionResult Post(ProductCategoryModel productCategoryModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var productCategory = _mapper.MapProductCategory(productCategoryModel);
                productCategory.IsActive = true;
                productCategory.SetUpdated();
                _productCategoryService.Add(productCategory);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Put(ProductCategoryModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productCategory = _productCategoryService.GetById(model.Id);
            if (productCategory != null)
            {
                try
                {
                    productCategory = _mapper.Map(model, productCategory);
                    productCategory.UpdatedDateTime = DateTime.UtcNow;
                    _productCategoryService.Update(productCategory);
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }

        [HttpPut]
        [Route("api/productCategories/inactivate")]
        public IHttpActionResult InactivateProductCategories([FromBody]List<int> productCategoryIds)
        {
            if (productCategoryIds.Any())
            {
                try
                {
                    _productCategoryService.Inactivate(productCategoryIds);
                    _productCategoryService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }

        public void Delete(int id)
        {
        }
    }
}