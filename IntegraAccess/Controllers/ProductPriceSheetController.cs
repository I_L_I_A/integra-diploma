﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class ProductPriceSheetController : BaseApiController
    {
        private const string DefaultSortColumn = "Name";
        protected readonly IPriceSheetService _priceSheetService;
        protected readonly IAccountService _accountService;
        protected readonly ILogService _logService;

        public ProductPriceSheetController(IPriceSheetService priceSheetService, IAccountService accountService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _priceSheetService = priceSheetService;
            _accountService = accountService;
            _logService = logService;
        }

        [HttpGet]
        public ProductPriceSheetModel Get(int id)
        {
            var priceSheet = _priceSheetService.GetById(id);
            if (priceSheet == null)
            {
                return new ProductPriceSheetModel();
            }
            return _mapper.MapToProductPriceSheetModel(priceSheet);

        }

        [HttpGet]
        [Route("api/priceSheets/getPagedPriceSheets")]
        public IHttpActionResult GetPagedPriceSheets([FromUri] TableParamsModel tableParams, int? companyId = null)
        {
            var priceSheets = _priceSheetService.GetAll();
            //filter by company
            if (companyId.HasValue)
            {
                priceSheets = priceSheets.Where(x => x.CompanyId == companyId.Value);
            }
            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                priceSheets = priceSheets.Where(x =>
                    x.Name.ToLower().Contains(tableParams.Search));
            }

            //include inactive
            if (!tableParams.IncludeInactive)
            {
                priceSheets = priceSheets.Where(ac => ac.IsActive);
            }

            var companyPriceSheetsPaged = priceSheets
               .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn) ? DefaultSortColumn : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
               .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = priceSheets.Count(),
                items = _mapper.MapToProductPriceSheetModels(companyPriceSheetsPaged)
            });
        }

        [HttpPut]
        public IHttpActionResult Put(ProductPriceSheetModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pricesheet = _priceSheetService.GetById(model.Id);
            if (pricesheet != null)
            {
                try
                {
                    pricesheet = _mapper.Map(model, pricesheet);
                    pricesheet.SetUpdated();
                    _priceSheetService.Update(pricesheet);
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }


        [HttpPut]
        [Authorize(Roles = "Supplier")]
        [Route("api/pricesheets/assignAccountsToPriceSheet/{id}")]
        public IHttpActionResult AssignAccountsToPriceSheet(int id, bool assign, [FromBody]List<int> accountsIds)
        {
            if (accountsIds == null || id == 0)
            {
                return BadRequest("Please check parameters");
            }

            try
            {
                _accountService.AssignAccountsToPriceSheet(id, accountsIds, assign);
                _accountService.SaveChanges();
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        [Route("api/pricesheets/inactivate")]
        public IHttpActionResult InactivatePriceSheets([FromBody]List<int> ids)
        {
            if (ids.Any())
            {
                try
                {
                    _priceSheetService.Inactivate(ids);
                    _priceSheetService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }
    }
}
