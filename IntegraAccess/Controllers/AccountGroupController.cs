﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Infrastructure;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class AccountGroupController : BaseApiController
    {
        protected readonly IAccountGroupService _accountGroupService;
        protected readonly ILogService _logService;
        const string DefaultSortColumn = "Name";

        public AccountGroupController(IAccountGroupService accountGroupService, IDataConverter mapper, ILogService logService)
            : base(mapper)
        {
            _accountGroupService = accountGroupService;
            _logService = logService;
        }
        // GET: api/AccoutGroup
        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public IEnumerable<AccountGroupModel> Get(int? accountGroupId = null)
        {
            var accountGroups = _accountGroupService.GetAll();
            if (accountGroupId.HasValue)
            {
                accountGroups = accountGroups.Where(x => x.Id == accountGroupId.Value);
            }
            return _mapper.MapAccountGroups(accountGroups);
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [Route("api/accountgroups/getPagedAccountGroups")]
        public IHttpActionResult GetPagedAccountGroups([FromUri] TableParamsModel tableParams, int? accountGroupId = null)
        {
            var accountGroups = _accountGroupService.GetAll();

            if (accountGroupId.HasValue)
            {
                accountGroups = accountGroups.Where(x => x.Id == accountGroupId.Value);
            }

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                accountGroups = accountGroups.Where(x =>
                    x.Name.ToLower().Contains(tableParams.Search) ||
                    x.ExternalCode.ToLower().Contains(tableParams.Search));
            }
            //include inactive
            if (!tableParams.IncludeInactive)
            {
                accountGroups = accountGroups.Where(ag => ag.IsActive == true);
            }

            var accountGroupsPaged = accountGroups
                .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                    ? DefaultSortColumn
                    : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = accountGroups.Count(),
                items = _mapper.MapAccountGroups(accountGroupsPaged)
            });
        }



        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "id")]
        public AccountGroupModel Get(int id)
        {
            var account = _accountGroupService.GetById(id);
            return account != null ? _mapper.MapAccountGroupModel(account) : null;
        }

        // POST: api/AccoutGroup
        [HttpPost]
        [Authorize(Roles = "Supplier")]
        public IHttpActionResult Post(AccountGroupModel accountGroupModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var accountGroup = _mapper.MapAccountGroup(accountGroupModel);
                accountGroup.SetUpdated();
                accountGroup.IsActive = true;
                _accountGroupService.Add(accountGroup);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // PUT: api/AccoutGroup/5
        [HttpPut]
        [Authorize(Roles = "Supplier")]
        public IHttpActionResult Put(int id, [FromBody]AccountGroupModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var accountGroup = _accountGroupService.GetById(model.Id);
            if (accountGroup != null)
            {
                try
                {
                    accountGroup = _mapper.Map(model, accountGroup);
                    accountGroup.SetUpdated();
                    _accountGroupService.Update(accountGroup);
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        [Route("api/accountgroups/inactivate")]
        public IHttpActionResult InactivateAccountGroups([FromBody]List<int> accountGroupsIds)
        {
            if (accountGroupsIds.Any())
            {
                try
                {
                    _accountGroupService.Inactivate(accountGroupsIds);
                    _accountGroupService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }

        // DELETE: api/AccoutGroup/5
        [HttpDelete]
        [Authorize(Roles = "Supplier")]
        public void Delete(int id)
        {
        }
    }
}
