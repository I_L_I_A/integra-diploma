﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class ProductPriceSheetItemController : BaseApiController
    {
        protected readonly IProductPriceSheetItemService _productPriceSheetItemService;
        protected readonly IProductTemplateItemService _productTemplateItemService;
        protected readonly IProductService _productService;
        protected readonly IAccountService _accountService;
        protected readonly ILogService _logService;
        const string defaultSortColumn = "Product.DisplayName";

        public ProductPriceSheetItemController(IProductPriceSheetItemService productPriceSheetItemService, IProductService productService, IProductTemplateItemService productTemplateItemService, IAccountService accountService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _productPriceSheetItemService = productPriceSheetItemService;
            _productTemplateItemService = productTemplateItemService;
            _productService = productService;
            _accountService = accountService;
            _logService = logService;
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        public IHttpActionResult Put([FromBody] ProductPriceSheetItemModel ppsItemModel)
        {
            if (!ppsItemModel.IsPricesValid)
            {
                ModelState.AddModelError(null, "Unable to save more than 1 pricing option!");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (ppsItemModel.ProductId.HasValue && ppsItemModel.PriceSheetId.HasValue)
            {
                var ppsItem = _productPriceSheetItemService.GetById(ppsItemModel.ProductId.Value, ppsItemModel.PriceSheetId.Value);
                if (ppsItem != null)
                {
                    try
                    {
                        ppsItem = _mapper.Map(ppsItemModel, ppsItem);
                        ppsItem.SetUpdated();
                        _productPriceSheetItemService.Update(ppsItem);
                        return Ok();
                    }
                    catch (Exception ex)
                    {
                        _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                        return BadRequest(ex.Message);
                    }
                }
            }
            return BadRequest("Product price sheet item not exist");
        }

        [HttpGet]
        [Route("api/ppsItems/getAccountPagedItems")]
        public IHttpActionResult GetAccountPagedItems([FromUri] TableParamsModel tableParams, [FromUri]int accountId, [FromUri]int? productCategoryId = null)
        {
            var account = _accountService.GetById(accountId);
            if (account == null)
            {
                return BadRequest("Account is not exist");
            }
            if (!account.PriceSheetId.HasValue)
            {
                return BadRequest("Account: Price Sheet is not specified");
            }
            if (!account.CompanyId.HasValue)
            {
                return BadRequest("Account: Company is not specified");
            }
            var productsAndTemplates = _productService.GetAccountProductAndTemplates(account);

            if (productsAndTemplates == null || !productsAndTemplates.Any())
            {
                return Ok(new { count = 0, items = new List<ProductPriceSheetItemModel>() });
            }

            FilterProducts(ref productsAndTemplates, tableParams, productCategoryId);

            var productsAndTemplatesPaged = productsAndTemplates
                .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn) ? "DisplayName" : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                .GetPage(tableParams.Page, tableParams.PageSize);

            var productsAndTemplatesModels = productsAndTemplatesPaged.ToList()
                .Select(item => _mapper.MapProductModel(item)).ToList();

            var ppsItemsModels = GetPPSItemsModels(productsAndTemplatesModels, account.PriceSheetId.Value, account.CompanyId.Value);

            return Ok(new { count = productsAndTemplates.Count(), items = ppsItemsModels });
        }

        [HttpGet]
        [Route("api/ppsItems/getNestedPagedItems")]
        public IHttpActionResult GetNestedPagedItems([FromUri] TableParamsModel tableParams, [FromUri]int? productId = null, [FromUri]int? priceSheetId = null, bool searchOnlyByProduct = false, bool searchOnlyByCompany = false)
        {
            var productPriceSheetItems = _productPriceSheetItemService.GetAll(productId, priceSheetId);
            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                productPriceSheetItems = productPriceSheetItems.Where(x =>
                    (!searchOnlyByProduct && x.ProductPriceSheet.Company.Name.ToLower().Contains(tableParams.Search)) ||
                    (!searchOnlyByProduct && x.ProductPriceSheet.Name.ToLower().Contains(tableParams.Search)) ||
                    (!searchOnlyByCompany && x.Product.DisplayName.ToLower().Contains(tableParams.Search)));
            }

            var itemsPaged = productPriceSheetItems
               .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn) ? defaultSortColumn : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
               .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = productPriceSheetItems.Count(),
                items = _mapper.MapToPriceSheetItemModels(itemsPaged)
            });
        }

        private List<ProductPriceSheetItemModel> GetPPSItemsModels(IEnumerable<ProductModel> productsAndTemplatesModels, int priceSheetId, int companyId)
        {
            var result = new List<ProductPriceSheetItemModel>();
            foreach (var item in productsAndTemplatesModels)
            {
                var ppsItemModel = new ProductPriceSheetItemModel();
                if (item.IsTemplate)
                {
                    ppsItemModel.IsTemplate = item.IsTemplate;
                    ppsItemModel.ProductName = item.DisplayName;
                    ppsItemModel.ProductShortDescription = item.ShortDescription;
                    ppsItemModel.ProductShortDescription2 = item.ShortDescription2;
                    ppsItemModel.ProductId = item.Id;
                    ppsItemModel.NestedPPSItems = GetTemplatePPSItems(ppsItemModel.ProductId.Value, priceSheetId, companyId);
                }
                else
                {
                    ppsItemModel = item.ProductPriceSheetItems.FirstOrDefault(x => x.PriceSheetId == priceSheetId);
                }
                if (ppsItemModel == null) continue;
                ppsItemModel.ProductImageUrl = Url.Route("ExtendedApi", new { controller = "product", action = "getImage", id = ppsItemModel.ProductId });
                result.Add(ppsItemModel);
            }
            return result;
        }

        private List<ProductPriceSheetItemModel> GetTemplatePPSItems(int templateId, int priceSheetId, int companyId)
        {
            var productTemplateItems = _productTemplateItemService.GetTemplateNestedItems(templateId, companyId).ToList();
            var ids = productTemplateItems.Select(x => x.ChildProductId).ToList();
            var ppsItems = _productPriceSheetItemService.GetAllFromPriceSheet(ids, priceSheetId).ToList();
            var result = new List<ProductPriceSheetItemModel>();
            foreach (var ppsItem in ppsItems)
            {
                var ppsItemModel = _mapper.MapToPriceSheetItemModel(ppsItem);
                ppsItemModel.ProductImageUrl = Url.Route("ExtendedApi", new { controller = "product", action = "getImage", id = ppsItemModel.ProductId });
                ppsItemModel.Quantity = productTemplateItems.Find(x => x.ChildProductId == ppsItem.ProductId).Quantity;
                ppsItemModel.AddedByProductTemplateId = templateId;
                result.Add(ppsItemModel);
            }
            return result;
        }

        private void FilterProducts(ref IQueryable<Product> products, TableParamsModel tableParams, int? categoryId)
        {
            //include inactive
            if (!tableParams.IncludeInactive)
            {
                products = products.Where(x => x.IsActive);
            }
            //filter by Category
            if (categoryId.HasValue)
            {
                products = products.Where(x => x.ProductsToCategories.Any(y => y.CategoryId == categoryId && y.IsActive));
            }
            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                products = products.Where(x =>
                    x.DisplayName.ToLower().Contains(tableParams.Search) ||
                    x.ExternalCode.ToLower().Contains(tableParams.Search) ||
                    x.ShortDescription.ToLower().Contains(tableParams.Search) ||
                    x.ShortDescription2.ToLower().Contains(tableParams.Search));
            }
        }
    }
}
