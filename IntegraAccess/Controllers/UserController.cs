﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Infrastructure;
using IntegraAccess.Infrastructure.Exceptions;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;
using Microsoft.AspNet.Identity;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class UserController : BaseApiController
    {
        private IAuthRepository _authRepository;
        protected readonly IAccountService _accountService;
        protected readonly IUserService _userService;
        protected readonly ILogService _logService;
        const string DefaultSortColumn = "FirstName";

        public UserController(IAuthRepository authRepository, IAccountService accountService, IDataConverter mapper, IUserService userService, ILogService logService)
            : base(mapper)
        {
            _authRepository = authRepository;
            _accountService = accountService;
            _userService = userService;
            _logService = logService;
        }

        [HttpGet]
        [ClaimsAuthorization(Role = "Account", ClaimType = "IsAccountGroupAdmin", ClaimValue = "1")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public IEnumerable<UserModel> Get([FromUri] int? accountGroupId = null)
        {
            var users = _authRepository.GetUsers();
            if (accountGroupId.HasValue)
            {
                users = users.Where(x => x.AccountGroupId == accountGroupId.Value);
            }
            return _mapper.MapUsers(users);
        }

        [HttpGet]
        [ClaimsAuthorization(Role = "Account", ClaimType = "IsAccountGroupAdmin", ClaimValue = "1")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [Route("api/users/getPagedUsers")]
        public IHttpActionResult GetPagedUsers([FromUri] TableParamsModel tableParams, [FromUri] int? accountGroupId = null, [FromUri] int? accountId = null, bool onlyConnected = true)
        {
            var users = _authRepository.GetUsers();

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                users = users.Where(x =>
                    x.FirstName.ToLower().Contains(tableParams.Search) ||
                    x.LastName.ToLower().Contains(tableParams.Search) ||
                    x.Login.ToLower().Contains(tableParams.Search));
            }
            if (accountGroupId.HasValue)
            {
                users = users.Where(x => x.AccountGroupId == accountGroupId.Value);
            }
            if (accountId.HasValue)
            {
                users = users.Where(x => x.AccountGroup.Accounts.Any(a => a.Id == accountId.Value));
                if (onlyConnected)
                {
                    users = users.Where(x => x.UserAccounts.Any(ua => ua.AccountId == accountId.Value && ua.IsActive));
                }
            }
            //include inactive
            if (!tableParams.IncludeInactive)
            {
                users = users.Where(user => user.IsActive == true);
            }
            var usersPaged = users
               .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn) ? DefaultSortColumn : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
               .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = users.Count(),
                items = _mapper.MapUsers(usersPaged)
            });
        }

        [HttpGet]
        [ClaimsAuthorization(Role = "Account", ClaimType = "IsAccountGroupAdmin", ClaimValue = "1")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public UserModel Get(string id, int? accountGroupId = null)
        {
            var user = _authRepository.FindUser(id);
            if (user != null && accountGroupId.HasValue && user.AccountGroupId != accountGroupId.Value)
            {
                return null;
            }
            return user != null ? _mapper.MapUserModel(user) : null;
        }

        [ClaimsAuthorization(Role = "Account", ClaimType = "IsAccountGroupAdmin", ClaimValue = "1")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public async Task<IHttpActionResult> Post(UserModel userModel, int? accountGroupId = null)
        {
            if (userModel.Password != userModel.ConfirmPassword)
            {
                ModelState.AddModelError(string.Empty, "Password mismatch. Please try again.");
            }

            var principal = RequestContext.Principal as ClaimsPrincipal;
            if (principal != null && principal.IsInRole("Account") && userModel.AccountType != "Account")
            {
                ModelState.AddModelError(string.Empty, "No permissions.You able to create only 'Account' type users");
            }

            if (accountGroupId.HasValue && userModel.AccountGroupId != accountGroupId.Value)
            {
                ModelState.AddModelError(string.Empty, "No permissions to create user for a foreign account group");
            }

            User user = _authRepository.FindUserByUserName(userModel.Login);
            if (user != null)
            {
                ModelState.AddModelError(string.Empty, "User with the same login already exist");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            user = _mapper.MapUser(userModel);

            IdentityResult result = await _authRepository.RegisterUser(user, userModel.Password);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        [ClaimsAuthorization(Role = "Account", ClaimType = "IsAccountGroupAdmin", ClaimValue = "1")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public async Task<IHttpActionResult> Put(UserModel userModel, int? accountGroupId = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var principal = RequestContext.Principal as ClaimsPrincipal;
            if (principal != null && principal.IsInRole("Account") && userModel.AccountType != "Account")
            {
                return BadRequest("No permissions.You able to update only 'Account' type users");
            }

            if (accountGroupId.HasValue && userModel.AccountGroupId != accountGroupId.Value)
            {
                return BadRequest("No permissions to manage user from a foreign account group");
            }

            try
            {

                var user = _authRepository.FindUser(userModel.Id);
                user = _mapper.Map(userModel, user);

                if (!userModel.Password.IsNullOrEmpty() &&
                    !userModel.ConfirmPassword.IsNullOrEmpty())
                {
                    if (!String.Equals(userModel.Password, userModel.ConfirmPassword))
                    {
                        throw new Exception("Passwords mismatch, pleasy try again.");
                    }
                    _userService.ForceChangePassword(userModel.Id, userModel.Password);
                    user.ChangePasswordOnLogin = true;
                }
                var errorResult = GetErrorResult(_authRepository.UpdateUser(user));

                if (errorResult != null)
                {
                    return errorResult;
                }
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _authRepository.Dispose();
            }

            base.Dispose(disposing);
        }

        [HttpPut]
        [ClaimsAuthorization(Role = "Account", ClaimType = "IsAccountGroupAdmin", ClaimValue = "1")]
        [Route("api/users/inactivate")]
        public IHttpActionResult InactivateUsers([FromBody]List<string> usersIds)
        {
            if (usersIds.Any())
            {
                try
                {
                    _authRepository.Inactivate(usersIds);
                    _authRepository.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }

        [HttpPut]
        [ClaimsAuthorization(Role = "Account", ClaimType = "IsAccountGroupAdmin", ClaimValue = "1")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [Route("api/users/grantAccessForUserToAccounts/{id}")]
        public IHttpActionResult GrantAccessForUserToAccounts(string id, bool grantAccess, [FromBody]List<int> accountsIds, int? accountGroupId = null)
        {
            if (accountsIds == null || string.IsNullOrEmpty(id))
            {
                return BadRequest("Please check parameters");
            }

            try
            {
                var accounts = _accountService.GetAll(accountsIds);
                //filter by accountGroup
                if (accountGroupId.HasValue)
                {
                    accounts = accounts.Where(x => x.AccountGroupId == accountGroupId);
                }
                //manage access
                if (grantAccess)
                {
                    _authRepository.GrantAccess(id, accounts.ToList());
                }
                else
                {
                    _authRepository.DenyAccess(id, accounts.ToList());
                }
                _authRepository.SaveChanges();
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("api/users/sendResetPasswordEmail/")]
        public async Task<IHttpActionResult> SendResetPasswordEmail(string userName, string email)//TODO: add restore password by email
        {
            if (userName == null && email == null)//are usernames and emails unique to perform a search on them? If no, a combination must be unique.
            {
                throw new BadRequestException("Both username and email are not specified. Either username or email must be specified to find the user.");
            }
            try
            {
                var user = (string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(email))?
                    _userService.FindUserByEmail(email):
                    _userService.FindUserByUserName(userName);

                var url = CreateResetPasswordUrlFor(user);
                _userService.SendResetPasswordMessageToUser(url, user);

                return Ok("Password reset successfully");
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/users/resetPassword/")]
        public async Task<IHttpActionResult> ResetPassword([FromBody] ResetPasswordModel model)//TODO: add restore password by email
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _userService.FindUserByUserName(model.UserName);
                    if(user == null)
                        throw new Exception(string.Format("No user found with userName '{0}'", model.UserName));
                    _userService.ResetPassword(user.Id, model.ResetPasswordToken, model.NewPassword);
                }
                else
                {
                    return BadRequest("Invalid Model");
                }
                return Ok("Password reset successfully");
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Supplier, Account")]
        [Route("api/users/changePassword/")]
        public async Task<IHttpActionResult> ChangePassword([FromBody] ChangePasswordModel model)//TODO: add restore password by email
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _userService.FindUserByUserName(model.UserName);
                    if (user == null)
                        throw new Exception(string.Format("No user found with userName '{0}'", model.UserName));
                    _userService.ChangePassword(user.Id, model.OldPassword, model.NewPassword);
                    if (user.ChangePasswordOnLogin)
                    {
                        user.ChangePasswordOnLogin = false;
                        _authRepository.UpdateUser(user);
                    }
                }
                else
                {
                    return BadRequest("Invalid Model");
                }
                return Ok("Password reset successfully");
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        protected string CreateResetPasswordUrlFor(User user)
        {
            var token = _userService.CreateResetTokenForUser(user);
            var uri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, String.Empty));
            return string.Format("{0}#/login/resetPassword/{1}/token/{2}", uri, user.UserName, token);
        }

        #region Helpers

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }



        #endregion
    }
}