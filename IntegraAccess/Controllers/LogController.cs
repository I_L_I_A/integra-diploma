﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier")]
    public class LogController : BaseApiController
    {
        const string DefaultSortColumn = "Error.Time";
        protected readonly ILogService _logService;

        public LogController(IDataConverter mapper, ILogService logService)
            : base(mapper)
        {
            _logService = logService;
        }

        [HttpGet]
        [Route("api/logs/getPagedLogs")]
        public IHttpActionResult GetPagedLogs([FromUri] LogsTableParamsModel tableParams)
        {
            var logs = _logService.GetAll();

            if (!String.IsNullOrEmpty(tableParams.Search))
            {
                logs = logs.Where(x =>
                    x.Message.ToLower().Contains(tableParams.Search) ||
                    x.Username.ToLower().Contains(tableParams.Search) ||
                    x.AdditionalInfo.ToLower().Contains(tableParams.Search) ||
                    x.Location.ToLower().Contains(tableParams.Search) ||
                    x.ErrorType.ToLower().Contains(tableParams.Search) ||
                    x.Date.ToString().Contains(tableParams.Search));
            }
            if (tableParams.FromDate != DateTime.MinValue)
            {
                logs = logs.Where(x => x.Date >= tableParams.FromDate);
            }
            if (tableParams.ToDate != DateTime.MinValue)
            {
                tableParams.ToDate = new DateTime(tableParams.ToDate.Year, tableParams.ToDate.Month, tableParams.ToDate.Day + 1);
                logs = logs.Where(x => x.Date <= tableParams.ToDate);
            }

            var orderedLogs = logs
                .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn) ? DefaultSortColumn : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = logs.Count(),
                items = orderedLogs.ToList()
            });
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("api/logs/log")]
        public IHttpActionResult LogError()
        {
            string json;
            var context = HttpContext.Current;
            using (var streamReader = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding))
            {
                json = streamReader.ReadToEnd();
            }
            dynamic jsnlog = JObject.Parse(json);
            var logModel = new LogModel();
            logModel.Location = jsnlog.r.location; 
            logModel.ErrorType = jsnlog.r.errorType;
            logModel.HostName = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            logModel.Message = jsnlog.r.message; 
            logModel.StackTrace = jsnlog.r.stackTrace; 
            logModel.LogLevel = jsnlog.r.logLevel; 
            logModel.Username = jsnlog.r.userName; 

            logModel.Date = DateTime.UtcNow;
            try
            {
                _logService.Log(_mapper.MapLogModel(logModel));
            }
            catch (Exception)
            {
                return InternalServerError();
            }
            return Ok();
        }
    }
}