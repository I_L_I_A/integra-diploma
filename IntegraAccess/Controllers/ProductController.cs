﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.ActionFilters.GZip;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Entities.Enums;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;
using Microsoft.AspNet.Identity;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class ProductController : BaseApiController
    {
        protected readonly IProductService _productService;
        protected readonly IProductCategoryService _productCategoryService;
        protected readonly IProductTemplateItemService _productTemplateItemService;
        protected readonly ILogService _logService;
        const string DefaultSortColumn = "DisplayName";

        public ProductController(IProductService productService, IProductTemplateItemService productTemplateItemService, IProductCategoryService productCategoryService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _productService = productService;
            _productCategoryService = productCategoryService;
            _productTemplateItemService = productTemplateItemService;
            _logService = logService;
        }

        [HttpGet]
        public IEnumerable<ProductModel> GetAll()
        {
            var products = _productService.GetAll();
            var models = _mapper.MapProducts(products);
            foreach (var productModel in models)
            {
                productModel.ImageUrl = CreateUriForImage(productModel);
            }
            return models;
        }

        [HttpGet]
        [Route("api/products/getPagedProducts")]
        public IHttpActionResult GetPagedProducts([FromUri] TableParamsModel tableParams, [FromUri] bool includeTemplates = true, [FromUri] int? companyId = null)
        {
            var products = _productService.GetAll(tableParams.IncludeInactive, includeTemplates);

            //filter products by company
            if (companyId.HasValue)
            {
                products = products
                    .Where(product => product.ProductPriceSheetItems
                        .Any(ppsItem => ppsItem.ProductPriceSheet.CompanyId == companyId.Value));
            }
            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                products = products.Where(x =>
                    x.DisplayName.ToLower().Contains(tableParams.Search) ||
                    x.ExternalCode.ToLower().Contains(tableParams.Search) ||
                    x.ShortDescription.ToLower().Contains(tableParams.Search) ||
                    x.ShortDescription2.ToLower().Contains(tableParams.Search));
            }

            var productsPaged = products
                .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn) ? DefaultSortColumn : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                .GetPage(tableParams.Page, tableParams.PageSize);

            var mappedProducts = _mapper.MapProducts(productsPaged);

            foreach (var product in mappedProducts)
            {
                product.ImageUrl = CreateUriForImage(product);
            }
            return Ok(new
            {
                count = products.Count(),
                items = mappedProducts.ToList()
            });
        }


        [HttpGet]
        public ProductModel Get(int id)
        {
            var product = _productService.GetById(id);
            var model = new ProductModel();
            if (product != null)
            {
                model = _mapper.MapProductModel(product);
                model.ProductTemplateItems = _productTemplateItemService.GetTemplateNestedItems(id).Select(x => new ProductTemplateItemModel()
                    {
                        CompanyName = x.Company.Name,
                        ChildProductName = x.ChildProduct.DisplayName,
                        Quantity = x.Quantity
                    }).ToList();
                if (model.HasImage)
                {
//                    var ms = new MemoryStream(product.Image);
//                    var bytes = ms.ToArray();
//                    model.ImageUrl = string.Format("data:image/{0};base64,{1}", Path.GetExtension(product.ImageName).Replace(".", ""), Convert.ToBase64String(bytes, 0, bytes.Length));
                    model.ImageUrl = CreateUriForImage(model);
                }
            }
            return model;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/products/getImage/{id}")]
        [IgnoreGZipCompression]
        public HttpResponseMessage GetImage(int id)
        {
            var product = _productService.GetById(id);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            if (product.Image != null)
            {
                var ms = new MemoryStream(product.Image);
                var bytes = ms.ToArray();
//                response.Content = new StringContent(string.Format("data:image/{0};base64,{1}",Path.GetExtension(product.ImageName).Replace(".", ""), Convert.ToBase64String(bytes,0, bytes.Length)));
                response.Content = new ByteArrayContent(ms.ToArray());
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = product.ImageName;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(string.Format("image/{0}", Path.GetExtension(product.ImageName).Replace(".", "")));
            }
            return response;
        }

        [HttpPost]
        [Route("api/products/post")]
        public IHttpActionResult Post(ProductModel productModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var product = _mapper.MapProduct(productModel);
                product.IsActive = true;
                product.SetUpdated();
                _productService.Add(product);

                product.EntityState = State.Unchanged;
                var categoriesIds = productModel.ProductsToCategories.Select(x => x.CategoryId).Distinct().ToList();
                var categories = _productCategoryService.GetAll(categoriesIds);
                _productService.AddToCategories(product,
                    categories != null ? categories.ToList() : new List<ProductCategory>(),
                    User.Identity.GetUserId());
                _productService.SaveChanges();

                return Ok(new { productId = product.Id });
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        [Route("api/product/postImage/")]
        [HttpPost]
        public IHttpActionResult PostImage(int productId = 0)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var product = _productService.GetById(productId);
                if (product == null)
                {
                    throw new Exception(string.Format("No such product with Id={0} in the database.", productId));
                }
                //                if (Request.Content.IsMimeMultipartContent())//TODO: figure out which approach is more efficient
                //                {
                //                    Request.Content.ReadAsMultipartAsync<MultipartMemoryStreamProvider>(new MultipartMemoryStreamProvider()).ContinueWith((task) =>
                //                    {
                //                        MultipartMemoryStreamProvider provider = task.Result;
                //                        var content = task.Result.Contents.FirstOrDefault();
                //                        new HttpContent().
                //                        var stream = content.ReadAsStreamAsync().Result;
                //                        byte[] result;
                //                        using (var streamReader = new MemoryStream())
                //                        {
                //                            stream.CopyTo(streamReader);
                //                            product.Image = streamReader.ToArray();
                //                        }
                //                    });
                //                    return result;
                //                }
                var file = HttpContext.Current.Request.Files[0];
                //TODO: create thumbnales for image here.
                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = file.InputStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    product.Image = ms.ToArray();
                }
                product.ImageName = file.FileName;
                product.IsActive = true;
                product.SetUpdated();
                _productService.Update(product);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Put(ProductModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = _productService.GetById(model.Id);

            if (product != null)
            {
                try
                {
                    product = _mapper.Map(model, product);
                    product.SetUpdated(User.Identity.GetUserId());
                    _productService.Update(product);
                    product.EntityState = State.Unchanged;
                    var categoriesIds = model.ProductsToCategories.Select(x => x.CategoryId).ToList();
                    var categories = _productCategoryService.GetAll(categoriesIds);
                    _productService.AddToCategories(product,
                        categories != null ? categories.ToList() : new List<ProductCategory>(),
                        User.Identity.GetUserId());
                    _productService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }

        [HttpPut]
        [Route("api/products/inactivate")]
        public IHttpActionResult InactivateProducts([FromBody]List<int> productsIds)
        {
            if (productsIds.Any())
            {
                try
                {
                    _productService.Inactivate(productsIds);
                    _productService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }

        public void Delete(int id)
        {
        }

        private string CreateUriForImage(ProductModel product)
        {
            var uri = String.Empty;
            if (product.ImageName != null)
            {
                uri = Url.Route("ExtendedApi",
                        new
                        {
                            controller = "products",
                            action = "getImage",
                            id = product.Id
                        });
            }
            else
            {
                uri = Url.Route("ExtendedApi",
                    new
                    {
                        controller = "products",
                        action = "getImage",
                        id = product.Id
                    });
            }
            return uri;
        }
    }
}