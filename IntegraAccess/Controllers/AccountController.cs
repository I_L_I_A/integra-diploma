﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.DataAccess.Repository.Abstract;
using IntegraAccess.Infrastructure;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class AccountController : BaseApiController
    {
        const string DefaultSortColumn = "Name";
        protected readonly IAccountService _accountService;
        protected readonly IFacilityService _facilityService;
        protected readonly IAuthRepository _authRepository;
        protected readonly IPriceSheetService _priceSheetService;
        protected readonly ILogService _logService;

        public AccountController(IAccountService accountService, IAuthRepository authRepository, IFacilityService facilityService, IPriceSheetService priceSheetService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _accountService = accountService;
            _authRepository = authRepository;
            _facilityService = facilityService;
            _priceSheetService = priceSheetService;
            _logService = logService;
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IEnumerable<AccountModel> Get([FromUri] int? accountGroupId = null, [FromUri] string userId = null, [FromUri]bool onlyConnected = true)
        {
            var accounts = _accountService.GetAll();

            if (accountGroupId.HasValue)
            {
                accounts = accounts.Where(x => x.AccountGroupId == accountGroupId.Value);
            }

            //only accounts connected to user
            if (!string.IsNullOrEmpty(userId))
            {
                accounts = accounts.Where(x => x.AccountGroup.Users.Any(user => user.Id == userId));
                if (onlyConnected)
                {
                    accounts = accounts.Where(x => x.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive));
                }
            }


            return _mapper.MapAccounts(accounts);
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        [Route("api/accounts/getPagedAccounts")]
        public IHttpActionResult GetPagedAccounts([FromUri] TableParamsModel tableParams, [FromUri] int? accountGroupId = null, [FromUri] string userId = null, [FromUri]bool onlyConnected = true)
        {
            var accounts = _accountService.GetAll();

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                accounts = accounts.Where(x =>
                    x.Name.ToLower().Contains(tableParams.Search) ||
                    x.ExternalId.ToLower().Contains(tableParams.Search));
            }
            if (accountGroupId.HasValue)
            {
                accounts = accounts.Where(x => x.AccountGroupId == accountGroupId.Value);
            }
            //only accounts connected to user
            if (!string.IsNullOrEmpty(userId))
            {
                accounts = accounts.Where(x => x.AccountGroup.Users.Any(user => user.Id == userId));
                if (onlyConnected)
                {
                    accounts = accounts.Where(x => x.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive));
                }
            }
            //include inactive
            if (!tableParams.IncludeInactive)
            {
                accounts = accounts.Where(ac => ac.IsActive);
            }

            var accountPaged = accounts
                .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                    ? DefaultSortColumn
                    : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new { count = accounts.Count(), items = _mapper.MapAccounts(accountPaged) });
        }

        [HttpGet]
        [Authorize(Roles = "Supplier")]
        [Route("api/accounts/getPriceSheetAccounts")]
        public IHttpActionResult GetPriceSheetAccounts([FromUri] TableParamsModel tableParams, [FromUri]int priceSheetId, [FromUri]bool onlyAssigned = true)
        {
            var priceSheet = _priceSheetService.GetById(priceSheetId);
            if (priceSheet == null)
            {
                return BadRequest("Price sheet is not exist");
            }
            //get all account with the same company as price sheet
            var accounts = _accountService.GetAll().Where(x => x.CompanyId == priceSheet.CompanyId);

            //only accounts assigned to price sheet
            if (onlyAssigned)
            {
                accounts = accounts.Where(x => x.PriceSheetId == priceSheetId);
            }

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                accounts = accounts.Where(x =>
                    x.Name.ToLower().Contains(tableParams.Search) ||
                    x.ExternalId.ToLower().Contains(tableParams.Search));
            }
           
            var accountPaged = accounts
                .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                    ? DefaultSortColumn
                    : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new { count = accounts.Count(), items = _mapper.MapAccounts(accountPaged) });
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public AccountModel Get(int id, [FromUri] int? accountGroupId = null, [FromUri] string userId = null, [FromUri]bool onlyConnected = true)
        {
            var account = _accountService.GetById(id);
            //restricted by account group
            if (account != null && accountGroupId.HasValue && account.AccountGroupId != accountGroupId.Value)
            {
                return null;
            }
            //restricted by user
            if (account != null && account.UserAccounts.Any() && !string.IsNullOrEmpty(userId) && !account.UserAccounts.Any(x => x.IsActive && x.UserId == userId))
            {
                return null;
            }
            return account != null ? _mapper.MapAccountModel(account) : null;
        }

        [HttpPost]
        [Authorize(Roles = "Supplier")]
        public IHttpActionResult Post(AccountModel accountModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var account = _mapper.MapAccount(accountModel);
                account.SetUpdated();
                account.IsActive = true;
                _accountService.Add(account);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        public IHttpActionResult Put(int id, [FromBody]AccountModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var account = _accountService.GetById(model.Id);
            if (account != null)
            {
                try
                {
                    account = _mapper.Map(model, account);
                    account.SetUpdated();
                    _accountService.Update(account);
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }


        [HttpPut]
        [Authorize(Roles = "Supplier")]
        [Route("api/accounts/inactivate")]
        public IHttpActionResult InactivateAccounts([FromBody]List<int> accountIds)
        {
            if (accountIds.Any())
            {
                try
                {
                    _accountService.Inactivate(accountIds);
                    _accountService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        [Route("api/accounts/connectFacilities/{id}")]
        public IHttpActionResult ConnectFacilities(int id, bool connect, [FromBody]List<int> facilitiesIds)
        {
            if (facilitiesIds == null || id == 0)
            {
                return BadRequest("Please check parameters");
            }

            try
            {
                if (connect)
                {
                    _accountService.ConnectFacilities(id, _facilityService.GetAll(facilitiesIds).ToList());
                }
                else
                {
                    _accountService.DisconnectFacilities(id, _facilityService.GetAll(facilitiesIds).ToList());
                }
                _accountService.SaveChanges();
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        [Authorize(Roles = "Supplier")]
        [Route("api/accounts/grantAccessForUsers/{id}")]
        public IHttpActionResult GrantAccessForUsers(int id, bool grantAccess, [FromBody]List<string> userIds)
        {
            if (userIds == null || id == 0)
            {
                return BadRequest("Please check parameters");
            }

            try
            {
                if (grantAccess)
                {
                    _accountService.GrantAccess(id, _authRepository.GetUsers(userIds).ToList());
                }
                else
                {
                    _accountService.DenyAccess(id, _authRepository.GetUsers(userIds).ToList());
                }
                _accountService.SaveChanges();
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // DELETE: api/AccoutGroup/5
        public void Delete(int id)
        {
        }
    }
}
