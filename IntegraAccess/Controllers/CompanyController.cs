﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier")]
    public class CompanyController : BaseApiController
    {
        protected readonly ICompanyService _companyService;
        protected readonly ILogService _logService;
        private const string DefaultSortColumn = "Name";

        public CompanyController(ICompanyService companyService, IDataConverter mapper, ILogService logService)
            : base(mapper)
        {
            _companyService = companyService;
            _logService = logService;
        }

        [HttpGet]
        public IEnumerable<CompanyModel> Get()
        {
            var companies = _companyService.GetAll();
            return _mapper.MapCompanies(companies);
        }

        [HttpGet]
        [Route("api/companies/getPagedCompanies")]
        public IHttpActionResult GetPagedCompanies([FromUri] TableParamsModel tableParams)
        {
            var companies = _companyService.GetAll();

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                tableParams.Search = tableParams.Search.ToLower();
                companies = companies.Where(x =>
                    x.Name.ToLower().Contains(tableParams.Search) ||
                    x.ExternalCode.ToLower().Contains(tableParams.Search) ||
                    x.Description.ToLower().Contains(tableParams.Search));
            }
            var companiesPaged = companies.OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                     ? DefaultSortColumn
                     : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
                 .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = companies.Count(),
                items = _mapper.MapCompanies(companiesPaged)
            });
        }

        [HttpGet]
        public CompanyModel Get(int id)
        {
            var company = _companyService.GetById(id);
            return company != null ? _mapper.MapCompanyModel(company) : null;
        }


        [HttpPost]
        public IHttpActionResult Post(CompanyModel companyModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var company = _mapper.MapCompany(companyModel);
                company.SetUpdated();
                _companyService.Add(company);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]CompanyModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var company = _companyService.GetById(model.Id);
            if (company != null)
            {
                try
                {
                    company = _mapper.Map(model, company);
                    company.SetUpdated();
                    _companyService.Update(company);
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }


        // DELETE: api/AccoutGroup/5
        public void Delete(int id)
        {
        }
    }
}
