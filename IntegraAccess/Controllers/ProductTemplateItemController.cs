﻿using System;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Entities.Entities;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class ProductTemplateItemController : BaseApiController
    {
        protected readonly IProductTemplateItemService _productTemplateItemService;
        protected readonly ILogService _logService;
        const string DefaultSortColumn = "DisplayName";

        public ProductTemplateItemController(IProductTemplateItemService productTemplateItemService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _productTemplateItemService = productTemplateItemService;
            _logService = logService;
        }

        [HttpGet]
        [Route("api/templateItems/getPagedTemplateItems")]
        public IHttpActionResult GetTemplatePagedItems([FromUri] TableParamsModel tableParams, [FromUri] int templateId, [FromUri] int? companyId = null)
        {
            var templateNestedItems = _productTemplateItemService.GetTemplateNestedItems(templateId);
            //filter by company
            if (companyId.HasValue)
            {
                templateNestedItems = templateNestedItems.Where(x => x.CompanyId == companyId.Value);
            }

            var itemsPaged = templateNestedItems
               .OrderBy(string.IsNullOrEmpty(tableParams.SortColumn) ? DefaultSortColumn : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
               .GetPage(tableParams.Page, tableParams.PageSize);

            var mappedItems = itemsPaged.Select(x => new ProductTemplateItemModel()
            {
                CompanyName = x.Company.Name,
                CompanyId = x.CompanyId,
                ChildProductId = x.ChildProductId,
                ChildProductDescription = x.ChildProduct.ShortDescription,
                ChildProductDescription2 = x.ChildProduct.ShortDescription2,
                TemplateId = x.ProductId,
                ChildProductName = x.ChildProduct.DisplayName,
                Quantity = x.Quantity
            }).ToList();

            return Ok(new
            {
                count = templateNestedItems.Count(),
                items = mappedItems
            });
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]ProductTemplateItemModel model)
        {
            try
            {
                var templateItem = new ProductTemplateItem()
                {
                    ChildProductId = model.ChildProductId,
                    ProductId = model.TemplateId,
                    CompanyId = model.CompanyId,
                    IsActive = true,
                    UpdatedDateTime = DateTime.UtcNow,
                };
                _productTemplateItemService.Add(templateItem);
                return Ok();
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IHttpActionResult Put([FromBody]ProductTemplateItemModel model)
        {
            try
            {
                var templateItem = _productTemplateItemService.GetById(model.ChildProductId, model.CompanyId, model.TemplateId);
                templateItem.Quantity = model.Quantity;
                templateItem.SetUpdated();
                _productTemplateItemService.Update(templateItem);
                return Ok();
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("api/templateItems/inactivate")]
        public IHttpActionResult InactivateTemplateItem([FromBody]ProductTemplateItemModel model)
        {
            try
            {
                _productTemplateItemService.Inactivate(model.TemplateId, model.ChildProductId, model.CompanyId);
                _productTemplateItemService.SaveChanges();
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        public void Delete(int id)
        {
        }
    }
}