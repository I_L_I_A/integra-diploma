﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Http;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Common.Extensions;
using IntegraAccess.Infrastructure;
using IntegraAccess.Infrastructure.Logging;
using IntegraAccess.Mapping.Conversions;
using IntegraAccess.Models;

namespace IntegraAccess.Controllers
{
    [Authorize(Roles = "Supplier, Account")]
    public class PatientController : BaseApiController
    {
        protected readonly IPatientService _patientService;
        protected readonly ILogService _logService;
        private const string DefaultSortColumn = "LastName";

        public PatientController(IPatientService patientService, ILogService logService, IDataConverter mapper)
            : base(mapper)
        {
            _patientService = patientService;
            _logService = logService;
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IEnumerable<PatientModel> Get(int? accountGroupId = null, int? accountId = null, [FromUri] string userId = null)
        {
            var patients = _patientService.GetAll();
            //restricted by account group
            if (accountGroupId.HasValue)
            {
                patients = patients.Where(x => x.AccountGroupId == accountGroupId.Value);
            }
            //restricted by account
            if (accountId.HasValue)
            {
                patients = patients.Where(x => x.AccountId == accountId.Value);
            }
            //restricted by user
            if (!string.IsNullOrEmpty(userId))
            {
                patients = patients.Where(x => x.Account.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive));
            }
            return _mapper.MapPatients(patients);
        }

        [HttpGet]
        [Route("api/patients/getPagedPatients")]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IHttpActionResult GetPagedPatients([FromUri] TableParamsModel tableParams, int? accountGroupId = null, int? accountId = null, [FromUri] string userId = null)
        {
            var patients = _patientService.GetAll();

            //search
            if (!string.IsNullOrWhiteSpace(tableParams.Search))
            {
                var search = tableParams.Search.ToLower().Replace(',', ' ').Split(' ');
                patients = search.Aggregate(patients, (current, searchItem) => 
                    current.Where(x => x.FirstName.ToLower().Contains(searchItem) 
                        || x.LastName.ToLower().Contains(searchItem)
                        || x.Email.ToLower().Contains(searchItem) 
                        || x.ExternalEMRCode.ToLower().Contains(searchItem) 
                        || x.ExternalCode.ToLower().Contains(searchItem)));
            }
            //restricted by account group
            if (accountGroupId.HasValue)
            {
                patients = patients.Where(x => x.AccountGroupId == accountGroupId.Value);
            }
            //restricted by Account and Company
            if (accountId.HasValue)
            {
                patients = patients.Where(x => x.AccountId.Value == accountId.Value);
            }
            //restricted by user
            if (!string.IsNullOrEmpty(userId))
            {
                patients = patients.Where(x => x.Account.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive));
            }
            //include only active
            if (!tableParams.IncludeInactive)
            {
                patients = patients.Where(ac => ac.IsActive == true);
            }
            var patientsPaged = patients.OrderBy(string.IsNullOrEmpty(tableParams.SortColumn)
                   ? DefaultSortColumn
                   : tableParams.SortColumn + (tableParams.Reverse ? " descending" : string.Empty))
               .GetPage(tableParams.Page, tableParams.PageSize);

            return Ok(new
            {
                count = patients.Count(),
                items = _mapper.MapPatients(patientsPaged)
            });
        }

        [HttpGet]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public PatientModel Get(int id, int? accountGroupId = null, [FromUri] string userId = null)
        {
            var patient = _patientService.GetById(id);
            //restricted by account group
            if (patient == null || accountGroupId.HasValue && patient.AccountGroupId != accountGroupId.Value)
            {
                return null;
            }
            //restricted by user
            if (!string.IsNullOrEmpty(userId) && patient.Account != null && !patient.Account.UserAccounts.Any(ua => ua.UserId == userId && ua.IsActive))
            {
                return null;
            }
            return _mapper.MapPatientModel(patient);
        }

        [HttpPost]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        [RestrictedByUserFilter(Role = "Account", ExcludeType = "IsAccountGroupAdmin", ExcludeValue = "1", ParameterName = "userId")]
        public IHttpActionResult Post(PatientModel model, int? accountGroupId = null, [FromUri] string userId = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (accountGroupId.HasValue && accountGroupId.Value != model.AccountGroupId)
            {
                return BadRequest("No permissions to create patients for current Account Group");
            }

            try
            {
                var patient = _mapper.MapPatient(model);
                patient.IsActive = true;
                patient.SetUpdated();
                _patientService.Add(patient);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut]
        [RestrictedByAccountGroupFilter(Role = "Account", ParameterName = "accountGroupId")]
        public IHttpActionResult Put(int id, [FromBody]PatientModel model, int? accountGroupId = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (accountGroupId.HasValue && accountGroupId.Value != model.AccountGroupId)
            {
                return BadRequest("No permissions");
            }

            var patient = _patientService.GetById(model.Id);
            if (patient != null)
            {
                try
                {
                    patient = _mapper.Map(model, patient);
                    patient.SetUpdated();
                    _patientService.Update(patient);
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }

        [HttpPut]
        [Route("api/patients/inactivate")]
        public IHttpActionResult InactivatePatients([FromBody]List<int> patientsIds)
        {
            if (patientsIds.Any())
            {
                try
                {
                    _patientService.Inactivate(patientsIds);
                    _patientService.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                    return BadRequest(ex.Message);
                }

            }
            return Ok();
        }

        [HttpPost]
        [Route("api/patients/checkDuplicate")]
        public IHttpActionResult CheckDuplicate([FromBody]CheckDuplicatePatientModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //            if (accountGroupId.HasValue && accountGroupId.Value != model.AccountGroupId)
            //            {
            //                return BadRequest("No permissions to create patients for current Account Group");
            //            }
            var isDuplicate = false;
            try
            {
                var patient = _mapper.MapCheckDuplicatePatientModel(model);
                isDuplicate = _patientService.Exists(patient);
            }
            catch (Exception ex)
            {
                _logService.Log(LoggingHelper.CreateErrorLog(HttpContext.Current, ex));
                return BadRequest(ex.Message);
            }

            return Ok(new
            {
                IsDuplicate = isDuplicate
            });
        }

        public void Delete(int id)
        {
        }
    }
}
