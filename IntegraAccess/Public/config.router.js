'use strict';

/**
 * Config for the router
 */
app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES', 'AppSettings',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires, AppSettings) {

    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;
    app.settings = AppSettings;

    // LAZY MODULES

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });

    // APPLICATION ROUTES
    // -----------------------------------
    //Because of a bug in ui-router, when using $urlStateProvider.otherwise we get an infinite digest loop error.
    //A workaround was found by @shoaibmerchant and it goes like this(details at https://github.com/Narzerus/angular-permission):
    $urlRouterProvider.otherwise(function ($injector) {// For any unmatched url, redirect to /app/home
        var $state = $injector.get("$state");
        $state.go('app.home');
    });
    //
    // Set up the states
    $stateProvider
        .state('app', {
            url: "/app",
            templateUrl: "Public/app/layout/views/app.html",
            resolve: loadSequence('modernizr', 'moment', 'angularMoment', 'uiSwitch', 'perfect-scrollbar-plugin', 'toaster', 'ngAside', 'vAccordion', 'chartjs', 'tc.chartjs', 'ngTable', 'blockUI', 'formUnsavedChangesService'),
            abstract: true,
            data: {
                authenticate: true,
                roles: ["Supplier", "Account"],
            }
        }).state('app.home', {
            url: "/home",
            templateUrl: "Public/app/home/views/main.html",
            resolve:  loadSequence('homeCtrl', 'accountsService', 'accountsGroupsService', 'facilitiesService'),
        })
         //accounts routes
        .state('app.accounts', {
            url: "/accounts",
            templateUrl: "Public/app/accounts/views/main.html",
            abstract: true,
            resolve: loadSequence('accountsGroupsService', 'usersService', 'facilitiesService', 'companiesService', 'accountsService', 'accountsCtrl', 'usersCtrl', 'facilitiesCtrl', 'productPriceSheetsService'),
            data: {
                roles: ["Supplier"]
            }
        })
        .state('app.accounts.create', {
            url: "/create/:accountGroupId",
            templateUrl: "Public/app/accounts/views/create.html",
        })
        .state('app.accounts.edit', {
            url: "/edit/:accountId",
            templateUrl: "Public/app/accounts/views/edit.html",
        })
         .state('app.accounts.list', {
             url: "/list",
             templateUrl: "Public/app/accounts/views/list.html",
         })
        //account groups routes
        .state('app.groups', {
            url: "/groups",
            templateUrl: "Public/app/groups/views/main.html",
            abstract: true,
            resolve: loadSequence('facilitiesService', 'usersService', 'companiesService', 'accountsService', 'accountsGroupsService', 'usersCtrl', 'facilitiesCtrl', 'accountsCtrl', 'accountsGroupsCtrl', 'productPriceSheetsService'),
            data: {
                roles: ["Supplier"]
            }
        })
         .state('app.groups.create', {
             url: "/create",
             templateUrl: "Public/app/groups/views/create.html",
         })
         .state('app.groups.edit', {
             url: "/edit/:accountGroupId",
             templateUrl: "Public/app/groups/views/edit.html",
         })
        .state('app.groups.list', {
            url: "/list",
            templateUrl: "Public/app/groups/views/list.html",
        })
        //users routes
        .state('app.users', {
            url: "/users",
            templateUrl: "Public/app/users/views/main.html",
            abstract: true,
            resolve: loadSequence('usersService', 'accountsGroupsService', 'accountsCtrl', 'accountsService', 'usersCtrl'),
            data: {
                permissions: ["AccountGroupAdmin"]
            }
        })
        .state('app.users.list', {
            url: "/list",
            templateUrl: "Public/app/users/views/list.html",
        })
        .state('app.users.create', {
            url: "/create/:accountGroupId?/:accountId?/:type?",
            templateUrl: "Public/app/users/views/create.html",
        })
         .state('app.users.edit', {
             url: "/edit/:userId",
             templateUrl: "Public/app/users/views/edit.html",
         })
        //patients routes
        .state('app.patients', {
            url: "/patients",
            templateUrl: "Public/app/patients/views/main.html",
            abstract: true,
            resolve: loadSequence('patientsService', 'accountsGroupsService', 'facilitiesService', 'accountsService', 'patientsCtrl')
        })
        .state('app.patients.list', {
            url: "/list",
            templateUrl: "Public/app/patients/views/list.html",
        })
        .state('app.patients.create', {
            url: "/create",
            templateUrl: "Public/app/patients/views/create.html",
        })
         .state('app.patients.edit', {
             url: "/edit/:patientId",
             templateUrl: "Public/app/patients/views/edit.html",
         })
        //patients routes
        .state('app.orders', {
            url: "/orders",
            templateUrl: "Public/app/orders/views/main.html",
            abstract: true,
            resolve: loadSequence('patientsService', 'deliveryTimesService', 'accountsGroupsService', 'productCategoriesService', 'productsService', 'facilitiesService',  'accountsService', 'productPriceSheetItemsService', 'orderLineItemsCtrl', 'ordersCtrl', 'ngFileUpload', 'afkl.lazyImage','ordersService')
        })
        .state('app.orders.list', {
            url: "/list",
            templateUrl: "Public/app/orders/views/list.html",
        })
        .state('app.orders.create', {
            url: "/create",
            templateUrl: "Public/app/orders/views/create.html"
        })
         .state('app.orders.edit', {
             url: "/edit/:orderId",
             templateUrl: "Public/app/orders/views/edit.html"
         })
         .state('app.orders.view', {
             url: "/view/:orderId",
             templateUrl: "Public/app/orders/views/view.html"
         })
        //companies routes
        .state('app.companies', {
            url: "/companies",
            templateUrl: "Public/app/companies/views/main.html",
            abstract: true,
            resolve: loadSequence('monospaced.elastic', 'companiesService', 'companiesCtrl', 'productPriceSheetsService', 'priceSheetsCtrl'),
            data: {
                roles: ["Supplier"]
            }
        })
        .state('app.companies.list', {
            url: "/list",
            templateUrl: "Public/app/companies/views/list.html",
        })
        .state('app.companies.create', {
            url: "/create",
            templateUrl: "Public/app/companies/views/create.html",
        })
         .state('app.companies.edit', {
             url: "/edit/:companyId",
             templateUrl: "Public/app/companies/views/edit.html",
         })
        //patients routes
        .state('app.facilities', {
            url: "/facilities",
            templateUrl: "Public/app/facilities/views/main.html",
            abstract: true,
            resolve: loadSequence('facilitiesService', 'accountsGroupsService', 'companiesService', 'facilitiesCtrl'),
            data: {
                roles: ["Supplier"]
            }
        })
        .state('app.facilities.list', {
            url: "/list",
            templateUrl: "Public/app/facilities/views/list.html",
        })
        .state('app.facilities.create', {
            url: "/create/:accountGroupId?",
            templateUrl: "Public/app/facilities/views/create.html",
        })
         .state('app.facilities.edit', {
             url: "/edit/:facilityId",
             templateUrl: "Public/app/facilities/views/edit.html",
         })
        //pricesheets
         .state('app.pricesheets', {
             url: "/pricesheets",
             templateUrl: "Public/app/pricesheets/views/main.html",
             abstract: true,
             resolve: loadSequence('xeditable', 'companiesService', 'accountsService', 'productPriceSheetsService', 'productPriceSheetItemsService', 'priceSheetsCtrl', 'accountsCtrl', 'priceSheetItemsCtrl'),
             data: {
                 roles: ["Supplier"]
             }
         })
        .state('app.pricesheets.list', {
            url: "/list",
            templateUrl: "Public/app/pricesheets/views/list.html",
        })
        .state('app.pricesheets.create', {
            url: "/create/:pricesheetId",
            templateUrl: "Public/app/pricesheets/views/create.html",
        })
         .state('app.pricesheets.edit', {
             url: "/edit/:pricesheetId",
             templateUrl: "Public/app/pricesheets/views/edit.html",
         })
        //products
        .state('app.products', {
            url: "/products",
            templateUrl: "Public/app/products/views/main.html",
            abstract: true,
            resolve: loadSequence('xeditable', 'productsCtrl', 'productsService', 'companiesService', 'productPriceSheetsService', 'productCategoriesService', 'ngFileUpload', 'afkl.lazyImage', 'productPriceSheetItemsService', 'priceSheetItemsCtrl'),
            data: {
                roles: ["Supplier"]
            }
        })
         .state('app.products.list', {
             url: "/list",
             resolve: loadSequence('ngFileUpload', 'templateItemsService'),
             templateUrl: "Public/app/products/views/list.html",
         })
        .state('app.products.create', {
            url: "/create/",
            templateUrl: "Public/app/products/views/create.html",
        })
         .state('app.products.edit', {
             url: "/edit/:productId",
             templateUrl: "Public/app/products/views/edit.html",
             resolve: loadSequence('ngFileUpload', 'templateItemsService')
         })
         //Reports routes
         .state('app.reports',{
             url: "/reports",
             templateUrl: "Public/app/reports/views/main.html",
             resolve: loadSequence('xeditable', 'reportsCtrl', 'reportsService', 'accountsService', 'accountsGroupsService', 'facilitiesService'),
             abstract:true
         })
         .state('app.reports.patient_census',{
             url: "/patient_census",
             templateUrl: "Public/app/reports/views/patientCensus.html",
             resolve:  loadSequence('patientsService'),
         })
        //Logs routes
        .state('app.logs', {
            url: '/logs',
            templateUrl: "Public/app/logs/views/list.html",
            resolve: loadSequence('logsCtrl', 'jsonFormatter'),
            data: {
                roles: ["Supplier"]
            }
        })
        // Error routes
        .state('error', {
            url: '/error',
            template: '<div ui-view class="fade-in-up"></div>'
        }).state('error.404', {
            url: '/404',
            templateUrl: "Public/vendor/clip-two/STANDARD/assets/views/utility_404.html",
        }).state('error.500', {
            url: '/500',
            templateUrl: "Public/vendor/clip-two/STANDARD/assets/views/utility_500.html",
        })

        // Login routes
        .state('login', {
            url: '/login',
            template: '<div ui-view class="fade-in-right-big smooth"></div>',
            abstract: true
        }).state('login.signin', {
            url: '/signin',
            resolve: loadSequence('signinCtrl'),
            templateUrl: "Public/app/auth/signin/views/main.html",
            params: {
                afterSignInState: "app.home",
                afterSignInStateParameters: null,
                wasRedirected: false
            }
        })
//        .state('login.signup', {
//            url: '/signup',
//            resolve: loadSequence('signupCtrl'),
//            templateUrl: "Public/app/auth/signup/views/main.html"
//        })
        .state('login.changePassword', {
            url: '/changePassword',
            resolve: loadSequence('changePasswordCtrl'),
            templateUrl: "Public/app/auth/changePassword/views/main.html",
            data: {
                authenticate: true
            }
        })
        .state('login.resetPassword', {
            url: '/resetPassword/{userName}/token/{token:.*}',
            resolve: loadSequence('resetPasswordCtrl'),
            templateUrl: "Public/app/auth/resetPassword/views/main.html"
        })
        .state('login.sendResetPasswordEmail', {
            url: '/sendResetPasswordEmail',
            resolve: loadSequence('sendResetPasswordEmailCtrl'),
            templateUrl: "Public/app/auth/sendResetPasswordEmail/views/main.html"
        });

    // Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
    function loadSequence() {
        var _args = arguments;
        return {
            deps: ['$ocLazyLoad', '$q',
			function ($ocLL, $q) {
			    var promise = $q.when(1);
			    for (var i = 0, len = _args.length; i < len; i++) {
			        promise = promiseThen(_args[i]);
			    }
			    return promise;

			    function promiseThen(_arg) {
			        if (typeof _arg == 'function')
			            return promise.then(_arg);
			        else
			            return promise.then(function () {
			                var nowLoad = requiredData(_arg);
			                if (!nowLoad)
			                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
			                //SPECIFICALLY FOR JSREQUIRES CONSTANT
                            if (nowLoad.files) {
                                for (var k = 0; k < nowLoad.files.length; k++) {
                                    nowLoad.files[k] += "?v=" + app.settings.version;
                                }
                            } else {
                                for (var k = 0; k < nowLoad.length; k++) {
                                    nowLoad[k] += "?v=" + app.settings.version;
                                }
                            }
			                return $ocLL.load(nowLoad);
			            });
			    }

			    function requiredData(name) {
			        if (jsRequires.modules)
			            for (var m in jsRequires.modules)
			                if (jsRequires.modules[m].name && jsRequires.modules[m].name === name) {
			                    return angular.copy(jsRequires.modules[m]);
			                }
			        return angular.copy(jsRequires.scripts && jsRequires.scripts[name]);
			    }
			}]
        };
    }
}]);