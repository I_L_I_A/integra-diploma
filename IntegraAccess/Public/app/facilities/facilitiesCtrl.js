﻿'use strict';

app.controller('FacilitiesCtrl', ["$scope", "FacilitiesService", "SweetAlert", "AccountGroupsService", "FormUnsavedChangesService", function ($scope, FacilitiesService, SweetAlert, AccountGroupsService, FormUnsavedChangesService) {
    $scope.facilitiesService = FacilitiesService;
    $scope.accountGroupsService = AccountGroupsService;
    $scope.sweetAlert = SweetAlert;
	$scope.formUnsavedChangesService = FormUnsavedChangesService;
    $scope.loadAccountGroupsData = function () {
        $scope.accountGroupsService.query().$promise.then(function (result) {
            $scope.accountGroups = result;
        });
    };
    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };
    $scope.getErrorMessage = function (response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null && response.data.modelState != null) {
            for (var i in response.data.modelState) {
                if (response.data.modelState[i].length > 0) {
                    message = "";
                    for (var j in response.data.modelState[i]) {
                        message += message.length > 0 ? '<br />' : '';
                        message += response.data.modelState[i][j];
                    }
                }
            }
        }

        return message;
    }
}]);

app.controller('CreateFacilityCtrl', ["$scope", "$state", "$stateParams", "$window", "CompaniesService", function ($scope, $state, $stateParams, $window, CompaniesService) {
    $scope.pageName = "New Facility";
    $scope.facility = {};

    $scope.companies = CompaniesService.query();
    //if navigated from management screen
    if ($stateParams.accountGroupId) {
        $scope.returnToPreviousPage = true;
        $scope.facility = { accountGroupId: $stateParams.accountGroupId };

        $scope.accountGroupsService.get({ id: $stateParams.accountGroupId }).$promise.then(function (accountGroupResult) {
            $scope.facility.accountGroupName = accountGroupResult.name;
        });
    }


    $scope.searchAccountGroups = function (searchStr) {
        if (searchStr) {
            $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function (response) {
                $scope.accountGroups = response.items;
            });
        } else {
            $scope.accountGroups = [];
        }
    };
    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);
    $scope.submit = function (form) {
        if (!form.$invalid) {
            $scope.facilitiesService.save($scope.facility, function(result) {
                if ($scope.returnToPreviousPage) {
                    $window.history.back();
                } else {
                    $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                    $state.go('app.facilities.list');
                }
            }, function (response) {
                $scope.sweetAlert.swal("Failed to Add the Facility Record!", $scope.getErrorMessage(response), "error");
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);

app.controller('EditFacilityCtrl', ["$scope", "$state", "$stateParams", "CompaniesService", function ($scope, $state, $stateParams, CompaniesService) {
    $scope.editMode = true;
    $scope.pageName = "Edit Facility";

    $scope.companies = CompaniesService.query();
    $scope.facility = $scope.facilitiesService.get({ id: $stateParams.facilityId });
    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);
    $scope.submit = function (form) {
        if (!form.$invalid) {
            $scope.facilitiesService.update({ id: $scope.facility.id }, $scope.facility, function () {
                $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                $state.go('app.facilities.list');
            }, function(response) {
                $scope.sweetAlert.swal('Failed to save the Facility Record!', $scope.getErrorMessage(response));
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };

}]);

app.controller('AllFacilitiesCtrl', ["$rootScope", "$scope", "ngTableParams", "blockUI", "FacilitiesService", function ($rootScope, $scope, ngTableParams, blockUI, FacilitiesService) {
    $scope.selectedFacilitiesIds = [];
    $scope.facilitiesTableBlock = blockUI.instances.get('facilitiesTableBlock');

    $scope.inactivateSelectedFacilities = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        var toInactiveStorage = [];

        angular.forEach(Object.keys($scope.selectedFacilitiesIds), function (facilityId) {
            var isFacilityChecked = $scope.selectedFacilitiesIds[facilityId];
            if (isFacilityChecked) {
                toInactiveStorage.push(facilityId);
            }
        });


        FacilitiesService.inactivate(toInactiveStorage).$promise.then(function (result) {
            $scope.tableParams.reload();
        }, function (response) {
            //TODO: add getErrorMessageService
            //SweetAlert.swal('Failed to inactivate  Accounts!', getErrorMessage(response), "error");
            $scope.sweetAlert.swal('Failed to inactivate  Facilities!', null, "error");
        });
    }

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { Name: "asc" },
        filter: { search: "", includeInactive: false }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.facilitiesTableBlock.start();

            var requestParams = {
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: params.filter.includeInactive,
                search: params.filter.search
            }
            $rootScope.$broadcast('load_facilities', requestParams);

            $scope.facilitiesLoadedCallback = function (response) {
                $scope.tableParams.isReady = true;
                $scope.facilitiesTableBlock.stop();
                $scope.selectedFacilitiesIds = [];
                params.total(response.count);
                $defer.resolve(response.items);
            };
        }
    });

    $rootScope.$on('facilities_successfully_loaded', function (events, response) {
        if ($scope.facilitiesLoadedCallback) {
            $scope.facilitiesLoadedCallback(response);
        }
    });

    $scope.$watch('tableParams.filter.includeInactive', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });
}]);

app.controller('FacilitiesListCtrl', ["$rootScope", "$scope", "FacilitiesService", function ($rootScope, $scope, FacilitiesService) {
    $scope.$on('load_facilities', function (events, args) {
        FacilitiesService.getPagedFacilities(args, function (response) {
            $rootScope.$emit('facilities_successfully_loaded', response);
        });
    });
}]);
