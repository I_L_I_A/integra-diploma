﻿'use strict';
app.controller('LogsCtrl', [
    "$rootScope", "$scope", "SweetAlert", "ngTableParams", "blockUI", "LogsResource", "$modal", function ($rootScope, $scope, SweetAlert, ngTableParams, blockUI, LogsResource, $modal) {
        $scope.logsResource = LogsResource;
        $scope.sweetAlert = SweetAlert;
        $scope.logsTableBlock = blockUI.instances.get('logsTableBlock');
        // Get the reference to the block service.
        $scope.tableParams = new ngTableParams({
            page: 1, count: 10,
            sorting: { "Date": "asc" },
            filter: { search: "", fromDate: "", toDate: "" }
        }, {
            getData: function ($defer, params) {
                var sortColumn = Object.keys(params.sorting());
                var reverse = params.sorting()[sortColumn] == "asc";
                $scope.tableParams.isReady = false;
                $scope.logsTableBlock.start();

                $scope.logsResource.getPagedLogs({
                    page: params.page(),
                    pageSize: params.count(),
                    sortColumn: sortColumn,
                    reverse: reverse,
                    search: $scope.tableParams.filter.search,
                    fromDate: $scope.tableParams.filter.fromDate,
                    toDate: $scope.tableParams.filter.toDate,
                }, function (response) {
                    $scope.tableParams.isReady = true;
                    $scope.logsTableBlock.stop();
                    params.total(response.count); //set total to create pagination items
                    $defer.resolve(response.items);
                });
            }
        });
        $scope.showStackTrace = function (stackTrace) {
            $modal.open({
                animation: true,
                template: '<div>Hello {{stackTrace}}</div>',
                controller: function ($scope, $modalInstance, stackTrace) {
                    $scope.stackTrace = stackTrace;
                },
                size: 'lg',
                resolve: {
                    stackTrace: function () {
                    return stackTrace;
                }
            }
            });
        };
        $scope.$watch('tableParams.filter.fromDate', function () {
            $scope.tableParams.$params.page = 1;
            $scope.tableParams.reload();
        });
        $scope.$watch('tableParams.filter.toDate', function () {
            $scope.tableParams.$params.page = 1;
            $scope.tableParams.reload();
        });
        $scope.$watch('tableParams.filter.search', function () {
            $scope.tableParams.$params.page = 1;
            $scope.tableParams.reload();
        });
    }
]);
