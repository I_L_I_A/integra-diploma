﻿'use strict';


app.controller('ReportsCtrl', ["$scope", "SweetAlert", "AccountGroupsService", "AccountsService", "FacilitiesService", "ReportsService", function ($scope, SweetAlert, AccountGroupsService, AccountsService, FacilitiesService, ReportsService) {
    $scope.sweetAlert = SweetAlert;
    $scope.accountGroupsService = AccountGroupsService;
    $scope.accountsService = AccountsService;
    $scope.facilitiesService = FacilitiesService;
    $scope.reportsService = ReportsService;
}]);


app.controller('PatientCensusCtrl', ["$scope", "ngTableParams", "blockUI", function ($scope, ngTableParams, blockUI) {
    $scope.patientsTableBlock = blockUI.instances.get('reportTableBlock');
    $scope.local = {
        selectedAccountGroupName: null,
        selectedAccountGroupId: null,
        isAccountSelectionDisabled: false,
        selectedAccount: null,
        selectedPatientsIds: [],
        matched: false,
        isActive: true,
        flagged: false
    }

    $scope.patients = $scope.reportsService.getPatientCensusPaged();
    angular.forEach($scope.patients, function (patient) {
        patient.flagged = false;
        patient.matched = false;
        patient.isActive = true;
    });

    $scope.filterPatients = function (item) {
        if ($scope.local.flagged === true && item.flagged !== true) {
            return false;
        }
        if ($scope.local.matched === true && item.matched !== true) {
            return false;
        }
        if ($scope.local.isActive === true && item.isActive !== true) {
            return false;
        }
        return true;
    };
    // init accountGroup and account by default for "Account" type user
    if ($scope.isInRole('Account')) {
        $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: '' }, function (response) {
            if (!response.count) {
                $scope.sweetAlert.swal("Account Group not specified for current user", null, "error");
                return;
            }
            $scope.local.selectedAccountGroupName = response.items[0].name;
            $scope.local.selectedAccountGroupId = response.items[0].id;
            $scope.accountsService.getPagedAccounts({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountGroupId: $scope.local.selectedAccountGroupId }, function (response) {
                if (!response.count) {
                    $scope.sweetAlert.swal("An Account not specified for current user", null, "error");
                    return;
                }
                if (response.count == 1) {
                    $scope.local.isAccountSelectionDisabled = true;
                    $scope.local.selectedAccount = response.items[0];
                }
            });
        });
    }

    $scope.searchAccountGroups = function (searchStr) {
        if (searchStr) {
            $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function (response) {
                $scope.accountGroups = response.items;
            });
        } else {
            $scope.accountGroups = [];
        }
    }

    $scope.accountGroupSelected = function () {
        $scope.local.selectedAccount = null;
        $scope.searchAccounts(null, $scope.local.selectedAccountGroupId);
    };

    $scope.searchAccounts = function (searchStr, accountGroupId) {
        if (accountGroupId) {
            $scope.accountsService.getPagedAccounts({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountGroupId: accountGroupId, search: searchStr }, function (response) {
                $scope.accounts = response.items;
            });
        } else {
            $scope.accounts = [];
        }
    };
}]);
