﻿'use strict';


app.controller('ProductsCtrl', ["$rootScope", "$scope", "SweetAlert", "ProductsService", "CompaniesService", "ProductPriceSheetsService", "ProductPriceSheetItemsService", "ProductCategoriesService", "Upload", "FormUnsavedChangesService",
    function ($rootScope, $scope, SweetAlert, ProductsService, CompaniesService, ProductPriceSheetsService, ProductPriceSheetItemsService, ProductCategoriesService, Upload, FormUnsavedChangesService) {
        $scope.productsService = ProductsService;
        $scope.companiesService = CompaniesService;
        $scope.productPriceSheetsService = ProductPriceSheetsService;
        $scope.ppsItemsService = ProductPriceSheetItemsService;
        $scope.productCategoriesService = ProductCategoriesService;
        $scope.uploader = Upload;
        $scope.sweetAlert = SweetAlert;
        $scope.formUnsavedChangesService = FormUnsavedChangesService;

        $scope.$on('load_products', function (events, args) {
            $scope.productsService.getPagedProducts(args, function (response) {
                $rootScope.$emit('products_successfully_loaded', response);
            });
        });

        $scope.$on('load_ppsItems', function (events, args) {
            $scope.ppsItemsService.getNestedPagedItems(args, args.productId, function (response) {
                $rootScope.$emit('ppsItems_successfully_loaded', response);
            });
        });
        $scope.editFormLoaded = function (form) {
            $scope.form = form;
        };
        $scope.getErrorMessage = function (response) {
            var message = 'Unknown Error';

            if (response != null && response.data != null && response.data.modelState != null) {
                for (var i in response.data.modelState) {
                    if (response.data.modelState[i].length > 0) {
                        message = "";
                        for (var j in response.data.modelState[i]) {
                            message += message.length > 0 ? '<br />' : '';
                            message += response.data.modelState[i][j];
                        }
                    }
                }
            }
            return message;
        };
    }]);

app.controller('CreateProductCtrl', ["$scope", "$stateParams", "$state", function ($scope, $stateParams, $state) {
    $scope.editMode = false;
    $scope.pageName = "New Product";
    $scope.product = { isActive: true };
    $scope.productCategories = [];

    $scope.$watch('product.image', function (newValue, oldValue) {
        $scope.product.previousImage = oldValue;
    });

    $scope.productCategoriesService.getPaged({ sortColumn: "Name" }, function (response) {
        $scope.productCategories = response.items;
    });

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function (form) {
        if (form.$valid) {
            $scope.product.productsToCategories = [];
            angular.forEach($scope.product.selectedCategories, function (categoryId) {
                $scope.product.productsToCategories.push({ categoryId: categoryId });
            });

            $scope.productsService.saveProduct($scope.product)
                .then(function (result) {
                    $scope.stateChangeStartEventStopper();
                    $state.go('app.products.list');
                }, function (response) {
                    $scope.sweetAlert.swal('Failed to save the Product Record!', $scope.getErrorMessage(response), 'error');
                    $scope.form.$submitted = false;
                    $scope.form.$setDirty();
                });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);


app.controller('EditProductCtrl', ["$scope", "$rootScope", "$stateParams", "$state", "$filter", "moment", function ($scope, $rootScope, $stateParams, $state, $filter, moment) {
    $scope.editMode = true;
    $scope.pageName = "Edit Product";
    $scope.product = { isActive: true };
    $scope.productCategories = [];
    $scope.local = {
        showProductPopup: false
    }

    $scope.productCategoriesService.getPaged({ sortColumn: "Name" }, function (response) {
        $scope.productCategories = response.items;
    });

    var fillAlreadySelectedCategories = function () {
        $scope.product.selectedCategories = [];
        if ($scope.product && $scope.product.productsToCategories.length > 0) {
            angular.forEach($scope.product.productsToCategories, function (relationship) {
                if (relationship.isActive && relationship.isCategoryActive) {
                    $scope.product.selectedCategories.push({ name: relationship.categoryName, id: relationship.categoryId });
                }
            });
        }
    }

    $rootScope.$on('edit_product', function (events, productId) {
        $scope.local.showProductPopup = true;
        $scope.productsService.get({ id: productId }).$promise.then(function (response) {
            $scope.unwatchProductImage();
            $scope.product = response;
            $scope.watchProductImage();
//            if ($scope.product && $scope.product.hasImage) {
//                $scope.productsService.getImage({ id: productId }).$promise.then(function (imageResponse) {
//                    $scope.product.image = imageResponse;
//                }, function() {
//                    $scope.product.image = null;
//                });
//            }
            fillAlreadySelectedCategories();
            if ($scope.product.isTemplate) {
                $rootScope.$emit("load_templateitems");
            } else {
                $rootScope.$broadcast("reload_pricesheetitems");
            }
        });
        
    });

    
    $scope.watchProductImage = function() {
        $scope.unwatchProductImage = $scope.$watch('product.image', function (newValue, oldValue) {
            if ($scope.product) {
                $scope.product.previousImage = oldValue;
            }
        });
    };
    $scope.watchProductImage();

    //$scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function (form) {
        if (form.$valid) {
            $scope.product.productsToCategories = [];
            angular.forEach($scope.product.selectedCategories, function (category) {
                var id = angular.isNumber(category) ? category : category.id;
                $scope.product.productsToCategories.push({ categoryId: id });
            });
            $scope.productsService.updateProduct($scope.product)
                .then(function (result) {
                    //$scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                    //$scope.local.showProductPopup = false;
                    //$rootScope.$emit("reload_products_list");
            }, function () {
                    scope.sweetAlert.swal('Failed to update the Product Record!', scope.getErrorMessage(response));
                    $scope.form.$submitted = false;
                });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
        }
    };
}]);

app.controller('AllProductsCtrl', ["$rootScope", "$scope", "$stateParams", "ngTableParams", "blockUI", function ($rootScope, $scope, $stateParams, ngTableParams, blockUI) {
    $scope.productsTableBlock = blockUI.instances.get('productsTableBlock');

    $scope.inactivateSelectedProducts = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        var toInactiveStorage = [];

        angular.forEach(Object.keys($scope.selectedProductsIds), function (productId) {
            var isProductChecked = $scope.selectedProductsIds[productId];
            if (isProductChecked) {
                toInactiveStorage.push(productId);
            }
        });

        $scope.productsService.inactivate(toInactiveStorage).$promise.then(function (result) {
            $scope.tableParams.reload();
        }, function (response) {
            //TODO: add getErrorMessageService
            //SweetAlert.swal('Failed to inactivate  Products!', getErrorMessage(response), "error");
            $scope.sweetAlert.swal('Failed to inactivate Products!', null, "error");
        });
    };

    $scope.editProduct = function (productId) {
        $stateParams.productId = productId;
        $rootScope.$broadcast('edit_product', productId);
    }

    // Get the reference to the block service.
    $scope.tableParams = new ngTableParams({
        page: 1, count: 10,
        sorting: { DisplayName: "asc" },
        filter: { search: "", includeInactive: false }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.productsTableBlock.start();

            $rootScope.$broadcast('load_products', {
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: params.filter.includeInactive,
                search: params.filter.search
            });

            $scope.productsLoadedCallback = function (response) {
                $scope.tableParams.isReady = true;
                $scope.productsTableBlock.stop();
                $scope.selectedProductsIds = [];
                params.total(response.count); //set total to create pagination items
                $defer.resolve(response.items);
            };
        }
    });


    $rootScope.$on('products_successfully_loaded', function (events, response) {
        if ($scope.productsLoadedCallback) {
            $scope.productsLoadedCallback(response);
        }
    });

    $scope.$watch('tableParams.filter.includeInactive', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $rootScope.$on('reload_products_list', function () {
        $scope.tableParams.reload();
    });
}]);

app.controller('ProductTemplateItems', ["$rootScope", "$stateParams", "$scope", "ngTableParams", "blockUI", "TemplateItemsService", "editableOptions", function ($rootScope, $stateParams, $scope, ngTableParams, blockUI, TemplateItemsService, editableOptions) {
    editableOptions.theme = 'bs3';
    $scope.local = {};
    $scope.productTemplateItemsTableBlock = blockUI.instances.get('productTemplateItemsTableBlock');

    $scope.companies = $scope.companiesService.query();

    $scope.searchProducts = function (searchStr) {
        if (searchStr && $scope.local.companyId) {
            $scope.productsService.getPagedProducts(
                {
                    page: 1, pageSize: $scope.settings.selectPageSize,
                    sortColumn: "DisplayName", search: searchStr,
                    includeTemplates: false, companyId: $scope.local.companyId
                }, function (response) {
                    $scope.products = response.items;
                });
        } else {
            $scope.products = [];
        }
    }

    $scope.companySelected = function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    }

    $scope.productSelected = function () {
        TemplateItemsService.save({ templateId: $stateParams.productId, childProductId: $scope.local.productItemId, companyId: $scope.local.companyId }).$promise.then(function (response) {
            $scope.tableParams.reload();
        }, function (response) {
            $scope.sweetAlert.swal("Failed to Add a new Template Item Record!", $scope.getErrorMessage(response), "error");
        });
        $scope.local.productItemId = null;
    }

    $scope.inactivate = function (templateItem) {
        TemplateItemsService.inactivate(templateItem).$promise.then(function (response) {
            $scope.tableParams.reload();
        }, function (response) {
            $scope.sweetAlert.swal("Failed to Remove a Tempalte Item Record!", $scope.getErrorMessage(response), "error");
        });
    }

    $scope.validateQuantity = function (quantity) {
        if (!quantity) {
            return false;
        }
    }

    $scope.updateTemplateItem = function (templateItem) {
        TemplateItemsService.update(templateItem).$promise.then(function (response) { }, function (response) {
            $scope.sweetAlert.swal("Failed to Save a Tempalte Item Record!", $scope.getErrorMessage(response), "error");
        });
    }

    // Get the reference to the block service.
    $scope.tableParams = new ngTableParams({
        page: 1, count: 10,
        sorting: { "Company.Name": "asc" },
        filter: { search: "", includeInactive: false }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.productTemplateItemsTableBlock.start();

            TemplateItemsService.getTemplatePagedItems({
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: $scope.includeInactive,
                includeTemplates: false,
                templateId: $stateParams.productId,
                companyId: $scope.local.companyId,
                search: params.filter.search
            }, function (response) {
                //set total items
                $scope.tableParams.isReady = true;
                $scope.productTemplateItemsTableBlock.stop();
                params.total(response.count);
                $defer.resolve(response.items);
            });
        }
    });
}]);


app.controller('ProductNestedPPSItemsListCtrl', ["$rootScope", "$scope", "$stateParams", "ProductPriceSheetItemsService", function ($rootScope, $scope, $stateParams, ProductPriceSheetItemsService) {
    $scope.$on('load_pricesheetitems', function (events, args) {
        args.productId = $stateParams.productId;
        if (!args.sortColumn && !args.reverse) {
            args.sortColumn = "ProductPriceSheet.Company.Name";
            args.reverse = false;
        }
        ProductPriceSheetItemsService.getNestedPagedItems(args, function (response) {
            $rootScope.$emit('pricesheetitems_successfully_loaded', response);
        });
    });
}]);