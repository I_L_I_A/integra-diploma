﻿'use strict';

app.controller('CompaniesCtrl', ["$scope", "CompaniesService", "SweetAlert", "FormUnsavedChangesService", function ($scope, CompaniesService, SweetAlert, FormUnsavedChangesService) {
    $scope.sweetAlert = SweetAlert;
    $scope.companiesService = CompaniesService;
    $scope.formUnsavedChangesService = FormUnsavedChangesService;

    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };
    $scope.getErrorMessage = function (response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null && response.data.modelState != null) {
            for (var i in response.data.modelState) {
                if (response.data.modelState[i].length > 0) {
                    message = "";
                    for (var j in response.data.modelState[i]) {
                        message += message.length > 0 ? '<br />' : '';
                        message += response.data.modelState[i][j];
                    }
                }
            }
        }

        return message;
    };
}]);

app.controller('CreateCompanyCtrl', ["$scope", "$state", "$stateParams", function ($scope, $state, $stateParams) {
    $scope.pageName = "New Company";
    $scope.company = {};
    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);
    $scope.submit = function (form) {
        if (!form.$invalid) {
            $scope.companiesService.save($scope.company, function (result) {
                $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                $state.go('app.companies.list');
            }, function (response) {
                $scope.sweetAlert.swal("Failed to Add the Company Record!", $scope.getErrorMessage(response), "error");
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);

app.controller('EditCompanyCtrl', ["$scope", "$state", "$stateParams", function ($scope, $state, $stateParams) {
    $scope.editMode = true;
    $scope.pageName = "Edit Company";

    $scope.company = $scope.companiesService.get({ id: $stateParams.companyId });
    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);
    $scope.submit = function (form) {
        if (!form.$invalid) {
            $scope.companiesService.update({ id: $scope.company.id }, $scope.company, function () {
                $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                $state.go('app.companies.list');
            }, function (response) {
                $scope.sweetAlert.swal('Failed to save the Company Record!', $scope.getErrorMessage(response));
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);


app.controller('AllCompaniesCtrl', ["$scope", "ngTableParams", "blockUI", function ($scope, ngTableParams, blockUI) {
    $scope.companiesTableBlock = blockUI.instances.get('companiesTableBlock');

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { Name: "asc" },
        filter: { search: "" }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.companiesTableBlock.start();

            $scope.companiesService.getPagedCompanies({
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: $scope.includeInactive,
                search: params.filter.search
            }, function (response) {
                //set total items
                $scope.tableParams.isReady = true;
                $scope.companiesTableBlock.stop();
                params.total(response.count);
                $defer.resolve(response.items);
            });
        }
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });
}]);


app.controller('CompanyPriceSheetsListCtrl', ["$rootScope", "$scope", "$stateParams", "ProductPriceSheetsService", function ($rootScope, $scope,  $stateParams,ProductPriceSheetsService) {
    $scope.$on('load_pricesheets', function (events, args) {
        args.companyId = $stateParams.companyId;
        ProductPriceSheetsService.getPagedPriceSheets(args, function (response) {
            $rootScope.$emit('pricesheets_successfully_loaded', response);
        });
    });
}]);