﻿'use strict';

app.controller('AccountsCtrl', ["$scope", "AccountsService", "SweetAlert", "AccountGroupsService", "CompaniesService", "ProductPriceSheetsService", "FormUnsavedChangesService", function ($scope, AccountsService, SweetAlert, AccountGroupsService, CompaniesService, ProductPriceSheetsService, FormUnsavedChangesService) {
    $scope.accountsService = AccountsService;
    $scope.companiesService = CompaniesService;
    $scope.accountGroupsService = AccountGroupsService;
    $scope.productPriceSheetsService = ProductPriceSheetsService;
    $scope.sweetAlert = SweetAlert;
    $scope.formUnsavedChangesService = FormUnsavedChangesService;

    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };
    $scope.getErrorMessage = function (response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null && response.data.modelState != null) {
            for (var i in response.data.modelState) {
                if (response.data.modelState[i].length > 0) {
                    message = "";
                    for (var j in response.data.modelState[i]) {
                        message += message.length > 0 ? '<br />' : '';
                        message += response.data.modelState[i][j];
                    }
                }
            }
        }

        return message;
    };
}]);

app.controller('CreateAccountCtrl', ["$scope", "$state", "$stateParams", "$window", function ($scope, $state, $stateParams, $window) {
    $scope.pageName = "New Account";
    $scope.account = {};
    $scope.accountGroups = [];

    //if navigated from management screen
    if ($stateParams.accountGroupId) {
        $scope.returnToPreviousPage = true;
        $scope.account = {
            accountGroupId: $stateParams.accountGroupId
        };

        $scope.accountGroupsService.get({ id: $scope.account.accountGroupId }).$promise.then(function (accountGroupResult) {
            $scope.accountGroup = accountGroupResult;
            $scope.account.accountGroupName = accountGroupResult.name;
        });
    }

    $scope.searchAccountGroups = function (searchStr) {
        if (searchStr) {
            $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function (response) {
                $scope.accountGroups = response.items;
            });
        } else {
            $scope.accountGroups = [];
        }
    };

    $scope.companies = $scope.companiesService.query();

    $scope.companySelected = function() {
        $scope.searchCompanyPriceSheets(null);
    };

    $scope.searchCompanyPriceSheets = function (searchStr) {
        if ($scope.account && $scope.account.companyId && searchStr) {
            $scope.productPriceSheetsService.getPagedPriceSheets({ companyId: $scope.account.companyId, page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function (response) {
                $scope.companyPriceSheets = response.items;
            });
        } else {
            $scope.companyPriceSheets = [];
        }
    };

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function (form) {
        if (!form.$invalid) {
            $scope.accountsService.save($scope.account, function (result) {
                if ($scope.returnToPreviousPage) {
                    $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                    $window.history.back();
                } else {
                    $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                    $state.go('app.accounts.list');
                }
            }, function (response) {
                $scope.sweetAlert.swal("Failed to Add the Account Record!", $scope.getErrorMessage(response), "error");
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);

app.controller('EditAccountCtrl', ["$scope", "$state", "$stateParams", function ($scope, $state, $stateParams) {
    $scope.editMode = true;
    $scope.pageName = "Edit Account";
    $scope.companies = $scope.companiesService.query();


    $scope.accountsService.get({ id: $stateParams.accountId }).$promise.then(function (accountResult) {
        $scope.account = accountResult;
        if ($scope.account.priceSheetId) {
            $scope.productPriceSheetsService.get({ id: $scope.account.priceSheetId }, function (priceSheetResult) {
                $scope.account.priceSheet = priceSheetResult;
                $scope.companyPriceSheets = [priceSheetResult];
            });
        }
    });

    $scope.companySelected = function() {
        $scope.searchCompanyPriceSheets(null);
    };

    $scope.searchCompanyPriceSheets = function (searchStr) {
        if ($scope.account && $scope.account.companyId && searchStr) {
            $scope.productPriceSheetsService.getPagedPriceSheets({ companyId: $scope.account.companyId, page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function (response) {
                $scope.companyPriceSheets = response.items;
            });
        } else {
            $scope.companyPriceSheets = [];
        }
    };

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function (form) {
        if (!form.$invalid) {
            $scope.accountsService.update({ id: $scope.account.id }, $scope.account, function () {
                $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                $state.go('app.accounts.list');
            }, function (response) {
                $scope.sweetAlert.swal('Failed to save the Account Record!', $scope.getErrorMessage(response));
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);

app.controller('UsersToAccountCtrl', ["$rootScope", "$scope", "$state", "$stateParams", "UsersService", function ($rootScope, $scope, $state, $stateParams, UsersService) {
    $scope.showOnlyConnectedUsers = true;
    $scope.grantAccessForUserToAccount = function (user, grantAccess) {
        var obj = {}; obj[user.id] = true;

        $scope.grantAccessForUsersToAccount(obj, grantAccess, function () {
            user.isAccessGranted = grantAccess;
        });
    }

    var getSelectedUsersIds = function (ids) {
        var usersStorage = [];

        angular.forEach(Object.keys(ids), function (userId) {
            var isUserChecked = ids[userId];
            if (isUserChecked) {
                usersStorage.push(userId);
            }
        });
        return usersStorage;
    };

    $scope.grantAccessForUsersToAccount = function (ids, grantAccess, successCallback) {
        var usersStorage = getSelectedUsersIds(ids);
        if (usersStorage.length == 0) return;

        $scope.accountsService.grantAccessForUsers({ id: $stateParams.accountId, grantAccess: grantAccess }, usersStorage, function (result) {
            if (successCallback) {
                successCallback();
            } else {
                $scope.selectedUsersIds = {};
                $scope.tableParams.reload();
            }
        }, function (response) {
            $scope.sweetAlert.swal('Failed to Grant Access for Users to  the Account!', $scope.getErrorMessage(response));
        });
    };

    $scope.$watch('showOnlyConnectedUsers', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$on('load_users', function (events, args) {
        args.accountId = $stateParams.accountId;
        args.onlyConnected = $scope.showOnlyConnectedUsers;
        UsersService.getPagedUsers(args, function (response) {
            markUsersWithAccessToAccount(response.items);
            $rootScope.$emit('users_successfully_loaded', response);
        });
    });

    var markUsersWithAccessToAccount = function (users) {
        angular.forEach(users, function (user) {
            user.isAccessGranted = false;
            angular.forEach(user.userAccounts, function (userAccountRelationship) {
                if (userAccountRelationship.accountId == $stateParams.accountId && userAccountRelationship.isActive) {
                    user.isAccessGranted = true;
                }
            });
        });
    };
}]);

app.controller('FacilitiesToAccountCtrl', ["$rootScope", "$scope", "$state", "$stateParams", "FacilitiesService", function ($rootScope, $scope, $state, $stateParams, FacilitiesService) {
    $scope.showOnlyConnectedFacilities = true;

    $scope.connectFacilityWithAccount = function (facility, connect /*true false*/) {
        var obj = {};
        obj[facility.id] = true;

        $scope.connectFacilitiesWithAccount(obj, connect, function () {
            facility.isConnected = connect;
        });
    };

    var getSelectedFacilitiesIds = function (ids) {
        var facilitiesStorage = [];

        angular.forEach(Object.keys(ids), function (facilityId) {
            var isFacilityChecked = ids[facilityId];
            if (isFacilityChecked) {
                facilitiesStorage.push(facilityId);
            }
        });
        return facilitiesStorage;
    };

    $scope.connectFacilitiesWithAccount = function (ids, connect /*true false*/, successCallback) {
        var facilitiesStorage = getSelectedFacilitiesIds(ids);
        if (facilitiesStorage.length == 0) return;

        $scope.accountsService.connectFacilities({ id: $stateParams.accountId, connect: connect }, facilitiesStorage, function (result) {
            if (successCallback) {
                successCallback();
            } else {
                $scope.selectedFacilitiesIds = {};
                $scope.tableParams.reload();
            }
        }, function (response) {
            $scope.sweetAlert.swal('Failed to Connect Facilities With Account!', $scope.getErrorMessage(response));
        });
    };

    $scope.$watch('showOnlyConnectedFacilities', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });


    $scope.$on('load_facilities', function (events, args) {
        args.accountId = $stateParams.accountId;
        args.onlyConnected = $scope.showOnlyConnectedFacilities;

        FacilitiesService.getPagedFacilities(args, function (response) {
            markConnectedFacilitiesWithAccount(response.items);
            $rootScope.$emit('facilities_successfully_loaded', response);
        });
    });

    var markConnectedFacilitiesWithAccount = function (facilities) {
        angular.forEach(facilities, function (facility) {
            facility.isConnected = false;
            angular.forEach(facility.facilityAccounts, function (facilityAccountRelationship) {
                if (facilityAccountRelationship.accountId == $stateParams.accountId && facilityAccountRelationship.isActive) {
                    facility.isConnected = true;
                }
            });
        });
    };
}]);


app.controller('AllAccountsCtrl', ["$rootScope", "$scope", "ngTableParams", "blockUI", function ($rootScope, $scope, ngTableParams, blockUI) {
    $scope.selectedAccountsIds = [];
    $scope.accountsTableBlock = blockUI.instances.get('accountsTableBlock');

    $scope.inactivateSelectedAccounts = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        var toInactiveStorage = [];

        angular.forEach(Object.keys($scope.selectedAccountsIds), function (accountId) {
            var isAccountChecked = $scope.selectedAccountsIds[accountId];
            if (isAccountChecked) {
                toInactiveStorage.push(accountId);
            }
        });

        $scope.accountsService.inactivate(toInactiveStorage).$promise.then(function (result) {
            $scope.tableParams.reload();
        }, function (response) {
            //TODO: add getErrorMessageService
            //SweetAlert.swal('Failed to inactivate  Accounts!', getErrorMessage(response), "error");
            $scope.sweetAlert.swal('Failed to inactivate  Accounts!', null, "error");
        });
    };

    // Get the reference to the block service.
    $scope.tableParams = new ngTableParams({
        page: 1, count: 10,
        sorting: { Name: "asc" },
        filter: { search: "", includeInactive: false }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.accountsTableBlock.start();

            $rootScope.$broadcast('load_accounts', {
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: params.filter.includeInactive,
                search: params.filter.search
            });

            $scope.accountsLoadedCallback = function (response) {
                $scope.tableParams.isReady = true;
                $scope.accountsTableBlock.stop();
                $scope.selectedAccountsIds = [];
                params.total(response.count); //set total to create pagination items
                $defer.resolve(response.items);
            };
        }
    });

    $rootScope.$on('accounts_successfully_loaded', function (events, response) {
        if ($scope.accountsLoadedCallback) {
            $scope.accountsLoadedCallback(response);
        }
    });

    $scope.$watch('tableParams.filter.includeInactive', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });
}]);

app.controller('AccountsListCtrl', ["$rootScope", "$scope", "AccountsService", function ($rootScope, $scope, AccountsService) {
    $scope.$on('load_accounts', function (events, args) {
        AccountsService.getPagedAccounts(args, function (response) {
            $rootScope.$emit('accounts_successfully_loaded', response);
        });
    });
}]);

