'use strict';
/**
 * Main Controller
 */
app.controller('AppCtrl', ['$rootScope', '$scope', '$state', '$localStorage', '$window', '$document', '$timeout', 'cfpLoadingBar', 'CurrentUserService',
function ($rootScope, $scope, $state, $localStorage, $window, $document, $timeout, cfpLoadingBar, CurrentUserService) {

    $scope.currentUserService = CurrentUserService;

    $rootScope.isInRole = function (role) {
        return CurrentUserService.isInRole(role);
    }

    $rootScope.hasPermissions = function (permissions) {
        return CurrentUserService.hasPermissions(permissions);
    }

    $rootScope.style = 'style2';
    $rootScope.theme = 'pink-blue';

    $scope.data = {};
    $scope.effect = '';
    $scope.header = {
        form: false,
        chat: false,
        theme: false,
        footer: true,
        history: false,
        animation: '',
        boxed: '',
        layout_menu: '',
        theme_style: 'style2',
        header_topbar: 'static',
        menu_style: 'sidebar-default',
        menu_collapse: '',
        layout_horizontal_menu: '',

        toggle: function (k) {
            switch (k) {
                case 'chat':
                    $scope.header.chat = !$scope.header.chat;
                    break;
                case 'form':
                    $scope.header.form = !$scope.header.form;
                    break;
                case 'sitebar':
                    $scope.header.menu_style = $scope.header.menu_style ? '' : (($scope.header.layout_menu === '') ? 'sidebar-collapsed' : 'right-side-collapsed');
                    break;
                case 'theme':
                    $scope.header.theme = !$scope.header.theme;
                    break;
                case 'history':
                    $scope.header.history = !$scope.header.history;
                    $scope.header.menu_style = $scope.header.history ? 'sidebar-collapsed' : 'sidebar-default';
                    break;
            }
        },

        collapse: function (c) {
            if (c === 'change') {
                $scope.header.menu_collapse = '';
            } else {
                if ($scope.header.menu_style) {
                    $scope.header.menu_style = '';
                    $scope.header.menu_collapse = $scope.header.menu_collapse ? '' : 'sidebar-collapsed';
                } else {
                    $scope.header.menu_collapse = $scope.header.menu_collapse ? '' : 'sidebar-collapsed';
                }
            }

        }
    };

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('.quick-sidebar').css('top', '0');
        } else {
            $('.quick-sidebar').css('top', '50px');
        }
    });
    $('.quick-sidebar > .header-quick-sidebar').slimScroll({
        "height": $(window).height() - 50,
        'width': '280px',
        "wheelStep": 30
    });
    $('#news-ticker-close').click(function (e) {
        $('.news-ticker').remove();
    });

    // Loading bar transition
    // -----------------------------------
    var $win = $($window);
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        //state is accessible for anonymous
        if (!toState.data || !toState.data.authenticate)
            return;
        var goToLoginPage = function () {
            event.preventDefault();
            $state.go("login.signin", { afterSignInState: toState.name, afterSignInStateParameters: toParams, wasRedirected: true }, { location: true, notify: false, reload: true })
                .then(function() {
                 $rootScope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
            });
        };
        // User isn�t authenticated
        if (toState.data.authenticate && !CurrentUserService.restore()) {
          goToLoginPage();
          return;
        } 
        
        if (toState.data.roles && toState.data.roles.length > 0) {
            if (toState.data.roles.indexOf(CurrentUserService.restore().roles) == -1) {
                //User tries to access the state which has role he doesn't have
                goToLoginPage();
                return;
            } else {
                if (CurrentUserService.restore().roles.indexOf("Supplier") > -1) {
                    return;
                }
                //We have a match in roles, lets check permissions
                if (toState.data.permissions && 
                    toState.data.permissions.length > 0) {
                    if (toState.data.permissions.indexOf(CurrentUserService.restore().permissions) == -1) {
                        //For example, reqular Account tries to access AccountGroupAdmin state.
                        goToLoginPage();
                        return;
                    }
                }
            }
        }
    });
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        //start loading bar on stateChangeStart

        cfpLoadingBar.start();

    });
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

        //stop loading bar on stateChangeSuccess
        event.targetScope.$watch("$viewContentLoaded", function () {

            cfpLoadingBar.complete();
        });

        // scroll top the page on change state

        $document.scrollTo(0, 0);

        if (angular.element('.email-reader').length) {
            angular.element('.email-reader').animate({
                scrollTop: 0
            }, 0);
        }

        // Save the route title
        $rootScope.currTitle = $state.current.title;
    });

    // State not found
    $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
        //$rootScope.loading = false;
        console.log(unfoundState.to);
        // "lazy.state"
        console.log(unfoundState.toParams);
        // {a:1, b:2}
        console.log(unfoundState.options);
        // {inherit:false} + default options
    });

    $rootScope.pageTitle = function () {
        return $rootScope.app.name + ' - ' + ($rootScope.currTitle || $rootScope.app.description);
    };

    // save settings to local storage
    if (angular.isDefined($localStorage.layout)) {
        $scope.app.layout = $localStorage.layout;

    } else {
        $localStorage.layout = $scope.app.layout;
    }
    $scope.$watch('app.layout', function () {
        // save to local storage
        $localStorage.layout = $scope.app.layout;
    }, true);

    //global function to scroll page up
    $scope.toTheTop = function () {

        $document.scrollTopAnimated(0, 600);

    };

    // Function that find the exact height and width of the viewport in a cross-browser way
    var viewport = function () {
        var e = window, a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return {
            width: e[a + 'Width'],
            height: e[a + 'Height']
        };
    };
    // function that adds information in a scope of the height and width of the page
    $scope.getWindowDimensions = function () {
        return {
            'h': viewport().height,
            'w': viewport().width
        };
    };
    // Detect when window is resized and set some variables
    $scope.$watch($scope.getWindowDimensions, function (newValue, oldValue) {
        $scope.windowHeight = newValue.h;
        $scope.windowWidth = newValue.w;
        if (newValue.w >= 992) {
            $scope.isLargeDevice = true;
        } else {
            $scope.isLargeDevice = false;
        }
        if (newValue.w < 992) {
            $scope.isSmallDevice = true;
        } else {
            $scope.isSmallDevice = false;
        }
        if (newValue.w <= 768) {
            $scope.isMobileDevice = true;
        } else {
            $scope.isMobileDevice = false;
        }
    }, true);



    // Apply on resize
    $win.on('resize', function () {
        $scope.$apply();
    });
}]);
