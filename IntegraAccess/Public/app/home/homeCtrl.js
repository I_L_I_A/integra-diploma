﻿'use strict';


app.controller('HomeCtrl', ["$scope", "$http", function ($scope, $http) {

}]);

app.controller('SpendingProjectionCtrl', ["$scope", "ngTableParams", "blockUI", "SweetAlert", "AccountGroupsService", "AccountsService", "FacilitiesService", function ($scope, ngTableParams, blockUI, SweetAlert, AccountGroupsService, AccountsService, FacilitiesService) {

    $scope.sweetAlert = SweetAlert;
    $scope.accountGroupsService = AccountGroupsService;
    $scope.accountsService = AccountsService;
    $scope.facilitiesService = FacilitiesService;

    $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
    $scope.series = ['Series A', 'Series B'];
    $scope.data = [
      [65, 59, 80, 81, 56, 55, 40],
      [28, 48, 40, 19, 86, 27, 90]
    ];
    $scope.onClick = function (points, evt) {
        console.log(points, evt);
    };

    $scope.patientsTableBlock = blockUI.instances.get('patientsTableBlock');
    $scope.local = {
        selectedAccountGroupId: null,
        selectedAccountId: null,
        selectedPatientsIds: []
    }

    $scope.searchAccountGroups = function (searchStr) {
        if (searchStr) {
            $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function (response) {
                $scope.accountGroups = response.items;
            });
        } else {
            $scope.accountGroups = [];
        }
    }

    $scope.searchAccounts = function (searchStr, accountGroupId) {
        if (accountGroupId) {
            $scope.accountsService.getPagedAccounts({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountGroupId: accountGroupId, search: searchStr }, function (response) {
                $scope.accounts = response.items;
            });
        } else {
            $scope.accounts = [];
        }
    }

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { LastName: "asc" },
        filter: { search: "" }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.patientsTableBlock.start();
            /*
            PatientsService.getPagedPatients({
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: $scope.includeInactive,
                search: params.filter.search
            }, function (response) {
                //set total items
                params.total(response.count);
                $scope.selectedPatientsIds = [];

                var items = [];
                angular.forEach(response.items, function (patient) {
                    patient.birthDate = patient.birthDate ? new Date(patient.birthDate) : null;
                    patient.deceasedDate = patient.deceasedDate ? new Date(patient.deceasedDate) : null;
                    items.push(patient);
                });

                $scope.tableParams.isReady = true;
                $scope.patientsTableBlock.stop();

                $defer.resolve(items);
            });*/
        }
    });
    /*
    $scope.$watch('includeInactive', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });*/
}]);