﻿'use strict';


app.controller('UsersCtrl', ["$scope", "UsersService", "SweetAlert", "AccountGroupsService", "FormUnsavedChangesService", function ($scope, UsersService, SweetAlert, AccountGroupsService, FormUnsavedChangesService) {
    $scope.usersService = UsersService;
    $scope.sweetAlert = SweetAlert;
    $scope.accountGroupsService = AccountGroupsService;
    $scope.formUnsavedChangesService = FormUnsavedChangesService;

 
    $scope.user = {
        accountType: $scope.isInRole('Supplier') ? "Supplier" : "Account",
        accounts: []
    };
    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };
    $scope.getErrorMessage = function(response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null)
            if (response.data.modelState != null) {
                for (var i in response.data.modelState) {
                    if (response.data.modelState[i].length > 0) {
                        message = "";
                        for (var j in response.data.modelState[i]) {
                            message += message.length > 0 ? '<br />' : '';
                            message += response.data.modelState[i][j];
                        }
                    }
                }
            } else {
                message = response.data.message;
            }
        return message;
    };
}]);

app.controller('CreateUsersCtrl', ["$scope", "$stateParams", "$state", "$window", function ($scope, $stateParams, $state, $window) {
    $scope.pageName = "New User";
    
    $scope.user = {
        accountType: $scope.isInRole('Supplier') ? "Supplier" : "Account",
    };
    $scope.accountGroups = [];

    //if navigated from management screen
    if ($stateParams.accountGroupId) {
        $scope.returnToPreviousPage = true;
        $scope.user = {
            accountGroupId: $stateParams.accountGroupId,
            accountType: $stateParams.type
        };
        $scope.accountGroup = $scope.accountGroupsService.get({ id: $scope.user.accountGroupId });
    }

    $scope.searchAccountGroups = function (searchStr) {
        if (searchStr) {
            $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function(response) {
                $scope.accountGroups = response.items;
            });
        } else {
            $scope.accountGroups = [];
        }
    };

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function(form) {
        if (!form.$invalid) {
            $scope.usersService.save($scope.user, function(result) {
                if ($scope.returnToPreviousPage) {
                    $window.history.back();
                } else {
                    $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                    $state.go('app.users.list');
                }
            }, function(response) {
                $scope.sweetAlert.swal("Failed to Add the User Record!", $scope.getErrorMessage(response), "error");
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);


app.controller('EditUsersCtrl', ["$scope", "$stateParams", "$state", function ($scope, $stateParams, $state) {
    $scope.editMode = true;
    $scope.pageName = "Edit User";
    $scope.user = {
        accountType: $scope.isInRole('Supplier') ? "Supplier" : "Account"
    };

    $scope.usersService.get({ id: $stateParams.userId }).$promise.then(function (userResult) {
        $scope.user = userResult;

        if ($scope.user.accountGroupId) {
            $scope.accountGroupsService.get({ id: $scope.user.accountGroupId }).$promise.then(function(accountGroupResult) {
                $scope.accountGroup = accountGroupResult;
            });
        }
    });

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function(form) {
        if (!form.$invalid) {
            $scope.usersService.update({ id: $scope.user.id }, $scope.user, function () {
                $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                $state.go('app.users.list');
            }, function(response) {
                $scope.sweetAlert.swal('Failed to save the User Record!', $scope.getErrorMessage(response));
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);

app.controller('AccountsToUserCtrl', ["$rootScope", "$scope", "$state", "$stateParams", "AccountsService", function ($rootScope, $scope, $state, $stateParams, AccountsService) {
    $scope.showOnlyConnectedAccounts = true;

    $scope.grantAccessForUserToAccount = function (account, grantAccess) {
        var obj = {}; obj[account.id] = true;

        $scope.grantAccessForUserToAccounts(obj, grantAccess, function () {
            account.hasUserAccess = grantAccess;
        });
    }

    var getSelectedAccountsIds = function (ids) {
        var accountsStorage = [];

        angular.forEach(Object.keys(ids), function (accountId) {
            var isAccountChecked = ids[accountId];
            if (isAccountChecked) {
                accountsStorage.push(accountId);
            }
        });
        return accountsStorage;
    }

    $scope.grantAccessForUserToAccounts = function (ids, grantAccess, successCallback) {
        var accountsStorage = getSelectedAccountsIds(ids);
        if (accountsStorage.length == 0) return;

        $scope.usersService.grantAccessForUserToAccounts({ id: $stateParams.userId, grantAccess: grantAccess }, accountsStorage, function (result) {
            if (successCallback) {
                successCallback();
            } else {
                $scope.selectedAccountsIds = {};
                $scope.tableParams.reload();
            }
        }, function (response) {
            $scope.sweetAlert.swal('Failed to Grant Access for User to  Accounts!', $scope.getErrorMessage(response));
        });
    }

    $scope.$watch('showOnlyConnectedAccounts', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$on('load_accounts', function (events, args) {
        args.userId = $stateParams.userId;
        args.onlyConnected = $scope.showOnlyConnectedAccounts;
        AccountsService.getPagedAccounts(args, function (response) {
            markAccountsWithUserAccess(response.items);
            $rootScope.$emit('accounts_successfully_loaded', response);
        });
    });

    var markAccountsWithUserAccess = function (accounts) {
        angular.forEach(accounts, function (account) {
            account.hasUserAccess = false;
            angular.forEach(account.userAccounts, function (userAccountRelationship) {
                if (userAccountRelationship.userId == $stateParams.userId && userAccountRelationship.isActive) {
                    account.hasUserAccess = true;
                }
            });
        });
    }
}]);

app.controller('AllUsersCtrl', ["$rootScope", "$scope", "ngTableParams", "blockUI", function ($rootScope, $scope, ngTableParams, blockUI) {
    $scope.usersTableBlock = blockUI.instances.get('usersTableBlock');
    $scope.selectedUsersIds = [];

    $scope.inactivateSelectedUsers = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        var toInactiveStorage = [];

        angular.forEach(Object.keys($scope.selectedUsersIds), function (userId) {
            var isUserChecked = $scope.selectedUsersIds[userId];
            if (isUserChecked) {
                toInactiveStorage.push(userId);
            }
        });

        $scope.usersService.inactivate(toInactiveStorage).$promise.then(function (result) {
            $scope.tableParams.reload();
        }, function (response) {
            //TODO: add getErrorMessageService
            //SweetAlert.swal('Failed to inactivate  Accounts!', getErrorMessage(response), "error");
            $scope.sweetAlert.swal('Failed to inactivate  Users!', null, "error");
        });
    }

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { LastName: "asc" },
        filter: { search: "", includeInactive: false }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.usersTableBlock.start();

            var requestParams = {
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: params.filter.includeInactive,
                search: params.filter.search
            }
            $rootScope.$broadcast('load_users', requestParams);

            $scope.usersLoadedCallback = function (response) {
                $scope.tableParams.isReady = true;
                $scope.usersTableBlock.stop();
                $scope.selectedUsersIds = [];
                params.total(response.count);
                $defer.resolve(response.items);
            };
        }
    });

    $rootScope.$on('users_successfully_loaded', function (events, response) {
        if ($scope.usersLoadedCallback) {
            $scope.usersLoadedCallback(response);
        }
    });

    $scope.$watch('tableParams.filter.includeInactive', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });
}]);

app.controller('UsersListCtrl', ["$rootScope", "$scope", "UsersService", function ($rootScope, $scope, UsersService) {
    $scope.$on('load_users', function (events, args) {
        UsersService.getPagedUsers(args, function (response) {
            $rootScope.$emit('users_successfully_loaded', response);
        });
    });
}]);


