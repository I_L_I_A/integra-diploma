﻿'use strict';

app.controller('PriceSheetsCtrl', ["$rootScope", "$scope", "SweetAlert",
        "CompaniesService",
        "ProductPriceSheetsService",
        "FormUnsavedChangesService",
    function ($rootScope, $scope, SweetAlert,
        CompaniesService, ProductPriceSheetsService, FormUnsavedChangesService) {
        $scope.companiesService = CompaniesService;
        $scope.ppsService = ProductPriceSheetsService;
        $scope.sweetAlert = SweetAlert;
        $scope.formUnsavedChangesService = FormUnsavedChangesService;
    }]);

app.controller('CreatePriceSheetCtrl', ["$scope", "$stateParams", "$state", function ($scope, $stateParams, $state) {
    $scope.editMode = false;
    $scope.pageName = "New PriceSheet";
    $scope.product = { isActive: true };

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };

    $scope.submit = function (form) {
        if (form.$valid) {
            $scope.productsService.saveProduct($scope.product)
                .then(function (result) {
                    $scope.stateChangeStartEventStopper();
                    $state.go('app.products.list');
                }, function (response) {
                    $scope.sweetAlert.swal('Failed to save the Product Record!', $scope.getErrorMessage(response), 'error');
                    $scope.form.$submitted = false;
                    $scope.form.$setDirty();
                });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);

app.controller('EditPriceSheetCtrl', ["$scope", "$stateParams", "$state", "ProductPriceSheetItemsService", function ($scope, $stateParams, $state, ProductPriceSheetItemsService) {
    $scope.editMode = true;
    $scope.pageName = "Edit Price Sheet";

    $scope.priceSheet = $scope.ppsService.get({ id: $stateParams.pricesheetId });
    $scope.companies = $scope.companiesService.query();

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };

    $scope.savePPSItem = function (data, priceSheetItem) {
        var ppsItem = angular.copy(priceSheetItem);
        angular.extend(ppsItem, data);
        if (validatePPSItem(ppsItem)) {
            ProductPriceSheetItemsService.update(ppsItem).$promise.then(function(result) {}, function() {
                $scope.sweetAlert.swal('Failed to update the Price Sheet Item Record!', scope.getErrorMessage(response));
            });
        } else {
            $scope.sweetAlert.swal('Unable to save more than 1 pricing option!', null);
            return '';
        }
    }

    var validatePPSItem = function(ppsItem) {
        if (ppsItem.priceSelling && (ppsItem.priceRental == null && ppsItem.priceDaily == null)) {
            return true;
        }
        if (ppsItem.priceRental && (ppsItem.priceSelling == null && !ppsItem.isSellingAllowed && ppsItem.priceDaily == null)) {
            return true;
        }
        if (ppsItem.priceDaily && (ppsItem.priceSelling == null && !ppsItem.isSellingAllowed && ppsItem.priceRental == null)) {
            return true;
        }
        return false;
    }

    $scope.submit = function (form) {
        if (form.$valid) {
            $scope.ppsService.update($scope.priceSheet).$promise
                .then(function (result) {
                    $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                    $state.go('app.pricesheets.list');
                }, function () {
                    scope.sweetAlert.swal('Failed to update the Price Sheet Record!', scope.getErrorMessage(response));
                    $scope.form.$submitted = false;
                });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
        }
    };
}]);

app.controller('AllPriceSheetsCtrl', ["$rootScope", "$scope", "ngTableParams", "blockUI", "SweetAlert", "ProductPriceSheetsService", "CompaniesService", function ($rootScope, $scope, ngTableParams, blockUI, SweetAlert, ProductPriceSheetsService, CompaniesService) {
    $scope.productPriceSheetsTableBlock = blockUI.instances.get('priceSheetsTableBlock');
    $scope.selectedIds = [];

    $scope.companies = CompaniesService.query();

    $scope.inactivateSelected = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        var toInactiveStorage = [];

        angular.forEach(Object.keys($scope.selectedIds), function (id) {
            var isChecked = $scope.selectedIds[id];
            if (isChecked) {
                toInactiveStorage.push(id);
            }
        });

        ProductPriceSheetsService.inactivate(toInactiveStorage).$promise.then(function (result) {
            $scope.tableParams.reload();
        }, function (response) {
            SweetAlert.swal('Failed to inactivate  Price Sheets!', $scope.getErrorMessage(response), "error");
        });
    };

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { Name: "asc" },
        filter: { search: "", companyId: null, includeInactive: false }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.productPriceSheetsTableBlock.start();

            $rootScope.$broadcast('load_pricesheets', {
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: params.filter.includeInactive,
                companyId: params.filter.companyId,
                search: params.filter.search
            });

            $scope.priceSheetsLoadedCallback = function (response) {
                $scope.tableParams.isReady = true;
                $scope.productPriceSheetsTableBlock.stop();
                $scope.selectedIds = [];
                params.total(response.count);
                $defer.resolve(response.items);
            };
        }
    });

    $scope.companySelected = function () {
        reloadData();
    }
    $scope.$watch('tableParams.filter.includeInactive', function () {
        reloadData();
    });
    $scope.$watch('tableParams.filter.search', function () {
        reloadData();
    });

    $rootScope.$on('pricesheets_successfully_loaded', function (events, response) {
        if ($scope.priceSheetsLoadedCallback) {
            $scope.priceSheetsLoadedCallback(response);
        }
    });

    var reloadData = function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    }
}]);

app.controller('PriceSheetsListCtrl', ["$rootScope", "$scope", "ProductPriceSheetsService", function ($rootScope, $scope, ProductPriceSheetsService) {
    $scope.$on('load_pricesheets', function (events, args) {
        ProductPriceSheetsService.getPagedPriceSheets(args, function (response) {
            $rootScope.$emit('pricesheets_successfully_loaded', response);
        });
    });
}]);

app.controller('PriceSheetNestedPPSItemsListCtrl', ["$rootScope", "$scope", "$stateParams", "ProductPriceSheetItemsService", function ($rootScope, $scope, $stateParams, ProductPriceSheetItemsService) {
    $scope.$on('load_pricesheetitems', function (events, args) {
        args.priceSheetId = $stateParams.pricesheetId;
        args.searchOnlyByProduct = true;
        if (!args.sortColumn && !args.reverse) {
            args.sortColumn = "Product.DisplayName";
            args.reverse = false;
        }
        ProductPriceSheetItemsService.getNestedPagedItems(args, function (response) {
            $rootScope.$emit('pricesheetitems_successfully_loaded', response);
        });
    });
}]);

app.controller('AccountsToPriceSheetCtrl', ["$rootScope", "$scope", "$state", "$stateParams", "AccountsService", function ($rootScope, $scope, $state, $stateParams, AccountsService) {
    $scope.showOnlyAssignedAccounts = true;

    $scope.assignAccountToPriceSheet = function (account, assign) {
        var obj = {}; obj[account.id] = true;

        $scope.assignAccountsToPriceSheet(obj, assign, function () {
            account.isAssignedToPriceSheet = assign;
        });
    }

    var getSelectedAccountsIds = function (ids) {
        var accountsStorage = [];

        angular.forEach(Object.keys(ids), function (accountId) {
            var isAccountChecked = ids[accountId];
            if (isAccountChecked) {
                accountsStorage.push(accountId);
            }
        });
        return accountsStorage;
    }

    $scope.assignAccountsToPriceSheet = function (ids, assign, successCallback) {
        var accountsStorage = getSelectedAccountsIds(ids);
        if (accountsStorage.length == 0) return;

        $scope.ppsService.assignAccountsToPriceSheet({ id: $stateParams.pricesheetId, assign: assign }, accountsStorage, function (result) {
            if (successCallback) {
                successCallback();
            } else {
                $scope.selectedAccountsIds = {};
                $scope.tableParams.reload();
            }
        }, function (response) {
            $scope.sweetAlert.swal('Failed to Assign Accounts to Price Sheet!', $scope.getErrorMessage(response));
        });
    }

    $scope.$watch('showOnlyAssignedAccounts', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$on('load_accounts', function (events, args) {
        args.priceSheetId = $stateParams.pricesheetId;
        args.onlyAssigned = $scope.showOnlyAssignedAccounts;
        AccountsService.getPriceSheetAccounts(args, function (response) {
            markAccountsAssignedToPriceSheet(response.items);
            $rootScope.$emit('accounts_successfully_loaded', response);
        });
    });

    var markAccountsAssignedToPriceSheet = function (accounts) {
        angular.forEach(accounts, function (account) {
            if (account.priceSheetId == $stateParams.pricesheetId) {
                account.isAssignedToPriceSheet = true;
            }
        });
    }
}]);