﻿'use strict';


app.controller('SendResetPasswordEmailCtrl', ['$rootScope', '$scope', '$state', 'authService', 'SweetAlert', function ($rootScope, $scope, $state, authService, SweetAlert) {
        $scope.userData = {
            email: "",
            userName: ""
        };
        $scope.$state = $state;
        $scope.message = "";

        $scope.sendResetPasswordEmail = function () {
            $scope.message = "";
            authService.sendResetPasswordEmail($scope.userData).then(function (response) {
                if (!response.hasError) {
                    $state.go('login.signin');
                    SweetAlert.swal({
                        title: "Password Restore",
                        text: "The instructions were sent to '"+ $scope.userData.email +"'.",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function(confirmButtonClicked) {
                    });
                } else {
                    SweetAlert.swal({
                        title: "Error",
                        text: response.data.message,
                        type: "error",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function (confirmButtonClicked) {
                        $scope.form.$submitted = false;
                    });
                }
            });
        };
    }
]);
