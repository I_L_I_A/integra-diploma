﻿'use strict';
app.factory('authInterceptorService', ['$q', '$injector', '$location', "CurrentUserService", function ($q, $injector, $location, CurrentUserService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var currentUser = CurrentUserService.get();
        if (currentUser != null) {
            config.headers.Authorization = 'Bearer ' + currentUser.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            var authService = $injector.get('authService');
            authService.logOut();
            $location.path('/login/signin');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);