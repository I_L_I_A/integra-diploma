﻿'use strict';


app.controller('ResetPasswordCtrl', ['$rootScope', '$scope', '$state', '$stateParams', 'authService', 'SweetAlert', function($rootScope, $scope, $state, $stateParams, authService, SweetAlert) {
        $scope.hasResetTokenParam = $stateParams.token ? true : false;
        $scope.hasUserNameParam = $stateParams.userName ? true : false;
        $scope.loginData = {
            newPassword: "",
            newPasswordRepeat: ""
        };

        $scope.message = "";

        $scope.resetPassword = function() {
            $scope.message = "";
            if (arePasswordsValid()) {
                var userData = {
                    newPassword: $scope.loginData.newPassword,
                    token: $stateParams.token,
                    userName: $stateParams.userName
                };
                authService.resetPassword(userData, function(response) {
                    $state.go('login.signin');
                    SweetAlert.swal({
                        title: "Password Reset",
                        text: "Password was successfully reset for '" + userData.userName + "'.",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function (confirmButtonClicked) {
                    });
                }, function (response) {
                    SweetAlert.swal({
                        title: "Error",
                        text: response.data.message,
                        type: "error",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function (confirmButtonClicked) {
                        $scope.form.$submitted = false;
                    });
                });
            } else {
                SweetAlert.swal("Passwords do not match!");
                $scope.form.$submitted = false;
            }
        };

        var arePasswordsValid = function() {
            if ($scope.loginData.newPassword == "") {
                return false;
            }
            if ($scope.loginData.newPasswordRepeat == "") {
                return false;
            }
            if ($scope.loginData.newPassword != $scope.loginData.newPasswordRepeat) {
                return false;
            }
            return true;
        };
    }
]);
