﻿'use strict';


app.controller('SigninCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$location', 'authService', function ($rootScope, $scope, $state, $stateParams, $location, authService) {

    authService.logOut();

    $scope.loginData = {
        login: "",
        password: "",
    };
    $scope.wasRedirected = $stateParams.wasRedirected ? true : false;
    $scope.message = "";

    $scope.login = function () {
        $scope.message = "";
        authService.login($scope.loginData).then(function (response) {
            if ($stateParams.afterSignInState) {
                $state.go($stateParams.afterSignInState, $stateParams.afterSignInStateParameters);
            } else {
                $state.go("app.home");
            }

        },
         function (err) {
             $scope.form.$submitted = false;
             $scope.message = err.error_description;
         });
    };



}]);
