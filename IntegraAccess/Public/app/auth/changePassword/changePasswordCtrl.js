﻿'use strict';


app.controller('ChangePasswordCtrl', ['$rootScope', '$scope', '$state', '$stateParams', 'authService', 'CurrentUserService', 'SweetAlert', function ($rootScope, $scope, $state, $stateParams, authService, CurrentUserService, SweetAlert) {
    $scope.loginData = {
        oldPassword: "",
        newPassword: "",
        newPasswordRepeat: ""
    };

    $scope.message = "";

    $scope.changePassword = function () {
        $scope.message = "";
 
        if (passwordsMatch()) {
            var user = CurrentUserService.get();
            authService.changePassword($scope.loginData, function (response) {
                    $state.go('login.signin');
                    SweetAlert.swal({
                        title: "Password Reset",
                        text: "Password was successfully changed for '" + CurrentUserService.get().login + "'.",
                        type: "success",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function (confirmButtonClicked) {
                    });
                },
                function (err) {
                    SweetAlert.swal({
                        title: "Error",
                        text: err.data.message,
                        type: "error",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        closeOnConfirm: true
                    }, function (confirmButtonClicked) {
                        $scope.form.$submitted = false;
                    });
                });
        } else {
            SweetAlert.swal("Passwords do not match!");
            $scope.form.$submitted = false;
        }
    };

    var passwordsMatch = function () {
        if ($scope.loginData.newPassword != $scope.loginData.newPasswordRepeat) {
            return false;
        }
        return true;
    };
}]);
