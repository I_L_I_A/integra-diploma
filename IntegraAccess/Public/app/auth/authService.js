﻿'use strict';
app.factory('authService', ['$rootScope', '$http', '$q', "CurrentUserService", "UsersService", function ($rootScope, $http, $q, CurrentUserService, UsersService) {

    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        login: "",
    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.login + "&password=" + loginData.password;//TODO: create password salt instead of passing raw password.

        var deferred = $q.defer();

        $http.post('api/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            _authentication.isAuth = true;
            _authentication.login = loginData.login;

            CurrentUserService.set({
                token: response.access_token,
                userName: response.user_name,
                roles: response.user_roles,
                permissions: response.user_permissions,
                login: loginData.login,
                changePassword: (response.change_password === "true") ? true : false
            });
            CurrentUserService.save();

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {
        _clearAuth();
        CurrentUserService.clear();
    };

    var _fillAuthData = function () {
        _clearAuth();
        var currentUser = CurrentUserService.restore();
        if (currentUser != null) {
            _authentication.isAuth = true;
            _authentication.login = currentUser.login;
        }
    };

    var _clearAuth = function() {
        _authentication.isAuth = false;
        _authentication.login = "";
    };

    var _changePassword = function (passwords, successfullCallback, errorCallback) {
        var deferred = $q.defer();
        UsersService.changePassword({},
            {
                UserName: CurrentUserService.get().login,
                OldPassword: passwords.oldPassword,
                NewPassword: passwords.newPassword
            },
            function (response) {
                if (successfullCallback) {
                    successfullCallback(response);
                }
                _logOut();
                return deferred.resolve(response);
            }, function (e) {
                if (errorCallback) {
                    errorCallback(e);
                }
                return deferred.resolve(e);
            });

        return deferred.promise;

    };

    var _sendResetPasswordEmail = function (userData) {
        var deferred = $q.defer();
        UsersService.sendResetPasswordEmail({ userName: userData.userName, email: userData.email }, function (response) {
            response.hasError = false;
            deferred.resolve(response);
        }, function(response) {
            response.hasError = true;
            deferred.resolve(response);
        });

        _logOut();
        return deferred.promise;
    };

    var _resetPassword = function (userData, successfullCallback, errorCallback) {
        var deferred = $q.defer();
        UsersService.resetPassword({},
            {
                UserName: userData.userName,
                ResetPasswordToken: userData.token,
                NewPassword: userData.newPassword
            },
            function (response) {
                if (successfullCallback) {
                    successfullCallback(response);
                }
                return deferred.resolve(response);
            }, function (e) {
                if (errorCallback) {
                    errorCallback(e);
                }
                return deferred.resolve(e);
            });

        _logOut();
        return deferred.promise;
    };

    authServiceFactory.authentication = _authentication;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.clearAuth = _clearAuth;
    authServiceFactory.changePassword = _changePassword;
    authServiceFactory.sendResetPasswordEmail = _sendResetPasswordEmail;
    authServiceFactory.resetPassword = _resetPassword;

    return authServiceFactory;
}]);