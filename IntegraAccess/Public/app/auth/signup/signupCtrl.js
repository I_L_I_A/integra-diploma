﻿'use strict';
/** 
  * controller for angular-aside
  * Off canvas side menu to use with ui-bootstrap. Extends ui-bootstrap's $modal provider.
*/
app.controller('SignupCtrl', ["$scope", "$http", "authService", "$timeout", "$location", function ($scope, $http, authService, $timeout, $location) {
    $scope.savedSuccessfully = false;
    $scope.message = "";

    $scope.user = {
        login: "",
        firstName: "",
        lastName: "",
        phone: "",
        email: "",
        password: "",
        confirmPassword: ""
    };

    $scope.signUp = function () {
        $scope.message = "";
        authService.saveRegistration($scope.user).then(function (response) {

            $scope.savedSuccessfully = true;
            $scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
            startTimer();

        },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             $scope.message = "Failed to register user due to:" + errors.join(' ');
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/login/signin');
        }, 2000);
    }
}]);