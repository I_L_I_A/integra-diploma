﻿'use strict';

app.controller('AccountsGroupsCtrl', ["$scope", "AccountGroupsService", "SweetAlert", "FormUnsavedChangesService", "ProductPriceSheetsService", function ($scope, AccountGroupsService, SweetAlert, FormUnsavedChangesService, ProductPriceSheetsService) {
    $scope.accountsGroupsService = AccountGroupsService;
    $scope.sweetAlert = SweetAlert;
    $scope.formUnsavedChangesService = FormUnsavedChangesService;
    $scope.productPriceSheetsService = ProductPriceSheetsService;

    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };
    $scope.getErrorMessage = function(response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null && response.data.modelState != null) {
            for (var i in response.data.modelState) {
                if (response.data.modelState[i].length > 0) {
                    message = "";
                    for (var j in response.data.modelState[i]) {
                        message += message.length > 0 ? '<br />' : '';
                        message += response.data.modelState[i][j];
                    }
                }
            }
        }

        return message;
    };
}]);

app.controller('CreateAccountsGroupsCtrl', ["$scope", "$state", function ($scope, $state) {
    $scope.pageName = "New Account Group";
    $scope.accountGroup = {};
    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);
    $scope.submit = function(form) {
        if (!form.$invalid) {
            $scope.accountsGroupsService.save($scope.accountGroup, function(result) {
                $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                $state.go('app.groups.list');
            }, function(response) {
                $scope.sweetAlert.swal("Failed to Add the Account Group Record!", $scope.getErrorMessage(response), "error");
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);

app.controller('EditAccountsGroupsCtrl', ["$rootScope", "$scope", "$state", "$stateParams", "$filter", "UsersService", "FacilitiesService", "AccountsService", function ($rootScope, $scope, $state, $stateParams, $filter, UsersService, FacilitiesService, AccountsService) {
    $scope.editMode = true;
    $scope.pageName = "Edit Account Group";

    $scope.accountGroup = $scope.accountsGroupsService.get({ id: $stateParams.accountGroupId });
    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);
    $scope.submit = function(form) {
        if (!form.$invalid) {
            $scope.accountsGroupsService.update({ id: $scope.accountGroup.id }, $scope.accountGroup, function() {
                $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                $state.go('app.groups.list');
            }, function(response) {
                $scope.sweetAlert.swal('Failed to save the Account Group Record!', $scope.getErrorMessage(response), "error");
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };

    $scope.$on('load_users', function (events, args) {
        args.accountGroupId = $stateParams.accountGroupId;
        UsersService.getPagedUsers(args, function (response) {
            $rootScope.$emit('users_successfully_loaded', response);
        });
    });
    $scope.$on('load_facilities', function (events, args) {
        args.accountGroupId = $stateParams.accountGroupId;
        FacilitiesService.getPagedFacilities(args, function (response) {
            $rootScope.$emit('facilities_successfully_loaded', response);
        });
    });
    $scope.$on('load_accounts', function (events, args) {
        args.accountGroupId = $stateParams.accountGroupId;
        AccountsService.getPagedAccounts(args, function (response) {
            $rootScope.$emit('accounts_successfully_loaded', response);
        });
    });
}]);


app.controller('AllAccountsGroupsCtrl', ["$scope", "ngTableParams", "blockUI", function ($scope, ngTableParams, blockUI) {
    $scope.selectedAccountGroupsIds = [];
    $scope.accountGroupsTableBlock = blockUI.instances.get('accountGroupsTableBlock');

    $scope.inactivateSelectedUsers = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        var toInactiveStorage = [];

        angular.forEach(Object.keys($scope.selectedAccountGroupsIds), function (accountGroupId) {
            var isAccountGroupChecked = $scope.selectedAccountGroupsIds[accountGroupId];
            if (isAccountGroupChecked) {
                toInactiveStorage.push(accountGroupId);
            }
        });

        $scope.accountsGroupsService.inactivate(toInactiveStorage).$promise.then(function (result) {
            $scope.tableParams.reload();
        }, function (response) {
            //TODO: add getErrorMessageService
            //SweetAlert.swal('Failed to inactivate  Account Groups!', getErrorMessage(response), "error");
            $scope.sweetAlert.swal('Failed to inactivate  Account Groups!', null, "error");
        });
    }

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { Name: "asc" },
        filter: { search: "" }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.accountGroupsTableBlock.start();

            $scope.accountsGroupsService.getPagedAccountGroups({
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: $scope.includeInactive,
                search: params.filter.search
            }, function (response) {
                //set total items
                $scope.tableParams.isReady = true;
                $scope.accountGroupsTableBlock.stop();
                $scope.selectedAccountGroupsIds = [];
                params.total(response.count);
                $defer.resolve(response.items);
            });
        }
    });

    $scope.$watch('includeInactive', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });
}]);

