﻿app.directive('stringToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value, 10);
            });
        }
    };
});

app.directive('backButton', function () {
    return {
        restrict: 'A',

        link: function (scope, element, attrs) {
            element.bind('click', goBack);

            function goBack() {
                history.back();
                scope.$apply();
            }
        }
    };
}); 

app.directive('closeBackdrop', function () {
    return {
        restrict: 'A',

        link: function (scope, element, attrs) {
            element.bind('click', close);

            function close() {
                $("#modal-header-primary").modal('hide');
                scope.$apply();
            }
        }
    };
});

app.directive("ngAccordion", function ($parse, $compile) {
    return {
        link: function ($scope, element, attributes) {
            $scope._accordion = { status: [], collapse: {} };

            $scope._accordion.collapse = function (i) {
                for (var j = 0; j < $scope._accordion.status.length; j++) {
                    if (i == j)
                        continue;
                    $scope._accordion.status[j] = true;
                }
                $scope._accordion.status[i] = !$scope._accordion.status[i];
            };

            $(">div", attributes.$$element).each(function (index, item) {
                $scope._accordion.status[index] = true;
                $(">.panel-heading>a", item).attr({ 'ng-click': '_accordion.collapse(' + index + ')', 'index': index });
                $(">.panel-collapse", item).attr({ 'collapse': '_accordion.status[' + index + ']', 'index': index });
            });

            element.html($compile(element.html())($scope));
        }
    };
});
app.directive("ngAnimation", function ($parse, $compile) {
    return {
        link: function ($scope, element, attributes) {
            $scope._animation_change = function (v) {
                $scope.header.effect = v;
            };

            attributes.$$element.find('button').each(function (index, value) {
                $(this).attr({ 'ng-click': "_animation_change('" + $(this).attr('data-value') + "')" });
            });

            element.html($compile(element.html())($scope));
        }
    };
});
app.directive("ngAreachartspline", function () {
    return {
        link: function ($scope, element, attributes) {
            //BEGIN AREA CHART SPLINE
            var d6_1 = [["Jan", 67], ["Feb", 91], ["Mar", 36], ["Apr", 150], ["May", 28], ["Jun", 123], ["Jul", 38]];
            var d6_2 = [["Jan", 59], ["Feb", 49], ["Mar", 45], ["Apr", 94], ["May", 76], ["Jun", 22], ["Jul", 31]];
            $.plot("#area-chart-spline", [{
                data: d6_1,
                label: "Upload",
                color: "#ffce54"
            }, {
                data: d6_2,
                label: "Download",
                color: "#01b6ad"
            }], {
                series: {
                    lines: {
                        show: !1
                    },
                    splines: {
                        show: !0,
                        tension: 0.4,
                        lineWidth: 2,
                        fill: 0.8
                    },
                    points: {
                        show: !0,
                        radius: 4
                    }
                },
                grid: {
                    borderColor: "#fafafa",
                    borderWidth: 1,
                    hoverable: !0
                },
                tooltip: !0,
                tooltipOpts: {
                    content: "%x : %y",
                    defaultTheme: true
                },
                xaxis: {
                    tickColor: "#fafafa",
                    mode: "categories"
                },
                yaxis: {
                    tickColor: "#fafafa"
                },
                shadowSize: 0
            });
            //END AREA CHART SPLINE
        }
    };
});
app.directive("ngDropzone", function ($parse, $compile) {
    return {
        link: function ($scope, element, attributes) {
            $(attributes.$$element).dropzone({
                url: "http://www.torrentplease.com/dropzone.php",
                maxFilesize: 100,
                paramName: "uploadfile",
                maxThumbnailFilesize: 5,
                init: function () {
                    //$scope.files.push({file: 'added'}); // here works
                    this.on('success', function (file, json) {
                    });

                    this.on('addedfile', function (file) {
                        $scope.$apply(function () {
                            //alert(file);
                            //$scope.files.push({file: 'added'});
                        });
                    });

                    this.on('drop', function (file) {
                        //alert('file');
                    });

                }

            });
        }
    };
});

app.directive("ngGeneraltab", function () {
    return {
        link: function (scope, element, attrs) {
            element.click(function (e) {
                e.preventDefault();
            });
        }
    };
});
//app.directive("ngMenu", function($parse, $compile){
//    return {
//        link: function($scope, element, attributes){
//            $scope._menu = {status:[], collapse:{}, hover:[]};

//			$scope._menu.mouseleave = function(){
//				for(var j=0; j<$scope._menu.hover.length; j++){
//					$scope._menu.hover[j] = '';
//				}
//			};
//			$scope._menu.mouseover = function(i){
//				for(var j=0; j<$scope._menu.hover.length; j++){
//					$scope._menu.hover[j] = '';
//				}
//				$scope._menu.hover[i] = 'nav-hover';
//			};
//            $scope._menu.collapse = function(i){
//                $scope._menu.status[i] = !$scope._menu.status[i];

//                var current = attributes.$$element.find('a[index='+i+']');

//                current.parent('li').addClass('active').siblings().removeClass('active').children('ul').each(function(){
//                    $scope._menu.status[$(this).attr('index')] = true;
//                });

//                if(current.hasClass('btn-fullscreen')){
//                    if (!document.fullscreenElement &&
//                        !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement){
//                        if (document.documentElement.requestFullscreen) {
//                            document.documentElement.requestFullscreen();
//                        } else if (document.documentElement.msRequestFullscreen) {
//                            document.documentElement.msRequestFullscreen();
//                        } else if (document.documentElement.mozRequestFullScreen) {
//                            document.documentElement.mozRequestFullScreen();
//                        } else if (document.documentElement.webkitRequestFullscreen) {
//                            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
//                        }
//                    } else {
//                        if (document.exitFullscreen) {
//                            document.exitFullscreen();
//                        } else if (document.msExitFullscreen) {
//                            document.msExitFullscreen();
//                        } else if (document.mozCancelFullScreen) {
//                            document.mozCancelFullScreen();
//                        } else if (document.webkitExitFullscreen) {
//                            document.webkitExitFullscreen();
//                        }
//                    }
//                }
//            };

//            attributes.$$element.find('li').children('a').each(function(index, value){
//                $scope._menu.status[index] = true;
//                $(this).attr({'ng-click': '_menu.collapse('+index+')', 'index':index});
//                $('>ul', $(this).parent('li')).attr({'collapse': '_menu.status['+index+']', 'index':index});
//            });

//			$(">li", attributes.$$element).each(function(index, value){
//				$scope._menu.hover[index] = '';
//				$(this).attr({'ng-mouseleave':'_menu.mouseleave()', 'ng-mouseover': '_menu.mouseover('+index+')', 'ng-class':'_menu.hover['+index+']'});
//			});

//            element.html($compile(element.html())($scope));
//        }
//    };
//});
app.directive("ngTab", function ($parse, $compile) {
    return {
        link: function ($scope, element, attributes) {
            $("a", element).click(function (e) {
                e.preventDefault();
            });
        }
    };
});
app.directive("ngTheme", function ($parse, $compile) {
    return {
        link: function ($scope, element, attributes) {
        }
    };
});
app.directive("ngZabutocalendar", function ($parse, $compile) {
    return {
        link: function ($scope, element, attributes) {
            //BEGIN CALENDAR
            $("#my-calendar").zabuto_calendar({
                language: "en"
            });
            //END CALENDAR
        }
    };
});
app.directive("scrollSpy", function ($window) {
    return {
        restrict: 'A',
        controller: function ($scope) {
            $scope.spies = [];
            this.addSpy = function (spyObj) {
                $scope.spies.push(spyObj);
            };
        },
        link: function (scope, elem, attrs) {
            var spyElems;
            spyElems = [];

            scope.$watch('spies', function (spies) {
                var spy, _i, _len, _results;
                _results = [];

                for (_i = 0, _len = spies.length; _i < _len; _i++) {
                    spy = spies[_i];

                    if (spyElems[spy.id] === null) {
                        _results.push(spyElems[spy.id] = elem.find('#' + spy.id));
                    }
                }
                return _results;
            });

            $($window).scroll(function () {
                var highlightSpy, pos, spy, _i, _len, _ref;
                highlightSpy = null;
                _ref = scope.spies;

                // cycle through `spy` elements to find which to highlight
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    spy = _ref[_i];
                    spy.out();

                    // catch case where a `spy` does not have an associated `id` anchor
                    if (spyElems === null || spyElems[spy.id] === null || spyElems[spy.id].offset() === undefined) {
                        continue;
                    }

                    if ((pos = spyElems[spy.id].offset().top) - $window.scrollY <= 0) {
                        // the window has been scrolled past the top of a spy element
                        spy.pos = pos;

                        if (highlightSpy === null) {
                            highlightSpy = spy;
                        }
                        if (highlightSpy.pos < spy.pos) {
                            highlightSpy = spy;
                        }
                    }
                }

                // select the last `spy` if the scrollbar is at the bottom of the page
                if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    spy.pos = pos;
                    highlightSpy = spy;
                }

                return highlightSpy !== null ? highlightSpy["in"]() : void 0;
            });
        }
    };
});

app.directive('spy', function ($location, $anchorScroll) {
    return {
        restrict: "A",
        require: "^scrollSpy",
        link: function (scope, elem, attrs, affix) {
            elem.click(function () {
                $location.hash(attrs.spy);
                $anchorScroll();
            });

            affix.addSpy({
                id: attrs.spy,
                in: function () {
                    elem.addClass('active');
                },
                out: function () {
                    elem.removeClass('active');
                }
            });
        }
    };
});

app.directive('maxlength', function () {
    return {
        restrict: 'A',
        link: function ($scope, $element, $attributes) {

            var limit = $attributes.maxlength;
            $element.bind('keyup', function (event) {
                var element = $element.closest(".form-group");

                element.toggleClass('has-warning', limit - $element.val().length <= 10);
                element.toggleClass('has-error', $element.val().length >= limit);
            });

            $element.bind('keypress', function (event) {
                // Once the limit has been met or exceeded, prevent all keypresses from working
                if ($element.val().length >= limit) {
                    // Except backspace
                    if (event.keyCode != 8) {
                        event.preventDefault();
                    }
                }
            });
        }
    };
});

/** 
  * Password-check directive.
*/
app.directive('compareTo', function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
});

/** 
  * Prevent default action on empty links.
*/
app.directive('a', function () {
    return {
        restrict: 'E',
        link: function (scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function (e) {
                    e.preventDefault();
                });
            }
        }
    };
});

/**
 * Make element 100% height of browser window.
 */
app.directive('ctFullheight', ['$window', '$rootScope', '$timeout', 'APP_MEDIAQUERY',
function ($window, $rootScope, $timeout, mq) {
    return {
        restrict: "AE",
        scope: {
            ctFullheightIf: '&'
        },
        link: function (scope, elem, attrs) {
            var $win = $($window);
            var $document = $(document);
            var exclusionItems;
            var exclusionHeight;
            var setHeight = true;
            var page;

            scope.initializeWindowSize = function () {
                $timeout(function () {
                    exclusionHeight = 0;
                    if (attrs.ctFullheightIf) {
                        scope.$watch(scope.ctFullheightIf, function (newVal, oldVal) {
                            if (newVal && !oldVal) {
                                setHeight = true;
                            } else if (!newVal) {
                                $(elem).css('height', 'auto');
                                setHeight = false;
                            }
                        });
                    }

                    if (attrs.ctFullheightExclusion) {
                        var exclusionItems = attrs.ctFullheightExclusion.split(',');
                        angular.forEach(exclusionItems, function (_element) {
                            exclusionHeight = exclusionHeight + $(_element).outerHeight(true);
                        });
                    }
                    if (attrs.ctFullheight == 'window') {
                        page = $win;
                    } else {
                        page = $document;
                    }

                    scope.$watch(function () {
                        scope.__height = page.height();
                    });
                    if (setHeight) {
                        $(elem).css('height', 'auto');
                        if (page.innerHeight() < $win.innerHeight()) {
                            page = $win;
                        }
                        $(elem).css('height', page.innerHeight() - exclusionHeight);
                    }
                }, 300);
            };

            scope.initializeWindowSize();
            scope.$watch('__height', function (newHeight, oldHeight) {
                scope.initializeWindowSize();
            });
            $win.on('resize', function () {
                scope.initializeWindowSize();
            });

        }
    };
}]);



/** 
  * It's like click, but when you don't click on your element. 
*/
app.directive('offClick', ['$document', '$timeout', function ($document, $timeout) {

    function targetInFilter(target, filter) {
        if (!target || !filter) return false;
        var elms = angular.element(document.querySelectorAll(filter));
        var elmsLen = elms.length;
        for (var i = 0; i < elmsLen; ++i)
            if (elms[i].contains(target)) return true;
        return false;
    }

    return {
        restrict: 'A',
        scope: {
            offClick: '&',
            offClickIf: '&'
        },
        link: function (scope, elm, attr) {

            if (attr.offClickIf) {
                scope.$watch(scope.offClickIf, function (newVal, oldVal) {
                    if (newVal && !oldVal) {
                        $timeout(function () {
                            $document.on('click', handler);
                        });
                    } else if (!newVal) {
                        $document.off('click', handler);
                    }
                }
                );
            } else {
                $document.on('click', handler);
            }

            scope.$on('$destroy', function () {
                $document.off('click', handler);
            });

            function handler(event) {
                // This filters out artificial click events. Example: If you hit enter on a form to submit it, an
                // artificial click event gets triggered on the form's submit button.
                if (event.pageX == 0 && event.pageY == 0) return;

                var target = event.target || event.srcElement;
                if (!(elm[0].contains(target) || targetInFilter(target, attr.offClickFilter))) {
                    scope.$apply(scope.offClick());
                }
            }
        }
    };
}]);


app.directive('perfectScrollbar', ['$parse', '$window',
function ($parse, $window) {
    var psOptions = ['wheelSpeed', 'wheelPropagation', 'minScrollbarLength', 'useBothWheelAxes', 'useKeyboard', 'suppressScrollX', 'suppressScrollY', 'scrollXMarginOffset', 'scrollYMarginOffset', 'includePadding'//, 'onScroll', 'scrollDown'
    ];

    return {
        restrict: 'EA',
        transclude: true,
        template: '<div><div ng-transclude></div></div>',
        replace: true,
        link: function ($scope, $elem, $attr) {
            var jqWindow = angular.element($window);
            var options = {};
            if (!$scope.app.isMobile) {
                for (var i = 0, l = psOptions.length; i < l; i++) {
                    var opt = psOptions[i];
                    if ($attr[opt] !== undefined) {
                        options[opt] = $parse($attr[opt])();
                    }
                }

                $scope.$evalAsync(function () {
                    $elem.perfectScrollbar(options);
                    var onScrollHandler = $parse($attr.onScroll);
                    $elem.scroll(function () {
                        var scrollTop = $elem.scrollTop();
                        var scrollHeight = $elem.prop('scrollHeight') - $elem.height();
                        $scope.$apply(function () {
                            onScrollHandler($scope, {
                                scrollTop: scrollTop,
                                scrollHeight: scrollHeight
                            });
                        });
                    });
                });

                function update(event) {
                    $scope.$evalAsync(function () {
                        if ($attr.scrollDown == 'true' && event != 'mouseenter') {
                            setTimeout(function () {
                                $($elem).scrollTop($($elem).prop("scrollHeight"));
                            }, 100);
                        }
                        $elem.perfectScrollbar('update');
                    });
                }

                // This is necessary when you don't watch anything with the scrollbar
                $elem.bind('mousemove', update);

                // Possible future improvement - check the type here and use the appropriate watch for non-arrays
                if ($attr.refreshOnChange) {
                    $scope.$watchCollection($attr.refreshOnChange, function () {
                        update();
                    });
                }

                // this is from a pull request - I am not totally sure what the original issue is but seems harmless
                if ($attr.refreshOnResize) {
                    jqWindow.on('resize', update);
                }

                $elem.bind('$destroy', function () {
                    jqWindow.off('resize', update);
                    $elem.perfectScrollbar('destroy');
                });
            }
        }
    };
}]);
app.directive('onErrorSrc', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            attrs.$observe("afklLazyImageLoaded", function (attrValue) {
                if (attrValue == false) {
                    var html = '<image class="temporary-image-loader" src="' + attrs.onLoad + '">';
                    element.append(html);
                    $compile(html)(scope);
                }
                if (attrValue == "done") {
                    element.children(".temporary-image-loader").remove();
                }
                if (attrValue == "fail") {
                    var html = '<div>' + attrs.onErrorSrc + '</div>';
                    var e = $compile(html)(scope);
                    element.replaceWith(e);
                }
            });
        }
    };
});

app.directive('phoneMask', function ($compile) {
    var masks = {
        "usa-international": "+1(999) 999 9999",
        "local": "999 99 99",
        "domestic": "(999) 999-9999"
    };
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            attrs.uiMask = masks[scope.phoneNumberUiMask] ? masks[scope.phoneNumberUiMask] : masks["usa-international"];
        }
    };
});

app.directive('dateMask', function ($compile) {
    var mask = "99/99/9999";

    var format = "MM/DD/YYYY";
    var placeholder = format;
    return {
        restrict: 'A',
        priority: 1001,
        terminal: true,
        require: 'ngModel',
        compile: function (el) {
            var x = 10;
            return function (scope, element, attrs, ngModel) {
                element.removeAttr("date-mask");
                element.attr("ui-mask", '');
                if (attrs.dateMask) {
                    mask = attrs.dateMask;
                }
                attrs.placeholder = placeholder;
                attrs.uiMask = mask;
                $compile(element)(scope);
                //            ngModel.$parsers.push(function (value) {
                //                if (attrs.dateMask) {
                //                    if()
                //                }
                //                return '' + value;
                //            });
                //            ngModel.$formatters.push(function (value) {
                //                if (value) {
                //                    return value.replace(/\//g, '');
                //                }
                //            });
            };
        }
    };
});

app.directive('pageLoader', function () {
    var loaders = {
        "32x32": "Public/content/images/pageloader/loader8.GIF",
        "41x32": "Public/content/images/pageloader/loader6.GIF",
        "174x15": "Public/content/images/pageloader/loader2.GIF",
        "100x15": "Public/content/images/pageloader/loader9.GIF",
        "120x128": "Public/content/images/pageloader/loader14.GIF",
    };
    var defaultSize = "32x32";
    return {
        restrict: 'E',
        template:
            '<img ng-src="{{pageLoaderUrl}}"/>',
        scope: {
            size: '@'
        },
        link: function (scope, $element, attrs) {
            var size = attrs.size ? attrs.size : defaultSize;
            scope.pageLoaderUrl = loaders[size] ? loaders[size] : loaders[defaultSize];
        }
    };
});

app.directive('rcSubmit', [
    '$parse', function ($parse) {
        return {
            restrict: 'A',
            require: 'form',
            link: function (scope, formElement, attributes, formController) {

                var fn = $parse(attributes.rcSubmit);

                formElement.bind('submit', function (event) {
                    // if form is not valid cancel it.
                    if (!formController.$valid) return false;

                    scope.$apply(function () {
                        fn(scope, { $event: event });
                    });
                });
            }
        };
    }
]);

app.directive('multipleEmails', function () {
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;

    function validateAll(ctrl, validatorName, value) {
        var validity = ctrl.$isEmpty(value) || value.split(',').every(
            function (email) {
                return EMAIL_REGEXP.test(email.trim());
            }
        );

        ctrl.$setValidity(validatorName, validity);
        return validity ? value : undefined;
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {
            function multipleEmailsValidator(value) {
                return validateAll(modelCtrl, 'multipleEmails', value);
            }

            modelCtrl.$formatters.push(multipleEmailsValidator);
            modelCtrl.$parsers.push(multipleEmailsValidator);
        }
    };
});

app.directive("inputNumber", ['$parse', '$compile', function ($parse, $compile) {
    return {
        restrict: 'A',
        priority: 1001,
        require: 'ngModel',
        scope: true,
        compile: function (el, attrs) {
            return function ($scope, $elem, $attrs, $modelCtrl) {
                $scope.checkLength = function (modelValue) {
                    modelValue = parseInt(modelValue);
                    if (modelValue) {
                        if (modelValue > $scope.maxValue) {
                            $scope.model.assign($scope, $scope.maxValue);
                        }
                        if (modelValue < $scope.minValue) {
                            $scope.model.assign($scope, $scope.minValue);
                        }
                    } else {
                        $scope.model.assign($scope, 0);
                    }
                };
                $elem.replaceWith($compile($elem.clone()
                                    .removeAttr("input-number")
                                    .attr("ng-change", "checkLength(" + $attrs.ngModel + ")"))($scope));
                $scope.minValue = $attrs.min ? parseInt($attr('min')) : 0;
                $scope.maxValue = $attrs.max? parseInt($attr('max')) : 100;
                $scope.model = $parse($attrs.ngModel);
            };
        }
    };
}]);

