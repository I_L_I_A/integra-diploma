'use strict';

app.factory('LogsResource', ["$resource", function ($resource) {
    return $resource('api/log/:id', {}, {
        getPagedLogs: {
            method: 'GET',
            url: 'api/logs/getPagedLogs'
        },
        log: {
            method: 'POST',
            url: 'api/logs/log'
        }
    });
}]);

app.service('LogsService', ['LogsResource', 'CurrentUserService', '$location',
    function (LogsResource, CurrentUserService, $location) {
        var log = function(logEntry) {
            logEntry.userName = CurrentUserService.restore() ? CurrentUserService.restore().login : "anonymous";
            logEntry.location = $location.path();
            logEntry.errorType = "Angular";
            JL.setOptions({
                'defaultAjaxUrl': 'api/logs/log',
                'requestId': logEntry
            });
            JL('Angular').error(logEntry);
        };
        this.debug = function(logEntry) {
            logEntry = logEntry ? logEntry : {};
            logEntry.logLevel = 'DEBUG';
            log(logEntry);
        };
        this.info = function(logEntry) {
            logEntry = logEntry ? logEntry : {};
            logEntry.logLevel = 'INFO';
            log(logEntry);
        };
        this.warn = function(logEntry) {
            logEntry = logEntry ? logEntry : {};
            logEntry.logLevel = 'WARN';
            log(logEntry);
        };
        this.error = function(exception, cause) {
            var logEntry = {};
            logEntry.logLevel = 'ERROR';
            logEntry.message = exception.message;
            logEntry.stackTrace = exception.stack;
            log(logEntry);
        };
    }])
.factory('$exceptionHandler', ['$injector', function ($injector) {
    return function (exception, cause) {
        console.error(exception.message, exception.stack);
//        var appSettings = $injector.get("AppSettings");
//        if (appSettings && appSettings.logClientSideErrors == 'true') {
//            var exceptionDescription = {
//                referrer: document.referrer
//            }
//            if (exception) {
//                exceptionDescription.error = exception.message;
//                exceptionDescription.stack = exception.stack;
//            }
//
//            JL().log(4000, exceptionDescription);
//        }
        var currentUserService = $injector.get("CurrentUserService");
        if (currentUserService && currentUserService.restore() && currentUserService.restore().token) {
            var logService = $injector.get("LogsService");
            logService.error(exception, cause);
        }
    };
}])
.factory('logToServerInterceptor', ['$q', '$injector', function ($q, $injector) {

    var myInterceptor = {
        'request': function (config) {
            config.msBeforeAjaxCall = new Date().getTime();
            return config;
        },
        'response': function (response) {
            if (response.config.warningAfter) {
                var msAfterAjaxCall = new Date().getTime();
                var timeTakenInMs = msAfterAjaxCall - response.config.msBeforeAjaxCall;
                if (timeTakenInMs > response.config.warningAfter) {
                    var LogsService = $injector.get('LogsService');
                    LogsService.warn({ timeTakenInMs: timeTakenInMs, config: response.config, data: response.data });
                }
            }

            return response;
        },
        'responseError': function (rejection) {
            if (rejection && rejection.status && rejection.status == 404) {
                var currentUserService = $injector.get("CurrentUserService");
                if (currentUserService.restore() && currentUserService.restore().token) {
                    var LogsService = $injector.get('LogsService');
                    LogsService.error({ message: "Not Found", status: rejection.status, stack: rejection.data.stackTrace, config: rejection.config }, rejection.data);
                }
            }
            return $q.reject(rejection);
        }
    };

    return myInterceptor;
}]);

