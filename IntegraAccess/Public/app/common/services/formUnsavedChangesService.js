﻿'use strict';

app.service('FormUnsavedChangesService', ["$state", 'SweetAlert', 'cfpLoadingBar', function ($state, SweetAlert, cfpLoadingBar) {
    var currentScope = null;
    this.listenToUnsavedChanges = function (scope) {
        if (scope) {
            currentScope = scope;
            currentScope.stopListeningToUnsavedChanges = this.stopListeningToUnsavedChanges;
            currentScope.stateChangeStartEventStopper = currentScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    if (!currentScope.form.$pristine) {
                        event.preventDefault();
                        SweetAlert.swal({
                            title: "Are you sure?",
                            text: "You have unsaved changes",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            closeOnConfirm: true
                        }, function(confirm) {
                            if (confirm) {
                                currentScope.stopListeningToUnsavedChanges();
                                $state.go(toState.name, toParams);
                            }
                        });
                    } else {
                        currentScope.stopListeningToUnsavedChanges();
                    }
                });
        }
    };

    this.stopListeningToUnsavedChanges = function () {
        if (currentScope && currentScope.stateChangeStartEventStopper) {
            currentScope.stateChangeStartEventStopper();
            currentScope = null;
        }
    };
}]);