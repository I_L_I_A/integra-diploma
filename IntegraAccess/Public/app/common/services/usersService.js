﻿'use strict';

app.factory('UsersService', ["$resource", function ($resource) {
    return $resource('api/user/:id', {
        oldPassword: '@oldPassword',
        newPassword: '@newPassword',
        userName: '@userName'
    }, {
        update: { method: 'PUT' },
        inactivate: {
            method: 'PUT',
            url: 'api/users/inactivate'
        },
        getPagedUsers: {
            method: 'GET',
            url: 'api/users/getPagedUsers'
        },
        grantAccessForUserToAccounts: {
            method: 'PUT',
            url: 'api/users/grantAccessForUserToAccounts/:id'
        },
        changePassword: {
            method: 'POST',
            url: 'api/users/changePassword/',
        },
        sendResetPasswordEmail: {
            method: 'GET',
            url: 'api/users/sendResetPasswordEmail/'
        },
        resetPassword: {
            method: 'POST',
            url: 'api/users/resetPassword/',
        }
    });
}]);