﻿'use strict';

app.factory('FacilitiesService', ["$resource", function ($resource) {
    return $resource('api/facility/:id', {}, {
        update: { method: 'PUT' },
        inactivate: {
            method: 'PUT',
            url: 'api/facilities/inactivate'
        },
        getPagedFacilities: {
            method: 'GET',
            url: 'api/facilities/getPagedFacilities'
        }
    });
}]);