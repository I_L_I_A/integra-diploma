﻿'use strict';

app.factory('ProductCategoriesService', ["$resource", function ($resource) {
    return $resource('api/productCategory/:id', {}, {
        getPaged: {
            method: 'GET',
            url: 'api/productCategories/getPaged'
        }
    });
}]);