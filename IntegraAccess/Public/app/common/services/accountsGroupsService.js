﻿'use strict';

app.factory('AccountGroupsService', [
    "$resource", function ($resource) {
        return $resource('api/accountgroup/:id', {}, {
            update: { method: 'PUT' },
            getPagedAccountGroups: {
                method: 'GET',
                url: 'api/accountgroups/getPagedAccountGroups'
            },
            inactivate: {
                method: 'PUT',
                url: 'api/accountgroups/inactivate'
            },
        });
    }
]);
