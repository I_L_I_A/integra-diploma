﻿'use strict';

app.factory('CompaniesService', ["$resource", function ($resource) {
    return $resource('api/company/:id', {}, {
        update: { method: 'PUT' },
        getPagedCompanies: {
            method: 'GET',
            url: 'api/companies/getPagedCompanies'
        }
    });
}]);