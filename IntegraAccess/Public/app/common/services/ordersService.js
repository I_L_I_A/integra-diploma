﻿'use strict';

app.factory('OrdersService', ["$resource", "PURCHASE_TYPES", function ($resource, PURCHASE_TYPES) {
    var service = $resource('api/order/:id', { isSubmitted: '@isSubmitted' }, {
        update: { method: 'PUT' },
        getPaged: {
            method: 'GET',
            url: 'api/orders/getPaged'
        },
        getPossibleOrderStateActions: {
            method: 'GET',
            url: 'api/orders/getPossibleOrderStateActions'
        },
        setState: {
            method: 'PUT',
            url: 'api/orders/setState'
        }
    });

    service.orderStorage = {
        currentOrder: {
            id: 0,
            patient: {},
            orderLineItems: []
        },

        setPatient: function (patient) {
            this.currentOrder.patient = patient;
        },

        setShippingInfo: function (shippingInfo) {
            this.currentOrder.shippingStreet1 = shippingInfo.street1;
            this.currentOrder.shippingStreet2 = shippingInfo.street2;
            this.currentOrder.shippingCity = shippingInfo.city;
            this.currentOrder.shippingState = shippingInfo.state;
            this.currentOrder.shippingZip = shippingInfo.zip;
        },

        addPPSItemToOrder: function (productPriceSheetItem) {
            if (productPriceSheetItem.isTemplate) {
                this.addTemplateToOrder(productPriceSheetItem);
            } else {
                this.addSimplePPSItemToOrder(productPriceSheetItem);
            }
        },

        addTemplateToOrder: function (templatePPSItem) {
            service.orderStorage.addSimplePPSItemToOrder(templatePPSItem);
            if (templatePPSItem && templatePPSItem.nestedPPSItems) {
                angular.forEach(templatePPSItem.nestedPPSItems, function (ppsItem) {
                    service.orderStorage.addSimplePPSItemToOrder(ppsItem, templatePPSItem.productId);
                });
            }
        },

        addSimplePPSItemToOrder: function (productPriceSheetItem, templateId) {
            if (this.IsExist(productPriceSheetItem.productId, templateId)) {
                this.remove(productPriceSheetItem.productId);
            }
            var purchaseTypeId = this.getPurchaseType(productPriceSheetItem);
            var orderLineItem = {
                product: this.ppsItemToProduct(productPriceSheetItem),
                productId: productPriceSheetItem.productId,
                orderId: this.currentOrder.id,
                price: productPriceSheetItem.isTemplate ? this.getTemplatePrice(productPriceSheetItem) : this.getProductPrice(productPriceSheetItem),
                purchaseTypeId: purchaseTypeId,
                purchaseTypeName: this.getPurchaseTypeName(purchaseTypeId),
                quantity: productPriceSheetItem.quantity,
                addedByProductTemplateId: templateId ? templateId : null
            };
            this.currentOrder.orderLineItems.push(orderLineItem);
        },

        getProductPrice: function (ppsItem) {
            return ppsItem.priceDaily || ppsItem.priceRental || (ppsItem.isSellingAllowed ? ppsItem.priceSelling : 0) || 0;
        }, 
        
        getPurchaseType: function(ppsItem) {
            if (ppsItem.priceDaily != null && ppsItem.priceDaily >= 0) {
                return PURCHASE_TYPES.DAILY;
            }
            if (ppsItem.priceRental != null && ppsItem.priceRental >= 0) {
                return PURCHASE_TYPES.MONTHLY;
            }
            if (ppsItem.priceSelling != null && ppsItem.isSellingAllowed && ppsItem.priceSelling >= 0) {
                return PURCHASE_TYPES.PURCHASE;
            }
            return PURCHASE_TYPES.DAILY;
        },

        getPurchaseTypeName: function (purchaseTypeId) {
            if (purchaseTypeId == PURCHASE_TYPES.DAILY) {
                return "Daily";
            }
            if (purchaseTypeId == PURCHASE_TYPES.MONTHLY) {
                return "Monthly";
            }
            if (purchaseTypeId == PURCHASE_TYPES.PURCHASE) {
                return "Purchase";
            }
            return "";
        },

        getTemplatePrice: function (templatePPSItem) {
            var price = 0;
            angular.forEach(templatePPSItem.nestedPPSItems, function (ppsItem) {
                price += ppsItem.quantity * service.orderStorage.getProductPrice(ppsItem);
            });
            return price;
        },

        ppsItemToProduct: function (ppsItem) {
            return {
                id: ppsItem.productId,
                displayName: ppsItem.productName,
                imageUrl: ppsItem.productImageUrl,
                shortDescription: ppsItem.productShortDescription,
                isTemplate: ppsItem.isTemplate
            }
        },

        //only for independent product or template ( not nested items)
        getQuantityOnOrder: function (productId) {
            var result = 0;
            angular.forEach(this.currentOrder.orderLineItems, function (orderLineItem) {
                if (orderLineItem.product.id == productId && !orderLineItem.addedByProductTemplateId) {
                    result = orderLineItem.quantity;
                }
            });
            return result;
        },

        total: function () {
            var result = 0;
            angular.forEach(this.currentOrder.orderLineItems, function (item) {
                if (item.isTemplate || !item.addedByProductTemplateId) {
                    result += item.price * item.quantity;
                }
            });
            return result;
        },

        IsExist: function (productId, templateId) {
            var result = false;
            angular.forEach(this.currentOrder.orderLineItems, function (orderLineItem) {
                if (orderLineItem.product.id == productId) {
                    //if independent product
                    if (!orderLineItem.addedByProductTemplateId && !templateId) {
                        result = true;
                    }
                    //if template nested item
                    if (orderLineItem.addedByProductTemplateId && templateId && orderLineItem.addedByProductTemplateId == templateId) {
                        result = true;
                    }
                }
            });
            return result;
        },

        remove: function (productId) {
            var lineItems = [];
            angular.forEach(this.currentOrder.orderLineItems, function (orderLineItem) {
                //for templates and independent items
                if (orderLineItem.product.id == productId && orderLineItem.addedByProductTemplateId) {
                    lineItems.push(orderLineItem);
                }
                //for nested items
                if (orderLineItem.product.id != productId && orderLineItem.addedByProductTemplateId != productId) {
                    lineItems.push(orderLineItem);
                }
            });
            this.currentOrder.orderLineItems = lineItems;
        },

        getOrderLineItems: function () {
            return this.currentOrder.orderLineItems;
        },

        clearOrderLineItems: function () {
            this.currentOrder.orderLineItems = [];
        },

        clearCurrentOrder: function () {
            this.currentOrder = {
                id: 0,
                patient: {},
                orderLineItems: []
            }
        }
    };



    return service;
}]);