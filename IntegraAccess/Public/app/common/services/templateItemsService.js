﻿'use strict';

app.factory('TemplateItemsService', ["$resource", "$q", "Upload", "$http", function ($resource, $q, Upload, $http) {
    var service = $resource('api/productTemplateItem/:id', {}, {
        update: {
            method: 'PUT'
        },
        inactivate: {
            method: 'PUT',
            url: 'api/templateItems/inactivate'
        },
        getTemplatePagedItems: {
            method: 'GET',
            url: 'api/templateItems/getPagedTemplateItems'
        }
    });
    return service;
}]);