﻿'use strict';

app.factory('AccountsService', ["$resource", function ($resource) {
    return $resource('api/account/:id', {}, {
        update: { method: 'PUT' },
        inactivate: {
            method: 'PUT',
            url: 'api/accounts/inactivate'
        },
        grantAccessForUsers: {
            method: 'PUT',
            url: 'api/accounts/grantAccessForUsers/:id'
        },
        connectFacilities: {
            method: 'PUT',
            url: 'api/accounts/connectFacilities/:id'
        },
        getPagedAccounts: {
            method: 'GET',
            url: 'api/accounts/getPagedAccounts'
        },
        getPriceSheetAccounts: {
            method: 'GET',
            url: 'api/accounts/getPriceSheetAccounts'
        }
    });
}]);