﻿'use strict';

app.factory('ReportsService', ["$resource", function ($resource) {
    var service = $resource('api/report/:id', {}, {
        update: { method: 'PUT' }
    });

    service.getPatientCensusPaged = function () {
        return [{ "id": 1, "firstName": "Nissim", "age": 41, "money": 454 }, { "id": 2, "firstName": "Mariko", "age": 10, "money": -100 }, { "id": 3, dmePrimeNotes: "(9/15/2015 2:50pm) We have issued a pickup.", clientNotes: "(9/15/2015 2:00pm) Patient discharged", "firstName": "Mark", "age": 39, "money": 291 }, { "id": 4, "firstName": "Allen", "age": 85, "money": 871 }, { "id": 5, clientNotes: "(9/15/2015 2:00pm) Patient discharged", "firstName": "Dustin", "age": 10, "money": 378 }, { "id": 6, "firstName": "Macon", "age": 9, "money": 128 }, { "id": 7, "firstName": "Ezra", "age": 78, "money": 11 }, { "id": 8, "firstName": "Fiona", "age": 87, "money": 285 }, { "id": 9, "firstName": "Ira", "age": 7, "money": 816 }, { "id": 10, "firstName": "Barbara", "age": 46, "money": 44 }, { "id": 11, "firstName": "Lydia", "age": 56, "money": 494 }, { "id": 12, "firstName": "Carlos", "age": 80, "money": 193 }];
    }

    return service;
}]);
