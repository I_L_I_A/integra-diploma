﻿'use strict';

app.factory('ProductPriceSheetItemsService', ["$resource", function ($resource) {
    return $resource('api/productPriceSheetItem/:id', {}, {
        update: { method: 'PUT' },
        getAccountPagedItems : {
            method: 'GET',
            url: 'api/ppsItems/getAccountPagedItems'
        },
        getNestedPagedItems: {
            method: 'GET',
            url: 'api/ppsItems/getNestedPagedItems'
        }
    });
}]);