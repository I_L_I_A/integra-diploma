﻿'use strict';

app.factory('PatientsService', ["$resource", function ($resource) {
    return $resource('api/patient/:id', {}, {
        update: { method: 'PUT' },
        inactivate: {
            method: 'PUT',
            url: 'api/patients/inactivate'
        },
        getPagedPatients: {
            method: 'GET',
            url: 'api/patients/getPagedPatients'
        },
        checkDuplicate: {
            method: 'POST',
            url: 'api/patients/checkDuplicate'
        }
    });
}]);