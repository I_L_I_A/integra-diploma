﻿'use strict';

app.service('CurrentUserService', ["$localStorage", function ($localStorage) {
    var currentUser = {
        token: "",
        userName: "",
        roles: "",
        permissions: "",
        login: ""
    }

    this.save = function () {
        $localStorage.currentUser = currentUser;
    }

    this.restore = function () {
        if ($localStorage && $localStorage.currentUser != null) {
            currentUser = $localStorage.currentUser;
        } else {
            currentUser = null;
        }
        return currentUser;
    }

    this.set = function (user) {
        currentUser = user; // or merge
        this.save();
    }

    this.get = function () {
        return currentUser;
    }

    this.clear = function () {
        this.set(null);
        this.save();
    }

    this.isInRole = function (role) {
        if (currentUser && currentUser.roles != null) {
            return currentUser.roles.indexOf(role) != -1;
        }
        return false;
    }

    this.hasPermissions = function (permissions) {
        if (currentUser && currentUser.roles != null) {
            return currentUser.permissions.indexOf(permissions) != -1;
        }
        return false;
    }
}]);