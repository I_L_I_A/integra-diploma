﻿'use strict';

app.factory('ProductPriceSheetsService', ["$resource", function ($resource) {
    return $resource('api/productPriceSheet/:id', {}, {
        update: {
            method: 'PUT',
        },
        getPagedPriceSheets: {
            method: 'GET',
            url: 'api/priceSheets/getPagedPriceSheets'
        },
        inactivate: {
            method: 'PUT',
            url: 'api/priceSheets/inactivate'
        },
        assignAccountsToPriceSheet: {
            method: 'PUT',
            url: 'api/pricesheets/assignAccountsToPriceSheet/:id'
        },
    });
}]);