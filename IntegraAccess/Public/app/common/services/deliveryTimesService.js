﻿'use strict';

app.factory('DeliveryTimesService', [
    "$resource", "DELIVERY_TIMES", function($resource, DELIVERY_TIMES) {
        var service = {
            getDeliveryTimes: function() {
                return DELIVERY_TIMES;
            },
            getTimeObject: function(startTime, endTime) {
                var result = DELIVERY_TIMES[0];
                angular.forEach(DELIVERY_TIMES, function(time) {
                    if (time.startTime == startTime && time.endTime == endTime) {
                        result =  time;
                    }
                });
                return result;
            }
    };



    return service;
}]);