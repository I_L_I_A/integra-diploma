﻿'use strict';

app.factory('ProductsService', ["$resource", "$q", "Upload", "$http", function ($resource, $q, Upload, $http) {
    var service = $resource('api/product/:id', {}, {
        update: {
            method: 'PUT'
        },
        inactivate: {
            method: 'PUT',
            url: 'api/products/inactivate'
        },
        getPagedProducts: {
            method: 'GET',
            url: 'api/products/getPagedProducts'
        },
        save: {
            method: 'POST',
            url: 'api/products/post'
        },
        getImage: {
            method: 'GET',
            url: 'api/products/getImage/:id',
            headers: { 'Accept': 'image/webp,image/*,*/*;q=0.8' }
}
    });

    service.postImage = function (product) {
        var deferred = $q.defer();
        Upload.upload({
            url: 'api/product/postImage',
            method: 'POST',
            params: { 'productId': product.id },
            file: product.image,
            headers: { 'Content-Type': 'multipart/form-data' },
        }).then(function () {
            deferred.resolve();
            return deferred.promise;
        });
        return deferred.promise;
    };
    service.saveProduct = function (product) {
        var deferred = $q.defer();
        var that = this;
        this.save(product)
            .$promise.then(function (result) {
                if (result.productId) {
                    product.id = result.productId;
                }
                if (product.image) {
                    that.postImage(product).then(function () {
                        deferred.resolve();
                        return deferred.promise;
                    });
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            });
        return deferred.promise;
    };
    service.updateProduct = function (product) {
        var deferred = $q.defer();
        var that = this;
        this.update(product)
            .$promise.then(function (result) {
                if (result.productId) {
                    product.id = result.productId;
                }
                if (product.image) {
                    that.postImage(product).then(function() {
                        deferred.resolve();
                        return deferred.promise;
                    });
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            });
        return deferred.promise;
    };

//    service.getImage = function (imageUrl) {
//        var deferred = $q.defer();
//        $http.get({ url: imageUrl })
//            .then(function (data, status, headers, config) {
//                deferred.resolve(new Blob([data]));
//            }, deferred.reject);
//        return deferred.promise;
//    };
    return service;
}]);