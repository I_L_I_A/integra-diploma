﻿'use strict';

app.controller('PriceSheetItemsListCtrl', ["$rootScope", "$scope", "ngTableParams", "blockUI", "ProductPriceSheetItemsService", function ($rootScope, $scope, ngTableParams, blockUI, ProductPriceSheetItemsService) {
    $scope.priceSheetItemsTableBlock = blockUI.instances.get('priceSheetItemsTableBlock');

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: null
    }, {
        getData: function ($defer, params) {
            $scope.tableParams.isReady = false;
            $scope.priceSheetItemsTableBlock.start();
            var paramsObj = {
                page: params.page(),
                pageSize: params.count(),
                search: params.filter.search
            };
            if (params.sorting()) {
                paramsObj.sortColumn = Object.keys(params.sorting());
                paramsObj.reverse = params.sorting()[paramsObj.sortColumn] == "desc";
            }

            $rootScope.$broadcast('load_pricesheetitems', paramsObj);

            $scope.priceSheetitemsLoadedCallback = function (response) {
                $scope.tableParams.isReady = true;
                $scope.priceSheetItemsTableBlock.stop();
                params.total(response.count);
                $defer.resolve(response.items);
            };
        }
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $rootScope.$on('reload_pricesheetitems', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $rootScope.$on('pricesheetitems_successfully_loaded', function (events, response) {
        if ($scope.priceSheetitemsLoadedCallback) {
            $scope.priceSheetitemsLoadedCallback(response);
        }
    });
}]);
