﻿'use strict';


app.controller('PatientsCtrl', ["$scope", "SweetAlert", "PatientsService", "FacilitiesService", "AccountGroupsService", "AccountsService", "FormUnsavedChangesService", "CurrentUserService", function ($scope, SweetAlert, PatientsService, FacilitiesService, AccountGroupsService, AccountsService, FormUnsavedChangesService, CurrentUserService) {
    $scope.sweetAlert = SweetAlert;
    $scope.patientsService = PatientsService;
    $scope.accountGroupsService = AccountGroupsService;
    $scope.facilitiesService = FacilitiesService;
    $scope.accountsService = AccountsService;
    $scope.formUnsavedChangesService = FormUnsavedChangesService;
    $scope.currentUserService = CurrentUserService;

    $scope.editFormLoaded = function (form) {
        $scope.form = form;
    };
    $scope.getErrorMessage = function(response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null && response.data.modelState != null) {
            for (var i in response.data.modelState) {
                if (response.data.modelState[i].length > 0) {
                    message = "";
                    for (var j in response.data.modelState[i]) {
                        message += message.length > 0 ? '<br />' : '';
                        message += response.data.modelState[i][j];
                    }
                }
            }
        }

        return message;
    };
}]);

app.controller('CreatePatientsCtrl', ["$scope", "$stateParams", "$state", function ($scope, $stateParams, $state) {
    $scope.pageName = "New Patient";
    $scope.showPatientPhysicianInfo = true;
    $scope.patient = {};
    $scope.accountGroups = [];
    $scope.accounts = [];
    $scope.facilities = [];
    // init accountGroup and account by default for "Account" type user
    if ($scope.isInRole('Account')) {
        $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: '' }, function (response) {
            if (!response.count) {
                $scope.sweetAlert.swal("Account Group not specified for current user", null, "error");
                return;
            }
            $scope.patient.accountGroupId = response.items[0].id;
            $scope.patient.accountGroupName = response.items[0].name;
            $scope.accountsService.getPagedAccounts({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountGroupId: $scope.patient.accountGroupId }, function (response) {
                if (!response.count) {
                    $scope.sweetAlert.swal("An Account not specified for current user", null, "error");
                    return;
                }
                if (response.count == 1) {
                    $scope.account = response.items[0];
                    $scope.patient.isAccountSelectionDisabled = true;
                    $scope.patient.accountId = response.items[0].id;
                    $scope.patient.accountName = response.items[0].name;
                }
            });
        });
    }

    $scope.searchAccountGroups = function (searchStr) {
        if (searchStr) {
            $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function(response) {
                $scope.accountGroups = response.items;
            });
        } else {
            $scope.accountGroups = [];
        }
    };

    $scope.searchAccounts = function(searchStr, accountGroupId) {
        if (accountGroupId && searchStr) {
            $scope.accountsService.getPagedAccounts({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountGroupId: accountGroupId, search: searchStr }, function(response) {
                $scope.accounts = response.items;
            });
        } else {
            $scope.accounts = [];
        }
    };

    $scope.searchFacilities = function (searchStr, accountId) {
        if (accountId && searchStr) {
            $scope.facilitiesService.getPagedFacilities({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountId: accountId, onlyConnected:true, search: searchStr }, function (response) {
                $scope.facilities = response.items;
            });
        } else {
            $scope.facilities = [];
        }
    };

    $scope.accountGroupSelected = function () {
        $scope.patient.accountId = null;
    };

    $scope.accountSelected = function() {
        $scope.patient.facilityId = null;
        angular.forEach($scope.accounts, function(account) {
            if (account.id == $scope.patient.accountId) {
                $scope.account = account;
            }
        });
    }

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function(form) {
        if (!form.$invalid) {
            $scope.patient.birthDate = $scope.patient.birthDateValue ? moment.parseZone($scope.patient.birthDateValue.toString())._d : null;
            $scope.patient.deceasedDate = $scope.patient.deceasedDateValue ? moment.parseZone($scope.patient.deceasedDateValue.toString())._d : null;
            $scope.patientsService.checkDuplicate($scope.patient, function(response) {
                if (response.isDuplicate) {
                    $scope.sweetAlert.swal("Patient with such personal info already exists.");
                    $scope.form.$submitted = false;
                    $scope.form.$setDirty();
                } else {
                    $scope.patientsService.save($scope.patient, function (result) {
                        $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                        $state.go('app.patients.list');
                    }, function (response) {
                        $scope.sweetAlert.swal("Failed to Add the Patient Record!", $scope.getErrorMessage(response), "error");
                        $scope.form.$submitted = false;
                        $scope.form.$setDirty();
                    });
                }
            }, function(response) {
                $scope.sweetAlert.swal("Failed to check whether patient info is duplicate!", null, "error");
                $scope.form.$submitted = false;
                $scope.form.$setDirty();
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        }
    };
}]);


app.controller('EditPatientsCtrl', ["$scope", "$stateParams", "$state", "$filter", "moment", function ($scope, $stateParams, $state, $filter, moment) {
    $scope.patient = {};
    $scope.editMode = true;
    $scope.showPatientPhysicianInfo = true;
    $scope.pageName = "Edit Patient";
    $scope.facilities = [];

    $scope.patientsService.get({ id: $stateParams.patientId }).$promise.then(function (result) {
        result.birthDateValue = result.birthDate ? $filter('date')(new Date(result.birthDate), $scope.settings.dateFormat) : null;
        result.deceasedDateValue = result.deceasedDate ? $filter('date')(new Date(result.deceasedDate), $scope.settings.dateFormat) : null;
        $scope.patient = result;

        $scope.accountsService.getPagedAccounts({ sortColumn: "Name", accountGroupId: $scope.patient.accountGroupId, search: $scope.patient.accountName }, function (response) {
            angular.forEach(response.items, function (account) {
                if (account.id == $scope.patient.accountId) {
                    $scope.account = account;
                }
            });
        });

        $scope.searchFacilities($scope.patient.facilityName, $scope.patient.accountId);
    });

    $scope.searchFacilities = function (searchStr, accountId) {
        if (accountId && searchStr) {
            $scope.facilitiesService.getPagedFacilities({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountId: accountId, onlyConnected: true, search: searchStr }, function (response) {
                $scope.facilities = response.items;
            });
        } else {
            $scope.facilities = [];
        }
    };

    $scope.formUnsavedChangesService.listenToUnsavedChanges($scope);

    $scope.submit = function(form) {
        if (form.$invalid) {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            $scope.form.$submitted = false;
            $scope.form.$setDirty();
        } else {
            var updatePatient = function () {
                $scope.patientsService.update({ id: $scope.patient.id }, $scope.patient, function () {
                    $scope.formUnsavedChangesService.stopListeningToUnsavedChanges();
                    $state.go('app.patients.list');
                }, function (response) {
                    $scope.sweetAlert.swal('Failed to save the Patient Record!', $scope.getErrorMessage(response));
                    $scope.form.$submitted = false;
                    $scope.form.$setDirty();
                });
            };
            $scope.patient.birthDate = $scope.patient.birthDateValue ? moment.parseZone($scope.patient.birthDateValue.toString())._d : null;
            $scope.patient.deceasedDate = $scope.patient.deceasedDateValue ? moment.parseZone($scope.patient.deceasedDateValue.toString())._d : null;
            //TODO: when have time, refactor in a way when duplicate patient check is performed on Name, Birthday change with some certain period.
            if (form.firstName.$pristine && form.lastName.$pristine && form.birthDate.$pristine && form.ssn.$pristine) {
                updatePatient();
            } else {
                $scope.patientsService.checkDuplicate($scope.patient, function (response) {
                    if (response.isDuplicate) {
                        $scope.sweetAlert.swal("Patient with such personal info already exists.");
                        $scope.form.$submitted = false;
                        $scope.form.$setDirty();
                    } else {
                        updatePatient();
                    }
                });
            }
        }
    };
}]);

app.controller('AllPatientsCtrl', ["$scope", "ngTableParams", "blockUI", function ($scope, ngTableParams, blockUI) {
    $scope.patientsTableBlock = blockUI.instances.get('patientsTableBlock');
    $scope.selectedPatientsIds = [];

    $scope.inactivateSelectedPatients = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        var toInactiveStorage = [];

        angular.forEach(Object.keys($scope.selectedPatientsIds), function(userId) {
            var isPatientChecked = $scope.selectedPatientsIds[userId];
            if (isPatientChecked) {
                toInactiveStorage.push(userId);
            }
        });

        $scope.patientsService.inactivate(toInactiveStorage).$promise.then(function(result) {
            $scope.tableParams.reload();
        }, function(response) {
            //TODO: add getErrorMessageService
            //SweetAlert.swal('Failed to inactivate  Accounts!', getErrorMessage(response), "error");
            $scope.sweetAlert.swal('Failed to inactivate  Patients!', null, "error");
        });
    };

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { LastName: "asc" },
        filter: { search: "" }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.patientsTableBlock.start();

            $scope.patientsService.getPagedPatients({
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                includeInactive: $scope.includeInactive,
                search: params.filter.search
            }, function (response) {
                //set total items
                params.total(response.count);
                $scope.selectedPatientsIds = [];

                var items = [];
                angular.forEach(response.items, function (patient) {
                    patient.birthDate = patient.birthDate ? new Date(patient.birthDate) : null;
                    patient.deceasedDate = patient.deceasedDate ? new Date(patient.deceasedDate) : null;
                    items.push(patient);
                });

                $scope.tableParams.isReady = true;
                $scope.patientsTableBlock.stop();

                $defer.resolve(items);
            });
        }
    });

    $scope.$watch('includeInactive', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });
}]);
