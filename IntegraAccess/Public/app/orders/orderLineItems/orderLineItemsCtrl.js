﻿'use strict';

app.controller('orderLineItemsCtrl', ["$scope", "$stateParams", "$state", "OrdersService", function ($scope, $stateParams, $state, OrdersService) {
    $scope.ordersService = OrdersService;

    $scope.restorePPSItemsQuantityOnOrder = function (ppsItems) {
        angular.forEach(ppsItems, function (ppsItem) {
            if (!ppsItem.addedByProductTemplateId /* not template nested item */) {
                ppsItem.quantity = $scope.orderStorage.getQuantityOnOrder(ppsItem.productId);
            }
        });
        return ppsItems;
    }

    $scope.increaseProductQuantity = function (ppsItem) {
        ppsItem.quantity += 1;
        $scope.orderStorage.addPPSItemToOrder(ppsItem);
    }

    $scope.decreaseProductQuantity = function (ppsItem) {
        if (ppsItem.quantity > 0) {
            ppsItem.quantity -= 1;
            if (ppsItem.quantity > 0) {
                $scope.orderStorage.addPPSItemToOrder(ppsItem);
            } else {
                $scope.orderStorage.remove(ppsItem.productId);
            }
        }
    }

    $scope.calcPagesCount = function (count, pageSize) {
        return Math.ceil(count / pageSize);
    }
}]);

app.controller('selectProductsTableCtrl', ["$scope", "$stateParams", "$state", function ($scope, $stateParams, $state) {
    $scope.existingPPSItems = [];
    $scope.orderStorage = $scope.ordersService.orderStorage;

    // get settings from parent CTRL
    var isEditable = $scope.local && $scope.local.enablePPSItemsSelection;
    $scope.local = {
        isEditable: isEditable,
        selectedPPSItem: null
    };

    $scope.searchPPSItems = function (searchStr) {
        if (searchStr) {
            $scope.$emit("search_pps_items", {
                search: searchStr,
                callbackEventName: "pps_items_loaded"
            });
        } else {
            $scope.existingPPSItems = [];
        }
    }

    $scope.$on("pps_items_loaded", function (events, response) {
        $scope.existingPPSItems = response.items;
    });

    $scope.$on("enable_pps_items_selection", function (events, enable) {
        $scope.local.isEditable = enable;
    });

    $scope.productSelected = function () {
        if (!$scope.local.isEditable) return;
        $scope.local.selectedPPSItem.quantity = 1;
        $scope.orderStorage.addPPSItemToOrder($scope.local.selectedPPSItem);
        $scope.local.selectedPPSItem = null;
    }

    $scope.removeProductFromOrder = function (productId) {
        if (!$scope.local.isEditable) return;
        $scope.orderStorage.remove(productId);
    }

  
}]);

app.controller('browseProductsCtrl', ["$scope", "$stateParams", "$state", "ProductCategoriesService", function ($scope, $stateParams, $state, ProductCategoriesService) {
    $scope.categoryPPSItems = [];
    $scope.categories = [];
    $scope.orderStorage = $scope.ordersService.orderStorage;
    $scope.params = {};
    $scope.patient = {};

    $scope.init = function () {
        $scope.params = {
            productCategoryId: null,
            page: 1,
            pageSize: 8,
            pagesCount: 0,
            sortColumn: "DisplayName",
            search: "",
        }
        //load patient info
        $scope.$emit("get_patient_info", $scope.params);
        //load categories
        ProductCategoriesService.getPaged({ sortColumn: "Name" }, function (response) {
            $scope.categories = response.items;
        });
        //load pps items for default category
        $scope.searchPPSItems();
    }

    $scope.init();

    $scope.searchPPSItems = function () {
        $scope.params.callbackEventName = "browse_pps_items_loaded";
        $scope.$emit("search_pps_items", $scope.params);
    }
    $scope.$on("patient_info_loaded", function (events, patient) {
        $scope.patient = patient;
    });

    $scope.$on("browse_pps_items_loaded", function (events, response) {
        $scope.restorePPSItemsQuantityOnOrder(response.items);
        $scope.categoryPPSItems = response.items;
        $scope.params.pagesCount = $scope.calcPagesCount(response.count, $scope.params.pageSize);
    });

    $scope.setCategory = function(categoryId) {
        $scope.params.page = 1;
        $scope.params.productCategoryId = categoryId;
        $scope.searchPPSItems();
    };

    $scope.$watch("params.search", function (events, response) {
        $scope.params.page = 1;
        $scope.searchPPSItems();
    });

    $scope.addPPSItemToOrder = function (item) {
        $scope.orderStorage.addPPSItemToOrder(item);
    }

    $scope.setPage = function (page) {
        if (page > 0 && page <= $scope.params.pagesCount) {
            $scope.params.page = page;
            $scope.searchPPSItems();
        }
    }

    $scope.removeProductFromOrder = function (index) {
        $scope.orderStorage.removeOrderLineItemFromOrder(index);
    }
}]);

app.controller('searchByCategoriesCtrl', ["$scope", "$stateParams", "$state", "ProductCategoriesService", function ($scope, $stateParams, $state, ProductCategoriesService) {
    $scope.categoryPPSItems = [];
    $scope.categories = [];
    $scope.orderStorage = $scope.ordersService.orderStorage;

    $scope.params = {
        productCategoryId: null,
        page: 1,
        pageSize: 4,
        pagesCount: 0,
        sortColumn: "DisplayName",
        search: "",
    }

    //load categories
    ProductCategoriesService.getPaged({ sortColumn: "Name" }, function (response) {
        $scope.categories = response.items;
        if ($scope.categories && $scope.categories.length > 0) {
            $scope.params.productCategoryId = $scope.categories[0].id;
        }
        $scope.searchPPSItems();
    });

    $scope.searchPPSItems = function () {
        //$scope.categoryPPSItems = [];
        $scope.params.callbackEventName = "category_pps_items_loaded";
        $scope.$emit("search_pps_items", $scope.params);
    }

    $scope.$on("load_category_pps_items", function (events, response) {
        $scope.searchPPSItems();
    });

    $scope.$on("category_pps_items_loaded", function (events, response) {
        $scope.restorePPSItemsQuantityOnOrder(response.items);
        $scope.categoryPPSItems = response.items;
        $scope.params.pagesCount = $scope.calcPagesCount(response.count, $scope.params.pageSize);
    });

    $scope.setPage = function (page) {
        if (page > 0 && page <= $scope.params.pagesCount) {
            $scope.params.page = page;
            $scope.searchPPSItems();
        }
    }

    $scope.setCategory = function (categoryId) {
        $scope.params.page = 1;
        $scope.params.productCategoryId = categoryId;
        $scope.searchPPSItems();
    };
}]);