﻿'use strict';


app.controller('OrdersCtrl', ["$scope", "ORDER_STATES", "PURCHASE_TYPES", "DeliveryTimesService", "SweetAlert", "PatientsService", "OrdersService", "FacilitiesService", "ProductPriceSheetItemsService", "AccountGroupsService", "AccountsService", "CurrentUserService", function ($scope, ORDER_STATES, PURCHASE_TYPES, DeliveryTimesService, SweetAlert, PatientsService, OrdersService, FacilitiesService, ProductPriceSheetItemsService, AccountGroupsService, AccountsService, CurrentUserService) {
    $scope.sweetAlert = SweetAlert;
    $scope.orderStates = ORDER_STATES;
    $scope.purchaseTypes = PURCHASE_TYPES;
    $scope.deliveryTimesService = DeliveryTimesService;
    $scope.patientsService = PatientsService;
    $scope.accountGroupsService = AccountGroupsService;
    $scope.facilitiesService = FacilitiesService;
    $scope.accountsService = AccountsService;
    $scope.ordersService = OrdersService;
    $scope.ppsItemsService = ProductPriceSheetItemsService;
    $scope.currentUserService = CurrentUserService;

    $scope.getErrorMessage = function (response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null && response.data.modelState != null) {
            for (var i in response.data.modelState) {
                if (response.data.modelState[i].length > 0) {
                    message = "";
                    for (var j in response.data.modelState[i]) {
                        message += message.length > 0 ? '<br />' : '';
                        message += response.data.modelState[i][j];
                    }
                }
            }
        }

        return message;
    }

    $scope.deliveryTimes = $scope.deliveryTimesService.getDeliveryTimes();
}]);

app.controller('CreateOrderCtrl', ["$scope", "$stateParams", "$state", "$filter", function ($scope, $stateParams, $state, $filter) {
    $scope.accountGroups = [];
    $scope.accounts = [];
    $scope.facilities = [];
    $scope.existingPatients = [];
    $scope.patient = {};
    $scope.orderStorage = $scope.ordersService.orderStorage;

    $scope.local = {
        createType: "newOrder",
        useExistingPatient: "true",
        isRequestedDeliveryDateOpen: false,
        useFacilityAddress: null,
        selectedAccountGroupName: null,
        selectedAccountGroupId: null,
        selectedAccount: null,
        isAccountSelectionDisabled: false,
        selectedExistingPatient: null,
        requestedDeliveryDateValue: null,
        deliveryTime: $scope.deliveryTimes[0],
        deliveryNotes: null
    }

    $scope.orderStorage.clearCurrentOrder();

    // init accountGroup and account by default for "Account" type user
    if ($scope.isInRole('Account')) {
        $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: '' }, function (response) {
            if (!response.count) {
                $scope.sweetAlert.swal("Account Group not specified for current user", null, "error");
                return;
            }
            $scope.local.selectedAccountGroupName = response.items[0].name;
            $scope.local.selectedAccountGroupId = response.items[0].id;
            $scope.$broadcast("enable_pps_items_selection", false);
            $scope.accountsService.getPagedAccounts({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountGroupId: $scope.local.selectedAccountGroupId }, function (response) {
                if (!response.count) {
                    $scope.sweetAlert.swal("An Account not specified for current user", null, "error");
                    return;
                }
                if (response.count == 1) {
                    $scope.local.isAccountSelectionDisabled = true;
                    $scope.local.selectedAccount = response.items[0];
                    $scope.searchFacilities(null, $scope.local.selectedAccount.id);
                    $scope.$broadcast("enable_pps_items_selection", true);
                }
            });
        });
    }


    $scope.searchAccountGroups = function (searchStr) {
        if (searchStr) {
            $scope.accountGroupsService.getPagedAccountGroups({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", search: searchStr }, function (response) {
                $scope.accountGroups = response.items;
            });
        } else {
            $scope.accountGroups = [];
        }
    }

    $scope.searchAccounts = function (searchStr, accountGroupId) {
        if (accountGroupId) {
            $scope.accountsService.getPagedAccounts({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountGroupId: accountGroupId, search: searchStr }, function (response) {
                $scope.accounts = response.items;
            });
        } else {
            $scope.accounts = [];
        }
    }

    $scope.searchFacilities = function (searchStr, accountId) {
        if (accountId) {
            $scope.facilitiesService.getPagedFacilities({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "Name", accountId: accountId, search: searchStr }, function (response) {
                $scope.facilities = response.items;
            });
        } else {
            $scope.facilities = [];
        }
    }

    $scope.searchPatients = function (searchStr, accountId) {
        if (accountId && searchStr) {
            $scope.patientsService.getPagedPatients({ page: 1, pageSize: $scope.settings.selectPageSize, sortColumn: "LastName", accountId: accountId, search: searchStr }, function (response) {
                angular.forEach(response.items, function (patient) {
                    patient.birthDateValue = patient.birthDate ? $filter('date')(new Date(patient.birthDate), $scope.settings.dateFormat) : null;
                    patient.deceasedDateValue = patient.deceasedDate ? $filter('date')(new Date(patient.deceasedDate), $scope.settings.dateFormat) : null;
                });
                $scope.existingPatients = response.items;
            });
        } else {
            $scope.existingPatients = [];
        }
    }

    $scope.$on('search_pps_items', function (events, paramsObj) {
        if ($scope.local.selectedAccount && paramsObj) {
            var params = {
                page: 1, pageSize: $scope.settings.selectPageSize,
                sortColumn: "DisplayName",
                accountId: $scope.local.selectedAccount.id,
            }
            angular.extend(params, paramsObj);
            $scope.ppsItemsService.getAccountPagedItems(params, function (response) {
                $scope.$broadcast(paramsObj.callbackEventName, response);
            }, function (response) {
                $scope.sweetAlert.swal("Failed to find products", response.data.message ? response.data.message : "Unknown Error", "error");
            });
        }
    });

    $scope.$on('get_patient_info', function () {
        $scope.$broadcast("patient_info_loaded", $scope.patient);
    });

    $scope.accountGroupSelected = function () {
        resetPatient();
        $scope.local.selectedAccount = null;
        $scope.searchAccounts(null, $scope.local.selectedAccountGroupId);
        $scope.orderStorage.clearCurrentOrder();
        $scope.$broadcast("enable_pps_items_selection", false);
    };

    $scope.accountSelected = function () {
        resetPatient();
        $scope.orderStorage.clearCurrentOrder();
        $scope.searchFacilities(null, $scope.local.selectedAccount.id);
        $scope.$broadcast("enable_pps_items_selection", true);
        $scope.$broadcast("load_category_pps_items");
    };

    $scope.existingPatientSelected = function () {
        $scope.local.editExistingPatient = false;
        $scope.patient = $scope.local.selectedExistingPatient;
        $scope.facilitiesService.get({ id: $scope.patient.facilityId }, function (result) {
            $scope.patientFacility = result;
            $scope.facilities = [$scope.patientFacility];
        });
    };

    $scope.facilitySelected = function () {
        $scope.facilitiesService.get({ id: $scope.patient.facilityId }, function (result) {
            $scope.patientFacility = result;
            $scope.patient.facilityName = result.name;
        });
    };

    $scope.$watch('local.useExistingPatient', function (newValue) {
        resetPatient();
        $scope.patient = {};
    });

    var resetPatient = function () {
        if ($scope.local.useExistingPatient == "true") {
            $scope.patient = {};
            $scope.local.selectedExistingPatient = null;
            $scope.local.editExistingPatient = false;
        }
        $scope.patient.facilityId = null;
        $scope.local.useFacilityAddress = null;
    }

    $scope.updatePatient = function (form) {
        if (!form.$invalid) {
            $scope.patient.birthDate = $scope.patient.birthDateValue ? moment.parseZone($scope.patient.birthDateValue.toString())._d : null;
            $scope.patient.deceasedDate = $scope.patient.deceasedDateValue ? moment.parseZone($scope.patient.deceasedDateValue.toString())._d : null;

            $scope.patientsService.update({ id: $scope.patient.id }, $scope.patient, function (result) {
                $scope.local.selectedExistingPatient = $scope.patient;
                $scope.local.editExistingPatient = false;
                //TODO: Add some notification for the user that saving is successfull
            }, function (response) {
                $scope.sweetAlert.swal("Failed to Update the Patient!", $scope.getErrorMessage(response), "error");
            });
        } else {
            $scope.sweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
        }
    }

    $scope.submit = function (patientForm, isSubmitted) {
        if (isOrderValid(patientForm)) {
            prepareOrderForSubmit();
            $scope.ordersService.save({ isSubmitted: isSubmitted }, $scope.orderStorage.currentOrder, function (result) {
                $scope.orderStorage.clearCurrentOrder();
                $state.go('app.orders.view', { orderId: result.id });
            }, function (response) {
                $scope.sweetAlert.swal("Failed to Add a new Order Record!", $scope.getErrorMessage(response), "error");
            });
        }
    }

    var prepareOrderForSubmit = function () {
        $scope.orderStorage.currentOrder.accountId = $scope.local.selectedAccount.id;
        $scope.orderStorage.currentOrder.accountGroupId = $scope.local.selectedAccountGroupId;
        $scope.orderStorage.currentOrder.requestedDeliveryDate = parseDateWithZone($scope.local.requestedDeliveryDateValue);
        $scope.orderStorage.currentOrder.requestedDeliveryStartTime = parseDateWithZone(new Date($scope.local.deliveryTime.startTime));
        $scope.orderStorage.currentOrder.requestedDeliveryEndTime = parseDateWithZone(new Date($scope.local.deliveryTime.endTime));
        $scope.orderStorage.currentOrder.deliveryNotes = $scope.local.deliveryNotes;
        $scope.orderStorage.setPatient(getOrderPatient());
        $scope.orderStorage.setShippingInfo(getShippingInfoSource());
    }

    var getShippingInfoSource = function () {
        //use facility address
        if ($scope.local.useFacilityAddress == "true") {
            return $scope.patientFacility;
        }
        //use patient address
        return $scope.patient;
    }

    var getOrderPatient = function () {
        if ($scope.local.useExistingPatient == "true") {
            $scope.patient = $scope.local.selectedExistingPatient;
        } else {
            $scope.patient.accountId = $scope.local.selectedAccount.id;
            $scope.patient.accountGroupId = $scope.local.selectedAccountGroupId;
        }
        $scope.patient.birthDate = parseDateWithZone($scope.patient.birthDateValue);
        $scope.patient.deceasedDate = parseDateWithZone($scope.patient.deceasedDateValue);
        return $scope.patient;
    }

    var isOrderValid = function (patientForm) {
        if ($scope.local.useExistingPatient == "true") {
            if (!$scope.local.selectedExistingPatient) {
                $scope.sweetAlert.swal("Please select an existing Patient!", null, "error");
                return false;
            }
        } else {
            if (patientForm.$invalid) {
                $scope.patientSubmitted = true;
                $scope.sweetAlert.swal("The patient cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return false;
            }
        }
        if ($scope.local.useFacilityAddress == null) {
            $scope.sweetAlert.swal("Please select a Ship To method!", null, "error");
            return false;
        }
        return true;
    }

    var parseDateWithZone = function (date) {
        return date ? moment.parseZone(date.toString())._d : null;
    }

}]);

app.controller('EditOrderCtrl', ["$rootScope", "$scope", "$stateParams", "$state", "$filter", "moment", function ($rootScope, $scope, $stateParams, $state, $filter, moment) {
    $scope.order = {};
    $scope.ordersService.orderStorage.clearCurrentOrder();
    $scope.local = {
        enablePPSItemsSelection: true
    }

    $scope.ordersService.get({ id: $stateParams.orderId }).$promise.then(function(result) {
        $scope.order = result;
        $scope.order.requestedDeliveryDateValue = result.requestedDeliveryDate ? $filter('date')(new Date(result.requestedDeliveryDate), $scope.settings.dateFormat) : null;
        $scope.order.deliveryTime = $scope.deliveryTimesService.getTimeObject(result.requestedDeliveryStartTime, result.requestedDeliveryEndTime);
        $scope.ordersService.orderStorage.currentOrder = result;
    });

    $rootScope.$on('search_pps_items', function (events, paramsObj) {
        if ($scope.order && $scope.order.id > 0 && paramsObj) {
            var params = {
                page: 1, pageSize: $scope.settings.selectPageSize,
                sortColumn: "DisplayName",
                accountId: $scope.order.accountId,
            }
            angular.extend(params, paramsObj);
            $scope.ppsItemsService.getAccountPagedItems(params, function (response) {
                $rootScope.$broadcast(paramsObj.callbackEventName, response);
            }, function (response) {
                $scope.sweetAlert.swal("Failed to find products", response.data.message ? response.data.message : "Unknown Error", "error");
            });
        }
    });

    $scope.submit = function() {
        prepareOrderForSubmit();
        $scope.ordersService.update($scope.order, function(result) {
            $scope.ordersService.orderStorage.clearCurrentOrder();
            $state.go('app.orders.view', { orderId: result.id });
        }, function(response) {
            $scope.sweetAlert.swal("Failed to Update the Order Record!", $scope.getErrorMessage(response), "error");
        });
    }

    var prepareOrderForSubmit = function () {
        $scope.order.requestedDeliveryDate = parseDateWithZone($scope.order.requestedDeliveryDateValue);
        $scope.order.requestedDeliveryStartTime = parseDateWithZone(new Date($scope.order.deliveryTime.startTime));
        $scope.order.requestedDeliveryEndTime = parseDateWithZone(new Date($scope.order.deliveryTime.endTime));
        $scope.order.orderLineItems = $scope.ordersService.orderStorage.getOrderLineItems();
    }

    $rootScope.$on('get_patient_info', function () {
        $rootScope.$broadcast("patient_info_loaded", $scope.order.patient);
    });

    var parseDateWithZone = function (date) {
        return date ? moment.parseZone(date.toString())._d : null;
    }

}]);


app.controller('OrderViewCtrl', ["$scope", "$stateParams", "$state", "$filter", "moment", function ($scope, $stateParams, $state, $filter, moment) {
    $scope.order = {};
    $scope.ordersService.orderStorage.clearCurrentOrder();
    $scope.acceptOrderActions = [];
    $scope.declineOrderActions = [];
    $scope.isOrderEditable = false;

    //init order
    var initOrder = function () {
        $scope.ordersService.get({ id: $stateParams.orderId }).$promise.then(function (result) {
            $scope.order = result;
            $scope.order.deliveryTime = $scope.deliveryTimesService.getTimeObject(result.requestedDeliveryStartTime, result.requestedDeliveryEndTime);
            $scope.ordersService.orderStorage.currentOrder = result;
        });
    }

    var initPossibleOrderStateActions = function (callback) {
        $scope.ordersService.getPossibleOrderStateActions({ id: $stateParams.orderId }).$promise.then(function (result) {
            $scope.isOrderEditable = result.isOrderEditable;
            $scope.order.submittedAt = result.order.submittedAt;
            $scope.order.submittedByName = result.order.submittedByName;
            $scope.acceptOrderActions = [];
            $scope.declineOrderActions = [];
            if (result.hasUserAccessToOrder) {
                if (callback) {callback();}
                angular.forEach(result.actions, function(action) {
                    if (action.orderState == $scope.orderStates.APPROVAL_REQUIRED || action.orderState == $scope.orderStates.DME_APPROVAL_REQUIRED || action.orderState == $scope.orderStates.DME_ACCEPTED || action.orderState == $scope.orderStates.COMPLETED) {
                        $scope.acceptOrderActions.push(action);
                    }
                    if (action.orderState == $scope.orderStates.ACCOUNT_REJECTED || action.orderState == $scope.orderStates.CANCELLED || action.orderState == $scope.orderStates.DME_CANCELLED || action.orderState == $scope.orderStates.DME_HOLD) {
                        $scope.declineOrderActions.push(action);
                    }
                });
            } else {
                $state.go('app.orders.list');
            }
        });
    }
    initPossibleOrderStateActions(initOrder);

    $scope.setState = function (orderState) {
        $scope.ordersService.setState({ state: orderState }, $scope.order).$promise.then(function (result) {
            initPossibleOrderStateActions();
            $scope.order.orderStateId = result.state;
            $scope.order.orderStateName = result.name;
        });
    }


}]);

app.controller('AllOrdersCtrl', ["$scope", "ngTableParams", "blockUI", function ($scope, ngTableParams, blockUI) {
    $scope.ordersTableBlock = blockUI.instances.get('ordersTableBlock');

    $scope.tableParams = new ngTableParams({
        page: 1,          // show first page
        count: 10,        // count per page
        sorting: { Id: "asc" },
        filter: { search: "" }
    }, {
        getData: function ($defer, params) {
            var sortColumn = Object.keys(params.sorting());
            var reverse = params.sorting()[sortColumn] == "desc";
            $scope.tableParams.isReady = false;
            $scope.ordersTableBlock.start();

            $scope.ordersService.getPaged({
                page: params.page(),
                pageSize: params.count(),
                sortColumn: sortColumn,
                reverse: reverse,
                search: params.filter.search
            }, function (response) {
                //set total items
                params.total(response.count);

                var items = [];
                angular.forEach(response.items, function (order) {
                    order.requestedDeliveryDate = order.requestedDeliveryDate ? new Date(order.requestedDeliveryDate) : null;
                    items.push(order);
                });

                $scope.tableParams.isReady = true;
                $scope.ordersTableBlock.stop();

                $defer.resolve(items);
            });
        }
    });

    $scope.$watch('tableParams.filter.search', function () {
        $scope.tableParams.$params.page = 1;
        $scope.tableParams.reload();
    });
}]);
