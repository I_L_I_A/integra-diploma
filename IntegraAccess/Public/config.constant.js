'use strict';

/**
 * Config constant
 */
app.constant('ORDER_STATES', {
    SAVED : 1,
    APPROVAL_REQUIRED : 2,
    ACCOUNT_REJECTED : 3,
    CANCELLED : 4,
    DME_APPROVAL_REQUIRED : 5,
    DME_ACCEPTED : 6,
    DME_HOLD : 7,
    COMPLETED : 8,
    DME_CANCELLED : 9
});

app.constant('PURCHASE_TYPES', {
    DAILY : 1,
    MONTHLY : 2,
    PURCHASE : 3
});

app.constant('DELIVERY_TIMES', [
    {
        label: "Anytime",
        startTime: "1970-01-01T00:00:00",
        endTime: "1970-01-01T23:59:59"
    },
    {
        label: "Morning (7:00am - 1:00pm)",
        startTime: "1970-01-01T07:00:00",
        endTime: "1970-01-01T13:00:00"
    },
    {
        label: "Afternoon (1:00pm - 7:00pm)",
        startTime: "1970-01-01T13:00:00",
        endTime: "1970-01-01T19:00:00"
    },
    {
        label: "Evening (7:00pm - 10:00pm)",
        startTime: "1970-01-01T19:00:00",
        endTime: "1970-01-01T22:00:00"
    }
]);

app.constant('APP_MEDIAQUERY', {
    'desktopXL': 1200,
    'desktop': 992,
    'tablet': 768,
    'mobile': 480
});
app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        //*** Javascript Plugins
        'modernizr': ['Public/components/components-modernizr/modernizr.js'],
        'moment': ['Public/components/moment/min/moment.min.js'],
        'spin': ['Public/components/spin.js/spin.js'],

        //*** jQuery Plugins
        'perfect-scrollbar-plugin': ['Public/components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js', 'Public/components/perfect-scrollbar/css/perfect-scrollbar.min.css'],
        'ladda': ['Public/components/ladda/dist/ladda.min.js', 'Public/components/ladda/dist/ladda-themeless.min.css'],
        'chartjs': ['Public/components/chartjs/Chart.min.js'],
        'jquery-sparkline': ['Public/components/jquery.sparkline.build/dist/jquery.sparkline.min.js'],
        'ckeditor-plugin': ['Public/components/ckeditor/ckeditor.js'],
        'jquery-nestable-plugin': ['Public/components/jquery-nestable/jquery.nestable.js'],
        'touchspin-plugin': ['Public/components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js', 'Public/components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],

        //***Own Controllers
        'homeCtrl': ['Public/app/home/homeCtrl.js'],
        'signinCtrl': ['Public/app/auth/signin/signinCtrl.js'],
        'signupCtrl': ['Public/app/auth/signup/signupCtrl.js'],
        'changePasswordCtrl': ['Public/app/auth/changePassword/changePasswordCtrl.js'],
        'resetPasswordCtrl': ['Public/app/auth/resetPassword/resetPasswordCtrl.js'],
        'sendResetPasswordEmailCtrl': ['Public/app/auth/sendResetPasswordEmail/sendResetPasswordEmailCtrl.js'],
        'accountsCtrl': ['Public/app/accounts/accountsCtrl.js'],
        'usersCtrl': ['Public/app/users/usersCtrl.js'],
        'patientsCtrl': ['Public/app/patients/patientsCtrl.js'],
        'reportsCtrl': ['Public/app/reports/reportsCtrl.js'],
        'ordersCtrl': ['Public/app/orders/ordersCtrl.js'],
        'orderLineItemsCtrl': ['Public/app/orders/orderLineItems/orderLineItemsCtrl.js'],
        'companiesCtrl': ['Public/app/companies/companiesCtrl.js'],
        'productsCtrl': ['Public/app/products/productsCtrl.js'],
        'facilitiesCtrl': ['Public/app/facilities/facilitiesCtrl.js'],
        'accountsGroupsCtrl': ['Public/app/groups/accountsGroupsCtrl.js'],
		'priceSheetsCtrl': ['Public/app/priceSheets/priceSheetsCtrl.js'],
		'priceSheetItemsCtrl': ['Public/app/priceSheetItems/priceSheetItemsCtrl.js'],
		'logsCtrl': ['Public/app/logs/logsCtrl.js'],
        //***Own Services
        'accountsGroupsService': ['Public/app/common/services/accountsGroupsService.js'],
        'accountsService': ['Public/app/common/services/accountsService.js'],
        'usersService': ['Public/app/common/services/usersService.js'],
        'facilitiesService': ['Public/app/common/services/facilitiesService.js'],
        'formUnsavedChangesService': ['Public/app/common/services/formUnsavedChangesService.js'],
        'companiesService': ['Public/app/common/services/companiesService.js'],
        'productsService': ['Public/app/common/services/productsService.js'],   
		'deliveryTimesService': ['Public/app/common/services/deliveryTimesService.js'], 
        'patientsService': ['Public/app/common/services/patientsService.js'], 
        'ordersService': ['Public/app/common/services/ordersService.js'], 
        'reportsService': ['Public/app/common/services/reportsService.js'],
        'productPriceSheetsService': ['Public/app/common/services/productPriceSheetsService.js'],
        'productPriceSheetItemsService': ['Public/app/common/services/productPriceSheetItemsService.js'],
        'productCategoriesService': ['Public/app/common/services/productCategoriesService.js'],
        'templateItemsService': ['Public/app/common/services/templateItemsService.js'],
        //*** Filters
        //'htmlToPlaintext': 'Public/vendor/clip-two/STANDARD/assets/js/filters/htmlToPlaintext.js'
    },
    //*** angularJS Modules
    modules: [{
        name: 'angularMoment',
        files: ['Public/components/angular-moment/angular-moment.min.js']
    }, {
        name: "blockUI",
        files: ['Public/components/angular-block-ui/dist/angular-block-ui.js', 'Public/components/angular-block-ui/dist/angular-block-ui.css']
    }, {
        name: 'toaster',
        files: ['Public/components/AngularJS-Toaster/toaster.js', 'Public/components/AngularJS-Toaster/toaster.css']
    }, {
        name: 'angularBootstrapNavTree',
        files: ['Public/components/angular-bootstrap-nav-tree/dist/abn_tree_directive.js', 'Public/components/angular-bootstrap-nav-tree/dist/abn_tree.css']
    }, {
        name: 'angular-ladda',
        files: ['Public/components/angular-ladda/dist/angular-ladda.min.js']
    }, {
        name: 'ngFileUpload',
        files: ['Public/components/ng-file-upload/ng-file-upload-shim.min.js', 'Public/components/ng-file-upload/ng-file-upload.min.js']
    }, {
        name: 'afkl.lazyImage',
        files: ['Public/components/ng-lazy-image/release/lazy-image.min.js', 'Public/components/ng-lazy-image/release/lazy-image-style.min.css']
    }, {
        name: 'jsonFormatter',
        files: ['Public/components/json-formatter/dist/json-formatter.min.js', 'Public/components/json-formatter/dist/json-formatter.min.css']
    }, {
        name: 'ngTable',
        files: ['Public/components/ng-table/dist/ng-table.min.js', 'Public/components/ng-table/dist/ng-table.min.css']
    }, {
        name: 'ui.select',
        files: ['Public/components/angular-ui-select/dist/select.min.js', 'Public/components/angular-ui-select/dist/select.min.css', 'Public/components/select2/dist/css/select2.min.css', 'Public/components/select2-bootstrap-css/select2-bootstrap.min.css', 'Public/components/selectize/dist/css/selectize.bootstrap3.css']
    }, {
        name: 'ui.mask',
        files: ['Public/components/ui-mask/dist/mask.min.js']
    }, {
        name: 'ngImgCrop',
        files: ['Public/components/ngImgCrop/compile/minified/ng-img-crop.js', 'Public/components/ngImgCrop/compile/minified/ng-img-crop.css']
    }, {
        name: 'ngAside',
        files: ['Public/components/angular-aside/dist/js/angular-aside.min.js', 'Public/components/angular-aside/dist/css/angular-aside.min.css']
    }, {
        name: 'truncate',
        files: ['Public/components/angular-truncate/src/truncate.js']
    }, {
        name: 'monospaced.elastic',
        files: ['Public/components/angular-elastic/elastic.js']
    }, {
        name: 'ngMap',
        files: ['Public/components/ngmap/build/scripts/ng-map.min.js']
    }, {
        name: 'tc.chartjs',
        files: ['Public/components/tc-angular-chartjs/dist/tc-angular-chartjs.min.js']
    }, {
        name: 'flow',
        files: ['Public/components/ng-flow/dist/ng-flow-standalone.min.js']
    }, {
        name: 'uiSwitch',
        files: ['Public/components/angular-ui-switch/angular-ui-switch.min.js', 'Public/components/angular-ui-switch/angular-ui-switch.min.css']
    }, {
        name: 'ckeditor',
        files: ['Public/components/angular-ckeditor/angular-ckeditor.min.js']
    }, {
        name: 'mwl.calendar',
        files: ['Public/components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar.js', 'Public/components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js', 'Public/components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css']
    }, {
        name: 'ng-nestable',
        files: ['Public/components/ng-nestable/src/angular-nestable.js']
    }, {
        name: 'vAccordion',
        files: ['Public/components/v-accordion/dist/v-accordion.min.js', 'Public/components/v-accordion/dist/v-accordion.min.css']
    }, {
        name: 'xeditable',
        files: ['Public/components/angular-xeditable/js/xeditable.min.js', 'Public/components/angular-xeditable/css/xeditable.css']
    }, {
        name: 'checklist-model',
        files: ['Public/components/checklist-model/checklist-model.js']
    }]
});
