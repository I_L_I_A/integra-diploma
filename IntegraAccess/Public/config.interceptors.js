'use strict';

/**
 * insert interceptors
 */
app.config(['$httpProvider',function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
    $httpProvider.interceptors.push('logToServerInterceptor');
}]);

