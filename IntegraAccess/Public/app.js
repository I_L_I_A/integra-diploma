/** 
  * declare 'integra' module with dependencies
*/
'use strict';
angular.module("integra", [
	'ngAnimate',
	'ngCookies',
	'ngStorage',
    'ui.select',
	'ngSanitize',
    'ngResource',
    'ui.mask',
    'angularMoment',
	'ngTouch',
    'oitozero.ngSweetAlert',
	'ui.router',
	'ui.bootstrap',
	'oc.lazyLoad',
	'cfp.loadingBar',
	'ncy-angular-breadcrumb',
	'duScroll',
//    'permission',
    'AppConfig',
    'chart.js'
]);