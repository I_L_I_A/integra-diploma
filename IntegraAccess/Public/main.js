var app = angular.module('integraApp', ['integra']);

app.run(['$rootScope', '$state', '$stateParams', 'authService', 'CurrentUserService', '$location',
function ($rootScope, $state, $stateParams, authService, CurrentUserService, $location) {
    authService.fillAuthData();

    $rootScope.settings = {
        pickerStartingDay: 1,
        showWeeks:false,
        dateFormat: "MM/dd/yyyy",
        timeFormat: "h:mm a",
        today: new Date(),
        selectPageSize: 10
    }

    $rootScope.getErrorMessage = function (response) {
        var message = 'Unknown Error';

        if (response != null && response.data != null && response.data.modelState != null) {
            for (var i in response.data.modelState) {
                if (response.data.modelState[i].length > 0) {
                    message = "";
                    for (var j in response.data.modelState[i]) {
                        message += message.length > 0 ? '<br />' : '';
                        message += response.data.modelState[i][j];
                    }
                }
            }
        }
        return message;
    };

    // Attach Fastclick for eliminating the 300ms delay between a physical tap and the firing of a click event on mobile browsers
    FastClick.attach(document.body);

    // Set some reference to access them from any scope
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    // GLOBAL APP SCOPE
    // set below basic information
    $rootScope.app = {
        isMobile: (function () {// true if the browser is a mobile device
            var check = false;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                check = true;
            };
            return check;
        })(),
        layout: {
            theme: 'theme-1', // indicate the theme chosen for your project
        }
    };

//    $rootScope.$on("$stateChangePermissionDenied", function (event, toState, toParams, fromState, fromParams) {
//        $state.go("login.signin", { afterSignInState: toState.name , afterSignInStateParameters: toParams}, {location: true, notify: true});
//    });

}]);


// Angular-Loading-Bar
// configuration
app.config(['cfpLoadingBarProvider',
function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = false;

}]);

app.config(function (datepickerConfig, datepickerPopupConfig) {
    datepickerConfig.showWeeks = false;
    datepickerPopupConfig.showButtonBar = false;
});

app.config(function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        colours: ['#97BBCD', '#DCDCDC', '#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
        responsive: true
    });
    // Configure all doughnut charts
    ChartJsProvider.setOptions('Doughnut', {
        animateScale: true
    });
});