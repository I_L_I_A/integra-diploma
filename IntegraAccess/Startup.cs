﻿using System;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Autofac;
using Autofac.Integration.WebApi;
using IntegraAccess;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Infrastructure;
using IntegraAccess.Infrastructure.Logging;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace IntegraAccess
{
    public partial class Startup
    {
        public void ConfigureOAuth(IAppBuilder app)
        {
            var OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        public void ConfigureLogger(HttpConfiguration config)
        {
            config.Services.Add(typeof(IExceptionLogger), 
                new UnhandledExceptionLogger(config.DependencyResolver.GetRootLifetimeScope().BeginLifetimeScope().Resolve<ILogService>()));
        }

        public void Configuration(IAppBuilder app)
        {
            // Get your HttpConfiguration. In OWIN, you'll create one
            // rather than using GlobalConfiguration.
            var config = new HttpConfiguration();
            // Run other optional steps, like registering filters,
            // per-controller-type services, etc., then set the dependency resolver
            // to be Autofac.
            var container = new InjectionConfigurationsWeb().Container;
            
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            
            // Register the Autofac middleware FIRST, then the Autofac Web API middleware,
            // and finally the standard Web API middleware.
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            ConfigureOAuth(app);
            ConfigureLogger(config);
            
            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

    }
}
