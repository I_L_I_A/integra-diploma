﻿using System;

namespace IntegraAccess.Models
{
    public class OrderLineItemModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }

        public int ProductId { get; set; }
        public int? AddedByProductTemplateId { get; set; }
        public ProductModel Product { get; set; }

        public int? PurchaseTypeId { get; set; }
        public string PurchaseTypeName { get; set; }

        public int ItemNumber { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Comments { get; set; }

        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }
    }
}