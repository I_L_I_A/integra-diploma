﻿using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class ChangePasswordModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}