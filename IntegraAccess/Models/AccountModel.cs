﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class AccountModel : AuditingEntityModel
    {
        public AccountModel()
        {
            UserAccounts = new List<UserAccountsModel>();
            FacilityAccounts = new List<FacilityAccountsModel>();
        }

        public string ExternalId { get; set; }
        public int? CompanyId { get; set; }
        public int? PriceSheetId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public int? AccountGroupId { get; set; }
        public string AccountGroupName { get; set; }
        [Required]
        public string Name { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string AdministrativeEmails { get; set; }
        public bool IsActive { get; set; }

        public List<UserAccountsModel> UserAccounts { get; set; }
        public List<FacilityAccountsModel> FacilityAccounts { get; set; }
        
    }
}