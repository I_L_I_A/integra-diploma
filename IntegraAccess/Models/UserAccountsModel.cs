﻿using System;

namespace IntegraAccess.Models
{
    public class UserAccountsModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        
    }
}