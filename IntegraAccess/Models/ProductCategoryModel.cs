﻿namespace IntegraAccess.Models
{
    public class ProductCategoryModel : AuditingEntityModel
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}