﻿using System;
using System.Collections.Generic;
using IntegraAccess.Entities.Enums;

namespace IntegraAccess.Models
{
    public class OrderModel : AuditingEntityModel
    {
        public OrderModel()
        {
            Patient = new PatientModel();
            OrderLineItems = new List<OrderLineItemModel>();
        }
        public int? AccountId { get; set; }
        public string AccountName { get; set; }

        public int? AccountGroupId { get; set; }
        public string AccountGroupName { get; set; }

        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }

        public int? OrderTypeId { get; set; }
        public string OrderTypeName { get; set; }

        public DateTime? SubmittedAt { get; set; }
        public string SubmittedByName { get; set; }
        public string CreatedByName { get; set; }

        public int? OrderStateId { get; set; }
        public string OrderStateName { get; set; }
        public OrderStates OrderStateEnumValue { get; set; }

        public string ExternalOrderNumber { get; set; }
        public string PONumber { get; set; }
        public string OrderNotes { get; set; }
        public DateTime OrderDate { get; set; }
        public string ShippingStreet1 { get; set; }
        public string ShippingStreet2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingZip { get; set; }
        public string ReferralSource { get; set; }
        public DateTime? RequestedDeliveryDate { get; set; }
        public DateTime? RequestedDeliveryStartTime { get; set; }
        public DateTime? RequestedDeliveryEndTime { get; set; }
        public string DeliveryNotes { get; set; }

        public PatientModel Patient { get; set; }

        public List<OrderLineItemModel> OrderLineItems { get; set; }
    }
}