﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class FacilityModel : AuditingEntityModel
    {
        public FacilityModel()
        {
            FacilityAccounts = new List<FacilityAccountsModel>();
        }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ExternalCode { get; set; }

        public bool IsActive { get; set; }
        public int? AccountGroupId { get; set; }
        public string AccountGroupName { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }

        public List<FacilityAccountsModel> FacilityAccounts { get; set; }
    }
}