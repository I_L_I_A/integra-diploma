﻿using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class AccountGroupModel : AuditingEntityModel
    {
        public string ExternalCode { get; set; }
        [Required]
        public string Name { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        [Required]
        public string ContactName { get; set; }
        [Required]
        public string ContactPhone { get; set; }
        [Required]
        public string ContactEmail { get; set; }
        public bool IsActive { get; set; }
    }
}