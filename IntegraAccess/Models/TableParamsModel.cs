﻿namespace IntegraAccess.Models
{
    public class TableParamsModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public string SortColumn { get; set; }
        public bool Reverse { get; set; }
        public bool IncludeInactive { get; set; }
    }
}