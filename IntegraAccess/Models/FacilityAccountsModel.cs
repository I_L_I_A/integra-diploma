﻿using System;

namespace IntegraAccess.Models
{
    public class FacilityAccountsModel
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string IsFacilityActive { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string IsAccountActive { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        
    }
}