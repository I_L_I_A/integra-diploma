﻿using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class ResetPasswordModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string ResetPasswordToken { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}