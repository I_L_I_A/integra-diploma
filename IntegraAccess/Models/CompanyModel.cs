﻿using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class CompanyModel : AuditingEntityModel
    {
        [Required]
        public string ExternalCode { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}