﻿namespace IntegraAccess.Models
{
    public class ProductPriceSheetModel : AuditingEntityModel
    {
        public string Name { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public bool IsActive { get; set; }
    }
}