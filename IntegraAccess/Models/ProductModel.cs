﻿using System.Collections.Generic;

namespace IntegraAccess.Models
{
    public class ProductModel : AuditingEntityModel
    {
        public ProductModel()
        {
            ProductTemplateItems = new List<ProductTemplateItemModel>();
            ProductPriceSheetItems = new List<ProductPriceSheetItemModel>();
            ProductsToCategories = new List<ProductsToCategoriesModel>();
        }

        public string DisplayName { get; set; }
        public string ExternalCode { get; set; }
        public string ShortDescription { get; set; }
        public string ShortDescription2 { get; set; }
        public string PortalDescription { get; set; }
        public string ImageUrl { get; set; }
        public string ImageName { get; set; }
        public string ManufacturerLink { get; set; }
        public decimal? PriceDefault { get; set; }
        public bool IsActive { get; set; }
        public bool IsTemplate { get; set; }
        public bool HasImage { get; set; }
        public bool AvailableOutsideTemplate { get; set; }


        public List<ProductTemplateItemModel> ProductTemplateItems { get; set; }
        public List<ProductsToCategoriesModel> ProductsToCategories { get; set; }
        public List<ProductPriceSheetItemModel> ProductPriceSheetItems { get; set; }

    }
}