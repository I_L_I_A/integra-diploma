﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class UserModel
    {
        public UserModel()
        {
            UserAccounts = new List<UserAccountsModel>();
        }

        public string Id;

        public string AccountType { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string Title { get; set; }

        [Required]
        public string Email { get; set; }

        public string Phone { get; set; }

        public bool? IsActive { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public bool ChangePasswordOnLogin { get; set; }

        public int? AccountGroupId { get; set; }

        public string AccountGroupName { get; set; }

        public List<UserAccountsModel> UserAccounts { get; set; }

        //Permission
        public bool IsAccountGroupAdmin { get; set; }
        public bool IsOrderApprovalAdmin { get; set; }
        public bool IsOrderApprovalOverride { get; set; }
        public bool ReceiveApprovalNeededEmail { get; set; }

    }
}