﻿namespace IntegraAccess.Models
{
    public class BaseEntityModel
    {
        public int Id { get; set; }
    }
}