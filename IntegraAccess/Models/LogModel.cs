﻿using System;

namespace IntegraAccess.Models
{
    public class LogModel : AuditingEntityModel
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string ErrorType { get; set; }
        public string Username { get; set; }
        public string StatusCode { get; set; }
        public string HostName { get; set; }
        public string LogLevel { get; set; }
        public string AdditionalInfo { get; set; }
        public string Location { get; set; }
        public DateTime Date { get; set; }
    }
}
