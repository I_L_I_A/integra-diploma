﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IntegraAccess.Models
{
    public class CheckDuplicatePatientModel : AuditingEntityModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string SSN { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}