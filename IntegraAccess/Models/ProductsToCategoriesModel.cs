﻿using System;

namespace IntegraAccess.Models
{
    public class ProductsToCategoriesModel
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public bool IsProductActive { get; set; }
        public bool IsCategoryActive { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }

        public bool IsActive { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public string UpdatedById { get; set; }
    }
}