﻿using System.Collections.Generic;

namespace IntegraAccess.Models
{
    public class ProductPriceSheetItemModel : AuditingEntityModel
    {
        public int? PriceSheetId { get; set; }
        public int Quantity { get; set; }
        public string PriceSheetName { get; set; }
        public decimal? PriceSelling { get; set; }
        public decimal? PriceRental { get; set; }
        public decimal? PriceDaily { get; set; }
        public bool IsSellingAllowed { get; set; }
        public bool IsActive { get; set; }
        public bool IsTemplate { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductShortDescription { get; set; }
        public string ProductShortDescription2 { get; set; }
        public string ProductImageUrl { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public List<ProductPriceSheetItemModel> NestedPPSItems { get; set; }
        public int? AddedByProductTemplateId { get; set; }

        public bool IsPricesValid
        {
            get
            {
                if (PriceSelling.HasValue && !PriceRental.HasValue && !PriceDaily.HasValue)
                {
                    return true;
                }
                if (PriceRental.HasValue && !PriceSelling.HasValue  && !IsSellingAllowed && !PriceDaily.HasValue)
                {
                    return true;
                }
                if (PriceDaily.HasValue && !PriceSelling.HasValue && !IsSellingAllowed && !PriceRental.HasValue)
                {
                    return true;
                }
                return false;
            }
        }
    }
}