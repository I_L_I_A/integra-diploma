﻿using System;

namespace IntegraAccess.Models
{
    public class ProductTemplateItemModel
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int ChildProductId { get; set; }
        public string ChildProductName { get; set; }
        public string ChildProductDescription { get; set; }
        public string ChildProductDescription2 { get; set; }
        public int Quantity { get; set; }
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string IsTemplateActive { get; set; }
        public bool IsActive { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}