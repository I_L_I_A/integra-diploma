﻿using System;
using System.Globalization;

namespace IntegraAccess
{
    public partial class Default : System.Web.UI.Page
    {
        protected string CurrentVersion { get; set; }
        protected string LastModifiedDate { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var fileInfo = new System.IO.FileInfo(assembly.Location);
            LastModifiedDate = fileInfo.LastWriteTime.ToString("d", new CultureInfo("en-US"));
            var version = assembly
                .GetName()
                .Version
                .ToString();
            CurrentVersion = version;
        }
    }
}