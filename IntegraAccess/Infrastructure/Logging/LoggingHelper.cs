﻿using System;
using System.Net;
using System.Web;
using Autofac;
using Autofac.Integration.Owin;
using Common.Logging;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Entities.Entities;
using Microsoft.AspNet.Identity;

namespace IntegraAccess.Infrastructure.Logging
{
    public static class LoggingHelper
    {
        public static Log CreateErrorLog(HttpContext context, Exception exc)
        {
            var log = new Log();
            log.Date = DateTime.UtcNow;
            log.ErrorType = HttpStatusCode.InternalServerError.ToString();
            log.HostName = context.Request.ServerVariables["HTTP_HOST"];
            log.LogLevel = LogLevel.Error.ToString().ToUpper();
            log.Message = exc.Message;
            log.StackTrace = exc.StackTrace;
            log.Location = context.Request.Url.PathAndQuery;
            var userService = context.Request.GetOwinContext().Request.Context.GetAutofacLifetimeScope().Resolve<IUserService>();
            if (context.Request.GetOwinContext().Authentication.User.Identity.IsAuthenticated)
            {
                var user = userService.FindUserById(context.Request.GetOwinContext().Authentication.User.Identity.GetUserId());
                log.Username = user.Login;
            }
            else
            {
                log.Username = "anonymous";
            }
            
//            log.StatusCode = HttpStatusCode.InternalServerError.ToString();
            return log;
        }
    }
}