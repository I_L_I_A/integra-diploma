﻿using System.Net.Http;
using System.Web;
using System.Web.Http.ExceptionHandling;
using IntegraAccess.Business.Services.Interfaces;

#pragma warning disable 1591

namespace IntegraAccess.Infrastructure.Logging
{
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        protected readonly ILogService _logService;

        public UnhandledExceptionLogger(ILogService logService)
        {
            _logService = logService;
        }

        public override void Log(ExceptionLoggerContext context)
        {
            // Retrieve the current HttpContext instance for this request.
            HttpContext httpContext = GetHttpContext(context.Request);

            if (httpContext == null)
                return;
            var log = LoggingHelper.CreateErrorLog(httpContext, context.Exception);
            _logService.Log(log);
            base.Log(context);
        }
        private static HttpContext GetHttpContext(HttpRequestMessage request)
        {
            if (request == null)
                return null;

            object value;
            if (request.Properties.TryGetValue("MS_HttpContext", out value))
            {
                HttpContextBase context = value as HttpContextBase;
                if (context != null)
                    return context.ApplicationInstance.Context;
            }

            return HttpContext.Current;
        }
    }
}

#pragma warning restore 1591