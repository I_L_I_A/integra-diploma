﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using IntegraAccess.Business;
using IntegraAccess.Mapping.Conversions;

namespace IntegraAccess.Infrastructure
{
    public class InjectionConfigurationsWeb : InjectionConfigurations
    {
        protected override void RegisterTypes()
        {
            ContainerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            ContainerBuilder.Register<IDataConverter>(x => new ViewModelConverter()).SingleInstance();

            base.RegisterTypes();
        }
    }
}