﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.AspNet.Identity;

namespace IntegraAccess.Infrastructure
{
    public class RestrictedByUserFilterAttribute : ActionFilterAttribute
    {
        public string Role { get; set; }
        public string ParameterName { get; set; }
        public string ExcludeType { get; set; }
        public string ExcludeValue { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (principal != null)
            {
                var excludeClaim = principal.Claims.FirstOrDefault(x => x.Type == ExcludeType);

                if (principal.IsInRole(Role) && (excludeClaim == null || excludeClaim.Value != ExcludeValue))
                {
                    actionContext.ActionArguments[ParameterName] = principal.Identity.GetUserId();
                }
            }
            base.OnActionExecuting(actionContext);
        }
    }
}