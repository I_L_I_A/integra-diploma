﻿using System;

namespace IntegraAccess.Infrastructure.Exceptions
{
    public class BadRequestException: Exception
    {
        public BadRequestException(string message)
            : base(message)
        {
        }

        public BadRequestException(Exception exception)
            : base("See inner exception", exception)
        {
        }

        public BadRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}