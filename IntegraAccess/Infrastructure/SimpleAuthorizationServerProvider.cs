﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Autofac;
using Autofac.Integration.Owin;
using IntegraAccess.Business.Services.Interfaces;
using IntegraAccess.Entities.Entities;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace IntegraAccess.Infrastructure
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            var userService = context.OwinContext.Request.Context.GetAutofacLifetimeScope().Resolve<IUserService>();
            var user = await userService.FindUser(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            if (user.IsActive.HasValue && !user.IsActive.Value)
            {
                context.SetError("invalid_grant", "User is deactivated.");
                return;
            }

            if (!user.UserRoles.Contains("Supplier") && !user.IsAccountGroupAdmin && !user.UserAccounts.Any(x=>x.IsActive))
            {
                context.SetError("invalid_grant", "User doesn't have access to any Account or all user's accounts are deactivated");
                return;
            }
            
            var authProps = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {"user_id", user.Id},
                    {"user_roles", string.Join(",", user.UserRoles.ToArray())},
                    {"user_permissions", string.Join(",",  GetUserPermissions(user).ToArray())},
                    {"user_name", string.Format("{0} {1}", user.FirstName, user.LastName)},
                    {"change_password", user.ChangePasswordOnLogin.ToString().ToLower() }
                });

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            identity.AddClaim(new Claim(ClaimTypes.Name, string.Format("{0} {1}", user.FirstName, user.LastName)));
            identity.AddClaims(GetRoleBasedClaims(user));
            foreach (var role in user.UserRoles)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
            }


            context.Validated(new AuthenticationTicket(identity, authProps));
        }

        public List<string> GetUserPermissions(User user)
        {
            var permissions = new List<string>();
            if (user.UserRoles != null && user.UserRoles.Contains("Account"))
            {
                if (user.IsAccountGroupAdmin)
                {
                    permissions.Add("AccountGroupAdmin");
                }
                if (user.IsOrderApprovalAdmin)
                {
                    permissions.Add("OrderApprovalAdmin");
                }
                if (user.IsOrderApprovalOverride)
                {
                    permissions.Add("OrderApprovalOverride");
                }
            }

            return permissions;
        }

        public IEnumerable<Claim> GetRoleBasedClaims(User user)
        {
            List<Claim> claims = new List<Claim>();
            if (user.UserRoles.Contains("Account"))
            {
                claims.Add(CreateClaim("IsAccountGroupAdmin", user.IsAccountGroupAdmin ? "1" : "0"));
                claims.Add(CreateClaim("IsOrderApprovalAdmin", user.IsOrderApprovalAdmin ? "1" : "0"));
                claims.Add(CreateClaim("IsOrderApprovalOverride", user.IsOrderApprovalOverride ? "1" : "0"));
                claims.Add(CreateClaim("AccountGroupId", user.AccountGroupId.ToString()));
            }

            return claims;
        }

        public static Claim CreateClaim(string type, string value)
        {
            return new Claim(type, value, ClaimValueTypes.String);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}