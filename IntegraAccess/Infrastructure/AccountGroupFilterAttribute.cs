﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace IntegraAccess.Infrastructure
{
    public class RestrictedByAccountGroupFilterAttribute : ActionFilterAttribute
    {
        public string Role { get; set; }
        public string ParameterName { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (principal != null)
            {
                var accountGroupId = principal.Claims.FirstOrDefault(x => x.Type == "AccountGroupId");

                if (accountGroupId != null && principal.IsInRole(Role))
                {
                    int acId;
                    if (int.TryParse(accountGroupId.Value, out acId))
                    {
                        actionContext.ActionArguments[ParameterName] = acId;
                    }
                    // return false;
                }
            }
            base.OnActionExecuting(actionContext);
        }
    }
}